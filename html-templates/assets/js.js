/* all js */

/* shrink nav & logo on scroll */
$(window).scroll(function(){
    if ($(this).scrollTop()>75){
        // small size:
        $('.shrink').stop().animate({ height: 100 },15);
    } else {
        //  original size
        $('.shrink').stop().animate({ height: 150 },15);
    }
}); 



/* animated hamburger */
$(document).ready(function () {
	$('.third-button').on('click', function () {
		$('.animated-icon3').toggleClass('open');
	});
});