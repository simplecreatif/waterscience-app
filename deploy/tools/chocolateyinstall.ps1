﻿$ErrorActionPreference = "Stop"; # stop on all errors

$packageDir = Join-Path $PSScriptRoot "../"
$packageWebDir = Join-Path $packageDir "TAG.Web"
$packageRealtimeDir = Join-Path $packageDir "TAG.Realtime"

$deploymentDir = "D:/deployments/TAG/"
$deploymentWebDir = Join-Path $deploymentDir "TAG.Web"
$deploymentRealtimeDir = Join-Path $deploymentDir "TAG.Realtime"

# Chocolatey will notify the user that the app has been installed to this path
$env:ChocolateyPackageInstallLocation = $deploymentDir

Write-Host "Ensure event viewer is closed (it locks nssm) before deploying."

if((Get-WebAppPoolState -Name "TAG.Web").Value -eq "Started") {
    Write-Host "Stopping IIS application pools..."
    Stop-WebAppPool -Name "TAG.Web"
}

$realtimeService = Get-Service "TAG.Realtime" -ErrorAction Ignore

# If the service exists, stop and delete it
If($realtimeService -ne $null) {
    Write-Host "Stopping and uninstalling TAG.Realtime service..."
    Push-Location $deploymentRealtimeDir
        Stop-Service -DisplayName "TAG.Realtime" -ErrorAction SilentlyContinue
        Start-Sleep -Seconds 5
        Invoke-Expression "npm run uninstall-service"
        Start-Sleep -Seconds 5
    Pop-Location
}
else {
    Write-Host "TAG.Realtime service does not exist."
}

Write-Host "Deleting current deployment..."
if(Test-Path $deploymentWebDir) {
    Remove-Item -Recurse $deploymentWebDir
}

if(Test-Path $deploymentRealtimeDir) {
    Remove-Item -Recurse $deploymentRealtimeDir
}

Write-Host "Copying new deployment..."
Copy-Item -Recurse $packageWebDir $deploymentWebDir
Copy-Item -Recurse $packageRealtimeDir $deploymentRealtimeDir

Write-Host "Installing and starting TAG.Realtime service..."
Push-Location $deploymentRealtimeDir
    Invoke-Expression "npm run install-service"
    Start-Sleep -Seconds 5
    Start-Service -DisplayName "TAG.Realtime"
    Start-Sleep -Seconds 5
Pop-Location

Write-Host "Starting IIS application pools..."
Start-WebAppPool -Name "TAG.Web"

Write-Host "Done."