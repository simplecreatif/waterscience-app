DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_roles  WHERE rolname = 'tag') THEN
        create user tag with password 'tag';
    END IF;
END
$$;

-- Grant r+w to tag on tag schema
grant connect on database tag to tag;

do $$
begin
if '${environment}' = 'dev' then
    grant all privileges on schema public to tag;
end if;
end
$$;

grant select, insert, update, delete on all tables in schema public to tag;
grant usage on all sequences in schema public to tag;
grant select on all tables in schema public to tag;

