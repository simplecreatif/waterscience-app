insert into searchproperty (name) values
	('CAS Number'),
	('Scientific Name'),
	('Common Name'),
	('Duration'),
	('Duration Unit'),
	('Effect'),
	('Endpoint'),
	('Concentration'),
	('Concentration Unit'),
	('Media Type (2000)'),
	('Test Type (2000)'),
	('Effect Used (2000)'),
	('Exposure Type (2000)'),
	('Concentration Code (2000)'),
	('Concentration Type (2000)'),
	('Status (2000)'),
	('Concentration Used (2000)'),
	('Conv. NOEC (2000)'),
	('Hardness Corrected Conc. (2000)'),
	('Temperature (2000)'),
	('pH (2000)'),
	('Hardness (2000)'),
	('Salinity (2000)'),
	('Total Ammonia (mg) (2000)'),
	('Total Ammonia pH8 (2000)'),
	('Is Chronic (2016)'),
	('Concentration Used (2016)'),
	('Record (2016)'),
	('Data Source (2016)'),
	('Media Type (2016)'),
	('Endpoint from Paper (2016)'),
	('Endpoint Measurement (2016)'),
	('Chemical Grade (AED)'),
	('Chemical Form (AED)'),
	('Solvent Unit (AED)'),
	('Age (AED)'),
	('Media (AED)'),
	('Test Form (AED)'),
	('Exposure Type (AED)'),
	('Error Measure (AED)'),
	('Statistic (AED)'),
	('Replication (AED)'),
	('Temporal Replication (AED)'),
	('Concentration Type (AED)'),
	('Country (AED)'),
	('Publication Language (AED)'),
	('Identifier (AED)'),
	('Chemical Grade Value (AED)'),
	('Is Solvent Used (AED)'),
	('Solvent Name (AED)'),
	('Solvent Concentration (AED)'),
	('Solvent Control (AED)'),
	('Species Strain (AED)'),
	('Species Age/Stage (AED)'),
	('Feeding (AED)'),
	('LCL 95 (AED)'),
	('UCL 95 (AED)'),
	('Error (AED)'),
	('Dose Response (AED)'),
	('Significance Level (AED)'),
	('Power (AED)'),
	('Has Ref. Tox. (AED)'),
	('Ref. Tox. Name (AED)'),
	('Has Control Effect (AED)'),
	('Control Effect (AED)'),
	('Invalidation Criteria (AED)'),
	('Loss of Chemical (AED)'),
	('Chemical Form Used (AED)'),
	('Is Physicochem Used (AED)'),
	('Temperature (AED)'),
	('pH (AED)'),
	('Conductivity (AED)'),
	('Hardness (AED)'),
	('Salinity (AED)'),
	('Alkalinity (AED)'),
	('Organic Carbon (AED)'),
	('Light Regime (AED)'),
	('Other Parameters (AED)'),
	('Method Reference (AED)'),
	('Site Location (AED)'),
	('Hard Copy (AED)'),
	('Comments (AED)'),
	('Quality Score (AED)'),
	('Chemical');
