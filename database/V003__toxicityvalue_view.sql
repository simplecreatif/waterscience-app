create materialized view toxicityvalueall
as
select
	tv.id as tv_id,
	tv.concentration as tv_concentration,
	tv.chemical_id as tv_chemical_id,
	tv.species_id as tv_species_id,
	tv.duration_id as tv_duration_id,
	tv.durationunit_id as tv_durationunit_id,
	tv.durationhour_id as tv_durationhour_id,
	tv.effect_id as tv_effect_id,
	tv.endpoint_id as tv_endpoint_id,
	tv.concentrationunit_id as tv_concentrationunit_id,
	tv2000.toxicityvalue_id as tv2000_toxicityvalue_id,
	tv2000.concentrationused as tv2000_concentrationused,
	tv2000.convnoec as tv2000_convnoec,
	tv2000.hardnesscorrectedconc as tv2000_hardnesscorrectedconc,
	tv2000.temperature as tv2000_temperature,
	tv2000.ph as tv2000_ph,
	tv2000.hardness as tv2000_hardness,
	tv2000.salinity as tv2000_salinity,
	tv2000.totalammoniamg as tv2000_totalammoniamg,
	tv2000.totalammoniaph8 as tv2000_totalammoniaph8,
	tv2000.mediatype_id as tv2000_mediatype_id,
	tv2000.testtype_id as tv2000_testtype_id,
	tv2000.effectused_id as tv2000_effectused_id,
	tv2000.exposuretype_id as tv2000_exposuretype_id,
	tv2000.concentrationcode_id as tv2000_concentrationcode_id,
	tv2000.concentrationtype_id as tv2000_concentrationtype_id,
	tv2000.reference_id as tv2000_reference_id,
	tv2000.status_id as tv2000_status_id,
	tv2016.toxicityvalue_id as tv2016_toxicityvalue_id,
	tv2016.record as tv2016_record,
	tv2016.datasource as tv2016_datasource,
	tv2016.ischronic as tv2016_ischronic,
	tv2016.concentrationused as tv2016_concentrationused,
	tv2016.mediatype_id as tv2016_mediatype_id,
	tv2016.endpointfrompaper_id as tv2016_endpointfrompaper_id,
	tv2016.endpointmeasurement_id as tv2016_endpointmeasurement_id,
	tv2016.guidelinegroup_id as tv2016_guidelinegroup_id,
	tvaed.toxicityvalue_id as tvaed_toxicityvalue_id,
	tvaed.identifier as tvaed_identifier,
	tvaed.chemicalgradevalue as tvaed_chemicalgradevalue,
	tvaed.issolventused as tvaed_issolventused,
	tvaed.solventname as tvaed_solventname,
	tvaed.solventconcentration as tvaed_solventconcentration,
	tvaed.solventcontrol as tvaed_solventcontrol,
	tvaed.speciesstrain as tvaed_speciesstrain,
	tvaed.speciesageorstage as tvaed_speciesageorstage,
	tvaed.feeding as tvaed_feeding,
	tvaed.lowerconfidencelimit95 as tvaed_lowerconfidencelimit95,
	tvaed.upperconfidencelimit95 as tvaed_upperconfidencelimit95,
	tvaed.errorvalue as tvaed_errorvalue,
	tvaed.doseresponse as tvaed_doseresponse,
	tvaed.significancelevel as tvaed_significancelevel,
	tvaed.power as tvaed_power,
	tvaed.hasreftox as tvaed_hasreftox,
	tvaed.reftoxname as tvaed_reftoxname,
	tvaed.hascontroleffect as tvaed_hascontroleffect,
	tvaed.controleffect as tvaed_controleffect,
	tvaed.invalidationcriteria as tvaed_invalidationcriteria,
	tvaed.lossofchemical as tvaed_lossofchemical,
	tvaed.chemicalformused as tvaed_chemicalformused,
	tvaed.isphysicochemmeasured as tvaed_isphysicochemmeasured,
	tvaed.temperature as tvaed_temperature,
	tvaed.ph as tvaed_ph,
	tvaed.conductivity as tvaed_conductivity,
	tvaed.hardness as tvaed_hardness,
	tvaed.salinity as tvaed_salinity,
	tvaed.alkalinity as tvaed_alkalinity,
	tvaed.organiccarbon as tvaed_organiccarbon,
	tvaed.lightregime as tvaed_lightregime,
	tvaed.otherparameters as tvaed_otherparameters,
	tvaed.methodreference as tvaed_methodreference,
	tvaed.sitelocation as tvaed_sitelocation,
	tvaed.hardcopy as tvaed_hardcopy,
	tvaed.comments as tvaed_comments,
	tvaed.qualityscore as tvaed_qualityscore,
	tvaed.chemicalgrade_id as tvaed_chemicalgrade_id,
	tvaed.chemicalform_id as tvaed_chemicalform_id,
	tvaed.solventunit_id as tvaed_solventunit_id,
	tvaed.age_id as tvaed_age_id,
	tvaed.media_id as tvaed_media_id,
	tvaed.testform_id as tvaed_testform_id,
	tvaed.exposuretype_id as tvaed_exposuretype_id,
	tvaed.errormeasure_id as tvaed_errormeasure_id,
	tvaed.statistic_id as tvaed_statistic_id,
	tvaed.replication_id as tvaed_replication_id,
	tvaed.temporalreplication_id as tvaed_temporalreplication_id,
	tvaed.concentrationtype_id as tvaed_concentrationtype_id,
	tvaed.reference_id as tvaed_reference_id,
	tvaed.country_id as tvaed_country_id,
	tvaed.publicationlanguage_id as tvaed_publicationlanguage_id,

	tv_chemical.name as tv_chemical_name,
	tv_species.scientificname as tv_species_scientificname,
	tv_species.commonname as tv_species_commonname,
	tv_species.animalcategory_id as tv_species_animalcategory_id,
	tv_duration.name as tv_duration_name,
	tv_durationunit.name as tv_durationunit_name,
	tv_durationhour.name as tv_durationhour_name,
	tv_effect.name as tv_effect_name,
	tv_endpoint.name as tv_endpoint_name,
	tv_concentrationunit.name as tv_concentrationunit_name,
	tv2000_mediatype.name as tv2000_mediatype_name,
	tv2000_testtype.name as tv2000_testtype_name,
	tv2000_effectused.name as tv2000_effectused_name,
	tv2000_exposuretype.name as tv2000_exposuretype_name,
	tv2000_concentrationcode.name as tv2000_concentrationcode_name,
	tv2000_concentrationtype.name as tv2000_concentrationtype_name,
	tv2000_status.name as tv2000_status_name,
	tv2016_mediatype.name as tv2016_mediatype_name,
	tv2016_endpointfrompaper.name as tv2016_endpointfrompaper_name,
	tv2016_endpointmeasurement.name as tv2016_endpointmeasurement_name,
	tv2016_guidelinegroup.guidelinevalue as tv2016_guidelinegroup_guidelinevalue,
	tvaed_chemicalgrade.name as tvaed_chemicalgrade_name,
	tvaed_chemicalform.name as tvaed_chemicalform_name,
	tvaed_solventunit.name as tvaed_solventunit_name,
	tvaed_age.name as tvaed_age_name,
	tvaed_media.name as tvaed_media_name,
	tvaed_testform.name as tvaed_testform_name,
	tvaed_exposuretype.name as tvaed_exposuretype_name,
	tvaed_errormeasure.name as tvaed_errormeasure_name,
	tvaed_statistic.name as tvaed_statistic_name,
	tvaed_replication.name as tvaed_replication_name,
	tvaed_temporalreplication.name as tvaed_temporalreplication_name,
	tvaed_concentrationtype.name as tvaed_concentrationtype_name,
	tvaed_country.name as tvaed_country_name,
	tvaed_publicationlanguage.name as tvaed_publicationlanguage_name,

	tv_chemical.casnumber as tv_chemical_casnumber,
	tv_chemical_toxicant.id as tv_chemical_toxicant_id,
	(
		case
			when tvaed.toxicityvalue_id is not null then 'AED'
			when tv2000.toxicityvalue_id is not null then 'WQG2000'
			when tv2016.toxicityvalue_id is not null then 'WQG2016'
		end
	) as source

from toxicityvalue tv
	left join toxicityvalue2000 tv2000
		on tv.id = tv2000.toxicityvalue_id
	left join toxicityvalue2016 tv2016
		on tv.id = tv2016.toxicityvalue_id
	left join toxicityvalueaed tvaed
		on tv.id = tvaed.toxicityvalue_id

	left join chemical tv_chemical on tv_chemical.id = tv.chemical_id
	left join species tv_species on tv_species.id = tv.species_id
	left join duration tv_duration on tv_duration.id = tv.duration_id
	left join durationunit tv_durationunit on tv_durationunit.id = tv.durationunit_id
	left join duration tv_durationhour on tv_durationhour.id = tv.durationhour_id
	left join effect tv_effect on tv_effect.id = tv.effect_id
	left join endpoint tv_endpoint on tv_endpoint.id = tv.endpoint_id
	left join concentrationunit tv_concentrationunit on tv_concentrationunit.id = tv.concentrationunit_id
	left join toxicityvalue tv2000_toxicityvalue on tv2000_toxicityvalue.id = tv2000.toxicityvalue_id
	left join mediatype tv2000_mediatype on tv2000_mediatype.id = tv2000.mediatype_id
	left join testtype tv2000_testtype on tv2000_testtype.id = tv2000.testtype_id
	left join effect tv2000_effectused on tv2000_effectused.id = tv2000.effectused_id
	left join exposuretype tv2000_exposuretype on tv2000_exposuretype.id = tv2000.exposuretype_id
	left join concentrationcode tv2000_concentrationcode on tv2000_concentrationcode.id = tv2000.concentrationcode_id
	left join concentrationtype tv2000_concentrationtype on tv2000_concentrationtype.id = tv2000.concentrationtype_id
	left join status tv2000_status on tv2000_status.id = tv2000.status_id
	left join toxicityvalue tv2016_toxicityvalue on tv2016_toxicityvalue.id = tv2016.toxicityvalue_id
	left join mediatype tv2016_mediatype on tv2016_mediatype.id = tv2016.mediatype_id
	left join endpoint tv2016_endpointfrompaper on tv2016_endpointfrompaper.id = tv2016.endpointfrompaper_id
	left join endpoint tv2016_endpointmeasurement on tv2016_endpointmeasurement.id = tv2016.endpointmeasurement_id
	left join guidelinegroup tv2016_guidelinegroup on tv2016_guidelinegroup.id = tv2016.guidelinegroup_id
	left join toxicityvalue tvaed_toxicityvalue on tvaed_toxicityvalue.id = tvaed.toxicityvalue_id
	left join chemicalgrade tvaed_chemicalgrade on tvaed_chemicalgrade.id = tvaed.chemicalgrade_id
	left join chemicalform tvaed_chemicalform on tvaed_chemicalform.id = tvaed.chemicalform_id
	left join concentrationunit tvaed_solventunit on tvaed_solventunit.id = tvaed.solventunit_id
	left join age tvaed_age on tvaed_age.id = tvaed.age_id
	left join media tvaed_media on tvaed_media.id = tvaed.media_id
	left join testform tvaed_testform on tvaed_testform.id = tvaed.testform_id
	left join exposuretype tvaed_exposuretype on tvaed_exposuretype.id = tvaed.exposuretype_id
	left join error tvaed_errormeasure on tvaed_errormeasure.id = tvaed.errormeasure_id
	left join statistic tvaed_statistic on tvaed_statistic.id = tvaed.statistic_id
	left join replication tvaed_replication on tvaed_replication.id = tvaed.replication_id
	left join temporalreplication tvaed_temporalreplication on tvaed_temporalreplication.id = tvaed.temporalreplication_id
	left join concentrationtype tvaed_concentrationtype on tvaed_concentrationtype.id = tvaed.concentrationtype_id
	left join country tvaed_country on tvaed_country.id = tvaed.country_id
	left join publicationlanguage tvaed_publicationlanguage on tvaed_publicationlanguage.id = tvaed.publicationlanguage_id

	left join toxicant tv_chemical_toxicant
		on tv_chemical_toxicant.id = tv_chemical.toxicant_id;

alter materialized view toxicityvalueall owner to tag;