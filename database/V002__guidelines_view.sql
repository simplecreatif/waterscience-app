create materialized view guideline
as
select
	chemical.casnumber as tv_chemical_casnumber,
	chemical.id as tv_chemical_id,
	toxicant.id as tv_chemical_toxicant_id,
	species.id as tv_species_id,
	effectused.id as tv_effect_id,
	endpoint.id as tv_endpoint_id,
	
	toxicant.name as toxicant,
	species.scientificname as speciesscientificname,
	species.commonname as speciescommonname,
	animalcategory.name as animalcategory,
	endpoint.name as endpoint,
	effectused.name as effect,
	mediatype.name as mediatype,
	testtype.name as testtype,
	durationhour.name as durationhour,
	concentrationunit.name as concentrationunit,
	round(exp(avg(ln(tv2000.concentrationused))), 4) as guidelinevalue,
	null as guidelinegroup_id,
	'WQG2000' as source
from
	toxicityvalue tv
	inner join toxicityvalue2000 tv2000 on tv2000.toxicityvalue_id = tv.id
	inner join chemical on chemical.id = tv.chemical_id
	inner join toxicant on toxicant.id = chemical.toxicant_id
	inner join species on species.id = tv.species_id
	left join animalcategory on animalcategory.id = species.animalcategory_id
	inner join endpoint on endpoint.id = tv.endpoint_id
	inner join effect effectused on effectused.id = tv2000.effectused_id
	inner join mediatype on mediatype.id = tv2000.mediatype_id
	inner join testtype on testtype.id = tv2000.testtype_id
	inner join duration durationhour on durationhour.id = tv.durationhour_id
	inner join concentrationunit on concentrationunit.id = tv.concentrationunit_id
where
	tv2000.concentrationused is not null
group by
	chemical.id,
	toxicant.id,
	species.id,
	effectused.id,
	endpoint.id,
	animalcategory.id,
	mediatype.id,
	testtype.id,
	durationhour.id,
	concentrationunit.id
union all
select
	chemical.casnumber as tv_chemical_casnumber,
	chemical.id as tv_chemical_id,
	toxicant.id as tv_chemical_toxicant_id,
	species.id as tv_species_id,
	effect.id as tv_effect_id,
	endpoint.id as tv_endpoint_id,
	
	toxicant.name as toxicant,
	species.scientificname as speciesscientificname,
	species.commonname as speciescommonname,
	animalcategory.name as animalcategory,
	endpoint.name as endpoint,
	effect.name as effect,
	mediatype.name as mediatype,
	null as testtype,
	duration.name as durationhour, -- assumed to be in hours
	concentrationunit.name as concentrationunit,
	guidelinegroup.guidelinevalue,
	guidelinegroup.id as guidelinegroup_id,
	'WQG2016' as source
from
	toxicityvalue tv
	inner join toxicityvalue2016 tv2016 on tv2016.toxicityvalue_id = tv.id
	inner join chemical on chemical.id = tv.chemical_id
	inner join toxicant on toxicant.id = chemical.toxicant_id
	inner join species on species.id = tv.species_id
	left join animalcategory on animalcategory.id = species.animalcategory_id
	inner join endpoint on endpoint.id = tv.endpoint_id
	inner join effect on effect.id = tv.effect_id
	inner join mediatype on mediatype.id = tv2016.mediatype_id
	inner join duration on duration.id = tv.duration_id
	inner join concentrationunit on concentrationunit.id = tv.concentrationunit_id
	inner join guidelinegroup on guidelinegroup.id = tv2016.guidelinegroup_id
where
	guidelinegroup.guidelinevalue is not null
group by
	chemical.id,
	toxicant.id,
	species.id,
	effect.id,
	endpoint.id,
	animalcategory.id,
	mediatype.id,
	duration.id,
	concentrationunit.id,
	guidelinegroup.id;

alter materialized view guideline owner to tag;