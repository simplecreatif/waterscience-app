TODO list for SETAC NET/React App:

**1.) Saved Searches** can be removed/disabled as we no longer need it.

**2.) Allow public to use tool:** Currently all users must log in, but they have decided to open this tool to the public, so we need two levels of users:

**Admin**

You can leave it exactly how it is now where the admins log in and can change any elements in the database. We don't need to style this any further than it currently is, the admin doesn't care what it looks like so the generic Bootstrap style is fine.

**Public**:

Can use ***Search*** and can export results as CSV (this functionality already exists)

They do not need user profiles and can NOT make any changes in the database.

You will see in the HTML a few places where the public user can "***Request Edit/Addition***" - we need this to open a modal window contact form where they can add their name/title/email/phone number and a textarea where they can type their requests. This should be emailed to the admin (can be hardcoded if that's easier).

In the ***Explore*** section of the site, I think it would be nice to display all of the potential parameters of the database (what is currently visible in the nav as Configuration, Species, Chemical, Media, Toxicity Value). The user should NOT be able to edit these values, only view them, and we might add a "Request Edit/Addition" button on these pages if they need to make a request.

**3.) Design changes**: The only thing we really need to style in HTML/CSS is the Search page (shown in html templates) and also a generic Request Edit/Addition modal.

The existing styles of the Bootstrap tables and results of their search was done in Bootstrap 3.6, so I might need to make a few minor changes to update to the current Bootstrap 4.3+ but we don't need anything fancy, just them to function well.

** Jul 13 2020 - Steve Gross - Wrote above change list.*