require 'fileutils'

PROJECT_ROOT_DIR = File.dirname(__FILE__)

CONFIGURATION = "Release"

SRC_DIR = File.join(PROJECT_ROOT_DIR, "src")
SRC_WEB_DIR = File.join(SRC_DIR, "TAG.Web")
PUBLISH_WEB_DIR = File.join(SRC_WEB_DIR, "bin", CONFIGURATION , "net452", "win7-x64", "publish")

SRC_WEBCLIENT_DIR = File.join(SRC_DIR, "TAG.WebClient")
PUBLISH_WEBCLIENT_DIR = File.join(SRC_DIR, "TAG.WebClient", "dist")

SRC_REALTIME_DIR = File.join(SRC_DIR, "TAG.Realtime")

desc "Compiles and bundles TAG.Web and TAG.WebClient. "
     "Requires nuget 3.5 beta 2 to be available on the system path. " +
     "Requires msbuild 14.0 update 3. " +
     "Requires dotnet core 1.0 RTM to be available on the system path. " +
     "Requires npm to be available on the system path. "
task :build do
    Dir.chdir(SRC_DIR) do
        # Restore nuget packages
        sh "nuget restore TAG.sln"

        # Build with MSBuild to generate project.fragment.lock.json
        sh "\"C:/Program Files (x86)/MSBuild/14.0/Bin/msbuild.exe\" TAG.sln /p:Configuration=\"" + CONFIGURATION + "\""

        # Publish TAG.Web with dotnet to generate web.config for IIS integration
        sh "dotnet publish TAG.Web --configuration " + CONFIGURATION
    end

    Dir.chdir(SRC_WEBCLIENT_DIR) do
        # Restore npm packages
        sh "npm install"

        # Copy across correct config file
        # WEBCLIENT_CONFIG_FILE environment variable must be set
        src_filepath = File.join("src", ENV['WEBCLIENT_CONFIG_FILE'])
        dest_filepath = File.join("src", "config.js")
        FileUtils.cp(src_filepath, dest_filepath)

        # Publish TAG.WebClient with react-slingshot's build task
        sh "npm run build"

        # Copy web.config file into publish directory (configures caching and SPA url rewriting)
        FileUtils.cp("web.config", PUBLISH_WEBCLIENT_DIR)
    end

    Dir.chdir(SRC_REALTIME_DIR) do
        # Restore npm packages for TAG.Realtime
        sh "npm install"

        # Copy across correct config file
        # REALTIME_CONFIG_FILE environment variable must be set
        FileUtils.cp(ENV['REALTIME_CONFIG_FILE'], "config.js")
    end
end

desc "Runs NUnit tests in TAG.Tests and saves results in TestResult.xml. " +
     "Requires NUnit to be available at D:/infrastructure/NUnit.org/bin/nunit3-console."
task :test => [:build] do
    # Run tests in TAG.Tests
    cmd = []
    cmd << "D:/infrastructure/NUnit.org/bin/nunit3-console"
    cmd << "\"" + File.join(SRC_DIR, "TAG.Tests", "bin", CONFIGURATION, "TAG.Tests.dll") + "\""
    cmd << "--result:TestResult.xml;format=nunit2"
    sh cmd.join(" ")
end

desc "Generates a chocolatey package of all deployable artifacts. " +
     "Requires chocolatey to be installed and available on the system path."
task :pack => [:test] do
    Dir.chdir(File.join(PROJECT_ROOT_DIR, "deploy")) do
        sh "choco pack tag.nuspec"
    end
end

desc "Deploys the chocolatey package to the server on which this script is executed."
task :deploy_to_test => [:pack] do
    sh "choco install TAG -y -force -source deploy"
end

desc "Deploys the chocolatey package to the production server. " +
     "Requires a putty saved session named tag-production with private key authentication. " +
     "Requires pscp and plink putty tools to be available on the system path."
task :deploy_to_prod => [:pack] do
    # pscp docs: http://the.earth.li/~sgtatham/putty/0.60/htmldoc/Chapter5.html
    # plink docs: http://the.earth.li/~sgtatham/putty/0.60/htmldoc/Chapter7.html
    # setting up saved session in putty: https://www.howtoforge.com/ssh_key_based_logins_putty_p3

    # Copy chocolatey package to prod server
    sh "pscp deploy/tag.1.0.0.nupkg tag-production:/packages/tag.1.0.0.nupkg"
    sh "plink tag-production choco install TAG -y -force -source D:/deployments/packages"
end
