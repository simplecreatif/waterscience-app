﻿using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Routing;

namespace TAG.Web.Middleware
{
    /// <summary>
    /// Validates the antiforgery tokens (cookie and HTTP header) in incoming POST, PUT and DELETE requests.
    /// Throws an exception if they are not valid.
    /// </summary>
    public class CSRFTokenValidationMiddleware
    {
        private readonly RequestDelegate next;
        private readonly IAntiforgery antiforgery;

        public CSRFTokenValidationMiddleware(RequestDelegate next, IAntiforgery antiforgery)
        {
            this.next = next;
            this.antiforgery = antiforgery;
        }

        public async Task Invoke(HttpContext context)
        {
            var action = context.Request.Path;
            if (action.Value == "/api/auth/signin")
            {
                await next(context);
            }

            var method = context.Request.Method.ToUpper();
            if (method == "POST" || method == "PUT" || method == "DELETE")
            {
                // Compare the token in the cookie called XSRF-TOKEN with the token in
                // the request header called X-XSRF-TOKEN. They must be identical.
                await antiforgery.ValidateRequestAsync(context);
            }

            await next(context);
        }
    }
}
