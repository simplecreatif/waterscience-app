﻿using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace TAG.Web.Middleware
{
    public class SinglePageAppRewriter
    {
        private readonly RequestDelegate next;
        private static readonly Regex IsKnownFileTypeRegex = new Regex(@"\.(png|ico|css|js|map)$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public SinglePageAppRewriter(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            // If the url isn't a known static file type, map it to index.html
            // for SPA url functionality
            if (!IsKnownFileTypeRegex.IsMatch(context.Request.Path.Value))
            {
                context.Request.Path = new PathString("/index.html");
            }

            await next.Invoke(context);
        }
    }
}
