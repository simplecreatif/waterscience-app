﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace TAG.Web.Middleware
{
    public class RequestForgeryValidator
    {
        private readonly RequestDelegate next;

        public RequestForgeryValidator(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var cookieToken = context.Request.Cookies["Csrf-Token"];
            var headerToken = context.Request.Headers["X-Csrf-Token"];

            if (string.IsNullOrEmpty(cookieToken) || cookieToken != headerToken)
            {
                context.Response.StatusCode = 401;
                return;
            }

            await next.Invoke(context);
        }
    }
}
