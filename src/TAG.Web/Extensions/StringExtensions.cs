﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TAG.Web.Extensions
{
    public static class StringExtensions
    {
        public static string ToPascalCase(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return value;
            }

            char[] chars = value.ToCharArray();
            
            // CASNumber -> casNumber
            // SomeThing -> someThing
            // SomeHTML -> someHTML
            for (int i = 0; i < chars.Length; i++)
            {
                var isUpper = chars[i] >= 'A' && chars[i] <= 'Z';
                var isNextUpper = i < chars.Length - 1 && chars[i + 1] >= 'A' && chars[i + 1] <= 'Z';

                // If all previous characters have been uppercase,
                // and the next character is also uppercase, convert
                // this to lowercase
                if (isUpper && (i == 0 || i == chars.Length - 1 || isNextUpper))
                {
                    chars[i] = char.ToLower(chars[i]);
                }
                else
                {
                    break;
                }

            }

            return new string(chars);
        }

        public static string ToPhrase(this string value)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return value;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(char.ToUpper(value[0]));

            for (int i = 1; i < value.Length; i++)
            {
                var _char = value[i];
                var nextChar = (i < value.Length - 1) ? value[i + 1] : default(char?);
                var isUpper = char.IsUpper(_char) || char.IsNumber(_char);
                var isNextUpper = nextChar.HasValue && (char.IsUpper(nextChar.Value) || char.IsNumber(nextChar.Value));
                sb.Append(_char);
                if (!isUpper && isNextUpper)
                {
                    sb.Append(" ");
                }
            }

            return sb.ToString();
        }
    }
}
