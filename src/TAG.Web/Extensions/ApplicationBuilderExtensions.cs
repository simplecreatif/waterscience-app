﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Web.Middleware;

namespace TAG.Web.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseRequestForgeryValidator(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestForgeryValidator>();
        }

        /// <summary>
        /// Rewrites all requests that aren't of a known file type (css, js, etc) to index.html.
        /// This assumes that index.html contains a single-page javascript application which
        /// interprets the url and generates an appropriate page. 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseSinglePageAppRewriter(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SinglePageAppRewriter>();
        }
    }
}
