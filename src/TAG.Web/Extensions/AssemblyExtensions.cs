﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace TAG.Web.Extensions
{
    public static class AssemblyExtensions
    {
        /// <summary>
        /// Returns all types within the assembly of the
        /// specified type which derive from the specified
        /// type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<Type> GetTypesDerivingFrom<T>()
        {
            var type = typeof(T);
            return Assembly
                .GetAssembly(type)
                .GetTypes()
                .Where(x => type.IsAssignableFrom(x) && type != x);
        }
    }
}
