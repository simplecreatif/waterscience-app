﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using TAG.Web.Infrastructure.Realtime;
using TAG.Web.Infrastructure.Scaffolding;
using TAG.Web.Models;
using TAG.Web.Models.Mappers;

namespace TAG.Web.Extensions
{
    public static class ControllerExtensions
    {
        /// <summary>
        /// Returns a FluentValidation-style validation result with the specified message.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static IActionResult ValidationFailure(this Controller controller, string message)
        {
            var result = new ValidationResult(new[] { new ValidationFailure(String.Empty, message) });

            return controller.BadRequest(result);
        }

        public static IActionResult ValidationFailure(this Controller controller, ValidationResult result)
        {
            return controller.BadRequest(result);
        }

        public static IActionResult EditResult(this Controller controller, ILifetimeScope scope, ScaffoldConfiguration config, object entity)
        {
            var cache = scope.Resolve<IMemoryCache>();
            var clients = scope.Resolve<IClients>();
            var listMapper = (IModelMapper)scope.Resolve(config.ListMapperType);
            var showMapper = (IModelMapper)scope.Resolve(config.ShowMapperType);
            var result = new EditResult(listMapper.ToModel(entity), showMapper.ToModel(entity), controller.Request.Headers["X-Client-Id"]);

            // Clear out cached lists of this entity type
            cache.Remove(config.EntityName.ToLower());

            // Notify other clients
            clients.SendEdit(config.EntityName, result);

            return controller.Ok(result);
        }

        public static IActionResult DeleteResult(this Controller controller, ILifetimeScope scope, ScaffoldConfiguration config, int id)
        {
            var cache = scope.Resolve<IMemoryCache>();
            var clients = scope.Resolve<IClients>();
            var result = new DeleteResult(id, controller.Request.Headers["X-Client-Id"]);

            // Clear out cached lists of this entity type
            cache.Remove(config.EntityName.ToLower());

            // Notify other clients
            clients.SendDelete(config.EntityName, result);

            return controller.Ok(result);
        }
    }
}
