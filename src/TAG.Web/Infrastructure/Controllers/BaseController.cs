﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models.Mappers;
using TAG.Web.Models.Validators;

namespace TAG.Web.Infrastructure.Controllers
{
    [Route("[controller]")]
    public abstract class BaseController<TEntity, TModel> : Controller
        where TEntity : class, IIdentifiable, new()
        where TModel : class, IIdentifiable, new()
    {
        protected readonly IRepository repository;
        protected readonly IModelMapper<TEntity, TModel> modelMapper;
        protected readonly IValidator<TModel> modelValidator;

        protected BaseController(IRepository repository, IModelMapper<TEntity, TModel> modelMapper, IValidator<TModel> modelValidator)
        {
            this.repository = repository;
            this.modelMapper = modelMapper;
            this.modelValidator = modelValidator;
        }

        // GET: api/entityName
        [HttpGet]
        public virtual IEnumerable<TModel> Get()
        {
            return repository.Query<TEntity>()
                .Select(modelMapper.ToModel);
        }

        // GET api/entityName/5
        [HttpGet("{id}")]
        public virtual TModel Get(Guid id)
        {
            var entity = repository.Get<TEntity>(id);
            var model = modelMapper.ToModel(entity);
            return model;
        }

        // POST api/entityName
        //[Authorize(Roles = TAGRole.Administrator)]
        [HttpPost]
        public virtual IActionResult Post([FromBody]TModel model)
        {
            if (model == null)
            {
                return BadRequest(new ValidationResult(new[] { new ValidationFailure("", "The entity is required.") }));
            }

            var validationResult = modelValidator.Validate(model);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult);
            }

            var entity = modelMapper.ToEntity(model);
            repository.Add(entity);
            return Ok(validationResult);
        }

        // PUT api/entityName/5
        //[Authorize(Roles = TAGRole.Administrator)]
        [HttpPut("{id}")]
        public virtual IActionResult Put(Guid id, [FromBody]TModel model)
        {
            if (model == null)
            {
                return BadRequest(new ValidationResult(new[] { new ValidationFailure("", "The entity is required.") }));
            }

            var validationResult = modelValidator.Validate(model);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult);
            }

            var entity = repository.Get<TEntity>(id);
            modelMapper.ToEntity(model, entity);
            repository.Add(entity);
            return Ok(validationResult);
        }

        // DELETE api/entityName/5
        //[Authorize(Roles = TAGRole.Administrator)]
        [HttpDelete("{id}")]
        public virtual void Delete(Guid id)
        {
            var entity = repository.Get<TEntity>(id);
            repository.Remove(entity);
        }
    }
}
