﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models.Mappers;

namespace TAG.Web.Infrastructure.Controllers
{
    //[Authorize(Roles = TAGRole.StandardUser)]
    [Route("[controller]")]
    public abstract class BaseReadOnlyController<TEntity, TModel> : Controller
        where TEntity : class, IIdentifiable, new()
        where TModel : class, IIdentifiable, new()
    {
        protected readonly IRepository repository;
        protected readonly IModelMapper<TEntity, TModel> modelMapper;

        protected BaseReadOnlyController(IRepository repository, IModelMapper<TEntity, TModel> modelMapper)
        {
            this.repository = repository;
            this.modelMapper = modelMapper;
        }

        // GET: api/entityName
        [HttpGet]
        public virtual IEnumerable<TModel> Get()
        {
            return repository.Query<TEntity>()
                .Select(modelMapper.ToModel);
        }

        // GET api/entityName/5
        [HttpGet("{id}")]
        public virtual TModel Get(Guid id)
        {
            var entity = repository.Get<TEntity>(id);
            var model = modelMapper.ToModel(entity);
            return model;
        }
    }
}
