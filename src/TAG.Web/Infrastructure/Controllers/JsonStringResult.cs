﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace TAG.Web.Infrastructure.Controllers
{
    /// <summary>
    /// Same as Content("{ someJson } ", "application/json; charset=utf-8")
    /// but faster on our cheap VPS hardware. It seems like validating the
    /// content type hurts performance so we bypass all validation here.
    /// See source of Microsoft.AspNetCore.Mvc.Internal.ContentResultExecutor
    /// And Microsoft.AspNetCore.Mvc.ContentResult
    /// </summary>
    public class JsonStringResult : ActionResult
    {
        private readonly string json;

        public JsonStringResult(string json)
        {
            this.json = json;
        }

        public override async Task ExecuteResultAsync(ActionContext context)
        {
            var response = context.HttpContext.Response;
            var encoding = Encoding.UTF8;
            var httpResponseStreamWriterFactory = context.HttpContext.RequestServices.GetRequiredService<IHttpResponseStreamWriterFactory>();

            response.ContentType = "application/json; charset=utf-8";
            response.ContentLength = encoding.GetByteCount(json);
            using (var textWriter = httpResponseStreamWriterFactory.CreateWriter(response.Body, encoding))
            {
                await textWriter.WriteAsync(json);

                // Flushing the HttpResponseStreamWriter does not flush the underlying stream. This just flushes
                // the buffered text in the writer.
                // We do this rather than letting dispose handle it because dispose would call Write and we want
                // to call WriteAsync.
                await textWriter.FlushAsync();
            }
        }
    }
}
