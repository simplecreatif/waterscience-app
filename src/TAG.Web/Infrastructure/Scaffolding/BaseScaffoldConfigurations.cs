﻿using System;
using System.Collections.Generic;
using TAG.Domain.Models;
using TAG.Web.Controllers.Scaffold;
using TAG.Web.Extensions;

namespace TAG.Web.Infrastructure.Scaffolding
{
    /// <summary>
    /// Container for all scaffold configurations. This class is
    /// used by ScaffoldController to retrieve all required information
    /// about an entity type to perform CRUD operations on that entity
    /// type.
    /// </summary>
    public abstract class BaseScaffoldConfigurations
    {
        public Dictionary<string, ScaffoldConfiguration> Configurations { get; } = new Dictionary<string, ScaffoldConfiguration>();
        
        public ScaffoldConfiguration TryGet(string entityType)
        {
            ScaffoldConfiguration config;
            if (!Configurations.TryGetValue(entityType.ToLower(), out config))
            {
                return null;
            }

            return config;
        }

        protected ScaffoldConfiguration Add<TEntity>()
        {
            return Add(typeof(TEntity));
        }

        protected ScaffoldConfiguration Add(Type tEntity)
        {
            var config = new ScaffoldConfiguration(tEntity);
            Configurations.Add(config.EntityName.ToLower(), config);
            return config;
        }

        protected void AddAllDerivedFrom<TBase>(Action<ScaffoldConfiguration> configureEach = null)
        {
            foreach (var derivedType in AssemblyExtensions.GetTypesDerivingFrom<TBase>())
            {
                var config = Add(derivedType);
                configureEach?.Invoke(config);
            }
        }
    }
}
