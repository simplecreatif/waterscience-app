﻿using System.Security.Claims;

namespace TAG.Web.Infrastructure.Scaffolding
{
    public interface IAuthorizationRules
    {
        bool CanCreate(ClaimsPrincipal principal);
        bool CanList(ClaimsPrincipal principal);
        bool CanShow(ClaimsPrincipal principal, object entity);
        bool CanUpdate(ClaimsPrincipal principal, object entity);
        bool CanDelete(ClaimsPrincipal principal, object entity);
    }
}
