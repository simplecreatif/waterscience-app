﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TAG.Web.Infrastructure.Scaffolding
{
    /// <summary>
    /// Specifies whether a user is authorised to perform certain
    /// actions provided by the scaffolded controller.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class BaseAuthorizationRules<TEntity> : IAuthorizationRules
    {
        public virtual bool CanCreate(ClaimsPrincipal principal)
        {
            return true;
        }

        public virtual bool CanList(ClaimsPrincipal principal)
        {
            return true;
        }

        public virtual bool CanShow(ClaimsPrincipal principal, TEntity entity)
        {
            return true;
        }

        public virtual bool CanUpdate(ClaimsPrincipal principal, TEntity entity)
        {
            return true;
        }

        public virtual bool CanDelete(ClaimsPrincipal principal, TEntity entity)
        {
            return true;
        }

        public bool CanShow(ClaimsPrincipal principal, object entity)
        {
            return CanShow(principal, (TEntity)entity);
        }

        public bool CanUpdate(ClaimsPrincipal principal, object entity)
        {
            return CanUpdate(principal, (TEntity)entity);
        }

        public bool CanDelete(ClaimsPrincipal principal, object entity)
        {
            return CanUpdate(principal, (TEntity)entity);
        }
    }
}
