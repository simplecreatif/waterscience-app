﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using Autofac;
using TAG.Domain.Models;
using TAG.Web.Extensions;
using TAG.Web.Models.Mappers;
using TAG.Web.Models.Validators;

namespace TAG.Web.Infrastructure.Scaffolding
{
    /// <summary>
    /// Captures all the information necessary to perform
    /// scaffolded CRUD operations on an entity. This includes
    /// things like the entity type, the model type, and optionally
    /// a validator.
    /// </summary>
    public class ScaffoldConfiguration
    {
        public static Type DefaultAuthorizationType { get; set; }

        /// <summary>
        /// The name of the domain entity in lowercase (e.g. "chemical")
        /// </summary>
        public string EntityName { get; private set; }

        /// <summary>
        /// The user-friendly name for this domain entity.
        /// </summary>
        public string DisplayName { get; private set; }

        /// <summary>
        /// The type of the domain entity (e.g. Chemical)
        /// </summary>
        public Type EntityType { get; private set; }

        /// <summary>
        /// The type of model to generate and provide to clients
        /// when requesting a list endpoint
        /// </summary>
        public Type ListModelType { get; private set; }

        /// <summary>
        /// The mapper to use when mapping domain entities to
        /// list models.
        /// </summary>
        public Type ListMapperType { get; private set; }

        public Type ListSpecificationType { get; private set; }

        public bool CanList => ListModelType != null && ListMapperType != null && ListSpecificationType != null;

        /// <summary>
        /// The type of model to generate and provide to clients
        /// when requesting a show endpoin
        /// </summary>
        public Type ShowModelType { get; private set; }

        /// <summary>
        /// The mapper to use when mapping domain entities to
        /// show models.
        /// </summary>
        public Type ShowMapperType { get; private set; }

        public bool CanShow => ShowModelType != null && ShowMapperType != null;

        /// <summary>
        /// The type of model to expect when a client submits
        /// data to an edit endpoint (either create or update)
        /// </summary>
        public Type EditModelType { get; private set; }

        /// <summary>
        /// The mapper to use when mapping edit models to
        /// domain entities.
        /// </summary>
        public Type EditMapperType { get; private set; }

        /// <summary>
        /// The validator to use when a client submits data
        /// to an edit endpoint (either create or update)
        /// </summary>
        public Type EditValidatorType { get; private set; }

        public bool CanEdit => EditModelType != null && EditMapperType != null && EditValidatorType != null;

        /// <summary>
        /// The validator to use when deleting an entity.
        /// </summary>
        public Type DeleteValidatorType { get; private set; }

        public bool CanDelete => DeleteValidatorType != null;

        /// <summary>
        /// The authorization rules to use when invoking an
        /// endpoint.
        /// </summary>
        public Type AuthorizationType { get; private set; }

        public Dictionary<string, object> Template { get; private set; }

        private static readonly HashSet<Type> NumericTypes = new HashSet<Type>
        {
            typeof(long),
            typeof(long?),
            typeof(decimal),
            typeof(decimal?),
            typeof(int),
            typeof(int?)
        };

        private static readonly HashSet<Type> BooleanTypes = new HashSet<Type>
        {
            typeof(bool),
            typeof(bool?)
        };

        public ScaffoldConfiguration(Type entityType)
        {
            EntityName = entityType.Name.ToLower();
            EntityType = entityType;
            DisplayName = entityType.Name.ToPhrase();
            AuthorizationType = DefaultAuthorizationType;
        }

        public ScaffoldConfiguration WithList<TModel, TMapper, TSpecification>()
            where TMapper : IModelMapper
        {
            ListModelType = typeof(TModel);
            ListMapperType = typeof(TMapper);
            ListSpecificationType = typeof(TSpecification);
            return this;
        }

        public ScaffoldConfiguration WithShow<TModel, TMapper>()
            where TMapper : IModelMapper
        {
            ShowModelType = typeof(TModel);
            ShowMapperType = typeof(TMapper);
            return this;
        }

        public ScaffoldConfiguration WithEdit<TModel, TMapper, TValidator>()
            where TMapper : IModelMapper
            where TValidator : IValidator

        {
            EditModelType = typeof(TModel);
            EditMapperType = typeof(TMapper);
            EditValidatorType = typeof(TValidator);

            Template = CreateTemplate(EntityType, EditModelType);
            return this;
        }

        public ScaffoldConfiguration WithDelete<TValidator>() where TValidator : IDeleteEntityValidator
        {
            DeleteValidatorType = typeof(TValidator);
            return this;
        }

        public ScaffoldConfiguration WithAuthorization<TAuthorization>() where TAuthorization : IAuthorizationRules
        {
            AuthorizationType = typeof(TAuthorization);
            return this;
        }

        /// <summary>
        /// Helper method to instantiate and invoke a method on Authorization
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="expr"></param>
        /// <returns></returns>
        public bool IsAuthorized(ILifetimeScope scope, Func<IAuthorizationRules, bool> expr)
        {
            if (AuthorizationType == null)
            {
                return true;
            }

            var authorization = (IAuthorizationRules)scope.Resolve(AuthorizationType);

            return expr.Invoke(authorization);
        }

        /// <summary>
        /// Generates information about each property
        /// in the specified type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="editModelType"></param>
        /// <returns></returns>
        private static Dictionary<string, object> CreateTemplate(Type entityType, Type editModelType)
        {
            var template = new Dictionary<string, object>();
            var entityProperties = entityType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var modelProperties = editModelType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in modelProperties)
            {
                string propertyType = null;

                if (property.Name.EndsWith("Id"))
                {
                    propertyType = GetReferenceType(property.Name, entityProperties);
                }
                else if (NumericTypes.Contains(property.PropertyType))
                {
                    propertyType = "numeric";
                }
                else if (BooleanTypes.Contains(property.PropertyType))
                {
                    propertyType = "bool";
                }
                else if (property.Name.ToLower().Contains("password"))
                {
                    propertyType = "password";
                }

                template[property.Name.ToPascalCase()] = propertyType;
            }
            return template;
        }

        private static string GetReferenceType(string modelPropertyName, IEnumerable<PropertyInfo> entityProperties)
        {
            // Trim "Id" from the model property name
            var entityPropertyName = modelPropertyName.Substring(0, modelPropertyName.Length - 2);

            // If the edit model has a property name ending with "Id", see if there is a corresponding
            // property on the entity type and check if it's a reference to another entity.
            var correspondingEntityProperty = entityProperties.SingleOrDefault(x => x.Name == entityPropertyName);
            var isIIdentfiable = correspondingEntityProperty != null &&
                                 typeof(IIdentifiable).IsAssignableFrom(correspondingEntityProperty.PropertyType);

            return isIIdentfiable ? correspondingEntityProperty.PropertyType.Name.ToPascalCase() : null;
        }
    }
}
