using System.Collections.Generic;

namespace TAG.Web.Infrastructure.Scaffolding
{
    public interface IScaffoldConfigurations
    {
        Dictionary<string, ScaffoldConfiguration> Configurations { get; }
        ScaffoldConfiguration TryGet(string entityType);
    }
}