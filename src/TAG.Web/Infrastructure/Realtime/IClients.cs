﻿using TAG.Domain.Models;
using TAG.Web.Infrastructure.Scaffolding;
using TAG.Web.Models;

namespace TAG.Web.Infrastructure.Realtime
{
    public interface IClients
    {
        void SendEdit(string entityType, EditResult result);
        void SendDelete(string entityType, DeleteResult result);
    }
}