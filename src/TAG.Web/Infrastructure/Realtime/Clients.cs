﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using TAG.Domain.Models;
using TAG.Web.Infrastructure.Scaffolding;
using TAG.Web.Models;
using TAG.Web.Models.Mappers;

namespace TAG.Web.Infrastructure.Realtime
{
    /// <summary>
    /// Represents connected web browsers.
    /// Actually just forwards messages to a REST endpoint
    /// which passes messages via socket.io.
    /// </summary>
    public class Clients : IClients
    {
        private readonly RestClient rest;

        public Clients()
        {
            rest = new RestClient("http://localhost:5000");
        }

        public void SendEdit(string entityType, EditResult result)
        {
            Send(entityType, result, Method.POST);
        }

        public void SendDelete(string entityType, DeleteResult result)
        {
            Send(entityType, result, Method.DELETE);
        }

        private void Send(string entityType, object payload, Method method)
        {
            lock (rest)
            {
                var payloadAsJson = JsonConvert.SerializeObject(payload, Formatting.None, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
                var request = new RestRequest(method);
                request.Resource = "/" + entityType;
                request.AddParameter("application/json", payloadAsJson, ParameterType.RequestBody);
                rest.Execute(request);
            }
        }
    }
}
