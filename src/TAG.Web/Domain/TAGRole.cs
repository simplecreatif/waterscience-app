﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TAG.Web.Domain
{
    public static class TAGRole
    {
        public const string StandardUser = "Standard User";
        public const string Contributor = "Contributor";
        public const string Administrator = "Administrator";

        private static readonly string[] Roles =
        {
            StandardUser,
            Contributor,
            Administrator
        };
    }
}
