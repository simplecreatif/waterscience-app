﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TAG.Web.Domain
{
    public class Token
    {
        public virtual string Jwt { get; set; }
        public virtual DateTime Expiry { get; set; }
    }
}
