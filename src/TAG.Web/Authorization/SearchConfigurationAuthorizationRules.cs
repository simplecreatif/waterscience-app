﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Web.Infrastructure.Scaffolding;

namespace TAG.Web.Authorization
{
    public class SearchConfigurationAuthorizationRules : BaseAuthorizationRules<SearchConfiguration>
    {
        public override bool CanCreate(ClaimsPrincipal principal)
        {
            return true;
        }

        public override bool CanUpdate(ClaimsPrincipal principal, SearchConfiguration entity)
        {
            return UpdatingSelf(principal, entity);
        }

        public override bool CanDelete(ClaimsPrincipal principal, SearchConfiguration entity)
        {
            return UpdatingSelf(principal, entity);
        }

        private bool UpdatingSelf(ClaimsPrincipal principal, SearchConfiguration entity)
        {
            var identity = (ClaimsIdentity)principal.Identity;
            var id = Convert.ToInt32(identity.Claims.Single(x => x.Type == ClaimTypes.Sid).Value);

            return id == entity.User.Id;
        }
    }
}
