﻿using System.Security.Claims;
using TAG.Web.Domain;
using TAG.Web.Extensions;
using TAG.Web.Infrastructure.Scaffolding;

namespace TAG.Web.Authorization
{
    public class DefaultAuthorizationRules : IAuthorizationRules
    {
        public bool CanCreate(ClaimsPrincipal principal)
        {
            return principal.IsInAnyRole(TAGRole.Administrator, TAGRole.Contributor);
        }

        public bool CanList(ClaimsPrincipal principal)
        {
            return true;
        }

        public bool CanShow(ClaimsPrincipal principal, object entity)
        {
            return true;
        }

        public bool CanUpdate(ClaimsPrincipal principal, object entity)
        {
            return principal.IsInAnyRole(TAGRole.Administrator, TAGRole.Contributor);
        }

        public bool CanDelete(ClaimsPrincipal principal, object entity)
        {
            return principal.IsInAnyRole(TAGRole.Administrator, TAGRole.Contributor);
        }
    }
}
