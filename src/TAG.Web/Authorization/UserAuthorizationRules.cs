﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Web.Domain;
using TAG.Web.Infrastructure.Scaffolding;

namespace TAG.Web.Authorization
{
    public class UserAuthorizationRules : BaseAuthorizationRules<User>
    {
        public override bool CanCreate(ClaimsPrincipal principal)
        {
            return principal.IsInRole(TAGRole.Administrator);
        }

        public override bool CanUpdate(ClaimsPrincipal principal, User entity)
        {
            var isAdministrator = principal.IsInRole(TAGRole.Administrator);
            var identity = (ClaimsIdentity)principal.Identity;
            var id = Convert.ToInt32(identity.Claims.Single(x => x.Type == ClaimTypes.Sid).Value);

            // Only administrators may update other users
            if (id != entity.Id)
            {
                return isAdministrator;
            }

            // A User may be modified by an administrator or the user themselves
            return isAdministrator || id == entity.Id;
        }

        public override bool CanDelete(ClaimsPrincipal principal, User entity)
        {
            var identity = (ClaimsIdentity)principal.Identity;
            var id = Convert.ToInt32(identity.Claims.Single(x => x.Type == ClaimTypes.Sid).Value);
            return principal.IsInRole(TAGRole.Administrator) || id == entity.Id;
        }
    }
}
