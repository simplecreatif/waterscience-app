﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Http;
using TAG.Web.Controllers.Scaffold;
using TAG.Web.Infrastructure;
using TAG.Web.Infrastructure.Realtime;
using Module = Autofac.Module;

namespace TAG.Web
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var executingAssembly = Assembly.GetExecutingAssembly();

            // Services
            builder.RegisterAssemblyTypes(executingAssembly)
                .Where(t => t.Namespace != null && t.Namespace.Contains("Services") && t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            // Model mappers
            builder.RegisterAssemblyTypes(executingAssembly)
                .Where(t => t.Namespace != null && t.Namespace.Contains("Mappers") && t.Name.EndsWith("Mapper"))
                .AsImplementedInterfaces()
                .AsSelf()
                .InstancePerLifetimeScope();

            // Validators
            builder.RegisterAssemblyTypes(executingAssembly)
                .Where(t => t.Namespace != null && t.Namespace.Contains("Validators") && t.Name.EndsWith("Validator"))
                .AsImplementedInterfaces()
                .AsSelf()
                .InstancePerLifetimeScope();

            // Scaffold configurations
            builder.RegisterType<ScaffoldConfigurations>()
                .AsImplementedInterfaces()
                .SingleInstance();

            // Realtime notification
            builder.RegisterType<Clients>()
                .AsImplementedInterfaces()
                .SingleInstance();

            // Authorization rules
            builder.RegisterAssemblyTypes(executingAssembly)
               .Where(t => t.Namespace != null && t.Namespace.Contains("Authorization") && t.Name.EndsWith("AuthorizationRules"))
               .AsImplementedInterfaces()
               .AsSelf()
               .InstancePerLifetimeScope();

            // Current user
            builder.Register(x =>
            {
                var contextAccessor = x.Resolve<IHttpContextAccessor>();
                var principal = contextAccessor.HttpContext.User;
                return principal;
            })
            .As<ClaimsPrincipal>()
            .InstancePerLifetimeScope();

            builder.RegisterType<PasswordHasher>().As<IPasswordHasher>();
        }
    }
}
