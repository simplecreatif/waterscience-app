﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Domain;
using TAG.Web.Models;
using TAG.Web.Services.Interfaces;

namespace TAG.Web.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IRepository repository;
        private readonly IPasswordHasher passwordHasher;

        public AuthenticationService(IRepository repository, IPasswordHasher passwordHasher)
        {
            this.repository = repository;
            this.passwordHasher = passwordHasher;
        }

        /// <summary>
        /// Gets the user with the given email and password, if one exists.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="user"></param>
        /// <returns>A ClaimsIdentity object with the user's email and role included as Claims.</returns>
        public ClaimsIdentity GetIdentity(string email, string password, User user)
        {
            if (passwordHasher.VerifyHashedPassword(user.PasswordHash, password) != PasswordVerificationResult.Success)
            {
                return null;
            };

            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim(ClaimTypes.Sid, user.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.Name));

            return identity;
        }

        public string HashPassword(string password)
        {
            return passwordHasher.HashPassword(password);
        }
    }
}
