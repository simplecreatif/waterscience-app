﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Web.Domain;
using TAG.Web.Models;

namespace TAG.Web.Services.Interfaces
{
    public interface IAuthenticationService
    {
        ClaimsIdentity GetIdentity(string email, string password, User user);
        string HashPassword(string password);
    }
}
