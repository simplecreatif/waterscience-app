﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using TAG.Domain.Models;
using TAG.Domain.Specifications;
using TAG.Persistence;
using TAG.Web.Infrastructure.Scaffolding;
using TAG.Web.Models.Mappers;

namespace TAG.Web.Services
{
    /// <summary>
    /// Handles GetJson requests for scaffolded entities.
    /// This has been moved out to a service because
    /// it's used not only in the scaffold controller,
    /// but also in the batch controller.
    /// </summary>
    public class ScaffoldService : IScaffoldService
    {
        private readonly IScaffoldConfigurations scaffoldConfigurations;
        private readonly IMemoryCache cache;
        private readonly JsonOutputFormatter formatter;

        /// <summary>
        /// We need Autofac's lifetime scope because we use it to resolve
        /// some per-request items which are specific to the entity being
        /// scaffolded. This includes validators and mappers.
        /// </summary>
        private readonly ILifetimeScope scope;

        public ScaffoldService(IScaffoldConfigurations scaffoldConfigurations, IMemoryCache cache, ILifetimeScope scope, JsonOutputFormatter formatter)
        {
            this.scaffoldConfigurations = scaffoldConfigurations;
            this.cache = cache;
            this.scope = scope;

            // We use the asp.net core JSON formatter rather than newing up our
            // own JsonSerializer because they are expensive to create (as documented
            // in JsonOutputFormatter source) and because we want to ensure this
            // JSON formatter's configuration is identical to that of all other
            // controllers.
            this.formatter = formatter;
        }

        /// <summary>
        /// Returns a string of JSON-encoded list entities.
        /// This is cached and optimised for performance. It is expected
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public string GetJson(ClaimsPrincipal principal, string entityType)
        {
            var config = scaffoldConfigurations.TryGet(entityType);
            if (config == null || !config.CanList)
            {
                return null;
            }

            if (!config.IsAuthorized(scope, x => x.CanList(principal)))
            {
                return null;
            }

            string json;
            if (!cache.TryGetValue(entityType.ToLower(), out json))
            {
                var models = GetModels(config);
                if (models == null)
                {
                    return null;
                }

                json = ToJson(models);

                // Effectively cache forever. We'll invalidate the cache manually.
                cache.Set(entityType.ToLower(), json, TimeSpan.FromDays(365));
            }

            return json;
        }

        private List<object> GetModels(ScaffoldConfiguration config)
        {
            var repository = scope.Resolve<IRepository>();
            var specification = (ISpecification)scope.Resolve(config.ListSpecificationType);
            var entities = specification.Apply(repository, config.EntityType);
            var mapper = (IModelMapper)scope.Resolve(config.ListMapperType);
            var models = entities.Select(mapper.ToModel).ToList();
            return models;
        }

        private string ToJson(object models)
        {
            using (var sw = new StringWriter())
            {
                formatter.WriteObject(sw, models);
                return sw.ToString();
            }
        }
    }
}
