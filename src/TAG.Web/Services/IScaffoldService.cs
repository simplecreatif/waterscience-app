using System.Collections.Generic;
using System.Security.Claims;

namespace TAG.Web.Services
{
    public interface IScaffoldService
    {
        string GetJson(ClaimsPrincipal principal, string entityType);
    }
}