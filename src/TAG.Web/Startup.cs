﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TAG.Domain;
using TAG.Persistence;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging.EventLog;
using TAG.Web.Infrastructure;
using Microsoft.Extensions.Options;
using TAG.Web.Middleware;
using TAG.Web.Extensions;

namespace TAG.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();

            // Add framework services.
            services.AddMvc();

            // Add authentication services.
            services.AddAuthentication();

            // Enable cors for requests coming from the development web server (localhost:3000)
            services.AddCors(x => x.AddPolicy("dev", new CorsPolicy
            {
                Origins = { "http://localhost:3000" },
                Headers = { "*" },
                Methods = { "*" },
                SupportsCredentials = true,
            }));

            services.AddCors(x => x.AddPolicy("test", new CorsPolicy
            {
                Origins = { "http://wqscidev:1087" },
                Headers = { "*" },
                Methods = { "*" },
                SupportsCredentials = true,
            }));

            // Enable accessing the current HttpContext (and ClaimsPrincipal)
            // from any class, not just Controllers.
            // see: https://github.com/aspnet/Hosting/issues/793
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Create the container builder.
            var builder = new ContainerBuilder();

            // Register dependencies, populate the services from
            // the collection, and build the container.
            builder.RegisterModule(new PersistenceModule(Configuration["ConnectionStrings:TAG"], "TAG.Domain"));
            builder.RegisterModule<DomainModule>();
            builder.RegisterModule<WebModule>();
            builder.Populate(services);
            var container = builder.Build();

            // Return the IServiceProvider resolved from the container.
            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

            if (env.IsDevelopment())
            {
                app.UseCors("dev");
                app.UseDeveloperExceptionPage();
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                loggerFactory.AddDebug();
            }
            else
            {
                loggerFactory.AddEventLog(LogLevel.Warning);
            }

            // Apply test cors policy if on wqscidev
            if (app.ServerFeatures.Get<Microsoft.AspNetCore.Hosting.Server.Features.IServerAddressesFeature>().Addresses
                .Single().ToLower().Contains("wqscidev"))
            {
                app.UseCors("test");
            }

            // All urls starting with /api should be treated as an api request
            app.Map("/api", ConfigureApi);

            // All other urls should be treated as a static file or rewritten
            // to index.html (for SPA url functionality)
            app.UseSinglePageAppRewriter();
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }

        private void ConfigureApi(IApplicationBuilder app)
        {
            app.UseRequestForgeryValidator();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = false
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
