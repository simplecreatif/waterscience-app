﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Web.Models.ToxicityValueAEDModels
{
    public class EditToxicityValueAEDModel : IIdentifiable
    {
        public int Id { get; set; }
        public int? ChemicalId { get; set; }
        public int? SpeciesId { get; set; }
        public int? DurationId { get; set; }
        public int? DurationUnitId { get; set; }
        public int? DurationHourId { get; set; }
        public int? EffectId { get; set; }
        public int? EndpointId { get; set; }
        public string Concentration { get; set; }
        public int? ConcentrationUnitId { get; set; }

        public string Identifier { get; set; }
        public int? ChemicalGradeId { get; set; }
        public string ChemicalGradeValue { get; set; }
        public int? ChemicalFormId { get; set; }
        public bool IsSolventUsed { get; set; }
        public string SolventName { get; set; }
        public string SolventConcentration { get; set; }
        public int? SolventUnitId { get; set; }
        public bool SolventControl { get; set; }
        public string SpeciesStrain { get; set; }
        public string SpeciesAgeOrStage { get; set; }
        public int? AgeId { get; set; }
        public string Feeding { get; set; }
        public int? MediaId { get; set; }
        public int? TestFormId { get; set; }
        public int? ExposureTypeId { get; set; }
        public string LowerConfidenceLimit95 { get; set; }
        public string UpperConfidenceLimit95 { get; set; }
        public int? ErrorMeasureId { get; set; }
        public int? ErrorValue { get; set; }
        public bool DoseResponse { get; set; }
        public int? StatisticId { get; set; }
        public string SignificanceLevel { get; set; }
        public bool? Power { get; set; }
        public int? ReplicationId { get; set; }
        public int? TemporalReplicationId { get; set; }
        public bool HasRefTox { get; set; }
        public string RefToxName { get; set; }
        public bool HasControlEffect { get; set; }
        public string ControlEffect { get; set; }
        public string InvalidationCriteria { get; set; }
        public string LossOfChemical { get; set; }
        public string ChemicalFormUsed { get; set; }
        public int? ConcentrationTypeId { get; set; }
        public bool IsPhysicochemMeasured { get; set; }
        public string Temperature { get; set; }
        public string PH { get; set; }
        public string Conductivity { get; set; }
        public string Hardness { get; set; }
        public string Salinity { get; set; }
        public string Alkalinity { get; set; }
        public string OrganicCarbon { get; set; }
        public string LightRegime { get; set; }
        public string OtherParameters { get; set; }
        public int? ReferenceId { get; set; }
        public string MethodReference { get; set; }
        public int? CountryId { get; set; }
        public string SiteLocation { get; set; }
        public bool HardCopy { get; set; }
        public int? PublicationLanguageId { get; set; }
        public string Comments { get; set; }
        public int? QualityScore { get; set; }
    }
}
