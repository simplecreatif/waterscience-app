﻿using TAG.Domain.Models;

namespace TAG.Web.Models
{
    public class SearchConfigurationModel : IIdentifiable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long? CASNumber { get; set; }
        public int? ChemicalId { get; set; }
        public int? ToxicantId { get; set; }
        public int UserId { get; set; }
        public int[] SpeciesIds { get; set; }
        public int[] EffectIds { get; set; }
        public int[] EndpointIds { get; set; }
        public int[] DatabaseIds { get; set; }
        public int[] SearchPropertyIds { get; set; }
    }
}
