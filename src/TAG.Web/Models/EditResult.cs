﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using TAG.Web.Models.Mappers;

namespace TAG.Web.Models
{
    /// <summary>
    /// When a user successfully edits an entity, this
    /// will be returned to the user and to all other
    /// connected users.
    /// </summary>
    public class EditResult
    {
        public object ListEntity { get; private set; }
        public object GetEntity { get; private set; }
        public string ClientId { get; private set; }

        public EditResult(object listEntity, object getEntity, string clientId)
        {
            ListEntity = listEntity;
            GetEntity = getEntity;
            ClientId = clientId;
        }
    }
}
