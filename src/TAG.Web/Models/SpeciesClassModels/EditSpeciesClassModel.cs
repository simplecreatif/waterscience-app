﻿using TAG.Domain.Models;

namespace TAG.Web.Models.SpeciesClassModels
{
    public class EditSpeciesClassModel : IIdentifiable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? PhylumId { get; set; }
    }
}
