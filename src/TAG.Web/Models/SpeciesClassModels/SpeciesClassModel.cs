﻿using TAG.Domain.Models;

namespace TAG.Web.Models.SpeciesClassModels
{
    public class SpeciesClassModel : IIdentifiable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? PhylumId { get; set; }
        public string PhylumName { get; set; }
    }
}
