﻿using System.Collections.Generic;

namespace TAG.Web.Models.SearchModels
{
    public class SearchResultModel
    {
        public ICollection<dynamic> ToxicityValues { get; set; }
        public ICollection<SearchGuidelineModel> Guidelines2000 { get; set; }
        public ICollection<SearchGuidelineModel> Guidelines2016 { get; set; }
    }
}
