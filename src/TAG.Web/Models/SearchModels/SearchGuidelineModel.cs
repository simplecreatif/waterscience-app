﻿namespace TAG.Web.Models.SearchModels
{
    public class SearchGuidelineModel
    {
        public string GuidelineValue { get; set; }

        public string Toxicant { get; set; }
        public string SpeciesScientificName { get; set; }
        public string SpeciesCommonName { get; set; }
        public string Endpoint { get; set; }
        public string Effect { get; set; }
        public string MediaType { get; set; }
        public string TestType { get; set; }
        public string DurationHour { get; set; }
        public string ConcentrationUnit { get; set; }
        public string AnimalCategory { get; set; }
    }
}
