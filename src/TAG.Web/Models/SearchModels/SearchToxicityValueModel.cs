﻿namespace TAG.Web.Models.SearchModels
{
    public class SearchToxicityValueModel
    {
        public int Id { get; set; }
        public string ChemicalName { get; set; }
        public string SpeciesName { get; set; }
        public string Duration { get; set; }
        public string DurationUnit { get; set; }
        public string Effect { get; set; }
        public string Endpoint { get; set; }
        public string Concentration { get; set; }
        public string ConcentrationUnit { get; set; }
    }
}
