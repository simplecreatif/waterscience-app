﻿using TAG.Domain.Models;

namespace TAG.Web.Models.GuidelineGroupModels
{
    public class EditGuidelineGroupModel : IIdentifiable
    {
        public int Id { get; set; }
        public decimal? GuidelineValue { get; set; }
    }
}
