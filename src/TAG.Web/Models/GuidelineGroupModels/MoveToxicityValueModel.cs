﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TAG.Web.Models.GuidelineGroupModels
{
    public class MoveToxicityValueModel
    {
        public int? ToxicityValueId { get; set; }
        public int? GuidelineGroupId { get; set; }
    }
}
