﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Web.Models
{
    public class EndpointModel : IIdentifiable
    {
        public int Id { get; set; }
        public string Endpoint { get; set; }
        public string Abbreviation { get; set; }
    }
}
