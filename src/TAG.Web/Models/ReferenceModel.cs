﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Web.Models
{
    public class ReferenceModel : IIdentifiable
    {
        public int Id { get; set; }
        public int? OrgRefNumber { get; set; }
        public string Authors { get; set; }
        public string AuthorsAbbreviated { get; set; }
        public string Title { get; set; }
        public string Journal { get; set; }
        public int? Year { get; set; }
        public int? Volume { get; set; }
        public int? IssueNumber { get; set; }
        public int? FirstPage { get; set; }
        public int? LastPage { get; set; }
    }
}
