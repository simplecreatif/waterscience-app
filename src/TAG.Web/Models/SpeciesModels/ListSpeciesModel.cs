﻿using TAG.Domain.Models;

namespace TAG.Web.Models.SpeciesModels
{
    public class ListSpeciesModel : IIdentifiable
    {
        public int Id { get; set; }
        public string ScientificName { get; set; }
        public string CommonName { get; set; }
    }
}
