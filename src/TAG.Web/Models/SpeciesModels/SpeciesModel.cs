﻿using TAG.Domain.Models;

namespace TAG.Web.Models.SpeciesModels
{
    public class SpeciesModel : IIdentifiable
    {
        public int Id { get; set; }
        public string ScientificName { get; set; }
        public string CommonName { get; set; }
        public string Status { get; set; }
        public int? AnimalTypeId { get; set; }
        public string AnimalTypeName { get; set; }
        public string MajorGroup { get; set; }
        public string MinorGroup { get; set; }
        public int? AnimalCategoryId { get; set; }
        public string AnimalCategoryName { get; set; }
        public int? ClassId { get; set; }
        public string ClassName { get; set; }
        public bool? IsHeterotroph { get; set; }
    }
}
