﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TAG.Web.Models
{
    public class DeleteResult
    {
        public int Id { get; private set; }
        public string ClientId { get; private set; }

        public DeleteResult(int id, string clientId)
        {
            Id = id;
            ClientId = clientId;
        }
    }
}
