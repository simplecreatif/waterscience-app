﻿using TAG.Domain.Models;

namespace TAG.Web.Models
{
    public class EditUserModel : IIdentifiable
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
        public int RoleId { get; set; }
    }
}
