﻿using TAG.Domain.Models;

namespace TAG.Web.Models.UserModels
{
    public class UserModel : IIdentifiable
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
    }
}
