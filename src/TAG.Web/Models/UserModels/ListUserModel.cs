﻿using TAG.Domain.Models;

namespace TAG.Web.Models.UserModels
{
    public class ListUserModel : IIdentifiable
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
    }
}
