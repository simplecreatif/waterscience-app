﻿using TAG.Domain.Models;

namespace TAG.Web.Models.ChemicalModels
{
    public class ListChemicalModel : IIdentifiable
    {
        public int Id { get; set; }
        public long CASNumber { get; set; }
        public string ToxicantName { get; set; }
        public string Name { get; set; }
        public string CommonName { get; set; }
    }
}
