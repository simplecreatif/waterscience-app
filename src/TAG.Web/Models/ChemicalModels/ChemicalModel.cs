﻿using TAG.Domain.Models;

namespace TAG.Web.Models.ChemicalModels
{
    public class ChemicalModel : IIdentifiable
    {
        public int Id { get; set; }
        public long CASNumber { get; set; }
        public int? ToxicantId { get; set; }
        public string ToxicantName { get; set; }
        public string Name { get; set; }
        public string CommonName { get; set; }
        public int? ChemicalTypeId { get; set; }
        public string ChemicalTypeName { get; set; }
        public string SpecificGravity { get; set; }
        public string Solubility { get; set; }
        public string BoilingPoint { get; set; }
        public string MeltingPoint { get; set; }
        public string FlashPoint { get; set; }
        public string VapourPressure { get; set; }
        public string LogKoc { get; set; }
        public string LogKow { get; set; }
        public string LogBCF { get; set; }
        public string MolecularWeight { get; set; }
        public string Formulae { get; set; }
        public string HalfLifeWater { get; set; }
        public string HalfLifeSediment { get; set; }
    }
}
