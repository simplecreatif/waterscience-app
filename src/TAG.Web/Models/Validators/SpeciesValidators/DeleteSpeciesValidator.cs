﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.SpeciesValidators
{
    public class DeleteSpeciesValidator : DeleteEntityValidator
    {
        public DeleteSpeciesValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<ToxicityValue>().Any(x => x.Species == entity);
        }
    }
}
