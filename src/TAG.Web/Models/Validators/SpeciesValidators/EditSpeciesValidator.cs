﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models.SpeciesModels;

namespace TAG.Web.Models.Validators.SpeciesValidators
{
    public class EditSpeciesValidator : SaveEntityValidator<Species, EditSpeciesModel>
    {
        public EditSpeciesValidator(IRepository repository) : base(repository)
        {
            // Everything's nullable!
        }

        protected override bool IsDuplicate(EditSpeciesModel model)
        {
            return !string.IsNullOrWhiteSpace(model.ScientificName)
                   && !string.IsNullOrWhiteSpace(model.CommonName)
                   && Repository.Query<Species>().Any(x =>
                       x.ScientificName.ToLower() == model.ScientificName.ToLower()
                       && x.CommonName.ToLower() == model.CommonName.ToLower()
                        && x.Id != model.Id);
        }
    }
}
