﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.SearchConfigurationValidators
{
    public class DeleteSearchConfigurationValidator : DeleteEntityValidator
    {
        /// <summary>
        /// SearchConfigurations can always be deleted so this is effectively a stubbed out class.
        /// </summary>
        /// <param name="repository"></param>
        public DeleteSearchConfigurationValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return false;
        }
    }
}
