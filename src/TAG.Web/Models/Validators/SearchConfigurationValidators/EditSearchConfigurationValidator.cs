﻿using System.Linq;
using FluentValidation.Results;
using TAG.Domain.Models;
using TAG.Persistence;
using FluentValidation;
using System.Security.Claims;
using System;

namespace TAG.Web.Models.Validators.SearchConfigurationValidators
{
    public class EditSearchConfigurationValidator : SaveEntityValidator<SearchConfiguration, SearchConfigurationModel>
    {
        private readonly ClaimsPrincipal principal;

        public EditSearchConfigurationValidator(IRepository repository, ClaimsPrincipal principal) : base(repository)
        {
            this.principal = principal;

            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.UserId).NotEmpty();
            Custom(HasAnyFieldsSet);
            Custom(UpdatingSelf);
        }

        protected override bool IsDuplicate(SearchConfigurationModel model)
        {
            return !string.IsNullOrWhiteSpace(model.Name) && Repository.Query<SearchConfiguration>().Any(x =>
                x.Name.ToLower() == model.Name.ToLower()
                && x.User.Id == model.UserId
                && x.Id != model.Id);
        }

        // At least one search field must be selected to save a search configuration
        private ValidationFailure HasAnyFieldsSet(SearchConfigurationModel model)
        {
            if (!model.CASNumber.HasValue
                && !model.ChemicalId.HasValue
                && !model.ToxicantId.HasValue
                && !model.SpeciesIds.Any()
                && !model.EffectIds.Any()
                && !model.EndpointIds.Any()
                && !model.DatabaseIds.Any()
                && !model.SearchPropertyIds.Any())
            {
                return new ValidationFailure("Id", "At least one search field must be chosen.");
            }

            return null;
        }

        private ValidationFailure UpdatingSelf(SearchConfigurationModel model)
        {
            var identity = (ClaimsIdentity)principal.Identity;
            var id = Convert.ToInt32(identity.Claims.Single(x => x.Type == ClaimTypes.Sid).Value);

            if (id != model.UserId)
            {
                return new ValidationFailure("UserId", "Search Configurations may only be created, edited and deleted by the User that owns them.");
            }

            return null;
        }
    }
}
