﻿using System.Linq;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models.ToxicityValue2000Models;
using TAG.Web.Models.ToxicityValue2016Models;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Validators.ToxicityValue2016Validators
{
    public class EditToxicityValue2016Validator : SaveEntityValidator<ToxicityValue2016, EditToxicityValue2016Model>
    {
        public EditToxicityValue2016Validator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.ChemicalId).NotNull();
            RuleFor(x => x.SpeciesId).NotNull();
            RuleFor(x => x.Record).NotEmpty();
            RuleFor(x => x.MediaTypeId).NotNull();
            RuleFor(x => x.ConcentrationUsed).NotNull();
        }

        protected override bool IsDuplicate(EditToxicityValue2016Model model)
        {
            return false;
        }
    }
}
