﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using TAG.Domain.Models;

namespace TAG.Web.Models.Validators
{
    public interface IDeleteEntityValidator
    {
        ValidationResult Validate(IIdentifiable entity);
    }
}
