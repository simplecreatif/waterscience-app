﻿using System.Linq;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ExposureTypeValidators
{
    public class EditExposureTypeValidator : SaveEntityValidator<ExposureType, ExposureTypeModel>
    {
        public EditExposureTypeValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.Name).NotEmpty();
        }

        protected override bool IsDuplicate(ExposureTypeModel model)
        {
            return !string.IsNullOrWhiteSpace(model.Name) && Repository.Query<ExposureType>().Any(x =>
                x.Name.ToLower() == model.Name.ToLower()
                && x.Id != model.Id);
        }
    }
}
