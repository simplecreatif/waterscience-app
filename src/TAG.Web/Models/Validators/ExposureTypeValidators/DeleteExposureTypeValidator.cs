﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ExposureTypeValidators
{
    public class DeleteExposureTypeValidator : DeleteEntityValidator
    {
        public DeleteExposureTypeValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<ToxicityValue2000>().Any(x => x.ExposureType == entity)
                || Repository.Query<ToxicityValueAED>().Any(x => x.ExposureType == entity);
        }
    }
}
