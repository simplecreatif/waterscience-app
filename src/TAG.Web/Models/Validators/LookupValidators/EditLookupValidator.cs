﻿using System;
using System.Linq;
using FluentValidation;
using FluentValidation.Results;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.LookupValidators
{
    public class EditLookupValidator : AbstractValidator<LookupModel> , IValidator<LookupModel>
    {
        private readonly IRepository repository;
        private Type lookupType;

        public EditLookupValidator(IRepository repository)
        {
            this.repository = repository;

            RuleFor(x => x.Id).Must(ExistIfUpdating).WithMessage("The entity does not exist.");
            RuleFor(x => x.Name).NotEmpty();
            Custom(NotBeDuplicate);
        }

        private ValidationFailure NotBeDuplicate(LookupModel model)
        {
            var isDuplicate = !string.IsNullOrWhiteSpace(model.Name)
                              && repository.List<Lookup>(lookupType).Any(x =>
                              x.Name.ToLower() == model.Name.ToLower()
                              && x.Id != model.Id);

            if (isDuplicate)
            {
                return new ValidationFailure("Id", "The entity is a duplicate.");
            }

            return null;
        }

        protected virtual bool ExistIfUpdating(int id)
        {
            return id == 0 || repository.Get<object>(id, lookupType) != null;
        }

        /// <summary>
        /// Lookup validator needs to know the type of the lookup entity
        /// so it knows what to query when checking for duplicates.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="lookupType"></param>
        /// <returns></returns>
        public ValidationResult Validate(object model, Type lookupType)
        {
            this.lookupType = lookupType;

            return base.Validate((LookupModel) model);
        }

        public ValidationResult Validate(object model)
        {
            throw new NotSupportedException();
        }
    }
}
