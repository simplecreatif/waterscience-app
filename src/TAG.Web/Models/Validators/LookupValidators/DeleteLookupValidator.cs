﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Extensions;

namespace TAG.Web.Models.Validators.LookupValidators
{
    public class DeleteLookupValidator : DeleteEntityValidator
    {
        private readonly ISession session;

        public DeleteLookupValidator(IRepository repository, ISession session) : base(repository)
        {
            this.session = session;
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            // Rather than write deletion validators for every lookup,
            // we'll get the lookup's type, reflect over TAG.Domain to
            // find domain models which use this type, then query the
            // repository for any such entity whose referring property
            // refers to this lookup entity.

            var lookupType = entity.GetType();
            var domainModels = AssemblyExtensions.GetTypesDerivingFrom<IIdentifiable>();

            foreach (var domainModel in domainModels)
            {
                // Find properties which refer to this lookup type
                var referringProperties = domainModel
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Where(x => x.PropertyType == lookupType)
                    .ToList();

                if (!referringProperties.Any())
                {
                    continue;
                }

                // Create a query which counts this type of domain model
                // where one or more of its properties refer to this lookup
                // entity specifically.
                var query = session.QueryOver<IIdentifiable>(domainModel.Name);
                var or = Restrictions.Disjunction();
                foreach (var referringProperty in referringProperties)
                {
                    or.Add(Restrictions.Eq(referringProperty.Name, entity));
                }

                var hasAssociations = query.And(or).RowCount() > 0;

                if (hasAssociations)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
