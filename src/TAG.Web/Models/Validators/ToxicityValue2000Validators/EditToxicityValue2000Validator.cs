﻿using System.Linq;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models.ToxicityValue2000Models;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Validators.ToxicityValue2000Validators
{
    public class EditToxicityValue2000Validator : SaveEntityValidator<ToxicityValue2000, EditToxicityValue2000Model>
    {
        public EditToxicityValue2000Validator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.ChemicalId).NotNull();
            RuleFor(x => x.SpeciesId).NotNull();
            RuleFor(x => x.ConcentrationCodeId).NotNull();
        }

        protected override bool IsDuplicate(EditToxicityValue2000Model model)
        {
            return false;
        }
    }
}
