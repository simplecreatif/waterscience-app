﻿using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ToxicityValue2000Validators
{
    public class DeleteToxicityValue2000Validator : DeleteEntityValidator
    {
        /// <summary>
        /// ToxicityValue2000 can always be deleted so this is effectively a stubbed-out class.
        /// </summary>
        /// <param name="repository"></param>
        public DeleteToxicityValue2000Validator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return false;
        }
    }
}
