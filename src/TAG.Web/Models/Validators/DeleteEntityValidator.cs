﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators
{
    /// <summary>
    /// Thin wrapper for fluent validator which allows us to validate
    /// deletion of something without having to submit a model for that
    /// thing (we can just submit an id and it will be wrapped in a temporary
    /// model by this class).
    /// </summary>
    public abstract class DeleteEntityValidator : AbstractValidator<IIdentifiable>, IDeleteEntityValidator
    {
        protected readonly IRepository Repository;

        protected DeleteEntityValidator(IRepository repository)
        {
            Repository = repository;

            Custom(NotHaveAssociations);
        }

        /// <summary>
        /// Negates the condition to make writing
        /// implementations easier (avoids double negatives)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private ValidationFailure NotHaveAssociations(IIdentifiable entity)
        {
            if (HasAssociations(entity))
            {
                return new ValidationFailure("Id", "The entity cannot be deleted because it is currently in use.");
            }

            return null;
        }

        protected abstract bool HasAssociations(IIdentifiable entity);
    }
}
