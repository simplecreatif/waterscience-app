﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ChemicalTypeValidators
{
    public class EditChemicalTypeValidator : SaveEntityValidator<ChemicalType, ChemicalTypeModel>
    {
        public EditChemicalTypeValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Class).NotEmpty();
        }

        protected override bool IsDuplicate(ChemicalTypeModel model)
        {
            return !string.IsNullOrWhiteSpace(model.Name) && Repository.Query<ChemicalType>().Any(x =>
                x.Name.ToLower() == model.Name.ToLower()
                && x.Id != model.Id);
        }
    }
}
