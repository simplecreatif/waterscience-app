﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ChemicalTypeValidators
{
    public class DeleteChemicalTypeValidator : DeleteEntityValidator
    {
        public DeleteChemicalTypeValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<Chemical>().Any(x => x.ChemicalType == entity);
        }
    }
}
