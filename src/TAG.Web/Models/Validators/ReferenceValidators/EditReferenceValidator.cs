﻿using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ReferenceValidators
{
    public class EditReferenceValidator : SaveEntityValidator<Reference, ReferenceModel>
    {
        public EditReferenceValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.Authors).NotEmpty();
        }

        protected override bool IsDuplicate(ReferenceModel model)
        {
            return false;
        }
    }
}
