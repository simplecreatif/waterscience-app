﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ReferenceValidators
{
    public class DeleteReferenceValidator : DeleteEntityValidator
    {
        public DeleteReferenceValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<ToxicityValue2000>().Any(x => x.Reference == entity)
                || Repository.Query<ToxicityValueAED>().Any(x => x.Reference == entity);
        }
    }
}
