﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators
{
    public abstract class SaveEntityValidator<TEntity, TModel> : AbstractValidator<TModel>, IValidator<TModel>
        where TModel : class, IIdentifiable
        where TEntity : class, IIdentifiable
    {
        protected readonly IRepository Repository;

        protected SaveEntityValidator(IRepository repository)
        {
            Repository = repository;

            RuleFor(x => x.Id).Must(ExistIfUpdating).WithMessage("The entity does not exist.");
            Custom(NotBeDuplicate);
        }

        protected virtual bool ExistIfUpdating(int id)
        {
            return id == 0 || Repository.Get<TEntity>(id) != null;
        }

        private ValidationFailure NotBeDuplicate(TModel model)
        {
            if (IsDuplicate(model))
            {
                return new ValidationFailure("Id", "The entity is a duplicate.");
            }

            return null;
        }

        protected abstract bool IsDuplicate(TModel model);

        public ValidationResult Validate(object model)
        {
            return base.Validate((TModel) model);
        }
    }
}
