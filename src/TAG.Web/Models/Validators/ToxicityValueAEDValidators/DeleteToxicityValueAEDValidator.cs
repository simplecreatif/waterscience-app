﻿using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ToxicityValueAEDValidators
{
    public class DeleteToxicityValueAEDValidator : DeleteEntityValidator
    {
        /// <summary>
        /// ToxicityValueAED can always be deleted so this is effectively a stubbed-out class.
        /// </summary>
        /// <param name="repository"></param>
        public DeleteToxicityValueAEDValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return false;
        }
    }
}
