﻿using System.Linq;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Validators.ToxicityValueAEDValidators
{
    public class EditToxicityValueAEDValidator : SaveEntityValidator<ToxicityValueAED, EditToxicityValueAEDModel>
    {
        public EditToxicityValueAEDValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.ChemicalId).NotNull();
            RuleFor(x => x.SpeciesId).NotNull();
            RuleFor(x => x.Identifier).NotEmpty();
        }

        protected override bool IsDuplicate(EditToxicityValueAEDModel model)
        {
            return !string.IsNullOrWhiteSpace(model.Identifier)
                   && Repository.Query<ToxicityValueAED>().Any(x =>
                       x.Identifier.ToLower() == model.Identifier.ToLower()
                        && x.Id != model.Id);
        }
    }
}
