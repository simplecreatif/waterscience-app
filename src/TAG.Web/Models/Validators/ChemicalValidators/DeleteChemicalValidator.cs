﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ChemicalValidators
{
    public class DeleteChemicalValidator : DeleteEntityValidator
    {
        public DeleteChemicalValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<ToxicityValue>().Any(x => x.Chemical == entity);
        }
    }
}
