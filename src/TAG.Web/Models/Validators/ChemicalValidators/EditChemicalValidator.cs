﻿using System.Linq;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models.ChemicalModels;

namespace TAG.Web.Models.Validators.ChemicalValidators
{
    public class EditChemicalValidator : SaveEntityValidator<Chemical, EditChemicalModel>
    {
        public EditChemicalValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.CASNumber).GreaterThan(0);
        }

        protected override bool IsDuplicate(EditChemicalModel model)
        {
            return Repository.Query<Chemical>().Any(x =>
                    x.CASNumber == model.CASNumber
                    && x.Id != model.Id);
        }
    }
}
