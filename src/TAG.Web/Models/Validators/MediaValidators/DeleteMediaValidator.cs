﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.MediaValidators
{
    public class DeleteMediaValidator : DeleteEntityValidator
    {
        public DeleteMediaValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<ToxicityValueAED>().Any(x => x.Media == entity);
        }
    }
}
