﻿using System.Linq;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models.MediaModels;

namespace TAG.Web.Models.Validators.MediaValidators
{
    public class EditMediaValidator : SaveEntityValidator<Media, EditMediaModel>
    {
        public EditMediaValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.Name).NotEmpty();
        }

        protected override bool IsDuplicate(EditMediaModel model)
        {
            return !string.IsNullOrWhiteSpace(model.Name) && Repository.Query<Media>().Any(x =>
                x.Name.ToLower() == model.Name.ToLower()
                && x.Id != model.Id);
        }
    }
}
