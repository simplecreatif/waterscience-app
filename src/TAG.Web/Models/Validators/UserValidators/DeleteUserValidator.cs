﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.UserValidators
{
    public class DeleteUserValidator : DeleteEntityValidator
    {
        /// <summary>
        /// User can always be deleted so this is effectively a stubbed-out class.
        /// </summary>
        /// <param name="repository"></param>
        public DeleteUserValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return false;
        }
    }
}
