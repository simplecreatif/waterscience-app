﻿using System;
using System.Linq;
using System.Security.Claims;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Domain;

namespace TAG.Web.Models.Validators
{
    public class EditUserValidator : SaveEntityValidator<User, EditUserModel>
    {
        public EditUserValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Password).NotEmpty().When(NewUser);
            RuleFor(x => x.PasswordConfirmation).NotEmpty().When(x => !string.IsNullOrEmpty(x.Password));
            RuleFor(x => x.Password).Equal(x => x.PasswordConfirmation)
                                    .When(x => !string.IsNullOrEmpty(x.Password) && !string.IsNullOrEmpty(x.PasswordConfirmation))
                                    .WithMessage("Password should be equal to Password Confirmation.");
            RuleFor(x => x.RoleId).NotEmpty();
        }

        protected override bool IsDuplicate(EditUserModel model)
        {
            return !String.IsNullOrEmpty(model.Email) && Repository.Query<User>()
                .Any(x =>
                    x.Email.ToLower() == model.Email.ToLower()
                    && x.Id != model.Id);
        }

        private bool NewUser(EditUserModel model)
        {
            return !Repository.Query<User>().Any(x => x.Id == model.Id);
        }
    }
}
