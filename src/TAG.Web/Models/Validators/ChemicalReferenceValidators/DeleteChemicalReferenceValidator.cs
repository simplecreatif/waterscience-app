﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ChemicalReferenceValidators
{
    public class DeleteChemicalReferenceValidator : DeleteEntityValidator
    {
        public DeleteChemicalReferenceValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<Chemical>().Any(x =>
                x.ReferenceSolubility == entity
                || x.ReferenceBoilingPoint == entity
                || x.ReferenceMeltingPoint == entity
                || x.ReferenceVapourPressure == entity
                || x.ReferenceKoc == entity
                || x.ReferenceKow == entity
                || x.ReferenceMolecularWeight == entity
                || x.ReferenceFormulae == entity
                || x.ReferenceBCF == entity
                || x.ReferenceHalfLifeWater == entity
                || x.ReferenceHalfLifeSediment == entity
                || x.ReferenceSpecificGravity == entity);
        }
    }
}
