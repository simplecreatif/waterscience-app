﻿using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.ChemicalReferenceValidators
{
    public class EditChemicalReferenceValidator : SaveEntityValidator<ChemicalReference, ChemicalReferenceModel>
    {
        public EditChemicalReferenceValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.Authors).NotEmpty();
            RuleFor(x => x.FirstPage).NotNull().GreaterThan(0);
            RuleFor(x => x.LastPage).NotNull().GreaterThan(0);
        }

        protected override bool IsDuplicate(ChemicalReferenceModel model)
        {
            return false;
        }
    }
}
