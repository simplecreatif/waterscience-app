﻿using System.Linq;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.SpeciesClassModels;

namespace TAG.Web.Models.Validators.SpeciesClassValidators
{
    public class EditSpeciesClassValidator : SaveEntityValidator<SpeciesClass, EditSpeciesClassModel>
    {
        public EditSpeciesClassValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.PhylumId).Must(HavePhylumThatExists)
                .WithMessage("The specified Phylum could not be found");
        }

        protected override bool IsDuplicate(EditSpeciesClassModel model)
        {
            return !string.IsNullOrEmpty(model.Name) && Repository.Query<SpeciesClass>().Any(x =>
                    x.Name.ToLower() == model.Name.ToLower()
                    && x.Id != model.Id);
        }

        private bool HavePhylumThatExists(int? phylumId)
        {
            return phylumId != null && Repository.Get<SpeciesPhylum>(phylumId.Value) != null;
        }
    }
}
