﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.SpeciesClassValidators
{
    public class DeleteSpeciesClassValidator : DeleteEntityValidator
    {
        public DeleteSpeciesClassValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<Species>().Any(x => x.Class == entity);
        }
    }
}
