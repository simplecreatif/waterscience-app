﻿using System;
using System.Linq;
using FluentValidation;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.EndpointValidator
{
    public class EditEndpointValidator : SaveEntityValidator<Endpoint, EndpointModel>
    {
        public EditEndpointValidator(IRepository repository) : base(repository)
        {
            RuleFor(x => x.Abbreviation).NotEmpty();
        }

        protected override bool IsDuplicate(EndpointModel model)
        {
            return !string.IsNullOrEmpty(model.Abbreviation) && Repository.Query<Endpoint>().Any(x =>
                    x.Abbreviation.ToLower() == model.Abbreviation.ToLower()
                    && x.Id != model.Id);
        }
    }
}
