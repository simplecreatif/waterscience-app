﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Validators.EndpointValidator
{
    public class DeleteEndpointValidator : DeleteEntityValidator
    {
        public DeleteEndpointValidator(IRepository repository) : base(repository)
        {
        }

        protected override bool HasAssociations(IIdentifiable entity)
        {
            return Repository.Query<ToxicityValue>().Any(x => x.Endpoint == entity)
                   || Repository.Query<ToxicityValue2016>().Any(x => 
                            x.EndpointFromPaper == entity
                            || x.EndpointMeasurement == entity);
        }
    }
}
