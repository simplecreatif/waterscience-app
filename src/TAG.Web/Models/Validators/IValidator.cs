﻿using FluentValidation.Results;

namespace TAG.Web.Models.Validators
{
    public interface IValidator
    {
        ValidationResult Validate(object model);
    }

    public interface IValidator<in T> : IValidator
    {
        ValidationResult Validate(T model);
    }
}
