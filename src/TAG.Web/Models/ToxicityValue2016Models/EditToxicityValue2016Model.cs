﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Web.Models.ToxicityValue2016Models
{
    public class EditToxicityValue2016Model : IIdentifiable
    {
        public int Id { get; set; }
        public int? ChemicalId { get; set; }
        public int? SpeciesId { get; set; }
        public int? DurationId { get; set; }
        public int? DurationUnitId { get; set; }
        public int? DurationHourId { get; set; }
        public int? EffectId { get; set; }
        public int? EndpointId { get; set; }
        public string Concentration { get; set; }
        public int? ConcentrationUnitId { get; set; }

        public string Record { get; set; }
        public string DataSource { get; set; }
        public bool IsChronic { get; set; }
        public decimal? ConcentrationUsed { get; set; }

        public int? MediaTypeId { get; set; }
        public int? AgeId { get; set; }
        public int? EndpointFromPaperId { get; set; }
        public int? EndpointMeasurementId { get; set; }
        public int? GuidelineGroupId { get; set; }
    }
}
