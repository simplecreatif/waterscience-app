﻿using TAG.Domain.Models;

namespace TAG.Web.Models.ToxicityValue2016Models
{
    public class ListToxicityValue2016Model : IIdentifiable
    {
        public int Id { get; set; }
        public string ChemicalName { get; set; }

        /// <summary>
        /// Chemical Id is only used for filtering toxicity values
        /// on the manage page.
        /// </summary>
        public int ChemicalId { get; set; }
        public decimal? GuidelineGroupValue { get; set; }
        public int GuidelineGroupId { get; set; }
        public int SpeciesId { get; set; }

        public string SpeciesScientificName { get; set; }
        public string Duration { get; set; }
        public string DurationUnit { get; set; }
        public string Effect { get; set; }
        public string Endpoint { get; set; }
        public string Concentration { get; set; }
        public string ConcentrationUnit { get; set; }
    }
}
