﻿using TAG.Domain.Models;

namespace TAG.Web.Models.ToxicityValue2016Models
{
    public class ToxicityValue2016Model : IIdentifiable
    {
        public int Id { get; set; }
        public string ChemicalName { get; set; }
        public string SpeciesScientificName { get; set; }
        public string Duration { get; set; }
        public string DurationUnit { get; set; }
        public string DurationHour { get; set; }
        public string Effect { get; set; }
        public string Endpoint { get; set; }
        public string Concentration { get; set; }
        public string ConcentrationUnit { get; set; }

        public string Record { get; set; }
        public string DataSource { get; set; }
        public string MediaType { get; set; }
        public string Age { get; set; }
        public string EndpointFromPaper { get; set; }
        public string EndpointMeasurement { get; set; }
        public bool IsChronic { get; set; }
        public decimal ConcentrationUsed { get; set; }

        public int? ChemicalId { get; set; }
        public int? SpeciesId { get; set; }
        public int? DurationId { get; set; }
        public int? DurationUnitId { get; set; }
        public int? DurationHourId { get; set; }
        public int? EffectId { get; set; }
        public int? EndpointId { get; set; }
        public int? ConcentrationUnitId { get; set; }
        public int? MediaTypeId { get; set; }
        public int? AgeId { get; set; }
        public int? EndpointFromPaperId { get; set; }
        public int? EndpointMeasurementId { get; set; }
        public int? GuidelineGroupId { get; set; }
    }
}
