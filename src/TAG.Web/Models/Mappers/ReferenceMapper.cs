﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Mappers
{
    public class ReferenceMapper : BaseModelMapper<Reference, ReferenceModel>
    {
        public override ReferenceModel ToModel(Reference entity)
        {
            return new ReferenceModel
            {
                Id = entity.Id,
                OrgRefNumber = entity.OrgRefNumber,
                Authors = entity.Authors,
                AuthorsAbbreviated = entity.AuthorsAbbreviated,
                Title = entity.Title,
                Journal = entity.Journal,
                Year = entity.Year,
                Volume = entity.Volume,
                IssueNumber = entity.IssueNumber,
                FirstPage = entity.FirstPage,
                LastPage = entity.LastPage
            };
        }

        public override void ToEntity(ReferenceModel model, Reference entity)
        {
            entity.OrgRefNumber = model.OrgRefNumber;
            entity.Authors = model.Authors;
            entity.AuthorsAbbreviated = model.AuthorsAbbreviated;
            entity.Title = model.Title;
            entity.Journal = model.Journal;
            entity.Year = model.Year;
            entity.Volume = model.Volume;
            entity.IssueNumber = model.IssueNumber;
            entity.FirstPage = model.FirstPage;
            entity.LastPage = model.LastPage;
        }
    }
}
