﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.ToxicityValue2000Models;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Mappers.ToxicityValue2000Mappers
{
    public class ListToxicityValue2000Mapper : BaseModelMapper<ToxicityValue2000, ListToxicityValue2000Model>
    {
        public override ListToxicityValue2000Model ToModel(ToxicityValue2000 entity)
        {
            return new ListToxicityValue2000Model
            {
                Id = entity.Id,
                ChemicalName = entity.Chemical?.Name,
                SpeciesScientificName = entity.Species?.ScientificName,
                Duration = entity.Duration?.Name,
                DurationUnit = entity.DurationUnit?.Name,
                Effect = entity.Effect?.Name,
                Endpoint = entity.Endpoint?.Name,
                Concentration = entity.Concentration,
                ConcentrationUnit = entity.ConcentrationUnit?.Name
            };
        }

        public override void ToEntity(ListToxicityValue2000Model model, ToxicityValue2000 entity)
        {
            throw new NotSupportedException();
        }
    }
}
