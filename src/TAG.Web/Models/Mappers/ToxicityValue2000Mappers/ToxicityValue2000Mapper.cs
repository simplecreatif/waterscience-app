﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.ToxicityValue2000Models;

namespace TAG.Web.Models.Mappers.ToxicityValue2000Mappers
{
    public class ToxicityValue2000Mapper : BaseModelMapper<ToxicityValue2000, ToxicityValue2000Model>
    {
        public override ToxicityValue2000Model ToModel(ToxicityValue2000 entity)
        {
            return new ToxicityValue2000Model
            {
                Id = entity.Id,
                ChemicalName = entity.Chemical?.Name,
                SpeciesScientificName = entity.Species?.ScientificName,
                Duration = entity.Duration?.Name,
                DurationUnit = entity.DurationUnit?.Name,
                DurationHour = entity.DurationHour?.Name,
                Effect = entity.Effect?.Name,
                Endpoint = entity.Endpoint?.Name,
                Concentration = entity.Concentration,
                ConcentrationUnit = entity.ConcentrationUnit?.Name,

                MediaType = entity.MediaType?.Name,
                TestType = entity.TestType?.Name,
                EffectUsed = entity.EffectUsed?.Name,
                ExposureType = entity.ExposureType?.Name,
                ConcentrationUsed = entity.ConcentrationUsed,
                ConcentrationCode = entity.ConcentrationCode?.Name,
                ConvNOEC = entity.ConvNOEC,
                HardnessCorrectedConc = entity.HardnessCorrectedConc,
                ConcentrationType = entity.ConcentrationType?.Name,
                Temperature = entity.Temperature,
                PH = entity.PH,
                Hardness = entity.Hardness,
                Salinity = entity.Salinity,
                ReferenceTitle = entity.Reference?.Title,
                Status = entity.Status?.Name,
                TotalAmmoniaMg = entity.TotalAmmoniaMg,
                TotalAmmoniaPh8 = entity.TotalAmmoniaPh8,

                ChemicalId = entity.Chemical?.Id,
                SpeciesId = entity.Species?.Id,
                DurationId = entity.Duration?.Id,
                DurationUnitId = entity.DurationUnit?.Id,
                DurationHourId = entity.DurationHour?.Id,
                EffectId = entity.Effect?.Id,
                EndpointId = entity.Endpoint?.Id,
                ConcentrationUnitId = entity.ConcentrationUnit?.Id,
                MediaTypeId = entity.MediaType?.Id,
                TestTypeId = entity.TestType?.Id,
                EffectUsedId = entity.EffectUsed?.Id,
                ExposureTypeId = entity.ExposureType?.Id,
                ConcentrationCodeId = entity.ConcentrationCode?.Id,
                ConcentrationTypeId = entity.ConcentrationType?.Id,
                ReferenceId = entity.Reference?.Id,
                StatusId = entity.Status?.Id
            };
        }

        public override void ToEntity(ToxicityValue2000Model model, ToxicityValue2000 entity)
        {
            throw new NotSupportedException();
        }
    }
}
