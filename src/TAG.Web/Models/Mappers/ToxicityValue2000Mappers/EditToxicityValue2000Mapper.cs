﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.ToxicityValue2000Models;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Mappers.ToxicityValue2000Mappers
{
    public class EditToxicityValue2000Mapper : BaseModelMapper<ToxicityValue2000, EditToxicityValue2000Model>
    {
        private readonly IRepository repository;

        public EditToxicityValue2000Mapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override EditToxicityValue2000Model ToModel(ToxicityValue2000 entity)
        {
            throw new NotSupportedException();
        }

        public override void ToEntity(EditToxicityValue2000Model model, ToxicityValue2000 entity)
        {
            entity.Chemical = repository.Get<Chemical>(model.ChemicalId);
            entity.Species = repository.Get<Species>(model.SpeciesId);
            entity.Duration = repository.Get<Duration>(model.DurationId);
            entity.DurationUnit = repository.Get<DurationUnit>(model.DurationUnitId);
            entity.DurationHour = repository.Get<Duration>(model.DurationHourId);
            entity.Effect = repository.Get<Effect>(model.EffectId);
            entity.Endpoint = repository.Get<Endpoint>(model.EndpointId);
            entity.Concentration = model.Concentration;
            entity.ConcentrationUnit = repository.Get<ConcentrationUnit>(model.ConcentrationUnitId);

            entity.ConcentrationUsed = model.ConcentrationUsed;
            entity.ConvNOEC = model.ConvNOEC;
            entity.HardnessCorrectedConc = model.HardnessCorrectedConc;
            entity.Temperature = model.Temperature;
            entity.PH = model.PH;
            entity.Hardness = model.Hardness;
            entity.Salinity = model.Salinity;
            entity.TotalAmmoniaMg = model.TotalAmmoniaMg;
            entity.TotalAmmoniaPh8 = model.TotalAmmoniaPh8;

            entity.MediaType = repository.Get<MediaType>(model.MediaTypeId);
            entity.TestType = repository.Get<TestType>(model.TestTypeId);
            entity.EffectUsed = repository.Get<Effect>(model.EffectId);
            entity.ExposureType = repository.Get<ExposureType>(model.ExposureTypeId);
            entity.ConcentrationCode = repository.Get<ConcentrationCode>(model.ConcentrationCodeId);
            entity.ConcentrationType = repository.Get<ConcentrationType>(model.ConcentrationTypeId);
            entity.Reference = repository.Get<Reference>(model.ReferenceId);
            entity.Status = repository.Get<Status>(model.StatusId);
        }
    }
}
