﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Services.Search;
using TAG.Web.Models.SearchModels;

namespace TAG.Web.Models.Mappers
{
    public class SearchResultMapper : BaseModelMapper<SearchResult, SearchResultModel>
    {
        public override SearchResultModel ToModel(SearchResult entity)
        {
            return new SearchResultModel
            {
                ToxicityValues = entity.ToxicityValues,
                Guidelines2000 = entity.Guidelines2000.Select(GuidelineToModel).ToList(),
                Guidelines2016 = entity.Guidelines2016.Select(GuidelineToModel).ToList()
            };
        }

        public override void ToEntity(SearchResultModel model, SearchResult entity)
        {
            throw new NotSupportedException();
        }

        private SearchGuidelineModel GuidelineToModel(Guideline entity)
        {
            return new SearchGuidelineModel
            {
                GuidelineValue = Math.Round(entity.GuidelineValue, 4).ToString("0.0000"),
                Toxicant = entity.Toxicant,
                SpeciesScientificName = entity.SpeciesScientificName,
                SpeciesCommonName = entity.SpeciesCommonName,
                Endpoint = entity.Endpoint,
                Effect = entity.Effect,
                MediaType = entity.MediaType,
                TestType = entity.TestType,
                DurationHour = entity.DurationHour,
                ConcentrationUnit = entity.ConcentrationUnit,
                AnimalCategory = entity.AnimalCategory
            };
        }
    }
}
