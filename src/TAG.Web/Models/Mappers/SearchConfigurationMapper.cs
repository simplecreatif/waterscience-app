﻿using System.Collections.Generic;
using System.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;

namespace TAG.Web.Models.Mappers
{
    public class SearchConfigurationMapper : BaseModelMapper<SearchConfiguration, SearchConfigurationModel>
    {
        private readonly IRepository repository;

        public SearchConfigurationMapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override SearchConfigurationModel ToModel(SearchConfiguration entity)
        {
            return new SearchConfigurationModel
            {
                Id = entity.Id,
                Name = entity.Name,
                CASNumber = entity.CASNumber,
                ChemicalId = entity.Chemical?.Id,
                ToxicantId = entity.Toxicant?.Id,
                UserId = entity.User.Id,
                SpeciesIds = entity.Species?.Select(x => x.Id).ToArray(),
                EffectIds = entity.Effects?.Select(x => x.Id).ToArray(),
                EndpointIds = entity.Endpoints?.Select(x => x.Id).ToArray(),
                DatabaseIds = entity.Databases?.Select(x => x.Id).ToArray(),
                SearchPropertyIds = entity.SearchProperties?.Select(x => x.Id).ToArray()
            };
        }

        public override void ToEntity(SearchConfigurationModel model, SearchConfiguration entity)
        {
            entity.Id = model.Id;
            entity.Name = model.Name;
            entity.CASNumber = model.CASNumber;
            entity.Chemical = repository.Get<Chemical>(model.ChemicalId);
            entity.Toxicant = repository.Get<Toxicant>(model.ToxicantId);
            entity.User = repository.Get<User>(model.UserId);
            entity.Species = new List<Species>();
            entity.Effects = new List<Effect>();
            entity.Endpoints = new List<Endpoint>();
            entity.Databases = new List<Database>();
            entity.SearchProperties = new List<SearchProperty>();

            if (model.SpeciesIds != null)
            {
                foreach (var speciesId in model.SpeciesIds)
                {
                    entity.Species.Add(repository.Get<Species>(speciesId));
                }
            }

            if (model.EffectIds != null)
            {
                foreach (var effectId in model.EffectIds)
                {
                    entity.Effects.Add(repository.Get<Effect>(effectId));
                }
            }

            if (model.EndpointIds != null)
            {
                foreach (var endpointId in model.EndpointIds)
                {
                    entity.Endpoints.Add(repository.Get<Endpoint>(endpointId));
                }
            }

            if (model.DatabaseIds != null)
            {
                foreach (var databaseId in model.DatabaseIds)
                {
                    entity.Databases.Add(repository.Get<Database>(databaseId));
                }
            }

            if (model.SearchPropertyIds != null)
            {
                foreach (var searchPropertyId in model.SearchPropertyIds)
                {
                    entity.SearchProperties.Add(repository.Get<SearchProperty>(searchPropertyId));
                }
            }
        }
    }
}
