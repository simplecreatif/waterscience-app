﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.ChemicalModels;

namespace TAG.Web.Models.Mappers.ChemicalMappers
{
    public class EditChemicalMapper : BaseModelMapper<Chemical, EditChemicalModel>
    {
        private readonly IRepository repository;

        public EditChemicalMapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override EditChemicalModel ToModel(Chemical entity)
        {
            throw new NotSupportedException();
        }

        public override void ToEntity(EditChemicalModel model, Chemical entity)
        {
            entity.CASNumber = model.CASNumber;
            entity.Toxicant = repository.Get<Toxicant>(model.ToxicantId);
            entity.Name = model.Name;
            entity.CommonName = model.CommonName;
            entity.ChemicalType = repository.Get<ChemicalType>(model.ChemicalTypeId);
            entity.SpecificGravity = model.SpecificGravity;
            entity.Solubility = model.Solubility;
            entity.BoilingPoint = model.BoilingPoint;
            entity.MeltingPoint = model.MeltingPoint;
            entity.FlashPoint = model.FlashPoint;
            entity.VapourPressure = model.VapourPressure;
            entity.LogKoc = model.LogKoc;
            entity.LogKow = model.LogKow;
            entity.LogBCF = model.LogBCF;
            entity.MolecularWeight = model.MolecularWeight;
            entity.Formulae = model.Formulae;
            entity.HalfLifeWater = model.HalfLifeWater;
            entity.HalfLifeSediment = model.HalfLifeSediment;
        }
    }
}
