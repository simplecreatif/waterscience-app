﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.ChemicalModels;

namespace TAG.Web.Models.Mappers.ChemicalMappers
{
    public class ListChemicalMapper : BaseModelMapper<Chemical, ListChemicalModel>
    {
        public override ListChemicalModel ToModel(Chemical entity)
        {
            return new ListChemicalModel
            {
                Id = entity.Id,
                CASNumber = entity.CASNumber,
                ToxicantName = entity.Toxicant?.Name,
                Name = entity.Name,
                CommonName = entity.CommonName
            };
        }

        public override void ToEntity(ListChemicalModel model, Chemical entity)
        {
            throw new NotSupportedException();
        }
    }
}
