﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.ChemicalModels;

namespace TAG.Web.Models.Mappers.ChemicalMappers
{
    public class ChemicalMapper : BaseModelMapper<Chemical, ChemicalModel>
    {
        private readonly IRepository repository;

        public ChemicalMapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override ChemicalModel ToModel(Chemical entity)
        {
            return new ChemicalModel
            {
                Id = entity.Id,
                CASNumber = entity.CASNumber,
                ToxicantId = entity.Toxicant?.Id,
                ToxicantName = entity.Toxicant?.Name,
                Name = entity.Name,
                CommonName = entity.CommonName,
                ChemicalTypeId = entity.ChemicalType?.Id,
                ChemicalTypeName = entity.ChemicalType?.Name,
                SpecificGravity = entity.SpecificGravity,
                Solubility = entity.Solubility,
                BoilingPoint = entity.BoilingPoint,
                MeltingPoint = entity.MeltingPoint,
                FlashPoint = entity.FlashPoint,
                VapourPressure = entity.VapourPressure,
                LogKoc = entity.LogKoc,
                LogKow = entity.LogKow,
                LogBCF = entity.LogBCF,
                MolecularWeight = entity.MolecularWeight,
                Formulae = entity.Formulae,
                HalfLifeWater = entity.HalfLifeWater,
                HalfLifeSediment = entity.HalfLifeSediment
            };
        }

        public override void ToEntity(ChemicalModel model, Chemical entity)
        {
            throw new NotSupportedException();
        }
    }
}
