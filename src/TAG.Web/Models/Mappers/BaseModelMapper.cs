﻿namespace TAG.Web.Models.Mappers
{
    public abstract class BaseModelMapper<TEntity, TModel> : IModelMapper<TEntity, TModel>
        where TEntity : new()
    {
        public abstract TModel ToModel(TEntity entity);

        public abstract void ToEntity(TModel model, TEntity entity);

        public TEntity ToEntity(TModel model)
        {
            var entity = new TEntity();
            ToEntity(model, entity);
            return entity;
        }

        public object ToModel(object entity)
        {
            return ToModel((TEntity)entity);
        }

        public object ToEntity(object model)
        {
            return ToEntity((TModel)model);
        }

        public void ToEntity(object model, object entity)
        {
            ToEntity((TModel)model, (TEntity)entity);
        }
    }
}
