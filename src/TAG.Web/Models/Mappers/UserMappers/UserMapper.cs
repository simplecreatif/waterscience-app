﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Web.Models.UserModels;

namespace TAG.Web.Models.Mappers.UserMappers
{
    public class UserMapper : BaseModelMapper<User, UserModel>
    {
        public override UserModel ToModel(User entity)
        {
            return new UserModel
            {
                Id = entity.Id,
                Email = entity.Email,
                RoleId = entity.Role.Id
            };
        }

        public override void ToEntity(UserModel model, User entity)
        {
            throw new NotSupportedException();
        }
    }
}
