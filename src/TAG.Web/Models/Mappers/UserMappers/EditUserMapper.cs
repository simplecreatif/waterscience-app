﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Domain;
using TAG.Web.Services.Interfaces;

namespace TAG.Web.Models.Mappers.UserMappers
{
    public class EditUserMapper : BaseModelMapper<User, EditUserModel>
    {
        private readonly IRepository repository;
        private readonly IAuthenticationService authenticationService;
        private readonly ClaimsPrincipal principal;

        public EditUserMapper(IRepository repository, IAuthenticationService authenticationService, ClaimsPrincipal principal)
        {
            this.repository = repository;
            this.authenticationService = authenticationService;
            this.principal = principal;
        }

        public override EditUserModel ToModel(User entity)
        {
            throw new NotSupportedException();
        }

        public override void ToEntity(EditUserModel model, User entity)
        {
            entity.Id = model.Id;
            entity.Email = model.Email;

            if (principal.IsInRole(TAGRole.Administrator))
            {
                entity.Role = repository.Get<Role>(model.RoleId);
            }

            if (model.Password != null)
            {
                entity.PasswordHash = authenticationService.HashPassword(model.Password);
            }
        }
    }
}
