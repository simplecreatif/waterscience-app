﻿using System;
using System.Linq;
using TAG.Domain.Models;
using TAG.Web.Models.UserModels;

namespace TAG.Web.Models.Mappers.UserMappers
{
    public class ListUserMapper : BaseModelMapper<User, ListUserModel>
    {
        public override ListUserModel ToModel(User entity)
        {
            return new ListUserModel
            {
                Id = entity.Id,
                Email = entity.Email,
                RoleId = entity.Role.Id
            };
        }

        public override void ToEntity(ListUserModel model, User entity)
        {
            throw new NotImplementedException();
        }
    }
}
