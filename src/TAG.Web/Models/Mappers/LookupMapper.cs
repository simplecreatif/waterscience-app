﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;

namespace TAG.Web.Models.Mappers
{
    public class LookupMapper : BaseModelMapper<Lookup, LookupModel>
    {
        public override LookupModel ToModel(Lookup entity)
        {
            return new LookupModel
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public override void ToEntity(LookupModel model, Lookup entity)
        {
            entity.Name = model.Name;
        }
    }
}
