﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Web.Models.Mappers
{
    public class ChemicalTypeMapper : BaseModelMapper<ChemicalType, ChemicalTypeModel>
    {
        public override ChemicalTypeModel ToModel(ChemicalType entity)
        {
            return new ChemicalTypeModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Class = entity.Class
            };
        }

        public override void ToEntity(ChemicalTypeModel model, ChemicalType entity)
        {
            entity.Name = model.Name;
            entity.Class = model.Class;
        }
    }
}
