﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Mappers
{
    public class ChemicalReferenceMapper : BaseModelMapper<ChemicalReference, ChemicalReferenceModel>
    {
        public override ChemicalReferenceModel ToModel(ChemicalReference entity)
        {
            return new ChemicalReferenceModel
            {
                Id = entity.Id,
                Authors = entity.Authors,
                Title = entity.Title,
                Journal = entity.Journal,
                Year = entity.Year,
                Publisher = entity.Publisher,
                Volume = entity.Volume,
                IssueNumber = entity.IssueNumber,
                FirstPage = entity.FirstPage,
                LastPage = entity.LastPage
            };
        }

        public override void ToEntity(ChemicalReferenceModel model, ChemicalReference entity)
        {
            entity.Authors = model.Authors;
            entity.Title = model.Title;
            entity.Journal = model.Journal;
            entity.Year = model.Year;
            entity.Publisher = model.Publisher;
            entity.Volume = model.Volume;
            entity.IssueNumber = model.IssueNumber;
            entity.FirstPage = model.FirstPage.Value;
            entity.LastPage = model.LastPage.Value;
        }
    }
}
