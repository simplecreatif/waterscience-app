﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Web.Models.Mappers
{
    public class ExposureTypeMapper : BaseModelMapper<ExposureType, ExposureTypeModel>
    {
        public override ExposureTypeModel ToModel(ExposureType entity)
        {
            return new ExposureTypeModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Abbreviation = entity.Abbreviation
            };
        }

        public override void ToEntity(ExposureTypeModel model, ExposureType entity)
        {
            entity.Name = model.Name;
            entity.Abbreviation = model.Abbreviation;
        }
    }
}
