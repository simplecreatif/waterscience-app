﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.SpeciesClassModels;

namespace TAG.Web.Models.Mappers.SpeciesClassMappers
{
    public class EditSpeciesClassMapper : BaseModelMapper<SpeciesClass, EditSpeciesClassModel>
    {
        private readonly IRepository repository;

        public EditSpeciesClassMapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override EditSpeciesClassModel ToModel(SpeciesClass entity)
        {
            throw new NotSupportedException();
        }

        public override void ToEntity(EditSpeciesClassModel model, SpeciesClass entity)
        {
            entity.Name = model.Name;
            entity.Phylum = repository.Get<SpeciesPhylum>(model.PhylumId);
        }
    }
}
