﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.SpeciesClassModels;

namespace TAG.Web.Models.Mappers.SpeciesClassMapper
{
    public class SpeciesClassMapper : BaseModelMapper<SpeciesClass, SpeciesClassModel>
    {
        public override SpeciesClassModel ToModel(SpeciesClass entity)
        {
            return new SpeciesClassModel
            {
                Id = entity.Id,
                Name = entity.Name,
                PhylumId = entity.Phylum.Id,
                PhylumName = entity.Phylum.Name
            };
        }

        public override void ToEntity(SpeciesClassModel model, SpeciesClass entity)
        {
            throw new NotSupportedException();
        }
    }
}
