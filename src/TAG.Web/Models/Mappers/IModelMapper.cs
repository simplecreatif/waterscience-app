﻿namespace TAG.Web.Models.Mappers
{
    public interface IModelMapper
    {
        object ToModel(object entity);
        object ToEntity(object model);
        void ToEntity(object model, object entity);
    }

    public interface IModelMapper<TEntity, TModel> : IModelMapper
    {
        TModel ToModel(TEntity entity);
        TEntity ToEntity(TModel model);
        void ToEntity(TModel model, TEntity entity);
    }
}
