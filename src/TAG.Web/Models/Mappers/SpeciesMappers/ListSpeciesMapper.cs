﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.SpeciesModels;

namespace TAG.Web.Models.Mappers.SpeciesMappers
{
    public class ListSpeciesMapper : BaseModelMapper<Species, ListSpeciesModel>
    {
        public override ListSpeciesModel ToModel(Species entity)
        {
            return new ListSpeciesModel
            {
                Id = entity.Id,
                ScientificName = entity.ScientificName,
                CommonName = entity.CommonName
            };
        }

        public override void ToEntity(ListSpeciesModel model, Species entity)
        {
            throw new NotSupportedException();
        }
    }
}
