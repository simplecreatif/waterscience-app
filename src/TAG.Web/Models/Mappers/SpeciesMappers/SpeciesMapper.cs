﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.SpeciesModels;

namespace TAG.Web.Models.Mappers.SpeciesMappers
{
    public class SpeciesMapper : BaseModelMapper<Species, SpeciesModel>
    {
        public override SpeciesModel ToModel(Species entity)
        {
            return new SpeciesModel
            {
                Id = entity.Id,
                ScientificName = entity.ScientificName,
                CommonName = entity.CommonName,
                Status = entity.Status,
                AnimalTypeId = entity.AnimalType?.Id,
                AnimalTypeName = entity.AnimalType?.Name,
                MajorGroup = entity.MajorGroup,
                MinorGroup = entity.MinorGroup,
                AnimalCategoryId = entity.AnimalCategory?.Id,
                AnimalCategoryName = entity.AnimalCategory?.Name,
                ClassId = entity.Class?.Id,
                ClassName = entity.Class?.Name,
                IsHeterotroph = entity.IsHeterotroph
            };
        }

        public override void ToEntity(SpeciesModel model, Species entity)
        {
            throw new NotSupportedException();
        }
    }
}
