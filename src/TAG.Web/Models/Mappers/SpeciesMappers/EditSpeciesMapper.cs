﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.SpeciesModels;

namespace TAG.Web.Models.Mappers.SpeciesMappers
{
    public class EditSpeciesMapper : BaseModelMapper<Species, EditSpeciesModel>
    {
        private readonly IRepository repository;

        public EditSpeciesMapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override EditSpeciesModel ToModel(Species entity)
        {
            throw new NotSupportedException();
        }

        public override void ToEntity(EditSpeciesModel model, Species entity)
        {
            entity.ScientificName = model.ScientificName;
            entity.CommonName = model.CommonName;
            entity.Status = model.Status;
            entity.AnimalType = repository.Get<AnimalType>(model.AnimalTypeId);
            entity.MajorGroup = model.MajorGroup;
            entity.MinorGroup = model.MinorGroup;
            entity.AnimalCategory = repository.Get<AnimalCategory>(model.AnimalCategoryId);
            entity.Class = repository.Get<SpeciesClass>(model.ClassId);
            entity.IsHeterotroph = model.IsHeterotroph;
        }
    }
}
