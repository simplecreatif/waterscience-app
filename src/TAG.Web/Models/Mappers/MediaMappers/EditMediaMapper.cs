﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.MediaModels;

namespace TAG.Web.Models.Mappers.MediaMappers
{
    public class EditMediaMapper : BaseModelMapper<Media, EditMediaModel>
    {
        private readonly IRepository repository;

        public EditMediaMapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override EditMediaModel ToModel(Media entity)
        {
            throw new NotSupportedException();
        }

        public override void ToEntity(EditMediaModel model, Media entity)
        {
            entity.Name = model.Name;
            entity.MediaType = repository.Get<MediaType>(model.MediaTypeId);
        }
    }
}
