﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.MediaModels;

namespace TAG.Web.Models.Mappers.MediaMappers
{
    public class MediaMapper : BaseModelMapper<Media, MediaModel>
    {
        private readonly IRepository repository;

        public MediaMapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override MediaModel ToModel(Media entity)
        {
            return new MediaModel
            {
                Id = entity.Id,
                Name = entity.Name,
                MediaTypeId = entity.MediaType?.Id,
                MediaTypeName = entity.MediaType?.Name
            };
        }

        public override void ToEntity(MediaModel model, Media entity)
        {
            entity.Name = model.Name;
            entity.MediaType = repository.Get<MediaType>(model.MediaTypeId);
        }
    }
}
