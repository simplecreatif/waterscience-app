﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.ToxicityValue2000Models;
using TAG.Web.Models.ToxicityValue2016Models;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Mappers.ToxicityValue2016Mappers
{
    public class ListToxicityValue2016Mapper : BaseModelMapper<ToxicityValue2016, ListToxicityValue2016Model>
    {
        public override ListToxicityValue2016Model ToModel(ToxicityValue2016 entity)
        {
            return new ListToxicityValue2016Model
            {
                Id = entity.Id,
                ChemicalName = entity.Chemical.Name,
                ChemicalId = entity.Chemical.Id,
                SpeciesId = entity.Species.Id,
                GuidelineGroupValue = entity.GuidelineGroup.GuidelineValue,
                GuidelineGroupId = entity.GuidelineGroup.Id,
                SpeciesScientificName = entity.Species?.ScientificName,
                Duration = entity.Duration?.Name,
                DurationUnit = entity.DurationUnit?.Name,
                Effect = entity.Effect?.Name,
                Endpoint = entity.Endpoint?.Name,
                Concentration = entity.Concentration,
                ConcentrationUnit = entity.ConcentrationUnit?.Name
            };
        }

        public override void ToEntity(ListToxicityValue2016Model model, ToxicityValue2016 entity)
        {
            throw new NotSupportedException();
        }
    }
}
