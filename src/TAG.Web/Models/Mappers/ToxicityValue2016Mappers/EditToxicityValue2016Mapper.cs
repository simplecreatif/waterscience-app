﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.ToxicityValue2000Models;
using TAG.Web.Models.ToxicityValue2016Models;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Mappers.ToxicityValue2016Mappers
{
    public class EditToxicityValue2016Mapper : BaseModelMapper<ToxicityValue2016, EditToxicityValue2016Model>
    {
        private readonly IRepository repository;

        public EditToxicityValue2016Mapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override EditToxicityValue2016Model ToModel(ToxicityValue2016 entity)
        {
            throw new NotSupportedException();
        }

        public override void ToEntity(EditToxicityValue2016Model model, ToxicityValue2016 entity)
        {
            entity.Chemical = repository.Get<Chemical>(model.ChemicalId);
            entity.Species = repository.Get<Species>(model.SpeciesId);
            entity.Duration = repository.Get<Duration>(model.DurationId);
            entity.DurationUnit = repository.Get<DurationUnit>(model.DurationUnitId);
            entity.DurationHour = repository.Get<Duration>(model.DurationHourId);
            entity.Effect = repository.Get<Effect>(model.EffectId);
            entity.Endpoint = repository.Get<Endpoint>(model.EndpointId);
            entity.Concentration = model.Concentration;
            entity.ConcentrationUnit = repository.Get<ConcentrationUnit>(model.ConcentrationUnitId);

            entity.Record = model.Record;
            entity.DataSource = model.DataSource;
            entity.IsChronic = model.IsChronic;
            entity.ConcentrationUsed = model.ConcentrationUsed.Value;

            entity.MediaType = repository.Get<MediaType>(model.MediaTypeId);
            entity.Age = repository.Get<Age>(model.AgeId);
            entity.EndpointFromPaper = repository.Get<Endpoint>(model.EndpointFromPaperId);
            entity.EndpointMeasurement = repository.Get<Endpoint>(model.EndpointMeasurementId);
            entity.GuidelineGroup = repository.Get<GuidelineGroup>(model.GuidelineGroupId); // todo: Delete old guideline group if empty.
        }
    }
}
