﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.ToxicityValue2000Models;
using TAG.Web.Models.ToxicityValue2016Models;

namespace TAG.Web.Models.Mappers.ToxicityValue2016Mappers
{
    public class ToxicityValue2016Mapper : BaseModelMapper<ToxicityValue2016, ToxicityValue2016Model>
    {
        public override ToxicityValue2016Model ToModel(ToxicityValue2016 entity)
        {
            return new ToxicityValue2016Model
            {
                Id = entity.Id,
                ChemicalName = entity.Chemical?.Name,
                SpeciesScientificName = entity.Species?.ScientificName,
                Duration = entity.Duration?.Name,
                DurationUnit = entity.DurationUnit?.Name,
                DurationHour = entity.DurationHour?.Name,
                Effect = entity.Effect?.Name,
                Endpoint = entity.Endpoint?.Name,
                Concentration = entity.Concentration,
                ConcentrationUnit = entity.ConcentrationUnit?.Name,

                Record = entity.Record,
                DataSource = entity.DataSource,
                MediaType = entity.MediaType?.Name,
                Age = entity.Age?.Name,
                EndpointFromPaper = entity.EndpointFromPaper?.Name,
                EndpointMeasurement = entity.EndpointMeasurement?.Name,
                IsChronic = entity.IsChronic,
                ConcentrationUsed = entity.ConcentrationUsed,

                ChemicalId = entity.Chemical?.Id,
                SpeciesId = entity.Species?.Id,
                DurationId = entity.Duration?.Id,
                DurationUnitId = entity.DurationUnit?.Id,
                DurationHourId = entity.DurationHour?.Id,
                EffectId = entity.Effect?.Id,
                EndpointId = entity.Endpoint?.Id,
                ConcentrationUnitId = entity.ConcentrationUnit?.Id,
                MediaTypeId = entity.MediaType?.Id,
                AgeId = entity.Age?.Id,
                EndpointFromPaperId = entity.EndpointFromPaper?.Id,
                EndpointMeasurementId = entity.EndpointMeasurement?.Id,
                GuidelineGroupId = entity.GuidelineGroup?.Id
            };
        }

        public override void ToEntity(ToxicityValue2016Model model, ToxicityValue2016 entity)
        {
            throw new NotSupportedException();
        }
    }
}
