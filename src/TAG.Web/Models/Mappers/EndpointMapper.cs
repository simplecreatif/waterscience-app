﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Web.Models.Mappers
{
    public class EndpointMapper : BaseModelMapper<Endpoint, EndpointModel>
    {
        public override EndpointModel ToModel(Endpoint entity)
        {
            return new EndpointModel
            {
                Id = entity.Id,
                Endpoint = entity.Name,
                Abbreviation = entity.Abbreviation
            };
        }

        public override void ToEntity(EndpointModel model, Endpoint entity)
        {
            entity.Name = model.Endpoint;
            entity.Abbreviation = model.Abbreviation;
        }
    }
}
