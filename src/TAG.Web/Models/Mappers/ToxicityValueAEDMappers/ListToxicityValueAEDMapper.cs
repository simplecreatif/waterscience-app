﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Mappers.ToxicityValueAEDMappers
{
    public class ListToxicityValueAEDMapper : BaseModelMapper<ToxicityValueAED, ListToxicityValueAEDModel>
    {
        public override ListToxicityValueAEDModel ToModel(ToxicityValueAED entity)
        {
            return new ListToxicityValueAEDModel
            {
                Id = entity.Id,
                Identifier = entity.Identifier,
                ChemicalName = entity.Chemical?.Name,
                SpeciesScientificName = entity.Species?.ScientificName,
                Duration = entity.Duration?.Name,
                DurationUnit = entity.DurationUnit?.Name,
                Effect = entity.Effect?.Name,
                Endpoint = entity.Endpoint?.Name,
                Concentration = entity.Concentration,
                ConcentrationUnit = entity.ConcentrationUnit?.Name
            };
        }

        public override void ToEntity(ListToxicityValueAEDModel model, ToxicityValueAED entity)
        {
            throw new NotSupportedException();
        }
    }
}
