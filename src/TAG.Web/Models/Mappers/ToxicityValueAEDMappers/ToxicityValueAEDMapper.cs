﻿using System;
using TAG.Domain.Models;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Mappers.ToxicityValueAEDMappers
{
    public class ToxicityValueAEDMapper : BaseModelMapper<ToxicityValueAED, ToxicityValueAEDModel>
    {
        public override ToxicityValueAEDModel ToModel(ToxicityValueAED entity)
        {
            return new ToxicityValueAEDModel
            {
                Id = entity.Id,
                ChemicalName = entity.Chemical?.Name,
                SpeciesScientificName = entity.Species?.ScientificName,
                Duration = entity.Duration?.Name,
                DurationUnit = entity.DurationUnit?.Name,
                DurationHour = entity.DurationHour?.Name,
                Effect = entity.Effect?.Name,
                Endpoint = entity.Endpoint?.Name,
                Concentration = entity.Concentration,
                ConcentrationUnit = entity.ConcentrationUnit?.Name,

                Identifier = entity.Identifier,
                ChemicalGrade = entity.ChemicalGrade?.Name,
                ChemicalGradeValue = entity.ChemicalGradeValue,
                ChemicalForm = entity.ChemicalForm?.Name,
                IsSolventUsed = entity.IsSolventUsed,
                SolventName = entity.SolventName,
                SolventConcentration = entity.SolventConcentration,
                SolventUnit = entity.SolventUnit?.Name,
                SolventControl = entity.SolventControl,
                SpeciesStrain = entity.SpeciesStrain,
                SpeciesAgeOrStage = entity.SpeciesAgeOrStage,
                Age = entity.Age?.Name,
                Feeding = entity.Feeding,
                Media = entity.Media?.Name,
                TestForm = entity.TestForm?.Name,
                ExposureType = entity.ExposureType?.Name,
                LowerConfidenceLimit95 = entity.LowerConfidenceLimit95,
                UpperConfidenceLimit95 = entity.UpperConfidenceLimit95,
                ErrorMeasure = entity.ErrorMeasure?.Name,
                ErrorValue = entity.ErrorValue,
                DoseResponse = entity.DoseResponse,
                Statistic = entity.Statistic?.Name,
                SignificanceLevel = entity.SignificanceLevel,
                Power = entity.Power,
                Replication = entity.Replication?.Name,
                TemporalReplication = entity.TemporalReplication?.Name,
                HasRefTox = entity.HasRefTox,
                RefToxName = entity.RefToxName,
                HasControlEffect = entity.HasControlEffect,
                ControlEffect = entity.ControlEffect,
                InvalidationCriteria = entity.InvalidationCriteria,
                LossOfChemical = entity.LossOfChemical,
                ChemicalFormUsed = entity.ChemicalFormUsed,
                ConcentrationType = entity.ConcentrationType?.Name,
                IsPhysicochemMeasured = entity.IsPhysicochemMeasured,
                Temperature = entity.Temperature,
                PH = entity.PH,
                Conductivity = entity.Conductivity,
                Hardness = entity.Hardness,
                Salinity = entity.Salinity,
                Alkalinity = entity.Alkalinity,
                OrganicCarbon = entity.OrganicCarbon,
                LightRegime = entity.LightRegime,
                OtherParameters = entity.OtherParameters,
                ReferenceTitle = entity.Reference?.Title,
                MethodReference = entity.MethodReference,
                Country = entity.Country?.Name,
                SiteLocation = entity.SiteLocation,
                HardCopy = entity.HardCopy,
                PublicationLanguage = entity.PublicationLanguage?.Name,
                Comments = entity.Comments,
                QualityScore = entity.QualityScore,

                ChemicalId = entity.Chemical?.Id,
                SpeciesId = entity.Species?.Id,
                DurationId = entity.Duration?.Id,
                DurationUnitId = entity.DurationUnit?.Id,
                DurationHourId = entity.DurationHour?.Id,
                EffectId = entity.Effect?.Id,
                EndpointId = entity.Endpoint?.Id,
                ConcentrationUnitId = entity.ConcentrationUnit?.Id,
                ChemicalGradeId = entity.ConcentrationUnit?.Id,
                ChemicalFormId = entity.ChemicalForm?.Id,
                SolventUnitId = entity.SolventUnit?.Id,
                AgeId = entity.Age?.Id,
                MediaId = entity.Media?.Id,
                TestFormId = entity.TestForm?.Id,
                ExposureTypeId = entity.ExposureType?.Id,
                ErrorMeasureId = entity.ErrorMeasure?.Id,
                StatisticId = entity.Statistic?.Id,
                ReplicationId = entity.Replication?.Id,
                TemporalReplicationId = entity.TemporalReplication?.Id,
                ConcentrationTypeId = entity.ConcentrationType?.Id,
                ReferenceId = entity.Reference?.Id,
                CountryId = entity.Country?.Id,
                PublicationLanguageId = entity.PublicationLanguage?.Id
            };
        }

        public override void ToEntity(ToxicityValueAEDModel model, ToxicityValueAED entity)
        {
            throw new NotSupportedException();
        }
    }
}
