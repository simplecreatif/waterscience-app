﻿using System;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;
using TAG.Web.Models.SpeciesModels;
using TAG.Web.Models.ToxicityValueAEDModels;

namespace TAG.Web.Models.Mappers.SpeciesMappers
{
    public class EditToxicityValueAEDMapper : BaseModelMapper<ToxicityValueAED, EditToxicityValueAEDModel>
    {
        private readonly IRepository repository;

        public EditToxicityValueAEDMapper(IRepository repository)
        {
            this.repository = repository;
        }

        public override EditToxicityValueAEDModel ToModel(ToxicityValueAED entity)
        {
            throw new NotSupportedException();
        }

        public override void ToEntity(EditToxicityValueAEDModel model, ToxicityValueAED entity)
        {
            entity.Chemical = repository.Get<Chemical>(model.ChemicalId);
            entity.Species = repository.Get<Species>(model.SpeciesId);
            entity.Duration = repository.Get<Duration>(model.DurationId);
            entity.DurationUnit = repository.Get<DurationUnit>(model.DurationUnitId);
            entity.DurationHour = repository.Get<Duration>(model.DurationHourId);
            entity.Effect = repository.Get<Effect>(model.EffectId);
            entity.Endpoint = repository.Get<Endpoint>(model.EndpointId);
            entity.Concentration = model.Concentration;
            entity.ConcentrationUnit = repository.Get<ConcentrationUnit>(model.ConcentrationUnitId);

            entity.Identifier = model.Identifier;
            entity.ChemicalGrade = repository.Get<ChemicalGrade>(model.ChemicalGradeId);
            entity.ChemicalGradeValue = model.ChemicalGradeValue;
            entity.ChemicalForm = repository.Get<ChemicalForm>(model.ChemicalFormId);
            entity.IsSolventUsed = model.IsSolventUsed;
            entity.SolventName = model.SolventName;
            entity.SolventConcentration = model.SolventConcentration;
            entity.SolventUnit = repository.Get<ConcentrationUnit>(model.SolventUnitId);
            entity.SolventControl = model.SolventControl;
            entity.SpeciesStrain = model.SpeciesStrain;
            entity.SpeciesAgeOrStage = model.SpeciesAgeOrStage;
            entity.Age = repository.Get<Age>(model.AgeId);
            entity.Feeding = model.Feeding;
            entity.Media = repository.Get<Media>(model.MediaId);
            entity.TestForm = repository.Get<TestForm>(model.TestFormId);
            entity.ExposureType = repository.Get<ExposureType>(model.ExposureTypeId);
            entity.LowerConfidenceLimit95 = model.LowerConfidenceLimit95;
            entity.UpperConfidenceLimit95 = model.UpperConfidenceLimit95;
            entity.ErrorMeasure = repository.Get<Error>(model.ErrorMeasureId);
            entity.ErrorValue = model.ErrorValue;
            entity.DoseResponse = model.DoseResponse;
            entity.Statistic = repository.Get<Statistic>(model.StatisticId);
            entity.SignificanceLevel = model.SignificanceLevel;
            entity.Power = model.Power;
            entity.Replication = repository.Get<Replication>(model.ReplicationId);
            entity.TemporalReplication = repository.Get<TemporalReplication>(model.TemporalReplicationId);
            entity.HasRefTox = model.HasRefTox;
            entity.RefToxName = model.RefToxName;
            entity.HasControlEffect = model.HasControlEffect;
            entity.ControlEffect = model.ControlEffect;
            entity.InvalidationCriteria = model.InvalidationCriteria;
            entity.LossOfChemical = model.LossOfChemical;
            entity.ChemicalFormUsed = model.ChemicalFormUsed;
            entity.ConcentrationType = repository.Get<ConcentrationType>(model.ConcentrationTypeId);
            entity.IsPhysicochemMeasured = model.IsPhysicochemMeasured;
            entity.Temperature = model.Temperature;
            entity.PH = model.PH;
            entity.Conductivity = model.Conductivity;
            entity.Hardness = model.Hardness;
            entity.Salinity = model.Salinity;
            entity.Alkalinity = model.Alkalinity;
            entity.OrganicCarbon = model.OrganicCarbon;
            entity.LightRegime = model.LightRegime;
            entity.OtherParameters = model.OtherParameters;
            entity.Reference = repository.Get<Reference>(model.ReferenceId);
            entity.MethodReference = model.MethodReference;
            entity.Country = repository.Get<Country>(model.CountryId);
            entity.SiteLocation = model.SiteLocation;
            entity.HardCopy = model.HardCopy;
            entity.PublicationLanguage = repository.Get<PublicationLanguage>(model.PublicationLanguageId);
            entity.Comments = model.Comments;
            entity.QualityScore = model.QualityScore;
        }
    }
}
