﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Web.Models
{
    public class ChemicalTypeModel : IIdentifiable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
    }
}
