﻿using TAG.Domain.Models;

namespace TAG.Web.Models.MediaModels
{
    public class EditMediaModel : IIdentifiable
    {
        public int Id { get; set; }
        public int? MediaTypeId { get; set; }
        public string Name { get; set; }
    }
}
