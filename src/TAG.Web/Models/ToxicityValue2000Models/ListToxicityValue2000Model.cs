﻿using TAG.Domain.Models;

namespace TAG.Web.Models.ToxicityValue2000Models
{
    public class ListToxicityValue2000Model : IIdentifiable
    {
        public int Id { get; set; }
        public string ChemicalName { get; set; }
        public string SpeciesScientificName { get; set; }
        public string Duration { get; set; }
        public string DurationUnit { get; set; }
        public string Effect { get; set; }
        public string Endpoint { get; set; }
        public string Concentration { get; set; }
        public string ConcentrationUnit { get; set; }
    }
}
