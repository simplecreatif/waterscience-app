﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Web.Models.ToxicityValue2000Models
{
    public class EditToxicityValue2000Model : IIdentifiable
    {
        public int Id { get; set; }
        public int? ChemicalId { get; set; }
        public int? SpeciesId { get; set; }
        public int? DurationId { get; set; }
        public int? DurationUnitId { get; set; }
        public int? DurationHourId { get; set; }
        public int? EffectId { get; set; }
        public int? EndpointId { get; set; }
        public string Concentration { get; set; }
        public int? ConcentrationUnitId { get; set; }

        public decimal? ConcentrationUsed { get; set; }
        public decimal? ConvNOEC { get; set; }
        public decimal? HardnessCorrectedConc { get; set; }
        public string Temperature { get; set; }
        public string PH { get; set; }
        public string Hardness { get; set; }
        public string Salinity { get; set; }
        public decimal? TotalAmmoniaMg { get; set; }
        public decimal? TotalAmmoniaPh8 { get; set; }

        public int? MediaTypeId { get; set; }
        public int? TestTypeId { get; set; }
        public int? EffectUsedId { get; set; }
        public int? ExposureTypeId { get; set; }
        public int? ConcentrationCodeId { get; set; }
        public int? ConcentrationTypeId { get; set; }
        public int? ReferenceId { get; set; }
        public int? StatusId { get; set; }
    }
}
