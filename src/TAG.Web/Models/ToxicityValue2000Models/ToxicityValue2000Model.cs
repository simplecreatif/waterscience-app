﻿using TAG.Domain.Models;

namespace TAG.Web.Models.ToxicityValue2000Models
{
    public class ToxicityValue2000Model : IIdentifiable
    {
        public int Id { get; set; }
        public string ChemicalName { get; set; }
        public string SpeciesScientificName { get; set; }
        public string Duration { get; set; }
        public string DurationUnit { get; set; }
        public string DurationHour { get; set; }
        public string Effect { get; set; }
        public string Endpoint { get; set; }
        public string Concentration { get; set; }
        public string ConcentrationUnit { get; set; }

        public string MediaType { get; set; }
        public string TestType { get; set; }
        public string EffectUsed { get; set; }
        public string ExposureType { get; set; }
        public decimal? ConcentrationUsed { get; set; }
        public string ConcentrationCode { get; set; }
        public decimal? ConvNOEC { get; set; }
        public decimal? HardnessCorrectedConc { get; set; }
        public string ConcentrationType { get; set; }
        public string Temperature { get; set; }
        public string PH { get; set; }
        public string Hardness { get; set; }
        public string Salinity { get; set; }
        public string ReferenceTitle { get; set; }
        public string Status { get; set; }
        public decimal? TotalAmmoniaMg { get; set; }
        public decimal? TotalAmmoniaPh8 { get; set; }

        public int? ChemicalId { get; set; }
        public int? SpeciesId { get; set; }
        public int? DurationId { get; set; }
        public int? DurationUnitId { get; set; }
        public int? DurationHourId { get; set; }
        public int? EffectId { get; set; }
        public int? EndpointId { get; set; }
        public int? ConcentrationUnitId { get; set; }
        public int? MediaTypeId { get; set; }
        public int? TestTypeId { get; set; }
        public int? EffectUsedId { get; set; }
        public int? ExposureTypeId { get; set; }
        public int? ConcentrationCodeId { get; set; }
        public int? ConcentrationTypeId { get; set; }
        public int? ReferenceId { get; set; }
        public int? StatusId { get; set; }
    }
}
