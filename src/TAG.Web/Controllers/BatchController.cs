﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models;
using TAG.Web.Models.Mappers;
using TAG.Web.Models.Validators;
using NHibernate.Linq;
using TAG.Domain.Services.Search;
using TAG.Web.Infrastructure.Controllers;
using TAG.Web.Models.SearchModels;
using TAG.Web.Services;

namespace TAG.Web.Controllers
{
    /// <summary>
    /// Supports listing multiple entity types in one query.
    /// Purely for performance improvements. By requesting
    /// several sets of entities in one request the overhead
    /// and number of concurrent HTTP requests are reduced.
    /// This can dramatically reduce load time if the number
    /// of requested entity types are greater than the number
    /// of allowed concurrent HTTP requests by the user's
    /// browser.
    /// </summary>
    [Route("[controller]")]
    public class BatchController : Controller
    {
        private readonly IScaffoldService scaffoldService;

        public BatchController(IScaffoldService scaffoldService)
        {
            this.scaffoldService = scaffoldService;
        }

        [HttpGet]
        [Route("{entityTypes}")]
        public IActionResult Get(string entityTypes)
        {
            if (string.IsNullOrWhiteSpace(entityTypes))
            {
                return NotFound();
            }
            
            // This ugly code takes each entity type,
            // loads their cached JSON list model, then
            // builds up a simple JSON object keyed by
            // each entity type. This is done to avoid
            // the JSON encoding overhead if we serialized
            // objects to JSON every request.
            var types = entityTypes.Split(',');
            var sb = new StringBuilder();
            sb.Append("{");
            for (int i = 0; i < types.Length; i++)
            {
                var type = types[i];
                var jsonModel = scaffoldService.GetJson(HttpContext.User, type);
                if (jsonModel != null)
                {
                    sb.Append($"\"{type}\":");
                    sb.Append(jsonModel);
                    if (i < types.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
            }
            
            sb.Append("}");
            return new JsonStringResult(sb.ToString());
        }
    }
}
