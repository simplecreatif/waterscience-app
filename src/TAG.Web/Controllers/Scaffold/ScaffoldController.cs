﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using TAG.Domain.Models;
using TAG.Domain.Specifications;
using TAG.Persistence;
using TAG.Web.Models.Mappers;
using TAG.Web.Extensions;
using TAG.Web.Infrastructure.Controllers;
using TAG.Web.Infrastructure.Realtime;
using TAG.Web.Infrastructure.Scaffolding;
using TAG.Web.Models;
using TAG.Web.Models.Validators;
using TAG.Web.Models.Validators.LookupValidators;
using TAG.Web.Services;

namespace TAG.Web.Controllers.Scaffold
{
    /// <summary>
    /// Scaffold controller handles requests which don't map
    /// to any other controller. This allows us to scaffold a REST
    /// api for most of the entities which don't have any special
    /// requirements.
    /// </summary>
    [Route("{entityType}")]
    public class ScaffoldController : Controller
    {
        private readonly IScaffoldConfigurations scaffoldConfigurations;
        private readonly IScaffoldService scaffoldService;

        /// <summary>
        /// We need Autofac's lifetime scope because we use it to resolve
        /// some per-request items which are specific to the entity being
        /// scaffolded. This includes validators and mappers.
        /// </summary>
        private readonly ILifetimeScope scope;

        public ScaffoldController(IScaffoldConfigurations scaffoldConfigurations, IScaffoldService scaffoldService, ILifetimeScope scope)
        {
            this.scaffoldConfigurations = scaffoldConfigurations;
            this.scaffoldService = scaffoldService;
            this.scope = scope;
        }

        [HttpGet]
        public IActionResult Get(string entityType)
        {
            string jsonModels = scaffoldService.GetJson(HttpContext.User, entityType);

            if (jsonModels == null)
            {
                return NotFound();
            }

            return new JsonStringResult(jsonModels);
        }

        [HttpGet("{id}")]
        public IActionResult Get(string entityType, int id)
        {
            var config = scaffoldConfigurations.TryGet(entityType);
            if (config == null || !config.CanShow)
            {
                return NotFound();
            }

            var repository = scope.Resolve<IRepository>();
            var entity = repository.Get<object>(id, config.EntityType);
            if (entity == null)
            {
                return NotFound();
            }

            if (!config.IsAuthorized(scope, x => x.CanShow(HttpContext.User, entity)))
            {
                return Forbid();
            }

            var mapper = (IModelMapper)scope.Resolve(config.ShowMapperType);
            var model = mapper.ToModel(entity);
            return Ok(model);
        }

        [HttpPost]
        public IActionResult Post(string entityType, [FromBody] JObject raw)
        {
            var config = scaffoldConfigurations.TryGet(entityType);
            if (config == null || !config.CanEdit)
            {
                return NotFound();
            }

            if (!config.IsAuthorized(scope, x => x.CanCreate(HttpContext.User)))
            {
                return Forbid();
            }

            // When a model is submitted to the ScaffoldController for create
            // or update, we can't deserialize it as a strongly-typed model
            // because the controller actions need to handle many different
            // types of model. Instead, we drop down a layer of abstraction
            // and retrieve the model as a Newtonsoft JObject. ASP.NET Core
            // provides this option as inbuilt functionality. We then use
            // Newtonsoft's inbuilt functionality to convert the JObject to
            // the desired model type
            var model = raw.ToObject(config.EditModelType);
            if (model == null)
            {
                return this.ValidationFailure("The entity is required");
            }

            var validationResult = ValidateEdit(config, model);
            if (validationResult != null)
            {
                return this.ValidationFailure(validationResult);
            }

            // We explicitly create the entity here rather than
            // allowing it to happen in the mapper because lookup
            // types need to be instantiated as their actual types
            // rather than the base Lookup type or nHibernate will
            // not know how to persist it.
            var repository = scope.Resolve<IRepository>();
            var entity = Activator.CreateInstance(config.EntityType);
            var mapper = (IModelMapper)scope.Resolve(config.EditMapperType);
            mapper.ToEntity(model, entity);
            repository.Add(entity);
            return this.EditResult(scope, config, entity);
        }

        [HttpPut("{id}")]
        public IActionResult Put(string entityType, int id, [FromBody] JObject raw)
        {
            var config = scaffoldConfigurations.TryGet(entityType);
            if (config == null || !config.CanEdit)
            {
                return NotFound();
            }

            var repository = scope.Resolve<IRepository>();
            var entity = repository.Get<object>(id, config.EntityType);

            if (!config.IsAuthorized(scope, x => x.CanUpdate(HttpContext.User, entity)))
            {
                return Forbid();
            }

            var model = raw.ToObject(config.EditModelType);
            if (model == null)
            {
                return this.ValidationFailure("The entity is required");
            }

            var validationResult = ValidateEdit(config, model);
            if (validationResult != null)
            {
                return this.ValidationFailure(validationResult);
            }

            var mapper = (IModelMapper)scope.Resolve(config.EditMapperType);
            mapper.ToEntity(model, entity);

            return this.EditResult(scope, config, entity);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string entityType, int id)
        {
            var config = scaffoldConfigurations.TryGet(entityType);
            if (config == null || !config.CanDelete)
            {
                return NotFound();
            }

            var repository = scope.Resolve<IRepository>();
            var entity = repository.Get<IIdentifiable>(id, config.EntityType);
            if (entity == null)
            {
                return NotFound();
            }

            if (!config.IsAuthorized(scope, x => x.CanDelete(HttpContext.User, entity)))
            {
                return Forbid();
            }

            var validationResult = ValidateDelete(config, entity);
            if (validationResult != null)
            {
                return this.ValidationFailure(validationResult);
            }

            repository.Remove(entity);

            return this.DeleteResult(scope, config, entity.Id);
        }

        private ValidationResult ValidateEdit(ScaffoldConfiguration config, object model)
        {
            var validator = scope.Resolve(config.EditValidatorType);
            ValidationResult validationResult;

            if (validator is EditLookupValidator)
            {
                // Special case for lookup validators - we need to pass the entity type
                // in so the repository knows which lookups to search for when testing
                // for duplicates.
                validationResult = ((EditLookupValidator)validator).Validate(model, config.EntityType);
            }
            else
            {
                validationResult = ((IValidator)validator).Validate(model);
            }

            if (validationResult.IsValid)
            {
                return null;
            }

            return validationResult;
        }

        private ValidationResult ValidateDelete(ScaffoldConfiguration config, IIdentifiable entity)
        {
            var validator = (IDeleteEntityValidator)scope.Resolve(config.DeleteValidatorType);
            var validationResult = validator.Validate(entity);

            if (validationResult.IsValid)
            {
                return null;
            }

            return validationResult;
        }
    }
}
