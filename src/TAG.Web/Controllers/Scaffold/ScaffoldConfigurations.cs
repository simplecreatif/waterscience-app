﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Domain.Specifications;
using TAG.Web.Authorization;
using TAG.Web.Extensions;
using TAG.Web.Infrastructure.Scaffolding;
using TAG.Web.Models;
using TAG.Web.Models.ChemicalModels;
using TAG.Web.Models.Mappers;
using TAG.Web.Models.Mappers.ChemicalMappers;
using TAG.Web.Models.Mappers.MediaMappers;
using TAG.Web.Models.Mappers.SpeciesClassMapper;
using TAG.Web.Models.Mappers.SpeciesClassMappers;
using TAG.Web.Models.Mappers.SpeciesMappers;
using TAG.Web.Models.Mappers.ToxicityValue2000Mappers;
using TAG.Web.Models.Mappers.ToxicityValue2016Mappers;
using TAG.Web.Models.Mappers.ToxicityValueAEDMappers;
using TAG.Web.Models.Mappers.UserMappers;
using TAG.Web.Models.MediaModels;
using TAG.Web.Models.SpeciesClassModels;
using TAG.Web.Models.SpeciesModels;
using TAG.Web.Models.ToxicityValue2000Models;
using TAG.Web.Models.ToxicityValue2016Models;
using TAG.Web.Models.ToxicityValueAEDModels;
using TAG.Web.Models.UserModels;
using TAG.Web.Models.Validators;
using TAG.Web.Models.Validators.ChemicalReferenceValidators;
using TAG.Web.Models.Validators.ChemicalTypeValidators;
using TAG.Web.Models.Validators.ChemicalValidators;
using TAG.Web.Models.Validators.EndpointValidator;
using TAG.Web.Models.Validators.ExposureTypeValidators;
using TAG.Web.Models.Validators.LookupValidators;
using TAG.Web.Models.Validators.MediaValidators;
using TAG.Web.Models.Validators.ReferenceValidators;
using TAG.Web.Models.Validators.SpeciesClassValidators;
using TAG.Web.Models.Validators.SpeciesValidators;
using TAG.Web.Models.Validators.ToxicityValue2000Validators;
using TAG.Web.Models.Validators.ToxicityValue2016Validators;
using TAG.Web.Models.Validators.ToxicityValueAEDValidators;
using TAG.Web.Models.Validators.UserValidators;
using TAG.Web.Models.Validators.SearchConfigurationValidators;

namespace TAG.Web.Controllers.Scaffold
{
    /// <summary>
    /// Container for all scaffold configurations. This class is
    /// used by ScaffoldController to retrieve all required information
    /// about an entity type to perform CRUD operations on that entity
    /// type.
    /// </summary>
    public class ScaffoldConfigurations : BaseScaffoldConfigurations, IScaffoldConfigurations
    {
        public ScaffoldConfigurations()
        {
            ScaffoldConfiguration.DefaultAuthorizationType = typeof(DefaultAuthorizationRules);

            Add<Chemical>()
                .WithList<ListChemicalModel, ListChemicalMapper, ListChemicalSpecification>()
                .WithShow<ChemicalModel, ChemicalMapper>()
                .WithEdit<EditChemicalModel, EditChemicalMapper, EditChemicalValidator>()
                .WithDelete<DeleteChemicalValidator>();

            Add<ChemicalReference>()
                .WithList<ChemicalReferenceModel, ChemicalReferenceMapper, ListChemicalReferenceSpecification>()
                .WithShow<ChemicalReferenceModel, ChemicalReferenceMapper>()
                .WithEdit<ChemicalReferenceModel, ChemicalReferenceMapper, EditChemicalReferenceValidator>()
                .WithDelete<DeleteChemicalReferenceValidator>();

            Add<ChemicalType>()
                .WithList<ChemicalTypeModel, ChemicalTypeMapper, ListChemicalTypeSpecification>()
                .WithShow<ChemicalTypeModel, ChemicalTypeMapper>()
                .WithEdit<ChemicalTypeModel, ChemicalTypeMapper, EditChemicalTypeValidator>()
                .WithDelete<DeleteChemicalTypeValidator>();

            Add<SpeciesClass>()
                .WithList<SpeciesClassModel, SpeciesClassMapper, ListSpeciesClassSpecification>()
                .WithShow<SpeciesClassModel, SpeciesClassMapper>()
                .WithEdit<EditSpeciesClassModel, EditSpeciesClassMapper, EditSpeciesClassValidator>()
                .WithDelete<DeleteSpeciesClassValidator>();

            Add<Endpoint>()
                .WithList<EndpointModel, EndpointMapper, ListEndpointSpecification>()
                .WithShow<EndpointModel, EndpointMapper>()
                .WithEdit<EndpointModel, EndpointMapper, EditEndpointValidator>()
                .WithDelete<DeleteEndpointValidator>();

            Add<Media>()
                .WithList<MediaModel, MediaMapper, ListMediaSpecification>()
                .WithShow<MediaModel, MediaMapper>()
                .WithEdit<EditMediaModel, EditMediaMapper, EditMediaValidator>()
                .WithDelete<DeleteMediaValidator>();

            Add<ExposureType>()
                .WithList<ExposureTypeModel, ExposureTypeMapper, ListExposureTypeSpecification>()
                .WithShow<ExposureTypeModel, ExposureTypeMapper>()
                .WithEdit<ExposureTypeModel, ExposureTypeMapper, EditExposureTypeValidator>()
                .WithDelete<DeleteExposureTypeValidator>();

            Add<Reference>()
                .WithList<ReferenceModel, ReferenceMapper, ListReferenceSpecification>()
                .WithShow<ReferenceModel, ReferenceMapper>()
                .WithEdit<ReferenceModel, ReferenceMapper, EditReferenceValidator>()
                .WithDelete<DeleteReferenceValidator>();

            Add<Species>()
                .WithList<ListSpeciesModel, ListSpeciesMapper, ListSpeciesSpecification>()
                .WithShow<SpeciesModel, SpeciesMapper>()
                .WithEdit<EditSpeciesModel, EditSpeciesMapper, EditSpeciesValidator>()
                .WithDelete<DeleteSpeciesValidator>();

            Add<ToxicityValueAED>()
                .WithList<ListToxicityValueAEDModel, ListToxicityValueAEDMapper, ListToxicityValueAEDSpecification>()
                .WithShow<ToxicityValueAEDModel, ToxicityValueAEDMapper>()
                .WithEdit<EditToxicityValueAEDModel, EditToxicityValueAEDMapper, EditToxicityValueAEDValidator>()
                .WithDelete<DeleteToxicityValueAEDValidator>();

            Add<ToxicityValue2000>()
                .WithList<ListToxicityValue2000Model, ListToxicityValue2000Mapper, ListToxicityValue2000Specification>()
                .WithShow<ToxicityValue2000Model, ToxicityValue2000Mapper>()
                .WithEdit<EditToxicityValue2000Model, EditToxicityValue2000Mapper, EditToxicityValue2000Validator>()
                .WithDelete<DeleteToxicityValue2000Validator>();

            Add<ToxicityValue2016>()
                .WithList<ListToxicityValue2016Model, ListToxicityValue2016Mapper, ListToxicityValue2016Specification>()
                .WithShow<ToxicityValue2016Model, ToxicityValue2016Mapper>()
                .WithEdit<EditToxicityValue2016Model, EditToxicityValue2016Mapper, EditToxicityValue2016Validator>()
                .WithDelete<DeleteToxicityValue2016Validator>();

            Add<User>()
                .WithList<ListUserModel, ListUserMapper, ListUserSpecification>()
                .WithShow<UserModel, UserMapper>()
                .WithEdit<EditUserModel, EditUserMapper, EditUserValidator>()
                .WithDelete<DeleteUserValidator>()
                .WithAuthorization<UserAuthorizationRules>();

            Add<SearchConfiguration>()
                .WithList<SearchConfigurationModel, SearchConfigurationMapper, ListSearchConfigurationForCurrentUserSpecification>()
                .WithShow<SearchConfigurationModel, SearchConfigurationMapper>()
                .WithEdit<SearchConfigurationModel, SearchConfigurationMapper, EditSearchConfigurationValidator>()
                .WithDelete<DeleteSearchConfigurationValidator>()
                .WithAuthorization<SearchConfigurationAuthorizationRules>();

            AddAllDerivedFrom<Lookup>(x => x
                .WithList<LookupModel, LookupMapper, ListLookupSpecification>()
                .WithShow<LookupModel, LookupMapper>()
                .WithEdit<LookupModel, LookupMapper, EditLookupValidator>()
                .WithDelete<DeleteLookupValidator>());
        }
    }
}
