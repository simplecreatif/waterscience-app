﻿using System;
using System.Linq;
using Autofac;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using TAG.Persistence;
using TAG.Web.Models.Mappers;
using TAG.Web.Extensions;
using TAG.Web.Infrastructure.Scaffolding;
using TAG.Web.Models.Validators;

namespace TAG.Web.Controllers.Scaffold
{
    /// <summary>
    /// Provides metadata to client about scaffolded entities
    /// so that appropriate pages can be generated on the client
    /// to interact with the entities.
    /// </summary>
    [Route("[controller]")]
    public class ScaffoldConfigurationController : Controller
    {
        private readonly IScaffoldConfigurations scaffoldConfigurations;

        public ScaffoldConfigurationController(IScaffoldConfigurations scaffoldConfigurations)
        {
            this.scaffoldConfigurations = scaffoldConfigurations;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var templates = scaffoldConfigurations
                .Configurations
                .ToDictionary(x => x.Key, x => new
                {
                    EntityType = x.Key,
                    DisplayName = x.Value.DisplayName,
                    Template = x.Value.Template
                });
            return Ok(templates);
        }
    }
}
