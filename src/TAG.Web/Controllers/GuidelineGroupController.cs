﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using TAG.Domain.Models;
using TAG.Domain.Services;
using TAG.Domain.Services.Search;
using TAG.Persistence;
using TAG.Web.Extensions;
using TAG.Web.Models.GuidelineGroupModels;

namespace TAG.Web.Controllers
{
    [Route("[controller]")]
    public class GuidelineGroupController : Controller
    {
        private readonly IDbConnection connection;
        private readonly IRepository repository;
        private readonly IMemoryCache cache;
        private readonly IGuidelineGroupService guidelineGroupService;

        public GuidelineGroupController(IDbConnection connection, IRepository repository, IMemoryCache cache, IGuidelineGroupService guidelineGroupService)
        {
            this.connection = connection;
            this.repository = repository;
            this.cache = cache;
            this.guidelineGroupService = guidelineGroupService;
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] EditGuidelineGroupModel model)
        {
            var entity = repository.Get<GuidelineGroup>(id);
            if (entity == null)
            {
                return NotFound();
            }
            entity.GuidelineValue = model.GuidelineValue;
            return Ok();
        }

        /// <summary>
        /// Moves a toxicity value from one guideline group to another
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("move")]
        public IActionResult Put([FromBody] MoveToxicityValueModel model)
        {
            var toxicityValue = repository.Get<ToxicityValue2016>(model.ToxicityValueId);
            var guidelineGroup = repository.Get<GuidelineGroup>(model.GuidelineGroupId);

            if (toxicityValue == null || guidelineGroup == null)
            {
                return NotFound();
            }

            guidelineGroupService.MoveToxicityValueIntoGroup(toxicityValue, guidelineGroup);
            cache.Remove("toxicityvalue2016");
            return Ok();
        }
    }
}
