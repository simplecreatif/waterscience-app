﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models;
using TAG.Web.Models.Mappers;
using TAG.Web.Models.Validators;
using NHibernate.Linq;
using TAG.Domain.Services;
using TAG.Domain.Services.Search;
using TAG.Web.Models.SearchModels;

namespace TAG.Web.Controllers
{
    [Route("[controller]")]
    public class SearchController : Controller
    {
        private readonly ISearchService searchService;
        private readonly IGuidelineService guidelineService;
        private readonly IGuidelineCsvService guidelineCsvService;
        private readonly IModelMapper<SearchResult, SearchResultModel> modelMapper;

        public SearchController(
            ISearchService searchService,
            IGuidelineService guidelineService,
            IGuidelineCsvService guidelineCsvService,
            IModelMapper<SearchResult, SearchResultModel> modelMapper)
        {
            this.searchService = searchService;
            this.guidelineService = guidelineService;
            this.guidelineCsvService = guidelineCsvService;
            this.modelMapper = modelMapper;
        }

        [HttpPost]
        public IActionResult Post([FromBody] SearchOptions options)
        {
            if (options == null)
            {
                return BadRequest();
            }

            var result = searchService.Search(options);
            var model = modelMapper.ToModel(result);
            return Ok(model);
        }

        [HttpPost("downloadGuidelines")]
        public IActionResult DownloadGuidelines([FromBody] SearchOptions options)
        {
            if (options == null)
            {
                return BadRequest();
            }

            var guidelines = guidelineService.ListGuideline(options);
            var csv = guidelineCsvService.ToCsv(guidelines);

            return File(csv, "text/csv", "guideline-derivation.csv");
        }
    }
}
