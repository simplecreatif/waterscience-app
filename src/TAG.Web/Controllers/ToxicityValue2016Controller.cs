﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using TAG.Domain.Models;
using TAG.Domain.Services.Search;
using TAG.Persistence;
using TAG.Web.Extensions;
using TAG.Web.Models.GuidelineGroupModels;
using TAG.Web.Models.ToxicityValue2016Models;
using TAG.Web.Infrastructure.Scaffolding;
using TAG.Web.Models.Mappers.ToxicityValue2016Mappers;
using TAG.Web.Models.Validators.ToxicityValue2016Validators;

namespace TAG.Web.Controllers
{
    [Route("[controller]")]
    public class ToxicityValue2016Controller : Controller
    {
        private readonly IRepository repository;
        private readonly EditToxicityValue2016Validator editValidator;
        private readonly EditToxicityValue2016Mapper editMapper;
        private readonly IScaffoldConfigurations scaffoldConfigurations;
        private readonly ILifetimeScope scope;

        public ToxicityValue2016Controller(
            IRepository repository,
            EditToxicityValue2016Validator editValidator,
            EditToxicityValue2016Mapper editMapper,
            IScaffoldConfigurations scaffoldConfigurations,
            ILifetimeScope scope)
        {
            this.repository = repository;
            this.editValidator = editValidator;
            this.editMapper = editMapper;
            this.scaffoldConfigurations = scaffoldConfigurations;
            this.scope = scope;
        }

        [HttpPost]
        public IActionResult Post([FromBody] EditToxicityValue2016Model model)
        {
            if (model == null)
            {
                return this.ValidationFailure("The entity is required");
            }

            var validationResult = editValidator.Validate(model);

            if (!validationResult.IsValid)
            {
                return this.ValidationFailure(validationResult);
            }

            var entity = editMapper.ToEntity(model);

            if (!model.GuidelineGroupId.HasValue)
            {
                // If no guideline group id was supplied, we're creating a
                // new guideline group
                var newGuidelineGroup = new GuidelineGroup();
                repository.Add(newGuidelineGroup);
                entity.GuidelineGroup = newGuidelineGroup;
            }

            repository.Add(entity);

            return this.EditResult(scope, scaffoldConfigurations.TryGet("toxicityvalue2016"), entity);
        }
    }
}
