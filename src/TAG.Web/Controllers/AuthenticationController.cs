﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TAG.Domain.Models;
using TAG.Persistence;
using TAG.Web.Models;
using TAG.Web.Models.Mappers;
using TAG.Web.Models.Validators;
using FluentValidation.Results;
using TAG.Web.Services.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using Microsoft.AspNetCore.Http;
using TAG.Domain.Models.Lookups;
using System.Security.Claims;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication.Cookies;
using TAG.Web.Models.UserModels;
using TAG.Web.Extensions;

namespace TAG.Web.Controllers
{
    [Route("auth")]
    public class AuthenticationController : Controller
    {
        private readonly IRepository repository;
        private readonly IAntiforgery antiforgery;
        private readonly IAuthenticationService authenticationService;
        private readonly IModelMapper<User, UserModel> userMapper;

        public AuthenticationController(
            IRepository repository,
            IAntiforgery antiforgery,
            IAuthenticationService authenticationService,
            IModelMapper<User, UserModel> userMapper)
        {
            this.repository = repository;
            this.antiforgery = antiforgery;
            this.authenticationService = authenticationService;
            this.userMapper = userMapper;
        }

        // POST api/auth/signin
        [HttpPost]
        [Route("signin")]
        public async Task<IActionResult> SignIn([FromBody]SignInModel model)
        {
            if (model == null)
            {
                return this.ValidationFailure("The entity is required.");
            }

            var validationResult = !(String.IsNullOrWhiteSpace(model.Email) || String.IsNullOrWhiteSpace(model.Password));
            if (!validationResult)
            {
                return this.ValidationFailure("Email and password must be provided.");
            }

            var user = repository.Query<User>().SingleOrDefault(x => x.Email.ToLower() == model.Email.ToLower());
            if (user == null)
            {
                return Unauthorized();
            }

            var identity = authenticationService.GetIdentity(model.Email, model.Password, user);
            if (identity == null)
            {
                return Unauthorized();
            }

            await HttpContext.Authentication.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));

            var userModel = userMapper.ToModel(user);
            return Ok(userModel);
        }

        // POST api/auth/signout
        [HttpPost]
        [Route("signout")]
        public async Task<IActionResult> SignOut()
        {
            await HttpContext.Authentication.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Ok();
        }

        [HttpGet]
        [Route("currentuser")]
        public ActionResult CurrentUser()
        {
            var currentIdentity = (ClaimsIdentity)Request.HttpContext.User.Identity;
            if (currentIdentity == null || !currentIdentity.IsAuthenticated)
            {
                return Unauthorized();
            }

            var email = currentIdentity.Claims.Single(x => x.Type == ClaimTypes.Email).Value;
            var user = repository.Query<User>().SingleOrDefault(x => x.Email == email.ToLower());
            if (user == null)
            {
                return NotFound();
            }

            var model = userMapper.ToModel(user);
            return Ok(model);
        }
    }
}
