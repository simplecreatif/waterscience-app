module.exports = {
    "internalListenPort": 5000,
    "externalListenHost": null,
    "externalListenPort": 7460,
    "allowedCorsOrigins": [
        "http://localhost:3000",
        "http://wqscidev:1087"
    ]
};
