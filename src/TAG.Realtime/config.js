module.exports = {
    "internalListenPort": 5001,
    "externalListenHost": null,
    "externalListenPort": 7460,
    "allowedCorsOrigins": [
        "http://localhost:3000"
    ]
};
