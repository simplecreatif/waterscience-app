var expressInternal = require("express")();
var httpInternal = require("http").Server(expressInternal);
var bodyParser = require("body-parser");

var expressExternal = require("express")();
var httpExternal = require("http").Server(expressExternal);
var io = require("socket.io")(httpExternal);
var cors = require("cors");

var config = require("./config.js");

// Add support for parsing JSON payloads in REST requests.
expressInternal.use(bodyParser.json());

expressExternal.use(cors({
    origin: config.allowedCorsOrigins
    , credentials: true
}));

// Receive POST requests from the internal express REST
// endpoint and forward them to clients via Socket.io.
expressInternal.post("/*", function(req, res, next) {
    console.log("action", "edit", req.url.substr(1), req.body);
    io.emit("action", "edit", req.url.substr(1), req.body);
    next();
});

// Receive DELETE requests from the internal express REST
// endpoint and forward them to clients via Socket.io.
expressInternal.delete("/*", function(req, res, next) {
    console.log("action", "delete", req.url.substr(1), req.body);
    io.emit("action", "delete", req.url.substr(1), req.body);
    next();
});

// Set up an internal REST endpoint which receives data
// from the application.
httpInternal.listen(config.internalListenPort, "localhost", function() {
    console.log("(Internal) Listening on localhost:" + config.internalListenPort);
});

// Set up a Socket.io endpoint through which clients can
// receive data.
httpExternal.listen(config.externalListenPort, config.externalListenHost, function() {
    var host = config.externalListenHost || "*";
    console.log( "(External) Listening on " + host + ":" + config.externalListenPort);
});

io.on("connection", function(socket) {
    console.log("A user connected!");
});
