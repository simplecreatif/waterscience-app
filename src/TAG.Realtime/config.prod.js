module.exports = {
    "internalListenPort": 5000,
    "externalListenHost": "setac-ecotox.bitproxima.net",
    "externalListenPort": 7460,
    "allowedCorsOrigins": [
        "http://setac-ecotox.bitproxima.net"
    ]
};
