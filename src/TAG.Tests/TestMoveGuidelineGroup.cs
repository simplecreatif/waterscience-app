﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using TAG.Domain.Models;
using TAG.Domain.Services;
using TAG.Persistence;

namespace TAG.Tests
{
    class TestMoveGuidelineGroup
    {
        private GuidelineGroupService guidelineGroupService;
        private IRepository repository;

        [SetUp]
        public void SetUp()
        {
            repository = Substitute.For<IRepository>();
            guidelineGroupService = new GuidelineGroupService(repository);
        }

        [Test]
        public void ToxicityValueIsMovedToNewGroup()
        {
            var oldGuidelineGroup = new GuidelineGroup { Id = 1 };
            var newGuidelineGroup = new GuidelineGroup { Id = 2 };
            var toxicityValue = new ToxicityValue2016 { GuidelineGroup = oldGuidelineGroup, Id = 100 };

            guidelineGroupService.MoveToxicityValueIntoGroup(toxicityValue, newGuidelineGroup);

            Assert.AreEqual(newGuidelineGroup, toxicityValue.GuidelineGroup);
        }

        [Test]
        public void OldGuidelineGroupRemainsIfStillInUse()
        {
            var oldGuidelineGroup = new GuidelineGroup { Id = 1 };
            var newGuidelineGroup = new GuidelineGroup { Id = 2 };
            var toxicityValue = new ToxicityValue2016 { GuidelineGroup = oldGuidelineGroup, Id = 100 };
            var anotherToxicityValue = new ToxicityValue2016 { GuidelineGroup = oldGuidelineGroup, Id = 101 };
            repository.Query<ToxicityValue2016>().Returns(new[] { toxicityValue, anotherToxicityValue }.AsQueryable());

            guidelineGroupService.MoveToxicityValueIntoGroup(toxicityValue, newGuidelineGroup);

            repository.DidNotReceive().Remove(oldGuidelineGroup);
        }

        [Test]
        public void OldGuidelineGroupIsDeletedIfNotInUse()
        {
            var oldGuidelineGroup = new GuidelineGroup { Id = 1 };
            var newGuidelineGroup = new GuidelineGroup { Id = 2 };
            var toxicityValue = new ToxicityValue2016 { GuidelineGroup = oldGuidelineGroup, Id = 100 };
            repository.Query<ToxicityValue2016>().Returns(new[] { toxicityValue }.AsQueryable());

            guidelineGroupService.MoveToxicityValueIntoGroup(toxicityValue, newGuidelineGroup);

            repository.Received().Remove(oldGuidelineGroup);
        }
    }
}
