﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TAG.Domain.Models;

namespace TAG.Tests
{
    public class TestCASNumberFormatting
    {
        [Test]
        public void TestFormats()
        {
            // see https://en.wikipedia.org/wiki/CAS_Registry_Number

            TestFormat(0, "00-00-0");
            TestFormat(00000, "00-00-0");
            TestFormat(10000, "10-00-0");
            TestFormat(12345, "12-34-5");
            TestFormat(123456, "123-45-6");
            TestFormat(1234567, "1234-56-7");
            TestFormat(1234567121, "1234567-12-1");
            TestFormat(9999999999, "9999999-99-9");
            TestFormat(1000000000, "1000000-00-0");
        }

        private void TestFormat(long casNumber, string expected)
        {
            var chemical = new Chemical { CASNumber = casNumber };
            var actual = chemical.CASNumberFormatted;

            Assert.AreEqual(expected, actual);
        }
    }
}
