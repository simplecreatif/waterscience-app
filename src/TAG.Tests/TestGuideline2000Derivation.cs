﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TAG.Domain.Models;

namespace TAG.Tests
{
    public class TestGuideline2000Derivation
    {
        [Test]
        public void ShouldDeriveCorrectGuideline()
        {
            // This test is based on real data which can be found
            // with the following search criteria:
            // Toxicant: Ammonia
            // Species: Ictalurus punctatus
            // Endpoint: GRO

            //var guideline = new Guideline2000
            //{
            //    ToxicityValues = new List<ToxicityValue2000>
            //    {
            //        new ToxicityValue2000 {ConcentrationUsed = 2.76m},
            //        new ToxicityValue2000 {ConcentrationUsed = 2.68m},
            //        new ToxicityValue2000 {ConcentrationUsed = 1.65m},
            //        new ToxicityValue2000 {ConcentrationUsed = 9.34m}
            //    }
            //};

            //Assert.AreEqual(3.27, guideline.GuidelineValue, 0.01);
            Assert.Inconclusive("This needs to be manually tested using the data contained within the test because this functionality has been pushed into the database.");
        }
    }
}
