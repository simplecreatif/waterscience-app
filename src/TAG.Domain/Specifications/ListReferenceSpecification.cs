﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Domain.Specifications
{
    public class ListReferenceSpecification : BaseSpecification<Reference>
    {
        public override IQueryable<Reference> Apply(IQueryable<Reference> query)
        {
            return query.OrderBy(x => x.Authors);
        }
    }
}
