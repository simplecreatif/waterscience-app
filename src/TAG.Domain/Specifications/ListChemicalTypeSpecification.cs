﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListChemicalTypeSpecification : BaseSpecification<ChemicalType>
    {
        public override IQueryable<ChemicalType> Apply(IQueryable<ChemicalType> query)
        {
            return query.OrderBy(x => x.Name);
        }
    }
}
