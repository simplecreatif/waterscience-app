﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListToxicityValue2000Specification : BaseSpecification<ToxicityValue2000>
    {
        public override IQueryable<ToxicityValue2000> Apply(IQueryable<ToxicityValue2000> query)
        {
            return query
                .Fetch(x => x.Chemical)
                .Fetch(x => x.Species)
                .Fetch(x => x.Duration)
                .Fetch(x => x.DurationUnit)
                .Fetch(x => x.Effect)
                .Fetch(x => x.Endpoint)
                .Fetch(x => x.ConcentrationUnit)
                .OrderBy(x => x.Chemical.Name);
        }
    }
}
