﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListSpeciesSpecification : BaseSpecification<Species>
    {
        public override IQueryable<Species> Apply(IQueryable<Species> query)
        {
            return query.OrderBy(x => x.ScientificName);
        }
    }
}
