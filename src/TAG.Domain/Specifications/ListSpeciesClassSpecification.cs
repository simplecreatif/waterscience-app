﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListSpeciesClassSpecification : BaseSpecification<SpeciesClass>
    {
        public override IQueryable<SpeciesClass> Apply(IQueryable<SpeciesClass> query)
        {
            return query
                .Fetch(x => x.Phylum)
                .OrderBy(x => x.Name);
        }
    }
}
