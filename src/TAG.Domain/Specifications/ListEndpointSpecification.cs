﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListEndpointSpecification : BaseSpecification<Endpoint>
    {
        public override IQueryable<Endpoint> Apply(IQueryable<Endpoint> query)
        {
            return query.OrderBy(x => x.Abbreviation);
        }
    }
}
