﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListChemicalSpecification : BaseSpecification<Chemical>
    {
        public override IQueryable<Chemical> Apply(IQueryable<Chemical> query)
        {
            return query
                .Fetch(x => x.Toxicant)
                .OrderBy(x => x.CommonName);
        }
    }
}
