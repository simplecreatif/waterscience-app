﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListLookupSpecification : BaseSpecification<Lookup>
    {
        public override IQueryable<Lookup> Apply(IQueryable<Lookup> query)
        {
            return query.OrderBy(x => x.Name);
        }
    }
}
