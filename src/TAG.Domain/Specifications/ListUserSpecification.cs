﻿using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Domain.Specifications
{
    public class ListUserSpecification : BaseSpecification<User>
    {
        public override IQueryable<User> Apply(IQueryable<User> query)
        {
            return query
                .Fetch(x => x.Role)
                .OrderBy(x => x.Email);
        }
    }
}
