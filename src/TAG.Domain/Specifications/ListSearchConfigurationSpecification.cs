﻿using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Domain.Specifications
{
    public class ListSearchConfigurationForCurrentUserSpecification : BaseSpecification<SearchConfiguration>
    {
        private readonly ClaimsPrincipal principal;

        public ListSearchConfigurationForCurrentUserSpecification(ClaimsPrincipal principal)
        {
            this.principal = principal;
        }

        public override IQueryable<SearchConfiguration> Apply(IQueryable<SearchConfiguration> query)
        {
            var currentUserId = Convert.ToInt32(principal.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Sid).Value);
            return query
                .Fetch(x => x.User)
                .Fetch(x => x.Chemical)
                .Fetch(x => x.Toxicant)
                .FetchMany(x => x.Species)
                .FetchMany(x => x.Effects)
                .FetchMany(x => x.Endpoints)
                .FetchMany(x => x.Databases)
                .FetchMany(x => x.SearchProperties)
                .Where(x => x.User.Id == currentUserId);
        }
    }
}
