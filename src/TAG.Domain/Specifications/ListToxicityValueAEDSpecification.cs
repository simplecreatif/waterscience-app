﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListToxicityValueAEDSpecification : BaseSpecification<ToxicityValueAED>
    {
        public override IQueryable<ToxicityValueAED> Apply(IQueryable<ToxicityValueAED> query)
        {
            return query
                .Fetch(x => x.Chemical)
                .Fetch(x => x.Species)
                .Fetch(x => x.Duration)
                .Fetch(x => x.DurationUnit)
                .Fetch(x => x.Effect)
                .Fetch(x => x.Endpoint)
                .Fetch(x => x.ConcentrationUnit)
                .OrderBy(x => x.Identifier);
        }
    }
}
