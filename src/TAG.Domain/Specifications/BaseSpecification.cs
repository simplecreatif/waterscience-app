﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Persistence;

namespace TAG.Domain.Specifications
{
    public abstract class BaseSpecification<T> : ISpecification, ISpecification<T> where T : class
    {
        public abstract IQueryable<T> Apply(IQueryable<T> query);

        public IQueryable<T> Apply(IRepository repository)
        {
            return Apply(repository.Query<T>());
        }

        public IQueryable<object> Apply(IQueryable<object> query)
        {
            return Apply((IQueryable<T>) query);
        }

        public IQueryable<object> Apply(IRepository repository, Type t)
        {
            return Apply(repository.Query<T>(t));
        }
    }
}
