﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Persistence;

namespace TAG.Domain.Specifications
{
    public interface ISpecification
    {
        IQueryable<object> Apply(IRepository repository, Type t);
    }

    public interface ISpecification<T> where T : class
    {
        IQueryable<T> Apply(IQueryable<T> query);
        IQueryable<T> Apply(IRepository repository);
    }
}
