﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListChemicalReferenceSpecification : BaseSpecification<ChemicalReference>
    {
        public override IQueryable<ChemicalReference> Apply(IQueryable<ChemicalReference> query)
        {
            return query.OrderBy(x => x.Authors);
        }
    }
}
