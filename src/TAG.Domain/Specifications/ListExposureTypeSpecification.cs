﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListExposureTypeSpecification : BaseSpecification<ExposureType>
    {
        public override IQueryable<ExposureType> Apply(IQueryable<ExposureType> query)
        {
            return query.OrderBy(x => x.Name);
        }
    }
}
