﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListMediaSpecification : BaseSpecification<Media>
    {
        public override IQueryable<Media> Apply(IQueryable<Media> query)
        {
            return query
                .Fetch(x => x.MediaType)
                .OrderBy(x => x.Name);
        }
    }
}
