﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Specifications
{
    public class ListToxicityValue2016Specification : BaseSpecification<ToxicityValue2016>
    {
        public override IQueryable<ToxicityValue2016> Apply(IQueryable<ToxicityValue2016> query)
        {
            return query
                .Fetch(x => x.Chemical)
                .Fetch(x => x.Species)
                .Fetch(x => x.Duration)
                .Fetch(x => x.DurationUnit)
                .Fetch(x => x.Effect)
                .Fetch(x => x.Endpoint)
                .Fetch(x => x.ConcentrationUnit)
                .OrderBy(x => x.Chemical.Name);
        }
    }
}
