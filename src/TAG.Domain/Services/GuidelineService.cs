﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using TAG.Domain.Extensions;
using TAG.Domain.Models;
using TAG.Domain.Services.DownloadGuidelines;
using TAG.Domain.Services.Search;
using TAG.Persistence;

namespace TAG.Domain.Services
{
    /// <summary>
    /// Handles retrieval of toxicity values and guidelines (for exporting 
    /// the full hierarchy of data as opposed to just to the final guideline
    /// values).
    /// </summary>
    public class GuidelineService : IGuidelineService
    {
        private readonly ISearchService searchService;

        public GuidelineService(ISearchService searchService)
        {
            this.searchService = searchService;
        }

        public List<IGuidelineModel> ListGuideline(SearchOptions options)
        {
            return Enumerable.Empty<IGuidelineModel>()
                .ConcatIf(options.Databases.Contains(Database.WQG2000), ListGuideline2000(options))
                .ConcatIf(options.Databases.Contains(Database.WQG2016), ListGuideline2016(options))
                .ToList();
        }

        public List<Guideline2000Model> ListGuideline2000(SearchOptions options)
        {
            var preparedOptions = options.Clone();

            preparedOptions.Databases = new[] { Database.WQG2000 };

            // Add some required fields for grouping toxicity values into
            // guidelines
            preparedOptions.SearchProperties =
                preparedOptions
                .SearchProperties
                .Concat(new[]
                {
                    // These properties are used for grouping WQG2000 toxicity
                    // values into guidelines
                    "tv_chemical_id",
                    "tv_chemical_toxicant_id",
                    "tv_species_id",
                    "tv2000_effectused_id",
                    "tv_endpoint_id",
                    "tv_species_animalcategory_id",
                    "tv2000_mediatype_id",
                    "tv2000_testtype_id",
                    "tv_durationhour_id",
                    "tv_concentrationunit_id",
                    // This property is reduced within a group of toxicity values
                    // to derive the guideline value for WQG2000 data
                    "tv2000_concentrationused"
                })
                .Distinct()
                .ToArray();

            return searchService
                .Search(preparedOptions)
                .ToxicityValues
                .Where(x => x.tv2000_concentrationused != null)
                .GroupBy(x => new
                {
                    x.tv_chemical_id,
                    x.tv_chemical_toxicant_id,
                    x.tv_species_id,
                    x.tv2000_effectused_id,
                    x.tv_endpoint_id,
                    x.tv_species_animalcategory_id,
                    x.tv2000_mediatype_id,
                    x.tv2000_testtype_id,
                    x.tv_durationhour_id,
                    x.tv_concentrationunit_id
                })
                .Select(x => new Guideline2000Model
                {
                    ToxicityValues = x.ToList()
                })
                .ToList();
        }

        public List<Guideline2016Model> ListGuideline2016(SearchOptions options)
        {
            var preparedOptions = options.Clone();

            preparedOptions.Databases = new[] { Database.WQG2016 };

            // Add some required fields for grouping toxicity values into
            // guidelines
            preparedOptions.SearchProperties =
                preparedOptions
                .SearchProperties
                .Concat(new[]
                {
                    // This property is used for grouping WQG2016 toxicity
                    // values into guidelines
                    "tv2016_guidelinegroup_id",
                    // This value is the value of the guideline group for
                    // a WQG2016 value.
                    "tv2016_guidelinegroup_guidelinevalue",
                })
                .Distinct()
                .ToArray();

            return searchService
                .Search(preparedOptions)
                .ToxicityValues
                .Where(x => x.tv2016_guidelinegroup_guidelinevalue != null)
                .GroupBy(x => new
                {
                    x.tv2016_guidelinegroup_id,
                })
                .Select(x => new Guideline2016Model
                {
                    ToxicityValues = x.ToList()
                })
                .ToList();
        }
    }
}
