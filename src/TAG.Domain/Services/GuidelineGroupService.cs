﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Persistence;

namespace TAG.Domain.Services
{
    public class GuidelineGroupService : IGuidelineGroupService
    {
        private readonly IRepository repository;

        public GuidelineGroupService(IRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Moves the specified toxicity value into the guideline group.
        /// Deletes the toxicity value's previous guideline group if it's
        /// not in use.
        /// </summary>
        /// <param name="toxicityValue"></param>
        /// <param name="guidelineGroup"></param>
        public void MoveToxicityValueIntoGroup(ToxicityValue2016 toxicityValue, GuidelineGroup guidelineGroup)
        {
            var oldGuidelineGroup = toxicityValue.GuidelineGroup;
            toxicityValue.GuidelineGroup = guidelineGroup;

            // Delete old guideline group if there are no other values in it
            var isOldGuidelineGroupInUse = repository.Query<ToxicityValue2016>()
                .Any(x => x.Id != toxicityValue.Id && x.GuidelineGroup.Id == oldGuidelineGroup.Id);

            if (!isOldGuidelineGroupInUse)
            {
                repository.Remove(oldGuidelineGroup);
            }
        }
    }
}
