﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Dapper;

namespace TAG.Domain.Services.Search
{
    public class SearchService : ISearchService
    {
        private readonly IDbConnection connection;

        public SearchService(IDbConnection connection)
        {
            this.connection = connection;
        }

        public SearchResult Search(SearchOptions options)
        {
            // We're dropping down to plain sql and Dapper here because
            // we need to dynamically choose which columns to fetch from the
            // merged view of all toxicity values. If we were prepared to
            // fetch every column we could do this in NHibernate but we'd
            // rather just select what the user has asked for. This saves
            // loading large amounts of unnecessary data (the merged view
            // contains about 50+ columns). By doing this we improve performance
            // and reduce memory usage. It is ugly, though.

            var toxicityValuesDynamic = ListToxicityValuesDynamic(options);
            var guidelines = ListGuidelines(options);

            return new SearchResult
            {
                ToxicityValues = toxicityValuesDynamic,
                Guidelines2000 = guidelines.Where(x => x.Source == Database.WQG2000).ToList(),
                Guidelines2016 = guidelines.Where(x => x.Source == Database.WQG2016).ToList()
            };
        }

        /// <summary>
        /// Retrieve a collection of guideline values.
        /// 
        /// Guideline values are generated for 2000 data and merged with
        /// the 2016 data. This is done through a materialized view which
        /// uses PostgreSQL's math functions to generate guidelines.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private List<Guideline> ListGuidelines(SearchOptions options)
        {
            var q = "select * from guideline";
            q = MaybeAddClauses(options, q);

            return connection.Query<Guideline>(q).ToList();
        }

        /// <summary>
        /// Retrieve a collection of dynamic toxicity values. This is
        /// dynamic because the user chooses which columns to load. This
        /// is also a point where optimisation is important otherwise we'd 
        /// return a fully-populated domain entity.
        /// 
        /// Toxicity values are retrieved from aed, 2000, and 2016 data
        /// using a materialized view with denormalised columns.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private List<dynamic> ListToxicityValuesDynamic(SearchOptions options)
        {
            var q = "select " + GetPropertiesToFetch(options) + " from toxicityvalueall";
            q = MaybeAddClauses(options, q);

            return connection.Query(q).ToList();
        }

        /// <summary>
        /// Add where clauses for any of the options specified in SearchOptions
        /// </summary>
        /// <param name="options"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        private string MaybeAddClauses(SearchOptions options, string q)
        {
            var where = new List<string>();
            MaybeAddClause(where, options.CASNumber, "tv_chemical_casnumber");
            MaybeAddClause(where, options.ChemicalId, "tv_chemical_id");
            MaybeAddClause(where, options.ToxicantId, "tv_chemical_toxicant_id");
            MaybeAddClause(where, options.SpeciesIds, "tv_species_id");
            MaybeAddClause(where, options.EffectIds, "tv_effect_id");
            MaybeAddClause(where, options.EndpointIds, "tv_endpoint_id");
            AddDatabaseClauses(where, options);


            if (where.Any())
            {
                q += " where (" + string.Join(" and ", where) + ")";
            }
            return q;
        }

        private void MaybeAddClause(ICollection<string> clauses, int[] option, string column)
        {
            if (option != null && option.Any())
            {
                clauses.Add(" " + column + " in (" + string.Join(",", option) + ")");
            }
        }

        private void MaybeAddClause(ICollection<string> clauses, int? option, string column)
        {
            if (option.HasValue)
            {
                clauses.Add(" " + column + "=" + option.Value);
            }
        }

        private void MaybeAddClause(ICollection<string> clauses, long? option, string column)
        {
            if (option.HasValue)
            {
                clauses.Add(" " + column + "=" + option.Value);
            }
        }

        /// <summary>
        /// This does NOT escape the specified option. Use only for hard-coded string comparisons.
        /// Do not invoke with user input.
        /// </summary>
        /// <param name="clauses"></param>
        /// <param name="option"></param>
        /// <param name="column"></param>
        private void MaybeAddClauseUnsanitized(ICollection<string> clauses, string[] option, string column)
        {
            if (option != null && option.Any())
            {
                clauses.Add(" " + column + " in (" + string.Join(",", option.Select(x => "'" + x + "'")) + ")");
            }
        }

        private void AddDatabaseClauses(ICollection<string> clauses, SearchOptions options)
        {
            if (options.SearchAEDDatabase
                && options.SearchWQG2000Database
                && options.SearchWQG2016Database)
            {
                // Searching all databases - don't add any clauses
                return;
            }

            var databases = options.Databases.Select(x => x.ToString()).ToArray();
            MaybeAddClauseUnsanitized(clauses, databases, "source");
        }

        /// <summary>
        /// Given a collection of untrusted database columns, return those
        /// which consist only of letters, numbers,  and underscores.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private string GetPropertiesToFetch(SearchOptions options)
        {
            var r = new Regex("^[a-z0-9_]+$");

            var requiredProperties = new[] { "tv_id" };
            var properties = options.SearchProperties.Where(x => r.IsMatch(x));

            return string.Join(", ", requiredProperties.Concat(properties));
        }
    }
}
