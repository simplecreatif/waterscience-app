using System.Collections.Generic;
using TAG.Domain.Models;

namespace TAG.Domain.Services.Search
{
    public interface ISearchService
    {
        SearchResult Search(SearchOptions options);
    }
}