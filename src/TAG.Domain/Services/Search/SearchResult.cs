﻿using System.Collections.Generic;

namespace TAG.Domain.Services.Search
{
    public class SearchResult
    {
        public ICollection<dynamic> ToxicityValues { get; set; }
        public ICollection<Guideline> Guidelines2000 { get; set; }
        public ICollection<Guideline> Guidelines2016 { get; set; }
    }
}
