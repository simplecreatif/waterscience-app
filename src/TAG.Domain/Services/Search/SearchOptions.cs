﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TAG.Domain.Services.Search
{
    public class SearchOptions : ICloneable
    {
        public long? CASNumber { get; set; }
        public int? ChemicalId { get; set; }
        public int? ToxicantId { get; set; }
        public int[] SpeciesIds { get; set; }
        public int[] EffectIds { get; set; }
        public int[] EndpointIds { get; set; }
        public Database[] Databases { get; set; }
        public ICollection<string> SearchProperties { get; set; }

        public bool SearchAEDDatabase => Databases.Contains(Database.AED);
        public bool SearchWQG2000Database => Databases.Contains(Database.WQG2000);
        public bool SearchWQG2016Database => Databases.Contains(Database.WQG2016);

        object ICloneable.Clone()
        {
            return Clone();
        }

        public SearchOptions Clone()
        {
            return new SearchOptions
            {
                CASNumber = CASNumber,
                ChemicalId = ChemicalId,
                ToxicantId = ToxicantId,
                SpeciesIds = SpeciesIds?.ToArray(),
                EffectIds = EffectIds?.ToArray(),
                EndpointIds = EndpointIds?.ToArray(),
                Databases = Databases?.ToArray(),
                SearchProperties = SearchProperties.ToList()
            };
        }
    }
}
