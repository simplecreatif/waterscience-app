﻿namespace TAG.Domain.Services.Search
{
    public class Guideline
    {
        public double GuidelineValue { get; set; }
        public int? GuidelineGroupId { get; set; }

        public string Toxicant { get; set; }
        public string SpeciesScientificName { get; set; }
        public string SpeciesCommonName { get; set; }
        public string Endpoint { get; set; }
        public string Effect { get; set; }
        public string MediaType { get; set; }
        public string TestType { get; set; }
        public string DurationHour { get; set; }
        public string ConcentrationUnit { get; set; }
        public string AnimalCategory { get; set; }

        /// <summary>
        /// Distinguishes between guideline 2000 and 2016 data
        /// </summary>
        public Database Source { get; set; }
    }
}

