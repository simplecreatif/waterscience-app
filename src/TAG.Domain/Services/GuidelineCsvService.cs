﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using TAG.Domain.Services.DownloadGuidelines;

namespace TAG.Domain.Services
{
    public class GuidelineCsvService : IGuidelineCsvService
    {
        public byte[] ToCsv(ICollection<IGuidelineModel> guidelines)
        {
            if (!guidelines.Any())
            {
                return new byte[0];
            }

            using (var ms = new MemoryStream())
            using (var sw = new StreamWriter(ms))
            using (var csv = new CsvWriter(sw))
            {
                var propertyNames = GetPropertyNames(guidelines);

                WriteHeaders(propertyNames, csv);
                WriteRows(guidelines, propertyNames, csv);

                sw.Flush();
                return ms.ToArray();
            }
        }

        private static void WriteRows(ICollection<IGuidelineModel> guidelines, List<string> propertyNames, CsvWriter csv)
        {
            foreach (var guideline in guidelines)
            {
                // Write out all toxicity value properties
                foreach (IDictionary<string, object> toxicityValue in guideline.ToxicityValues)
                {
                    foreach (var propertyName in propertyNames)
                    {
                        csv.WriteField(toxicityValue[propertyName] ?? string.Empty);
                    }
                    csv.NextRecord();
                }

                // Write out the guideline value for the above rows
                foreach (var _ in propertyNames)
                {
                    csv.WriteField(string.Empty);
                }
                csv.WriteField(guideline.GuidelineValue);
                csv.NextRecord();
            }
        }

        private static void WriteHeaders(List<string> propertyNames, CsvWriter csv)
        {
            foreach (var propertyName in propertyNames)
            {
                var headerName = ToxicityValueProperties.Map(propertyName);
                csv.WriteField(headerName);
            }
            csv.WriteField("Toxic Concentration");
            csv.NextRecord();
        }

        private static List<string> GetPropertyNames(ICollection<IGuidelineModel> guidelines)
        {
            var firstValue = guidelines.First().ToxicityValues.First();

            // Exclude AED columns (not applicable to guidelines) and id columns
            return ((IDictionary<string, object>) firstValue)
                .Keys
                .Where(x => !x.StartsWith("tvaed_") && !x.EndsWith("_id") && x != "tv2016_guidelinegroup_guidelinevalue")
                .ToList();
        }
    }
}
