﻿using TAG.Domain.Models;

namespace TAG.Domain.Services
{
    public interface IGuidelineGroupService
    {
        /// <summary>
        /// Moves the specified toxicity value into the guideline group.
        /// Deletes the toxicity value's previous guideline group if it's
        /// not in use.
        /// </summary>
        /// <param name="toxicityValue"></param>
        /// <param name="guidelineGroup"></param>
        void MoveToxicityValueIntoGroup(ToxicityValue2016 toxicityValue, GuidelineGroup guidelineGroup);
    }
}