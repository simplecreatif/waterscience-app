using System.Collections.Generic;
using System.IO;
using TAG.Domain.Services.DownloadGuidelines;

namespace TAG.Domain.Services
{
    public interface IGuidelineCsvService
    {
        byte[] ToCsv(ICollection<IGuidelineModel> guidelines);
    }
}