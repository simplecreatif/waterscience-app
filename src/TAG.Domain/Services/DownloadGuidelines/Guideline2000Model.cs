﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Domain.Services.DownloadGuidelines
{
    public class Guideline2000Model : IGuidelineModel
    {
        public ICollection<dynamic> ToxicityValues { get; set; }

        public double GuidelineValue => Math.Round(Math.Exp(
            ToxicityValues
            .Where(x => x.tv2000_concentrationused != null)
            .Average(x => (double)Math.Log(decimal.ToDouble(x.tv2000_concentrationused)))), 4);
    }
}
