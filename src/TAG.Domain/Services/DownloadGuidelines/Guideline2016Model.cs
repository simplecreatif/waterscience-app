﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;

namespace TAG.Domain.Services.DownloadGuidelines
{
    public class Guideline2016Model : IGuidelineModel
    {
        public ICollection<dynamic> ToxicityValues { get; set; }

        public double GuidelineValue => decimal.ToDouble(ToxicityValues
            .First()
            .tv2016_guidelinegroup_guidelinevalue);
    }
}
