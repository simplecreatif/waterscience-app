﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Services.DownloadGuidelines
{
    public interface IGuidelineModel
    {
        ICollection<dynamic> ToxicityValues { get; }
        double GuidelineValue { get; }
    }
}
