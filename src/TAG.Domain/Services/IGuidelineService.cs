using System.Collections.Generic;
using TAG.Domain.Services.DownloadGuidelines;
using TAG.Domain.Services.Search;

namespace TAG.Domain.Services
{
    public interface IGuidelineService
    {
        List<IGuidelineModel> ListGuideline(SearchOptions options);
        List<Guideline2000Model> ListGuideline2000(SearchOptions options);
        List<Guideline2016Model> ListGuideline2016(SearchOptions options);
    }
}