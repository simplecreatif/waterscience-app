﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Services
{
    public static class ToxicityValueProperties
    {
        /// <summary>
        /// An identical set of mappings is maintained in TAG.WebClient toxicityValueProperties.js
        /// </summary>
        private static Dictionary<string, string> mappings = new Dictionary<string, string>
        {
            // tv refs
            { "tv_chemical_casnumber", "CAS Number" },
            { "tv_chemical_name", "Chemical" },
            { "tv_species_scientificname", "Scientific Name" },
            { "tv_species_commonname", "Common Name" },
            { "tv_duration_name", "Duration" },
            { "tv_durationunit_name", "Duration Unit" },
            { "tv_effect_name", "Effect" },
            { "tv_endpoint_name", "Endpoint" },
            { "tv_concentration", "Concentration" },
            { "tv_concentrationunit_name", "Concentration Unit" },

            // tv 2000 refs
            { "tv2000_mediatype_name", "Media Type (2000)" },
            { "tv2000_testtype_name", "Test Type (2000)" },
            { "tv2000_effectused_name", "Effect Used (2000)" },
            { "tv2000_exposuretype_name", "Exposure Type (2000)" },
            { "tv2000_concentrationcode_name", "Concentration Code (2000)" },
            { "tv2000_concentrationtype_name", "Concentration Type (2000)" },
            { "tv2000_status_name", "Status (2000)" },

            // tv 2000 values
            { "tv2000_concentrationused", "Concentration Used (2000)" },
            { "tv2000_convnoec", "Conv. NOEC (2000)" },
            { "tv2000_hardnesscorrectedconc", "Hardness Corrected Conc. (2000)" },
            { "tv2000_temperature", "Temperature (2000)" },
            { "tv2000_ph", "pH (2000)" },
            { "tv2000_hardness", "Hardness (2000)" },
            { "tv2000_salinity", "Salinity (2000)" },
            { "tv2000_totalammoniamg", "Total Ammonia (mg) (2000)" },
            { "tv2000_totalammoniaph8", "Total Ammonia pH8 (2000)" },

            // tv 2016 refs
            { "tv2016_ischronic", "Is Chronic (2016)" },
            { "tv2016_concentrationused", "Concentration Used (2016)" },

            // tv 2016 values
            { "tv2016_record", "Record (2016)" },
            { "tv2016_datasource", "Data Source (2016)" },
            { "tv2016_mediatype_name", "Media Type (2016)" },
            { "tv2016_endpointfrompaper_name", "Endpoint from Paper (2016)" },
            { "tv2016_endpointmeasurement_name", "Endpoint Measurement (2016)" },

            // tv aed refs
            { "tvaed_chemicalgrade_name", "Chemical Grade (AED)" },
            { "tvaed_chemicalform_name", "Chemical Form (AED)" },
            { "tvaed_solventunit_name", "Solvent Unit (AED)" },
            { "tvaed_age_name", "Age (AED)" },
            { "tvaed_media_name", "Media (AED)" },
            { "tvaed_testform_name", "Test Form (AED)" },
            { "tvaed_exposuretype_name", "Exposure Type (AED)" },
            { "tvaed_errormeasure_name", "Error Measure (AED)" },
            { "tvaed_statistic_name", "Statistic (AED)" },
            { "tvaed_replication_name", "Replication (AED)" },
            { "tvaed_temporalreplication_name", "Temporal Replication (AED)" },
            { "tvaed_concentrationtype_name", "Concentration Type (AED)" },
            { "tvaed_country_name", "Country (AED)" },
            { "tvaed_publicationlanguage_name", "Publication Language (AED)" },

            // tv aed values
            { "tvaed_identifier", "Identifier (AED)" },
            { "tvaed_chemicalgradevalue", "Chemical Grade Value (AED)" },
            { "tvaed_issolventused", "Is Solvent Used (AED)" },
            { "tvaed_solventname", "Solvent Name (AED)" },
            { "tvaed_solventconcentration", "Solvent Concentration (AED)" },
            { "tvaed_solventcontrol", "Solvent Control (AED)" },
            { "tvaed_speciesstrain", "Species Strain (AED)" },
            { "tvaed_speciesageorstage", "Species Age/Stage (AED)" },
            { "tvaed_feeding", "Feeding (AED)" },
            { "tvaed_lowerconfidencelimit95", "LCL 95 (AED)" },
            { "tvaed_upperconfidencelimit95", "UCL 95 (AED)" },
            { "tvaed_errorvalue", "Error (AED)" },
            { "tvaed_doseresponse", "Dose Response (AED)" },
            { "tvaed_significancelevel", "Significance Level (AED)" },
            { "tvaed_power", "Power (AED)" },
            { "tvaed_hasreftox", "Has Ref. Tox. (AED)" },
            { "tvaed_reftoxname", "Ref. Tox. Name (AED)" },
            { "tvaed_hascontroleffect", "Has Control Effect (AED)" },
            { "tvaed_controleffect", "Control Effect (AED)" },
            { "tvaed_invalidationcriteria", "Invalidation Criteria (AED)" },
            { "tvaed_lossofchemical", "Loss of Chemical (AED)" },
            { "tvaed_chemicalformused", "Chemical Form Used (AED)" },
            { "tvaed_isphysicochemmeasured", "Is Physicochem Used (AED)" },
            { "tvaed_temperature", "Temperature (AED)" },
            { "tvaed_ph", "pH (AED)" },
            { "tvaed_conductivity", "Conductivity (AED)" },
            { "tvaed_hardness", "Hardness (AED)" },
            { "tvaed_salinity", "Salinity (AED)" },
            { "tvaed_alkalinity", "Alkalinity (AED)" },
            { "tvaed_organiccarbon", "Organic Carbon (AED)" },
            { "tvaed_lightregime", "Light Regime (AED)" },
            { "tvaed_otherparameters", "Other Parameters (AED)" },
            { "tvaed_methodreference", "Method Reference (AED)" },
            { "tvaed_sitelocation", "Site Location (AED)" },
            { "tvaed_comments", "Comments (AED)" },
            { "tvaed_qualityscore", "Quality Score (AED)" }
        };

        public static string Map(string property)
        {
            return mappings[property];
        }
    }
}
