﻿namespace TAG.Domain.Models.Lookups
{
    public class Lookup : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
