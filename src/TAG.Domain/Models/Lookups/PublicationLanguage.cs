﻿namespace TAG.Domain.Models.Lookups
{
    /// <summary>
    /// Was Language table in old system.
    /// </summary>
    public class PublicationLanguage : Lookup
    {
    }
}
