﻿namespace TAG.Domain.Models.Lookups
{
    /// <summary>
    /// % purity, commercial, etc.
    /// Was ChemicalGrade table in old system.
    /// </summary>
    public class ChemicalGrade : Lookup
    {
    }
}
