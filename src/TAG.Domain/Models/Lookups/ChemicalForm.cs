﻿namespace TAG.Domain.Models.Lookups
{
    /// <summary>
    /// Solid, Liquid, Gas, etc.
    /// Was Chemicalform table in old system
    /// </summary>
    public class ChemicalForm : Lookup
    {
    }
}
