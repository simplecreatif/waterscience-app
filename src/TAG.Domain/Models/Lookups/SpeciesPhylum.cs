﻿namespace TAG.Domain.Models.Lookups
{
    /// <summary>
    /// Was SpeciesPhylum column in guideline derivation spreadsheets
    /// </summary>
    public class SpeciesPhylum : Lookup
    {
    }
}
