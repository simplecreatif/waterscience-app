﻿namespace TAG.Domain.Models.Lookups
{
    /// <summary>
    /// Was Temporal table in old system.
    /// </summary>
    public class TemporalReplication : Lookup
    {
    }
}
