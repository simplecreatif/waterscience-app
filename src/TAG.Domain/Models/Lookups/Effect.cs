﻿namespace TAG.Domain.Models.Lookups
{
    /// <summary>
    /// Was Effect table in old toxicology system.
    /// Should contain values like 'EC10' (10% effect concentration)
    /// </summary>
    public class Effect : Lookup
    {
    }
}
