﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was Speciesdata table in old system
    /// </summary>
    public class Species : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string ScientificName { get; set; }
        public virtual string CommonName { get; set; }
        public virtual string Status { get; set; }

        /// <summary>
        /// Was Animal Type ID in old system. Referred to
        /// Animaltype table in old system.
        /// </summary>
        public virtual AnimalType AnimalType { get; set; }

        public virtual string MajorGroup { get; set; }
        public virtual string MinorGroup { get; set; }

        /// <summary>
        /// This was named WQcategory and Animal TypeWQ in old system.
        /// Referred to "WQcateogary" (sic) table in old system.
        /// </summary>
        public virtual AnimalCategory AnimalCategory { get; set; }

        /// <summary>
        /// This field is from guidelines derivation worksheet
        /// </summary>
        public virtual SpeciesClass Class { get; set; }

        /// <summary>
        /// True if Heterotroph, false if Phototroph (from guidelines spreadsheets)
        /// </summary>
        public virtual bool? IsHeterotroph { get; set; }
    }
}
