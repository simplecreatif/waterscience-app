﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was Endpoint table in old system
    /// </summary>
    public class Endpoint : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Abbreviation { get; set; }
    }
}
