﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    public class User : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual string Email { get; set; }
        public virtual Role Role { get; set; }
        public virtual ICollection<SearchConfiguration> SearchConfigurations { get; set; }
    }
}
