﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    public class SearchConfiguration : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual long? CASNumber { get; set; }
        public virtual Chemical Chemical { get; set; }
        public virtual Toxicant Toxicant { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Species> Species { get; set; }
        public virtual ICollection<Effect> Effects { get; set; }
        public virtual ICollection<Endpoint> Endpoints { get; set; }
        public virtual ICollection<Database>  Databases { get; set; }
        public virtual ICollection<SearchProperty> SearchProperties { get; set; }
    }
}
