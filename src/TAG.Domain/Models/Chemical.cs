﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was named Chemdata in old system
    /// </summary>
    public class Chemical : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual long CASNumber { get; set; }

        /// <summary>
        /// Was ChemCAS No in old system
        /// </summary>
        public virtual string CASNumberFormatted => $"{CASNumber:#####00-00-0}";

        /// <summary>
        /// Was Toxicant No in old system. Refers to 
        /// Toxicant table in old system.
        /// </summary>
        public virtual Toxicant Toxicant { get; set; }

        /// <summary>
        /// Was Chemical Name in old system.
        /// </summary>
        public virtual string Name { get; set; }

        public virtual string CommonName { get; set; }

        /// <summary>
        /// Was ChemID in old system. Refers to 
        /// Chemtype table in old system.
        /// </summary>
        public virtual ChemicalType ChemicalType { get; set; }

        public virtual string SpecificGravity { get; set; }
        
        /// <summary>
        /// Solubility (H2O)(mg/L)
        /// </summary>
        public virtual string Solubility { get; set; }
        
        /// <summary>
        /// Boiling Point (degrees Celcius)
        /// </summary>
        public virtual string BoilingPoint { get; set; }

        /// <summary>
        /// Melting Point (degrees Celcius)
        /// </summary>
        public virtual string MeltingPoint { get; set; }

        /// <summary>
        /// Flash Point (degrees Celcius)
        /// </summary>
        public virtual string FlashPoint { get; set; }

        /// <summary>
        /// Vapour Pressure (mm Hg)
        /// </summary>
        public virtual string VapourPressure { get; set; }

        /// <summary>
        /// Soil organic carbon-water partition coefficient
        /// </summary>
        public virtual string LogKoc { get; set; }

        /// <summary>
        /// Log octanol-water partition coefficient
        /// </summary>
        public virtual string LogKow { get; set; }

        /// <summary>
        /// Log bioconcentration factor
        /// </summary>
        public virtual string LogBCF { get; set; }

        public virtual string MolecularWeight { get; set; }
        public virtual string Formulae { get; set; }
        public virtual string HalfLifeWater { get; set; }
        public virtual string HalfLifeSediment { get; set; }

        public virtual ChemicalReference ReferenceSolubility { get; set; }
        public virtual ChemicalReference ReferenceBoilingPoint { get; set; }
        public virtual ChemicalReference ReferenceMeltingPoint { get; set; }
        public virtual ChemicalReference ReferenceVapourPressure { get; set; }
        public virtual ChemicalReference ReferenceKoc { get; set; }
        public virtual ChemicalReference ReferenceKow { get; set; }
        public virtual ChemicalReference ReferenceMolecularWeight { get; set; }
        public virtual ChemicalReference ReferenceFormulae { get; set; }
        public virtual ChemicalReference ReferenceBCF { get; set; }
        public virtual ChemicalReference ReferenceHalfLifeWater { get; set; }
        public virtual ChemicalReference ReferenceHalfLifeSediment { get; set; }
        public virtual ChemicalReference ReferenceSpecificGravity { get; set; }
    }
}
