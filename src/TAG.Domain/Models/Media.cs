﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was TblMedia table in old system.
    /// </summary>
    public class Media : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }

        /// <summary>
        /// Was Media TypeID in old system. Referred to
        /// TblMediaType table in old system.
        /// </summary>
        public virtual MediaType MediaType { get; set; }
    }
}
