﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was Refdata table in old system
    /// </summary>
    public class Reference : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual int? OrgRefNumber { get; set; }
        public virtual string Authors { get; set; }
        public virtual string AuthorsAbbreviated { get; set; }
        public virtual string Title { get; set; }
        public virtual string Journal { get; set; }
        public virtual int? Year { get; set; }
        public virtual int? Volume { get; set; }
        public virtual int? IssueNumber { get; set; }
        public virtual int? FirstPage { get; set; }
        public virtual int? LastPage { get; set; }
    }
}
