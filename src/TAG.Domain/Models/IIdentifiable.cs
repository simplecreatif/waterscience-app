﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Models
{
    public interface IIdentifiable
    {
        int Id { get; }
    }
}
