﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was the guideline derivation spreadsheets used
    /// in 2016
    /// </summary>
    public class ToxicityValue2016 : ToxicityValue
    {
        public virtual string Record { get; set; }
        public virtual string DataSource { get; set; }
        
        /// <summary>
        /// Was Media Type in guideline derivation spreadsheet
        /// </summary>
        public virtual MediaType MediaType { get; set; }

        // Not retaining "Type of Organism"
        // it is equivalent to AnimalType on Species.

        /// <summary>
        /// Was Life Stage in guideline derivation spreadsheet
        /// </summary>
        public virtual Age Age { get; set; }

        /// <summary>
        /// Was Endpoint (Directly from Paper) in guideline
        /// derivation spreadsheet.
        /// </summary>
        public virtual Endpoint EndpointFromPaper { get; set; }

        /// <summary>
        /// Was Endpoint Measurement in guideline derivation
        /// spreadsheet.
        /// </summary>
        public virtual Endpoint EndpointMeasurement { get; set; }

        /// <summary>
        /// True if Chronic, False if Acute.
        /// </summary>
        public virtual bool IsChronic { get; set; }

        /// <summary>
        /// Concentration after converting to ug/L, NOEC, Chronic.
        /// </summary>
        public virtual decimal ConcentrationUsed { get; set; }

        /// <summary>
        /// A group which several related toxicity Values
        /// belong to. Each toxicity Value in a guideline
        /// group is processed (transformed, filtered, reduced)
        /// to create a final guideline value for that group of
        /// Values.
        /// </summary>
        public virtual GuidelineGroup GuidelineGroup { get; set; }
    }
}
