﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was the toxdata table in the 2000 Australian
    /// ecotoxicology database
    /// </summary>
    public class ToxicityValueAED : ToxicityValue
    {
        /// <summary>
        /// "Identifyer" (sic) in old system
        /// </summary>
        public virtual string Identifier { get; set; }

        /// <summary>
        /// Was Chemical GradeID in old system. Referred to
        /// ChemicalGrade table in old system.
        /// </summary>
        public virtual ChemicalGrade ChemicalGrade { get; set; }

        public virtual string ChemicalGradeValue { get; set; }

        /// <summary>
        /// Was Chemical Form ID in old system. Referred to
        /// Chemicalform table in old system.
        /// </summary>
        public virtual ChemicalForm ChemicalForm { get; set; }

        public virtual bool IsSolventUsed { get; set; }
        public virtual string SolventName { get; set; }

        /// <summary>
        /// Conc of Solvent in old system
        /// </summary>
        public virtual string SolventConcentration { get; set; }

        /// <summary>
        /// Was Solvent UnitID in old system. Referred to
        /// SolventUnit table in old system.
        /// </summary>
        public virtual ConcentrationUnit SolventUnit { get; set; }

        public virtual bool SolventControl { get; set; }

        /// <summary>
        /// SpStrain in old system
        /// </summary>
        public virtual string SpeciesStrain { get; set; }
        
         /// <summary>
         /// Age/Stage in old system
         /// </summary>
         public virtual string SpeciesAgeOrStage { get; set; }

        /// <summary>
        /// AbrAge in old system. Referred to
        /// TblAgecode table in old system.
        /// </summary>
        public virtual Age Age { get; set; }

        public virtual string Feeding { get; set; }

        /// <summary>
        /// MediaID in old toxicity system. Referred to
        /// TblMedia table in old toxicity system.
        /// </summary>
        public virtual Media Media { get; set; }

        /// <summary>
        /// Test ID in old system. Referred to
        /// Testform table in old systme.
        /// </summary>
        public virtual TestForm TestForm { get; set; }

        /// <summary>
        /// Was MethodID in old system. Referred to
        /// Method table in old system.
        /// </summary>
        public virtual ExposureType ExposureType { get; set; }

        public virtual string LowerConfidenceLimit95 { get; set; }
        public virtual string UpperConfidenceLimit95 { get; set; }

        /// <summary>
        /// Was Measure of Error in old system. Referred to
        /// TblError table in old system.
        /// </summary>
        public virtual Error ErrorMeasure { get; set; }

        public virtual int? ErrorValue { get; set; }

        public virtual bool DoseResponse { get; set; }

        /// <summary>
        /// Was StatsID in old system. Referred to
        /// Stats table in old system.
        /// </summary>
        public virtual Statistic Statistic { get; set; }

        public virtual string SignificanceLevel { get; set; }

        public virtual bool? Power { get; set; }

        /// <summary>
        /// Was ReplicationID in old system. Referred to
        /// Replication table in old system.
        /// </summary>
        public virtual Replication Replication { get; set; }

        /// <summary>
        /// Was TemporalID in old system. Referred to
        /// Temporal table in old system.
        /// </summary>
        public virtual TemporalReplication TemporalReplication { get; set; }

        /// <summary>
        /// Was ReftoxID in old system.
        /// </summary>
        public virtual bool HasRefTox { get; set; }
        public virtual string RefToxName { get; set; }

        public virtual bool HasControlEffect { get; set; }
        public virtual string ControlEffect { get; set; }

        public virtual string InvalidationCriteria { get; set; }

        /// <summary>
        /// Percent loss of chemical
        /// </summary>
        public virtual string LossOfChemical { get; set; }
        public virtual string ChemicalFormUsed { get; set; }

        /// <summary>
        /// Was ConcID in old system. Referred to
        /// Conctype table in old system.
        /// </summary>
        public virtual ConcentrationType ConcentrationType { get; set; }
        
        public virtual bool IsPhysicochemMeasured { get; set; }

        /// <summary>
        /// Temperature (degrees Celcius)
        /// </summary>
        public virtual string Temperature { get; set; }
        public virtual string PH { get; set; }

        /// <summary>
        /// Conductivity (uS/Cm)
        /// </summary>
        public virtual string Conductivity { get; set; }
        public virtual string Hardness { get; set; }
        public virtual string Salinity { get; set; }
        public virtual string Alkalinity { get; set; }

        /// <summary>
        /// Organic Carbon (mg/L)
        /// </summary>
        public virtual string OrganicCarbon { get; set; }
        public virtual string LightRegime { get; set; }
        public virtual string OtherParameters { get; set; }

        /// <summary>
        /// Was ReferenceNo in old system. Referred to
        /// Refdata table in old system.
        /// </summary>
        public virtual Reference Reference { get; set; }
        public virtual string MethodReference { get; set; }

        /// <summary>
        /// Was denormalised string field in old system.
        /// </summary>
        public virtual Country Country { get; set; }
        public virtual string SiteLocation { get; set; }
        public virtual bool HardCopy { get; set; }

        /// <summary>
        /// Was LanguageID in old system. Referred to
        /// Language table in old system.
        /// </summary>
        public virtual PublicationLanguage PublicationLanguage { get; set; }
        public virtual string Comments { get; set; }
        public virtual int? QualityScore { get; set; }
    }
}
