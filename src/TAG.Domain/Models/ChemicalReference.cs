﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was Chemref table in old guidelines system
    /// </summary>
    public class ChemicalReference : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string Authors { get; set; }
        public virtual string Title { get; set; }
        public virtual string Journal { get; set; }
        public virtual int? Year { get; set; }
        public virtual string Publisher { get; set; }
        public virtual int? Volume { get; set; }
        public virtual int? IssueNumber { get; set; }
        public virtual int FirstPage { get; set; }
        public virtual int LastPage { get; set; }
    }
}
