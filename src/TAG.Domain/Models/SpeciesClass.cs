﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was Class column in guideline derivation spreadsheets.
    /// A SpeciesClass belongs to a SpeciesPhylum.
    /// </summary>
    public class SpeciesClass : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual SpeciesPhylum Phylum { get; set; }
    }
}
