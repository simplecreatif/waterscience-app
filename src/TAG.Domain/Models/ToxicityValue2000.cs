﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Was the toxdata table in the 2000 water quality
    /// guidelines database
    /// </summary>
    public class ToxicityValue2000 : ToxicityValue
    {
        /// <summary>
        /// Was TestMediaID in old system. Referred
        /// to Testmedia table in old system.
        /// </summary>
        public virtual MediaType MediaType { get; set; }

        public virtual TestType TestType { get; set; }

        /// <summary>
        /// Was EndpointID(U) in old guidelines db.
        /// Referred to Endpoint table. Should really
        /// refer to Effect table as Endpoint / Effect tables
        /// are incorrectly named (they are named Effect / Endpoint
        /// in the toxicology db which is correct)
        /// </summary>
        public virtual Effect EffectUsed { get; set; }

        /// <summary>
        /// Was MethodID in old system. Referred to
        /// Method table
        /// </summary>
        public virtual ExposureType ExposureType { get; set; }
        
        public virtual decimal? ConcentrationUsed { get; set; }
        public virtual ConcentrationCode ConcentrationCode { get; set; }

        /// <summary>
        /// Concentration converted to NOEC effect type>?
        /// </summary>
        public virtual decimal? ConvNOEC { get; set; }
        public virtual decimal? HardnessCorrectedConc { get; set; }

        /// <summary>
        /// Was ConcID in old system. Referred to
        /// Conctype table
        /// </summary>
        public virtual ConcentrationType ConcentrationType { get; set; }
        
        public virtual string Temperature { get; set; }

        public virtual string PH { get; set; }

        // Note we are not carrying over PHID column or table
        // to new system as it's redundant.

        public virtual string Hardness { get; set; }

        public virtual string Salinity { get; set; }

        public virtual Reference Reference { get; set; }

        /// <summary>
        /// Was StatusID in old system. Referred to
        /// Status table
        /// </summary>
        public virtual Status Status { get; set; }

        /// <summary>
        /// Total Ammonia as mg
        /// </summary>
        public virtual decimal? TotalAmmoniaMg { get; set; }

        /// <summary>
        /// Total Ammonia pH8 (mg N/L)
        /// </summary>
        public virtual decimal? TotalAmmoniaPh8 { get; set; }
    }
}
