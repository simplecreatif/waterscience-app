﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Inorganic, metals, etc.
    /// Was Chemtype table in old system.
    /// </summary>
    public class ChemicalType : IIdentifiable
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Class { get; set; }
    }
}
