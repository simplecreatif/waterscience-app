﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Models
{
    /// <summary>
    /// Base class for shared properties between
    /// ToxicityValueAED, ToxicityValue2000, and ToxicityValue2016
    /// </summary>
    public class ToxicityValue : IIdentifiable
    {
        public virtual int Id { get; set; }

        /// <summary>
        /// Was CASNumber in old system. Referred to
        /// Chemdata table in old system.
        /// </summary>
        public virtual Chemical Chemical { get; set; }

        /// <summary>
        /// Was Species Scientific Name in guideline
        /// derivation spreadsheet
        /// </summary>
        public virtual Species Species { get; set; }

        /// <summary>
        /// DurationID in old toxicology system. Referred to
        /// Duration table in old toxicology system. Used in
        /// combination with DurationUnit.
        /// 
        /// Was Exposure Duration in guideline derivation
        /// spreadsheet
        /// 
        /// Was DurationID in old guideline db. Referred
        /// to Duration table. Presumably in hours
        /// </summary>
        public virtual Duration Duration { get; set; }

        /// <summary>
        /// DurationUnitID in old toxicology system. Referred to
        /// Durationunit table in old toxicology system.
        /// 
        /// Was Exposure Duration Units in guideline derivation
        /// spreadsheet
        /// </summary>
        public virtual DurationUnit DurationUnit { get; set; }

        /// <summary>
        /// DurHourID in old toxicology system. Referred to
        /// Duration table in old toxicology system. Appears to
        /// be duration which is assumed to be in hours.
        /// </summary>
        public virtual Duration DurationHour { get; set; }

        /// <summary>
        /// Was EffectID in old toxicity system. Referred to
        /// Effect table in old toxicity system.
        /// 
        /// Was EndpointID in old guidelines db.
        /// Referred to Endpoint table. Should really
        /// refer to Effect table as Endpoint / Effect tables
        /// are incorrectly named (they are named Effect / Endpoint
        /// in the toxicology db which is correct)
        /// 
        /// Was Toxicity Value in guideline derivation
        /// spreadsheet
        /// </summary>
        public virtual Effect Effect { get; set; }

        /// <summary>
        /// Was EndpointID in old toxicity system. Referred to
        /// Endpoint table in old toxicity system.
        /// 
        /// Was EffectID in old guidelines db. (incorrect naming)
        /// Referred to Effect table
        /// 
        /// Was Endpoint in guidelines spreadsheet
        /// </summary>
        public virtual Endpoint Endpoint { get; set; }

        /// <summary>
        /// Was Concentration in toxicology database.
        /// (nullable)
        /// 
        /// Was ConcentrationReported in guidelines database.
        /// (not nullable)
        /// 
        /// Was Concentration Stated in Paper in guideline
        /// derivation spreadsheet (not nullable)
        /// </summary>
        public virtual string Concentration { get; set; }

        /// <summary>
        /// Was UnitID in toxicology database. Referred to
        /// Unit table. (nullable)
        /// 
        /// Was UnitID in guidelines database. Referred to
        /// Unit table (nullable)
        /// 
        /// Was Units in guideline derivation spreadsheet
        /// (nullable)
        /// </summary>
        public virtual ConcentrationUnit ConcentrationUnit { get; set; }
    }
}
