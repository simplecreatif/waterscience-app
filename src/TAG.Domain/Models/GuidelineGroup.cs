﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Models
{
    public class GuidelineGroup : IIdentifiable
    {
        public virtual int Id { get; set; }

        /// <summary>
        /// Guideline based on the toxicity Values
        /// assigned to this group. Measured in ug/L.
        /// </summary>
        public virtual decimal? GuidelineValue { get; set; }
    }
}
