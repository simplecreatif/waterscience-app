﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class StatisticMap : LookupMap<Statistic>
    {
        public StatisticMap() : base("Statistic") { }
    }
}
