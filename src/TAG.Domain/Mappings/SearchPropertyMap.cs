﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class SearchPropertyMap : LookupMap<SearchProperty>
    {
        public SearchPropertyMap() : base("SearchProperty") { }
    }
}
