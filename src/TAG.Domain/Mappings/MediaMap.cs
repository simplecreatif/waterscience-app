﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class MediaMap : ClassMap<Media>
    {
        public MediaMap()
        {
            Table("Media");
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable().Unique();
            References(x => x.MediaType).Nullable();
        }
    }
}
