﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class ReferenceMap : ClassMap<Reference>
    {
        public ReferenceMap()
        {
            Table("Reference");
            Id(x => x.Id);
            Map(x => x.OrgRefNumber).Nullable();
            Map(x => x.Authors).Not.Nullable();
            Map(x => x.AuthorsAbbreviated).Nullable();
            Map(x => x.Title).Nullable();
            Map(x => x.Journal).Nullable();
            Map(x => x.Year).Nullable();
            Map(x => x.Volume).Nullable();
            Map(x => x.IssueNumber).Nullable();
            Map(x => x.FirstPage).Nullable();
            Map(x => x.LastPage).Nullable();
        }
    }
}
