﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class ChemicalGradeMap : LookupMap<ChemicalGrade>
    {
        public ChemicalGradeMap() : base("ChemicalGrade") { }
    }
}
