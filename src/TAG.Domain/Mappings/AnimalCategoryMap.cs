﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class AnimalCategoryMap : LookupMap<AnimalCategory>
    {
        public AnimalCategoryMap() : base("AnimalCategory") { }
    }
}
