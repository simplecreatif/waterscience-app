﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class EffectMap : LookupMap<Effect>
    {
        public EffectMap() : base("Effect") { }
    }
}
