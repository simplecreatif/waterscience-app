﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class AgeMap : LookupMap<Age>
    {
        public AgeMap() : base("Age") { }
    }
}
