﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class SpeciesPhylumMap : LookupMap<SpeciesPhylum>
    {
        public SpeciesPhylumMap() : base("SpeciesPhylum") { }
    }
}
