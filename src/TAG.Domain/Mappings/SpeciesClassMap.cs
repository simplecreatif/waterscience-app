﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    /// <summary>
    /// This is a Map for the Class domain entity.
    /// </summary>
    public class SpeciesClassMap : ClassMap<SpeciesClass>
    {
        public SpeciesClassMap()
        {
            Table("SpeciesClass");
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable().Unique();
            References(x => x.Phylum).Not.Nullable();
        } 
    }
}
