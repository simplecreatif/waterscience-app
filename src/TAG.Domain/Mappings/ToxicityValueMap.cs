﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class ToxicityValueMap : ClassMap<ToxicityValue>
    {
        public ToxicityValueMap()
        {
            Table("ToxicityValue");
            Id(x => x.Id);
            References(x => x.Chemical).Not.Nullable();
            References(x => x.Species).Not.Nullable();
            References(x => x.Duration).Nullable();
            References(x => x.DurationUnit).Nullable();
            References(x => x.DurationHour).Nullable();
            References(x => x.Effect).Nullable();
            References(x => x.Endpoint).Nullable();
            Map(x => x.Concentration).Nullable();
            References(x => x.ConcentrationUnit).Nullable();
        }
    }
}
