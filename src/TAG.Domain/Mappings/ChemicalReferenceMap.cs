﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class ChemicalReferenceMap : ClassMap<ChemicalReference>
    {
        public ChemicalReferenceMap()
        {
            Table("ChemicalReference");
            Id(x => x.Id);
            Map(x => x.Authors).Not.Nullable();
            Map(x => x.Title).Nullable();
            Map(x => x.Journal).Nullable();
            Map(x => x.Year).Nullable();
            Map(x => x.Publisher).Nullable();
            Map(x => x.Volume).Nullable();
            Map(x => x.IssueNumber).Nullable();
            Map(x => x.FirstPage).Not.Nullable();
            Map(x => x.LastPage).Not.Nullable();
        }
    }
}
