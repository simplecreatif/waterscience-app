﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class ToxicityValueAEDMap : SubclassMap<ToxicityValueAED>
    {
        public ToxicityValueAEDMap()
        {
            Table("ToxicityValueAED");
            Map(x => x.Identifier).Not.Nullable();
            References(x => x.ChemicalGrade).Nullable();
            Map(x => x.ChemicalGradeValue).Nullable();
            References(x => x.ChemicalForm).Nullable();
            Map(x => x.IsSolventUsed).Not.Nullable();
            Map(x => x.SolventName).Nullable();
            Map(x => x.SolventConcentration).Nullable();
            References(x => x.SolventUnit).Nullable();
            Map(x => x.SolventControl).Nullable();
            Map(x => x.SpeciesStrain).Nullable();
            Map(x => x.SpeciesAgeOrStage).Nullable();
            References(x => x.Age).Nullable();
            Map(x => x.Feeding).Nullable();
            References(x => x.Media).Nullable();
            References(x => x.TestForm).Nullable();
            References(x => x.ExposureType).Nullable();
            Map(x => x.LowerConfidenceLimit95).Nullable();
            Map(x => x.UpperConfidenceLimit95).Nullable();
            References(x => x.ErrorMeasure).Nullable();
            Map(x => x.ErrorValue).Nullable();
            Map(x => x.DoseResponse).Not.Nullable();
            References(x => x.Statistic).Nullable();
            Map(x => x.SignificanceLevel).Nullable();
            Map(x => x.Power).Nullable();
            References(x => x.Replication).Nullable();
            References(x => x.TemporalReplication).Nullable();
            Map(x => x.HasRefTox).Not.Nullable();
            Map(x => x.RefToxName).Nullable();
            Map(x => x.HasControlEffect).Not.Nullable();
            Map(x => x.ControlEffect).Nullable();
            Map(x => x.InvalidationCriteria).Nullable();
            Map(x => x.LossOfChemical).Nullable();
            Map(x => x.ChemicalFormUsed).Nullable();
            References(x => x.ConcentrationType).Nullable();
            Map(x => x.IsPhysicochemMeasured).Not.Nullable();
            Map(x => x.Temperature).Nullable();
            Map(x => x.PH).Nullable();
            Map(x => x.Conductivity).Nullable();
            Map(x => x.Hardness).Nullable();
            Map(x => x.Salinity).Nullable();
            Map(x => x.Alkalinity).Nullable();
            Map(x => x.OrganicCarbon).Nullable();
            Map(x => x.LightRegime).Nullable();
            Map(x => x.OtherParameters).Nullable();
            References(x => x.Reference).Nullable();
            Map(x => x.MethodReference).Nullable();
            References(x => x.Country).Nullable();
            Map(x => x.SiteLocation).Nullable();
            Map(x => x.HardCopy).Not.Nullable();
            References(x => x.PublicationLanguage).Nullable();
            Map(x => x.Comments).Nullable();
            Map(x => x.QualityScore).Nullable();
        }
    }
}
