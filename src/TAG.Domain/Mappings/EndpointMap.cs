﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class EndpointMap : ClassMap<Endpoint>
    {
        public EndpointMap()
        {
            Table("Endpoint");
            Id(x => x.Id);
            Map(x => x.Name).Nullable();
            Map(x => x.Abbreviation).Not.Nullable().Unique();
        }
    }
}
