﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class StatusMap : LookupMap<Status>
    {
        public StatusMap() : base("Status") { }
    }
}
