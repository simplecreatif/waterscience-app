﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class ConcentrationTypeMap : LookupMap<ConcentrationType>
    {
        public ConcentrationTypeMap() : base("ConcentrationType") { }
    }
}
