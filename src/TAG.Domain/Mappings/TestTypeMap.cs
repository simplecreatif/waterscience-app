﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class TestTypeMap : LookupMap<TestType>
    {
        public TestTypeMap() : base("TestType") { }
    }
}
