﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class ExposureTypeMap : ClassMap<ExposureType>
    {
        public ExposureTypeMap()
        {
            Table("ExposureType");
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable().Unique();
            Map(x => x.Abbreviation).Nullable();
        }
    }
}
