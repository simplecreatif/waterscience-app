﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class MediaTypeMap : LookupMap<MediaType>
    {
        public MediaTypeMap() : base("MediaType") { }
    }
}
