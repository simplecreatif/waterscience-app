﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class CountryMap : LookupMap<Country>
    {
        public CountryMap() : base("Country") { }
    }
}
