﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class TestFormMap : LookupMap<TestForm>
    {
        public TestFormMap() : base("TestForm") { }
    }
}
