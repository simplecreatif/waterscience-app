﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class ToxicantMap : LookupMap<Toxicant>
    {
        public ToxicantMap() : base("Toxicant") { }
    }
}
