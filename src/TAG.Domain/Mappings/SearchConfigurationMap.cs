﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class SearchConfigurationMap : ClassMap<SearchConfiguration>
    {
        public SearchConfigurationMap()
        {
            Table("SearchConfiguration");
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable();
            Map(x => x.CASNumber).Nullable();
            References(x => x.Chemical).Nullable();
            References(x => x.Toxicant).Nullable();
            References(x => x.User).Not.Nullable();
            HasManyToMany(x => x.Species).AsSet();
            HasManyToMany(x => x.Effects).AsSet();
            HasManyToMany(x => x.Endpoints).AsSet();
            HasManyToMany(x => x.Databases).AsSet();
            HasManyToMany(x => x.SearchProperties).AsSet();
        }
    }
}
