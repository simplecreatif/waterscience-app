﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class DurationUnitMap : LookupMap<DurationUnit>
    {
        public DurationUnitMap() : base("DurationUnit") { }
    }
}
