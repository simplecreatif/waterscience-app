﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class AnimalTypeMap : LookupMap<AnimalType>
    {
        public AnimalTypeMap() : base("AnimalType") { }
    }
}
