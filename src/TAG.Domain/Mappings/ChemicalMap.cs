﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class ChemicalMap : ClassMap<Chemical>
    {
        public ChemicalMap()
        {
            Table("Chemical");
            Id(x => x.Id);
            Map(x => x.CASNumber).Not.Nullable().Unique();
            References(x => x.Toxicant).Nullable();
            Map(x => x.Name).Nullable();
            Map(x => x.CommonName).Nullable();
            References(x => x.ChemicalType).Nullable();
            Map(x => x.SpecificGravity).Nullable();
            Map(x => x.Solubility).Nullable();
            Map(x => x.BoilingPoint).Nullable();
            Map(x => x.MeltingPoint).Nullable();
            Map(x => x.FlashPoint).Nullable();
            Map(x => x.VapourPressure).Nullable();
            Map(x => x.LogKoc).Nullable();
            Map(x => x.LogKow).Nullable();
            Map(x => x.LogBCF).Nullable();
            Map(x => x.MolecularWeight).Nullable();
            Map(x => x.Formulae).Nullable();
            Map(x => x.HalfLifeWater).Nullable();
            Map(x => x.HalfLifeSediment).Nullable();
            References(x => x.ReferenceSolubility).Nullable();
            References(x => x.ReferenceBoilingPoint).Nullable();
            References(x => x.ReferenceMeltingPoint).Nullable();
            References(x => x.ReferenceVapourPressure).Nullable();
            References(x => x.ReferenceKoc).Nullable();
            References(x => x.ReferenceKow).Nullable();
            References(x => x.ReferenceMolecularWeight).Nullable();
            References(x => x.ReferenceFormulae).Nullable();
            References(x => x.ReferenceBCF).Nullable();
            References(x => x.ReferenceHalfLifeWater).Nullable();
            References(x => x.ReferenceHalfLifeSediment).Nullable();
            References(x => x.ReferenceSpecificGravity).Nullable();
        }
    }
}
