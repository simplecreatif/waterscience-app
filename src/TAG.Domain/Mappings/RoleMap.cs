﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class RoleMap : LookupMap<Role>
    {
        public RoleMap() : base("Role") { }
    }
}
