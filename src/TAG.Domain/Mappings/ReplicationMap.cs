﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class ReplicationMap : LookupMap<Replication>
    {
        public ReplicationMap() : base("Replication") { }
    }
}
