﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class PublicationLanguageMap : LookupMap<PublicationLanguage>
    {
        public PublicationLanguageMap() : base("PublicationLanguage") { }
    }
}
