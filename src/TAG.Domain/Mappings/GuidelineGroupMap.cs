﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class GuidelineGroupMap : ClassMap<GuidelineGroup>
    {
        public GuidelineGroupMap()
        {
            Table("GuidelineGroup");
            Id(x => x.Id);
            Map(x => x.GuidelineValue).Nullable();
        }
    }
}
