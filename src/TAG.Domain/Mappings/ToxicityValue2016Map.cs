﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class ToxicityValue2016Map : SubclassMap<ToxicityValue2016>
    {
        public ToxicityValue2016Map()
        {
            Table("ToxicityValue2016");
            Map(x => x.Record).Not.Nullable();
            Map(x => x.DataSource).Nullable();
            References(x => x.MediaType).Not.Nullable();
            References(x => x.Age).Nullable();
            References(x => x.EndpointFromPaper).Nullable();
            References(x => x.EndpointMeasurement).Nullable();
            Map(x => x.IsChronic).Not.Nullable();
            Map(x => x.ConcentrationUsed).Not.Nullable();
            References(x => x.GuidelineGroup).Not.Nullable();
        }
    }
}
