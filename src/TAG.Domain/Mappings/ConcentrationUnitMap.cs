﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class ConcentrationUnitMap : LookupMap<ConcentrationUnit>
    {
        public ConcentrationUnitMap() : base("ConcentrationUnit") { }
    }
}
