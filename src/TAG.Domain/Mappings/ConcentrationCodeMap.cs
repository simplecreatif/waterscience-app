﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class ConcentrationCodeMap : LookupMap<ConcentrationCode>
    {
        public ConcentrationCodeMap() : base("ConcentrationCode") { }
    }
}
