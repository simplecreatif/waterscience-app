﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class DurationMap : LookupMap<Duration>
    {
        public DurationMap() : base("Duration") { }
    }
}
