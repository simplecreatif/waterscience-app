﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class ToxicityValue2000Map : SubclassMap<ToxicityValue2000>
    {
        public ToxicityValue2000Map()
        {
            Table("ToxicityValue2000");
            References(x => x.MediaType).Nullable();
            References(x => x.TestType).Nullable();
            References(x => x.EffectUsed).Nullable();
            References(x => x.ExposureType).Nullable();
            Map(x => x.ConcentrationUsed).Nullable();
            References(x => x.ConcentrationCode).Not.Nullable();
            Map(x => x.ConvNOEC).Nullable();
            Map(x => x.HardnessCorrectedConc).Nullable();
            References(x => x.ConcentrationType).Nullable();
            Map(x => x.Temperature).Nullable();
            Map(x => x.PH).Nullable();
            Map(x => x.Hardness).Nullable();
            Map(x => x.Salinity).Nullable();
            References(x => x.Reference).Nullable();
            References(x => x.Status).Nullable();
            Map(x => x.TotalAmmoniaMg).Nullable();
            Map(x => x.TotalAmmoniaPh8).Nullable();
        }
    }
}
