﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class LookupMap<T> : ClassMap<T> where T : Lookup
    {
        public LookupMap(string tableName)
        {
            Table(tableName);
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable().Unique();
        } 
    }
}
