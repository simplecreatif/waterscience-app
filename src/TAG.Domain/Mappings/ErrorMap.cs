﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class ErrorMap : LookupMap<Error>
    {
        public ErrorMap() : base("Error") { }
    }
}
