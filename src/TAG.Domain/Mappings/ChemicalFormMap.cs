﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class ChemicalFormMap : LookupMap<ChemicalForm>
    {
        public ChemicalFormMap() : base("ChemicalForm") { }
    }
}
