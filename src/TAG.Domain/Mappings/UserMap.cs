﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("\"user\"");
            Id(x => x.Id);
            Map(x => x.Email).Not.Nullable().Unique();
            Map(x => x.PasswordHash).Not.Nullable();
            References(x => x.Role).Not.Nullable();
            HasMany(x => x.SearchConfigurations)
                .Cascade.All()
                .Inverse();
        }
    }
}
