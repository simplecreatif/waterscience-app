﻿using FluentNHibernate.Mapping;
using TAG.Domain.Models;

namespace TAG.Domain.Mappings
{
    public class SpeciesMap : ClassMap<Species>
    {
        public SpeciesMap()
        {
            Table("Species");
            Id(x => x.Id);
            Map(x => x.ScientificName).Nullable().UniqueKey("ScientificName-CommonName");
            Map(x => x.CommonName).Nullable().UniqueKey("ScientificName-CommonName");
            Map(x => x.Status).Nullable();
            References(x => x.AnimalType).Nullable();
            Map(x => x.MajorGroup).Nullable();
            Map(x => x.MinorGroup).Nullable();
            References(x => x.AnimalCategory).Nullable();
            References(x => x.Class).Nullable();
            Map(x => x.IsHeterotroph).Nullable();
        }
    }
}
