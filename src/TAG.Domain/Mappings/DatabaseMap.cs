﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class DatabaseMap : LookupMap<Database>
    {
        public DatabaseMap() : base("Database") { }
    }
}
