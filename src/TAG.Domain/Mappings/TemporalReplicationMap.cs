﻿using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;

namespace TAG.Domain.Mappings
{
    public class TemporalReplicationMap : LookupMap<TemporalReplication>
    {
        public TemporalReplicationMap() : base("TemporalReplication") { }
    }
}
