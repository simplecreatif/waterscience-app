﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Extensions
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Allows conditional invocation of a Concat operation
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="condition"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> ConcatIf<TSource>(this IEnumerable<TSource> source, bool condition, IEnumerable<TSource> items)
        {
            if (!condition)
            {
                return source;
            }

            return source.Concat(items);
        }
    }
}
