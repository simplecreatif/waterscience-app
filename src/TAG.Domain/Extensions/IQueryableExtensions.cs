﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Domain.Extensions
{
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Allows conditional invocation of a Where clause.
        /// Useful when conditionally combining a number of Where
        /// clauses such as when performing a search with several
        /// optional search parameters.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="condition"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IQueryable<TSource> WhereIf<TSource>(this IQueryable<TSource> source, bool condition, Expression<Func<TSource, bool>> predicate)
        {
            if (!condition)
            {
                return source;
            }

            return source.Where(predicate);
        }
    }
}
