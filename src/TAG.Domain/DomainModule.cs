﻿using System.Reflection;
using Autofac;
using Module = Autofac.Module;

namespace TAG.Domain
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var executingAssembly = Assembly.GetExecutingAssembly();

            // Services
            builder.RegisterAssemblyTypes(executingAssembly)
                .Where(t => t.Namespace != null && t.Namespace.Contains("Services") && t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            // Specifications
            builder.RegisterAssemblyTypes(executingAssembly)
                .Where(t => t.Namespace != null && t.Namespace.Contains("Specifications") && t.Name.EndsWith("Specification"))
                .AsSelf()
                .InstancePerLifetimeScope();
        }
    }
}
