﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Extensions
{
    public static class BoolExtensions
    {
        private static string[] defaultTrueOptions = { "yes", "y" };
        private static string[] defaultFalseOptions = { "no", "n" };

        public static bool? FromOption(string value, string trueOption = null, string falseOption = null)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            var trueOptions = trueOption != null ? new[] {trueOption} : defaultTrueOptions;
            var falseOptions = falseOption != null ? new[] {falseOption} : defaultFalseOptions;

            value = value.Trim().ToLower();

            if (trueOptions.Contains(value))
            {
                return true;
            }

            if (falseOptions.Contains(value))
            {
                return false;
            }

            Logger.Log($"Unknown boolean option '{value}' expected one of '{string.Join(", ", trueOptions.Concat(falseOptions))}'. Assuming value is false.");
            return false;
        }

        public static bool FromOptionRequired(string value, string trueOption, string falseOption)
        {
            var result = FromOption(value, trueOption, falseOption);

            if (!result.HasValue)
            {
                throw new ArgumentException("Expected one of two options but got nothing.");
            }

            return result.Value;
        }
    }
}
