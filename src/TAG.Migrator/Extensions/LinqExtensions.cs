﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TAG.Migrator.Extensions
{
    public static class LinqExtensions
    {
        /// <summary>
        /// Returns distinct elements from a sequence using the specified
        /// function to compare values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="values"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public static IEnumerable<T> DistinctBy<T, V>(this IEnumerable<T> values, Func<T, V> property)
        {
            return values.GroupBy(property).Select(x => x.First()).ToList();
        } 
    }
}
