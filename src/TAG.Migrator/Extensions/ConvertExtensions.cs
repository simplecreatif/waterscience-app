﻿using System;

namespace TAG.Migrator.Extensions
{
    public static class ConvertExtensions
    {
        /// <summary>
        /// Supports changing type to nullable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ChangeType<T>(object value)
        {
            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return value == null ? default(T) : (T)Convert.ChangeType(value, t);
        }
    }
}
