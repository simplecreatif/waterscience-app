﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace TAG.Migrator.Extensions
{
    public static class ReflectionExtensions
    {
        /// <summary>
        /// Assigns a value to a property of the target object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="target"></param>
        /// <param name="memberExpression"></param>
        /// <param name="value"></param>
        public static void SetPropertyValue<T, V>(T target, Expression<Func<T, V>> memberExpression, object value)
        {
            var member = memberExpression.Body as MemberExpression;
            var unary = memberExpression.Body as UnaryExpression;
            var memberSelectorExpression = member ?? (unary != null ? unary.Operand as MemberExpression : null);

            if (memberSelectorExpression != null)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    property.SetValue(target, value, null);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public static V GetPropertyValue<V>(object src, string propName)
        {
            return (V)src.GetType().GetProperty(propName).GetValue(src, null);
        }
    }
}
