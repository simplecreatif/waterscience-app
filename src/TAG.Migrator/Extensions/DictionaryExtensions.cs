﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Migrator.Extensions
{
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Treats a Dictionary of string => string as a remapper
        /// which convers a given string to a canonicalised string.
        /// Useful for quickly generating dictionaries which map common
        /// misspellings to their correct spellings.
        /// 
        /// Lookups performed on the string remapping should be done with
        /// the input value trimmed and in lower case.
        /// </summary>
        /// <param name="stringMappings"></param>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <returns></returns>
        public static Dictionary<string, string> AddStringRemapping(
            this Dictionary<string, string> stringMappings,
            string to, params string[] from)
        {
            foreach (var fromItem in from)
            {
                stringMappings.Add(fromItem.ToLower().Trim(), to);
            }
            return stringMappings;
        }
    }
}
