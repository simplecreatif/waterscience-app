﻿using System;
using System.Linq.Expressions;

namespace TAG.Migrator.Extensions
{
    public static class FuncExtensions
    {
        /// <summary>
        /// Curries an expression by filling in the first argument
        /// https://stackoverflow.com/questions/19151810/how-can-i-curry-an-expression-into-another-expression
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="expression"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Expression<Func<T2, TResult>> ApplyPartial<T1, T2, TResult>(this Expression<Func<T1, T2, TResult>> expression, T1 value)
        {
            var parameter = expression.Parameters[0];
            var constant = Expression.Constant(value, parameter.Type);
            var visitor = new ReplacementVisitor(parameter, constant);
            var newBody = visitor.Visit(expression.Body);
            return Expression.Lambda<Func<T2, TResult>>(newBody, expression.Parameters[1]);
        }

        private class ReplacementVisitor : ExpressionVisitor
        {
            private readonly Expression original, replacement;

            public ReplacementVisitor(Expression original, Expression replacement)
            {
                this.original = original;
                this.replacement = replacement;
            }

            public override Expression Visit(Expression node)
            {
                return node == original ? replacement : base.Visit(node);
            }
        }
    }
}
