﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TAG.Migrator.Extensions
{
    public static class DapperExtensions
    {
        /// <summary>
        /// Gets the n'th column of a dynamic dapper object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dapperRow"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static T GetValue<T>(dynamic dapperRow, int index)
        {
            object value = ((IDictionary<string, object>)dapperRow).Values.Skip(index).Take(1).Single();

            return ConvertExtensions.ChangeType<T>(value);
        }

        /// <summary>
        /// Gets a value from a dynamic dapper object. Supports
        /// getting values for columns that have spaces in their names.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dapperRow"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static T GetValue<T>(dynamic dapperRow, string columnName)
        {
            object value = GetValue(dapperRow, columnName);

            return ConvertExtensions.ChangeType<T>(value);
        }

        /// <summary>
        /// Gets a value from a dynamic dapper object and throws an
        /// exception if the column name does not exist.
        /// </summary>
        /// <param name="dapperRow"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        private static object GetValue(dynamic dapperRow, string columnName)
        {
            IDictionary<string, object> dict = ((IDictionary<string, object>) dapperRow);

            if (!dict.ContainsKey(columnName))
            {
                throw new ArgumentException("Tried to get unknown column from a dapper entity: " + columnName);
            }

            return dict[columnName];
        }
    }
}
