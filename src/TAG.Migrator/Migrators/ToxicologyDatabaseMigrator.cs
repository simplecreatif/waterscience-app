﻿using System;
using System.Data;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Extensions;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.Common;
using TAG.Persistence;
using TAG.Migrator.Migrators.ToxicologyDatabase;

namespace TAG.Migrator.Migrators
{
    /// <summary>
    /// Migrates toxicology database to TAG
    /// </summary>
    public class ToxicologyDatabaseMigrator : DatabaseMigrator
    {
        public ToxicologyDatabaseMigrator(IRepository repository, IDbConnection conn) : base(repository, conn)
        {
        }

        public void Migrate()
        {
            Logger.LogHeading("Migrating Toxicology Database");

            MigrateAnimalType.Migrate(this);
            MigrateLookup<ChemicalForm>("Chemicalform");
            MigrateLookup<ChemicalGrade>("ChemicalGrade");
            MigrateLookup<ConcentrationType>("Conctype");
            MigrateLookup<Duration>("Duration");
            MigrateLookup<DurationUnit>("Durationunit");
            MigrateLookup<Effect>("Effect");
            MigrateLookup<PublicationLanguage>("Language");
            MigrateLookup<Replication>("Replication");
            // Not migrated. Duplicate of Unit.
            //MigrateLookup<ConcentrationUnit>("SolventUnit");
            MigrateLookup<Statistic>("Stats");
            MigrateStatus.Migrate(this);
            MigrateLookup<Age>("TblAgecode");
            MigrateLookup<Error>("TblError");
            MigrateLookup<MediaType>("TblMediaType");
            MigrateLookup<TemporalReplication>("Temporal");
            MigrateLookup<TestForm>("Testform");
            MigrateLookup<TestType>("Testtype");
            MigrateLookup<Toxicant>("Toxicant", capitalise: false);
            MigrateLookup<ConcentrationUnit>("Unit", capitalise: false);
            MigrateLookup<AnimalCategory>("WQCateogary");
            MigrateCountry.Migrate(this);
            MigrateChemicalType.Migrate(this);
            MigrateChemical.Migrate(this);
            MigrateEndpoint.Migrate(this);
            MigrateExposureType.Migrate(this);
            MigrateReference.Migrate(this);
            MigrateSpecies.Migrate(this);
            MigrateMedia.Migrate(this);
            MigrateToxicityValue.Migrate(this);
        }
    }
}
