﻿using System.Data;
using System.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Extensions;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.Common;
using TAG.Migrator.Migrators.GuidelineDatabase;
using TAG.Persistence;
using MigrateEndpoint = TAG.Migrator.Migrators.GuidelineDatabase.MigrateEndpoint;

namespace TAG.Migrator.Migrators
{
    /// <summary>
    /// Migrates guidelines database to TAG
    /// </summary>
    public class GuidelineDatabaseMigrator : DatabaseMigrator
    {
        public GuidelineDatabaseMigrator(IRepository repository, IDbConnection conn) : base(repository, conn)
        {
        }

        public void Migrate()
        {
            Logger.LogHeading("Migrating Guideline Database");

            MigrateLookup<AnimalType>("Animaltype");
            MigrateChemicalReference.Migrate(this);
            MigrateLookup<ConcentrationCode>("ConcCode");
            MigrateLookup<ConcentrationType>("Conctype");
            MigrateLookup<Duration>("Duration");
            MigrateEndpoint.Migrate(this);
            MigrateLookup<Effect>("Endpoint");
            MigrateExposureType.Migrate(this);
            MigrateStatus.Migrate(this);
            // TblCategory not migrated - not in use, WQcateogary is used to populate AnimalCategory
            MigrateLookup<MediaType>("Testmedia");
            MigrateLookup<TestType>("Testtype");
            MigrateLookup<Toxicant>("Toxicant", capitalise: false);
            MigrateLookup<ConcentrationUnit>("Unit", capitalise: false);
            MigrateLookup<AnimalCategory>("WQcateogary");
            MigrateChemicalType.Migrate(this);
            MigrateChemical.Migrate(this);
            MigrateReference.Migrate(this, false);
            MigrateSpecies.Migrate(this);
            MigrateToxicityValue.Migrate(this);
        }
    }
}
