﻿using System;
using System.Collections.Generic;
using System.Linq;
using OfficeOpenXml.FormulaParsing;
using OfficeOpenXml.FormulaParsing.Excel.Functions;
using OfficeOpenXml.FormulaParsing.ExpressionGraph;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Excel
{
    /// <summary>
    /// Implementation of excel geomean function for guideline spreadsheet importing
    /// </summary>
    class Geomean : ExcelFunction
    {
        public override CompileResult Execute(IEnumerable<FunctionArgument> arguments, ParsingContext context)
        {
            // Ensure at least one input value has been provided
            ValidateArguments(arguments, 1);
            var values = ArgsToDoubleEnumerable(arguments, context).ToList();

            // geomean is n'th root of (y1*y2*y3...*yn)
            var geomean = NthRoot(values.Aggregate(1.0, (x, y) => x * y), values.Count);
            return CreateResult(geomean, DataType.Decimal);
        }

        private double NthRoot(double value, int n)
        {
            return Math.Pow(value, 1.0 / n);
        }
    }
}
