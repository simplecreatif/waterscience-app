﻿using System.Collections.Generic;
using OfficeOpenXml.FormulaParsing.Excel.Functions;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Excel
{
    /// <summary>
    /// Glue class to integrate our geomean implementation with
    /// the EPPlus excel library
    /// </summary>
    class CustomFunctionsModule : IFunctionModule
    {
        public IDictionary<string, ExcelFunction> Functions { get; }

        public CustomFunctionsModule()
        {
            Functions = new Dictionary<string, ExcelFunction>
            {
                { "geomean", new Geomean() }
            };
        }
    }
}
