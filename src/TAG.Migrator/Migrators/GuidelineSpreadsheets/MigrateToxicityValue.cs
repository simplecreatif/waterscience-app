﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets
{
    public static class MigrateToxicityValue
    {
        public static void Migrate(GuidelineSpreadsheetsMigrator x, ToxicityValueGroup group)
        {
            var guidelineGroup = new GuidelineGroup {GuidelineValue = group.GuidelineValue};
            x.Repository.Add(guidelineGroup);

            // ToxicityValue2016
            // todo: find a natural key for this data
            foreach (var model in group)
            {
                var newEntity = new ToxicityValue2016
                {
                    Chemical = x.Mappers.Get<Chemical>(model.CASNumber.ToString()),
                    Record = model.Record,
                    DataSource = model.DataSource,
                    MediaType = x.Mappers.Get<MediaType>(model.MediaType),
                    Species = x.Mappers.Get<Species>(model.SpeciesScientificName),
                    Age = x.Mappers.Get<Age>(model.LifeStage),
                    EndpointFromPaper = model.EndpointFromPaper != null ? x.Mappers.Get<Endpoint>(model.EndpointFromPaper) : null,
                    Endpoint = model.Endpoint != null ? x.Mappers.Get<Endpoint>(model.Endpoint) : null,
                    EndpointMeasurement = x.Mappers.Get<Endpoint>(model.EndpointMeasurement),
                    Effect = x.Mappers.Get<Effect>(model.Effect),
                    Duration = x.Mappers.Get<Duration>(model.Duration),
                    DurationUnit = x.Mappers.Get<DurationUnit>(model.DurationUnits),
                    IsChronic = model.IsChronic,
                    Concentration = StringRemapper.Remap(model.Concentration),
                    ConcentrationUnit = x.Mappers.Get<ConcentrationUnit>(model.ConcentrationUnit),
                    ConcentrationUsed = model.ConcentrationUsed.Value,

                    GuidelineGroup = guidelineGroup
                };

                x.Repository.Add(newEntity);
                Logger.LogMigratingEntity(model.RowNumber.ToString(), newEntity);
            }
        }
    }
}
