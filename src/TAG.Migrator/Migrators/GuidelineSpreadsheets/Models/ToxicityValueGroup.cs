﻿using System.Collections.Generic;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Models
{
    /// <summary>
    /// Represents a group of toxicity values used to derive
    /// a single guideline value
    /// </summary>
    public class ToxicityValueGroup : List<ToxicityValueModel>
    {
        public decimal? GuidelineValue { get; set; }
    }
}
