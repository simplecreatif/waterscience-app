﻿namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Models
{
    /// <summary>
    /// Represents one row of toxicity data in a guideline
    /// derivation spreadsheet
    /// </summary>
    public class ToxicityValueModel
    {
        public int RowNumber { get; set; }

        public string Chemical { get; set; }
        public int CASNumber { get; set; }
        public string Toxicant { get; set; }

        public string Record { get; set; }
        public string DataSource { get; set; }
        public string MediaType { get; set; }
        public string SpeciesScientificName { get; set; }
        public string Phylum { get; set; }
        public string Class { get; set; }
        public string OrganismType { get; set; }
        public bool? IsHeterotroph { get; set; }
        public string LifeStage { get; set; }
        public string EndpointFromPaper { get; set; }
        public string Endpoint { get; set; }
        public string EndpointMeasurement { get; set; }
        public string Effect { get; set; }
        public string Duration { get; set; }
        public string DurationUnits { get; set; }
        public bool IsChronic { get; set; }
        public string Concentration { get; set; }
        public string ConcentrationUnit { get; set; }

        /// <summary>
        /// Chronic NEC/EC10/NOEC Concentration (ug/L)
        /// </summary>
        public decimal? ConcentrationUsed { get; set; }

        /// <summary>
        /// If set, this is probably the guideline
        /// value for this toxicity value's group.
        /// </summary>
        public decimal? LowestGuidelineValueForSpecies { get; set; }

        /// <summary>
        /// If set, either this record's guideline value
        /// is the guideline value for the group, or a single
        /// other record in this group has the guideline value.
        /// </summary>
        public bool UseThisGuidelineValue { get; set; }
    }
}
