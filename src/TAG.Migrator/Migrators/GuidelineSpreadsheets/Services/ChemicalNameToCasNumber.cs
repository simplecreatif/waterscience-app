﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Services
{
    public static class ChemicalNameToCasNumber
    {
        public static IEnumerable<string> KnownChemicals => ChemicalNameToCASNumber.Keys;
        /// <summary>
        /// Chemical name to CAS Number mappings for guideline spreadsheets
        /// see: http://www.commonchemistry.org/
        /// </summary>
        private static readonly Dictionary<string, int> ChemicalNameToCASNumber = new Dictionary<string, int>
        {
            // Unless specified otherwise, CAS Numbers are from Wikipedia

            { "Glyphosate", 1071836 }, // http://www.commonchemistry.org/search.aspx?terms=glyphosate
            { "Hexazinone", 51235042 },
            { "Tebuthiuron", 34014181 },
            { "Ametryn", 834128 }, // http://www.commonchemistry.org/ChemicalDetail.aspx?ref=834-12-8&terms=ametryn
            { "Diuron", 330541 }, // https://en.wikipedia.org/wiki/DCMU
            { "2,4-D", 94757 },
            { "Bromacil", 314409 },
            { "Chlorothalonil", 1897456 },
            { "Fipronil", 120068373 },
            { "Fluometuron", 2164172 },
            { "Prometryn", 7287196 }, // http://www.scbt.com/datasheet-250779-prometryn.html
            { "Imazapic", 104098488 }, // http://www.scbt.com/datasheet-224022-imazapic.html remap Imazapic FW & SW
            { "Imidacloprid", 138261413 },
            { "Isoxaflutole", 141112290 },  // http://www.scbt.com/datasheet-228380-isoxaflutole.html remap Isoxaflutole Marine
            { "Metolachlor", 51218452 },
            { "Metribuzin", 21087649 },
            { "Metsulfuron-methyl", 74223646 },
            { "Terbutryn", 886500 }, // http://www.scbt.com/datasheet-251121-terbutryn.html
            { "Triclopyr", 55335063 }, //parsing issue expected
            { "Simazine", 122349 },
            { "Fluroxypyr", 69377817 },
            { "Haloxyfop", 69806344 } // https://www3.scbt.com/scbt/product/haloxyfop-69806-34-4
        };

        public static int Map(string chemicalName)
        {
            return ChemicalNameToCASNumber[chemicalName];
        }
    }
}
