﻿using System;
using System.Collections.Generic;
using System.Linq;
using TAG.Migrator.Extensions;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Services
{
    /// <summary>
    /// Reads a guidelines spreadsheet into groups of toxicity models
    /// and a single guideline value (if available) associated with each
    /// group of toxicology models.
    /// </summary>
    class GuidelineSpreadsheetReader
    {
        private readonly Spreadsheet spreadsheet;

        public GuidelineSpreadsheetReader(Spreadsheet spreadsheet)
        {
            this.spreadsheet = spreadsheet;
        }

        /// <summary>
        /// Gets toxicity records from the spreadsheet, grouping the records
        /// such that each group contains all toxicology records used to derive
        /// a particular guideline.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ToxicityValueGroup> GetToxicityValueGroups()
        {
            // Create a new group
            var toxicityValueGroup = new ToxicityValueGroup();

            foreach (var row in spreadsheet.GetRows())
            {
                // We've hit a dividing row, return all the toxicity records
                // we've accumulated so far and try to provide a single guideline
                // value if there was a valid guideline value in this group.
                if (row == Row.DividingRow)
                {
                    MaybeSetGuideline(toxicityValueGroup);
                    yield return toxicityValueGroup;

                    toxicityValueGroup = new ToxicityValueGroup();
                    continue;
                }


                // Assume rows with blank Record ID to be empty
                if (row.GetValue("record id") == null)
                {
                    continue;
                }

                // Some toxicity records have effect concentrations which
                // can't be converted to a common effect. These records can't
                // be used for deriving guidelines but we probably want to address
                // them. Log an error and continue.
                if (!HasEffectConversionFactor(row))
                {
                    Logger.Error("Toxicity value has no effect conversion factor. Row Number: " + row.RowNumber);
                    continue;
                }

                // Map the row to a toxicity model
                var chemicalName = StringRemapper.Remap(spreadsheet.Chemical);
                var toxicityValue = new ToxicityValueModel
                {
                    RowNumber = row.RowNumber,
                    Chemical = chemicalName,
                    CASNumber = ChemicalNameToCasNumber.Map(chemicalName),
                    Toxicant = StringRemapper.Remap(spreadsheet.Toxicant),
                    Record = StringRemapper.Remap(row.GetValue("record id")),
                    DataSource = StringRemapper.Remap(row.GetValue("data source id")),
                    MediaType = StringRemapper.Remap(row.GetValue("media type")),
                    SpeciesScientificName = StringRemapper.Remap(row.GetValue("species scientific name")),
                    Phylum = StringRemapper.Remap(row.GetValue("phylum")), // Not present in Ametryn spreadsheet
                    Class = StringRemapper.Remap(row.GetValue("class")),
                    OrganismType = StringRemapper.Remap(row.GetValue("type of organism", fuzzy: true)),
                    LifeStage = StringRemapper.Remap(row.GetValue("life stage")),
                    EndpointFromPaper = StringRemapper.Remap(row.GetValue("endpoint (directly from paper)")),  // Not present in Ametryn spreadsheet
                    Endpoint = StringRemapper.Remap(row.GetValue("endpoint")),  // Not present in Ametryn spreadsheet
                    EndpointMeasurement = StringRemapper.Remap(row.GetValue("endpoint measurement")),
                    // diverging from language used in spreadsheet here
                    Effect = StringRemapper.Remap(row.GetValue("toxicity value")),
                    Duration = StringRemapper.Remap(row.GetValue("exposure duration")),
                    DurationUnits = StringRemapper.Remap(row.GetValue("exposure duration units")),
                    IsChronic = BoolExtensions.FromOptionRequired(row.GetValue("acute/ chronic"), "chronic", "acute"),
                    Concentration = StringRemapper.Remap(row.GetValue("concentration stated in paper")),
                    ConcentrationUnit = StringRemapper.Remap(row.GetValue("units")),
                    ConcentrationUsed = row.GetValue<decimal?>("chronic nec/ec10/noec concentration", fuzzy: true),
                    LowestGuidelineValueForSpecies = row.GetValue<decimal?>("3. lowest value for species", fuzzy: true),
                    UseThisGuidelineValue = BoolExtensions.FromOption(row.GetValue("use this data value", fuzzy: true)).GetValueOrDefault()
                };

                MaybeMapHeteroPhototroph(row, toxicityValue);

                if (toxicityValue.ConcentrationUsed == null)
                {
                    throw new InvalidOperationException("Tried to read a toxicology record with no 'Concentration Used' value (column AD)");
                }
                toxicityValueGroup.Add(toxicityValue);
            }

            // If we've reached the end of the document but have accumulated
            // some toxicity records, return them now.
            if (toxicityValueGroup.Any())
            {
                MaybeSetGuideline(toxicityValueGroup);
                yield return toxicityValueGroup;
            }
        }

        /// <summary>
        /// Maybe maps the hetero / phototroph value. This contains an edge case for one
        /// of the spreadsheets which uses unsupported values (insect and crustacean) in
        /// the hetero / phototroph field. These values are ignored without producing a 
        /// warning and a null hetero / phototroph value is used.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="toxicityValue"></param>
        private static void MaybeMapHeteroPhototroph(Row row, ToxicityValueModel toxicityValue)
        {
            var heteroPhototrophValue = row.GetValue("hetero/ phototroph").ToLower();
            var supportsHeteroPhototrop = !(heteroPhototrophValue.Contains("insect") || heteroPhototrophValue.Contains("crustacean"));
            if (supportsHeteroPhototrop)
            {
                toxicityValue.IsHeterotroph = BoolExtensions.FromOptionRequired(heteroPhototrophValue, "heterotroph", "phototroph");
            }
        }

        private static bool HasEffectConversionFactor(Row row)
        {
            var effectConversionFactor = row.GetCell("toxicity value conversion factor");
            return effectConversionFactor.Value == null || effectConversionFactor.Value.ToString() != "#N/A";
        }

        /// <summary>
        /// If this group has a "Use this data value" column set to Yes then
        /// try to set the guideline value for this group.
        /// </summary>
        /// <param name="toxicityValueGroup"></param>
        private static void MaybeSetGuideline(ToxicityValueGroup toxicityValueGroup)
        {
            // There should be exactly zero or one accepted guideline value
            var usedValue = toxicityValueGroup.SingleOrDefault(x => x.UseThisGuidelineValue);

            if (usedValue == null)
            {
                return;
            }

            // If the value with "Use this data value" = "Yes" has a guideline, that's
            // the guideline we should use.
            if (usedValue.LowestGuidelineValueForSpecies != null)
            {
                toxicityValueGroup.GuidelineValue = usedValue.LowestGuidelineValueForSpecies;
                return;
            }

            // The row having "Use this data value" = "Yes" does not have a guideline value.
            // there should be one other row with a guideline value, take that one.
            var guidelineValue = toxicityValueGroup.SingleOrDefault(x => x.LowestGuidelineValueForSpecies.HasValue);

            if (guidelineValue != null)
            {
                toxicityValueGroup.GuidelineValue = guidelineValue.LowestGuidelineValueForSpecies;
                return;
            }

            throw new InvalidOperationException("Expected a guideline value for a toxicity group but could not find one.");
        }
    }
}
