﻿using System.Collections.Generic;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Services
{
    /// <summary>
    /// A collection of mappings from lower-case column headings
    /// to column ids in a spreadsheet.
    /// </summary>
    class Headers : Dictionary<string, int>
    {
    }
}
