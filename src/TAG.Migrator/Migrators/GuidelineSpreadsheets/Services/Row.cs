﻿using System;
using System.Linq;
using OfficeOpenXml;
using TAG.Migrator.Extensions;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Services
{
    /// <summary>
    /// A row in a guidelines spreadsheet. Provides functionality
    /// to get cell values based on a header name. Also detects
    /// when a row is a "dividing row" (the grey divider between
    /// groups of toxicology records in a guidelines spreadsheet).
    /// </summary>
    class Row
    {
        public static readonly Row DividingRow = new Row();
        private readonly ExcelWorksheet worksheet;
        public int RowNumber { get; private set; }
        private Headers headers;

        private Row()
        {   
        }

        public Row(ExcelWorksheet worksheet, int row, Headers headers)
        {
            this.worksheet = worksheet;
            RowNumber = row;
            this.headers = headers;
        }

        public string GetValue(string propertyName, bool fuzzy = false)
        {
            return GetValue<string>(propertyName, fuzzy);
        }

        public T GetValue<T>(string propertyName, bool fuzzy = false)
        {
            var cell = fuzzy ? GetCellFuzzy(propertyName) : GetCell(propertyName);
            return ConvertExtensions.ChangeType<T>(cell.Value);
        }

        public ExcelRange GetCell(string propertyName)
        {
            if (this == DividingRow)
            {
                throw new InvalidOperationException("Tried to read data from a dividing row");
            }

            if (!headers.ContainsKey(propertyName))
            {
                throw new ArgumentException("Tried to retrieve a non-existing spreadsheet property: " + propertyName);
            }

            var col = headers[propertyName];
            return worksheet.Cells[RowNumber, col];
        }

        /// <summary>
        /// Like GetCell, but does a weaker property name check.
        /// Only checks that the specified name is contained within
        /// the actual property name. Ignores case.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public ExcelRange GetCellFuzzy(string propertyName)
        {
            var actualHeader = headers.Keys.FirstOrDefault(x => x.ToLower().Contains(propertyName.ToLower()));

            if (actualHeader == null)
            {
                throw new ArgumentException("Could not find matching spreadsheet property: " + propertyName);
            }

            return GetCell(actualHeader);
        }
    }
}
