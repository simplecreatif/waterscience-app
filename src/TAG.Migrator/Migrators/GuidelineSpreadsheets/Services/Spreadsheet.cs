﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Excel;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets.Services
{
    /// <summary>
    /// Wrapper for an EPPlus excel reader. Designed for
    /// reading guidelines spreadsheets and makes a number
    /// of assumptions that are specific to guidelines spreadsheets
    /// such as the location of the header row.
    /// </summary>
    class Spreadsheet : IDisposable
    {
        private readonly ExcelPackage package;
        private readonly ExcelWorksheet worksheet;
        private readonly string fileName;
        private Headers headers;
        private int HeaderRow;
        private int BodyRow;
        public string Chemical { get; private set; }
        public string Toxicant { get; private set; }

        public Spreadsheet(string fileName)
        {
            this.fileName = fileName;

            package = new ExcelPackage(new FileInfo(fileName));

            // Load our excel geomean implementation
            package.Workbook.FormulaParserManager.LoadFunctionModule(new CustomFunctionsModule());

            // Calculate all formulae in workbook
            package.Workbook.Calculate();

            // Whichever sheet was last selected will be taken as the guidelines sheet
            worksheet = package.Workbook.Worksheets.Single(f => f.View.TabSelected);
            
            GetChemical();
            GetToxicant(Chemical);
            FindHeaderAndBodyRows();
            GetHeaders();
        }

        private void GetChemical()
        {
            Chemical = GetChemicalFromWorksheet() ?? GetChemicalFromFileName();

            if (Chemical == null)
            {
                throw new InvalidOperationException("Could not determine chemical.");
            }
        }

        private string GetChemicalFromWorksheet()
        {
            // Look for "chemical:" in a top-left cell and take the value from the next cell over.
            for (int r = 1; r < 4; r++)
            {
                for (char c = 'A'; c < 'E'; c++)
                {
                    var cellValue = worksheet.Cells[c.ToString() + r].Value;
                    // We've found a cell which contains "chemical:", now start looking
                    // through cells to the right of it for a non-blank value. Take this
                    // as the chemical.
                    if (cellValue != null && cellValue.ToString().ToLower() == "chemical:")
                    {
                        for (char nextC = (char)(c + 1); nextC <= 'E'; nextC++)
                        {
                            var nextCellValue = worksheet.Cells[nextC.ToString() + r].Value;
                            if (nextCellValue != null)
                            {
                                return nextCellValue.ToString();
                            }
                        }
                    }
                }
            }

            return null;
        }

        private string GetChemicalFromFileName()
        {
            return ChemicalNameToCasNumber.KnownChemicals.SingleOrDefault(x => fileName.ToLower().Contains(x.ToLower()));
        }

        private void GetToxicant(string chemical)
        {
            Toxicant = (worksheet.Cells["D2"].Value ?? worksheet.Cells["E2"].Value ?? chemical).ToString();
        }

        private void FindHeaderAndBodyRows()
        {
            // Header row will contain "Record ID" in one of the cells.
            // Body rows will occur immediately after the header row.
            int headerRow = 1;
            int maxExpectedHeaderRow = 10;
            for (; headerRow <= maxExpectedHeaderRow; headerRow++)
            {
                foreach (var col in new[] {"A", "B", "C", "D"})
                {
                    var cellValue = worksheet.Cells[col + headerRow].Value;
                    if (cellValue != null && cellValue.ToString() == "Record ID")
                    {
                        HeaderRow = headerRow;
                        BodyRow = headerRow + 1;
                        return;
                    }
                }
            }
            throw new InvalidOperationException("Could not find header row in " + fileName);
        }

        /// <summary>
        /// Load guideline spreadsheet headers from row 6 into name -> column id mappings.
        /// </summary>
        private void GetHeaders()
        {
            headers = new Headers();
            for (int c = 1; c <= worksheet.Dimension.Columns; c++)
            {
                var maybeHeader = (worksheet.Cells[HeaderRow, c].Value ?? "").ToString().Trim().ToLower();

                if (string.IsNullOrWhiteSpace(maybeHeader))
                {
                    // see if there's a header two rows above (such as 'Use this value' headers)
                    maybeHeader = (worksheet.Cells[HeaderRow - 2, c].Value ?? "").ToString().Trim().ToLower();
                }

                if (string.IsNullOrWhiteSpace(maybeHeader)) continue;
                if (headers.ContainsKey(maybeHeader)) continue; // ignore duplicate headers

                headers.Add(maybeHeader, c);
            }
        }

        /// <summary>
        /// Gets all toxicology rows in the spreadsheet including blank rows and dividing rows.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Row> GetRows()
        {
            for (int r = BodyRow; r <= worksheet.Dimension.Rows; r++)
            {
                var backgroundColor = worksheet.Cells[r, 1].Style.Fill.BackgroundColor.Rgb;
                if (backgroundColor == "FFA5A5A5")
                {
                    yield return Row.DividingRow;
                }
                else
                {
                    yield return new Row(worksheet, r, headers);
                }
            }
        }

        public void Dispose()
        {
            package.Dispose();
        }
    }
}
