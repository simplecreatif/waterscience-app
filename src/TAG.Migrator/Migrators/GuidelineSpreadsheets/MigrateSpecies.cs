﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;
using TAG.Persistence;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets
{
    public static class MigrateSpecies
    {
        // In cases where there is more than one common name for a scientific name,
        // this maps the scientific name to a single common name. This is needed because
        // the species common name is not provided in guideline worksheets.
        private static Dictionary<string, string> ScientificToCommonName = new Dictionary<string, string>
        {
            { "Cyprinus carpio", "Common carp" },
            { "Lemna aequinoctialis", "Duckweed" },
            { "Mogurnda mogurnda", "Purple Spotted Gudgeon (fish)" },
            { "Paratya australiensis", "Freshwater Shrimp" },
            { "Proisotoma minuta", "Collembola" },
            { "Scenedesmus obliquus", "Green algae" },
        };
        public static void Migrate(GuidelineSpreadsheetsMigrator x, ToxicityValueGroup group)
        {
            x.Migrate<Species>(group, model =>
            {
                var newEntity = new Species
                {
                    ScientificName = StringRemapper.Remap(model.SpeciesScientificName),
                    Class = x.Mappers.Get<SpeciesClass>(model.Class),
                    AnimalCategory = x.Mappers.Get<AnimalCategory>(model.OrganismType),
                    IsHeterotroph = model.IsHeterotroph
                };

                // Either load the common name from an existing entity
                // (so the deduplication logic can still work), or try
                // to get a common name from our own scientific to
                // common mappings, which handle cases where a scientific
                // name has multiple common names

                string commonName;

                var speciesWithSameScientificName = x.Repository.Query<Species>().Where(y => y.ScientificName == newEntity.ScientificName).ToList();

                if (speciesWithSameScientificName.Count == 1)
                {
                    commonName = speciesWithSameScientificName.Single().CommonName;
                }
                else
                {
                    ScientificToCommonName.TryGetValue(newEntity.ScientificName, out commonName);
                }

                newEntity.CommonName = commonName;

                return newEntity;
            },
            model => StringRemapper.Remap(model.SpeciesScientificName),
            (newEntity, entity) =>
                    entity.ScientificName == newEntity.ScientificName
                    && entity.CommonName == newEntity.CommonName);
        } 
    }
}
