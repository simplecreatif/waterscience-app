﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets
{
    public static class MigrateToxicant
    {
        public static void Migrate(GuidelineSpreadsheetsMigrator x, ToxicityValueGroup toxicityValueGroup)
        {
            var toxicantName = toxicityValueGroup.First().Toxicant;
            var toxicant = x.Repository.Query<Toxicant>().SingleOrDefault(y => y.Name == toxicantName);
            if (toxicant == null)
            {
                toxicant = new Toxicant { Name = toxicantName };
                x.Repository.Add(toxicant);
            }
        }
    }
}
