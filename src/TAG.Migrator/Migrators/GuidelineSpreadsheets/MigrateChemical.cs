﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets
{
    public static class MigrateChemical
    {
        public static void Migrate(GuidelineSpreadsheetsMigrator x, ToxicityValueGroup toxicityValueGroup)
        {

            var chemicalName = StringRemapper.Remap(toxicityValueGroup.First().Chemical, capitalise: false);
            var casNumber = toxicityValueGroup.First().CASNumber;

            if (x.Mappers.HasMappingFor<Chemical>(casNumber.ToString()))
            {
                // Don't try to migrate something migrated in an earlier toxicology group
                return;
            }

            var toxicant = x.Repository.Query<Toxicant>().SingleOrDefault(y => y.Name == toxicityValueGroup.First().Toxicant);

            // Try to fetch this entity from db first, if no such entity exists create one.
            var entity = x.Repository.Query<Chemical>().SingleOrDefault(y => y.CASNumber == casNumber);
            if (entity == null)
            {
                entity = new Chemical
                {
                    Name = chemicalName,
                    CASNumber = casNumber,
                    Toxicant = toxicant
                };

                x.Repository.Add(entity);
                Logger.LogMigratingEntity(chemicalName, entity);
            }
            else
            {
                Logger.LogEntityAlreadyExists(chemicalName, entity);
            }

            x.Mappers.AddMapping<Chemical>(casNumber.ToString(), entity);
        }
    }
}
