﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets
{
    public static class MigrateSpeciesClass
    {
        public static void Migrate(GuidelineSpreadsheetsMigrator x, ToxicityValueGroup group)
        {
            x.Migrate<SpeciesClass>(group, model => new SpeciesClass
            {
                Name = StringRemapper.Remap(model.Class),
                Phylum = x.Mappers.Get<SpeciesPhylum>(model.Phylum)
            },
            model => StringRemapper.Remap(model.Class),
            (newEntity, entity) => entity.Name == newEntity.Name);
        } 
    }
}
