﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;

namespace TAG.Migrator.Migrators.GuidelineSpreadsheets
{
    public static class MigrateEndpoint
    {
        public static void Migrate(GuidelineSpreadsheetsMigrator x, ToxicityValueGroup group)
        {
            // Endpoint from paper (column L)
            x.Migrate<Endpoint>(group, model =>
            {
                var newEntity = new Endpoint
                {
                    Abbreviation = StringRemapper.Remap(model.EndpointFromPaper)
                };

                return newEntity.Abbreviation != null ? newEntity : null;
            },
            model => StringRemapper.Remap(model.EndpointFromPaper),
            (newEntity, entity) => entity.Abbreviation == newEntity.Abbreviation);

            // Endpoint (column M)
            x.Migrate<Endpoint>(group, model =>
            {
                var newEntity = new Endpoint
                {
                    Abbreviation = StringRemapper.Remap(model.Endpoint)
                };

                return newEntity.Abbreviation != null ? newEntity : null;
            },
            model => StringRemapper.Remap(model.Endpoint),
            (newEntity, entity) => entity.Abbreviation == newEntity.Abbreviation);

            // Endpoint (column N)
            x.Migrate<Endpoint>(group, model => new Endpoint
            {
                Abbreviation = StringRemapper.Remap(model.EndpointMeasurement)
            },
            model => StringRemapper.Remap(model.EndpointMeasurement),
            (newEntity, entity) => entity.Abbreviation == newEntity.Abbreviation);
        } 
    }
}
