﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.ToxicologyDatabase
{
    public static class MigrateMedia
    {
        public static void Migrate(ToxicologyDatabaseMigrator x)
        {
            x.Migrate<Media>("TblMedia", row =>
            {
                var newEntity = new EntityMigration<Media>(row)
                    .Map(dest => dest.Name, "Media description")
                    .MapReference(dest => dest.MediaType, "Media TypeID", x.Mappers)
                    .Result;

                if (newEntity.Name == null)
                {
                    return null;
                }

                return newEntity;
            },
                (newEntity, entity) => entity.Name == newEntity.Name);
        }
    }
}
