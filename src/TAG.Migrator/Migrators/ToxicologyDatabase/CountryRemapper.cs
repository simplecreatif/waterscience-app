﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Migrator.Migrators.ToxicologyDatabase
{
    public static class CountryRemapper
    {
        public static string Remap(string country)
        {
            if (string.IsNullOrWhiteSpace(country) || country == "0")
            {
                return null;
            }

            var test = country.ToLower().Substring(0, 2);

            switch (test)
            {
                case "au": return "Australia";
                case "ma": return "Malaysia";
                case "ph": return "Philippines";
                case "ne": return "New Zealand";
                default: return country.Substring(0, 1).ToUpper() + country.Substring(1).ToLower();
            }
        }
    }
}
