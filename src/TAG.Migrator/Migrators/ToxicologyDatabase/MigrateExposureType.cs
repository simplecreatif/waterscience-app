﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.ToxicologyDatabase
{
    public static class MigrateExposureType
    {
        public static void Migrate(ToxicologyDatabaseMigrator x)
        {
            x.Migrate<ExposureType>("Method", row =>
            {
                var newEntity = new EntityMigration<ExposureType>(row)
                    .Map(dest => dest.Name, "Method")
                    .Map(dest => dest.Abbreviation, "Abb")
                    .Result;

                if (newEntity.Name == null)
                {
                    return null;
                }

                return newEntity;
            },
            (newEntity, entity) => entity.Name == newEntity.Name);
        }
    }
}
