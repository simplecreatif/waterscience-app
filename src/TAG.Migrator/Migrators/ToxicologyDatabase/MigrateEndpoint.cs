﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.ToxicologyDatabase
{
    public static class MigrateEndpoint
    {
        public static void Migrate(ToxicologyDatabaseMigrator x)
        {
            x.Migrate<Endpoint>("Endpoint", row =>
                {
                    var newEntity = new EntityMigration<Endpoint>(row)
                        .Map(dest => dest.Abbreviation, "Endpoint")
                        .Map(dest => dest.Name, "Endpoint detail")
                        .Result;

                    if (newEntity.Abbreviation == null)
                    {
                        return null;
                    }

                    return newEntity;
                },
               (newEntity, entity) => entity.Abbreviation == newEntity.Abbreviation);
        }
    }
}
