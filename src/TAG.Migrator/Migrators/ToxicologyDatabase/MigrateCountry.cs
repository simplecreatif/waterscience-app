﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.ToxicologyDatabase
{
    /// <summary>
    /// Country - New lookup based on Toxdata.country
    /// </summary>
    public static class MigrateCountry
    {
        public static void Migrate(ToxicologyDatabaseMigrator x)
        {
            x.MigrateQuery<Country>(
                "select distinct country as id, country as name from Toxdata where country is not null",
                "country (from toxdata)",
                row =>
                {
                    var newEntity = new Country
                    {
                        Name = CountryRemapper.Remap(row.name)
                    };

                    return newEntity.Name != null ? newEntity : null;
                },
                (newEntity, entity) => entity.Name == newEntity.Name);
        }
    }
}
