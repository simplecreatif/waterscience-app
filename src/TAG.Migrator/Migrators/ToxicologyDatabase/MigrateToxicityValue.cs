﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Extensions;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.ToxicologyDatabase
{
    public static class MigrateToxicityValue
    {
        public static void Migrate(ToxicologyDatabaseMigrator x)
        {
            x.Migrate<ToxicityValueAED>("Toxdata", row =>
            {
                var casNumber = DapperExtensions.GetValue<string>(row, "CAS NUMBER");
                if (casNumber == null)
                {
                    Logger.Error("Toxdata has missing cas number.", row.ID.ToString());
                    return null;
                }

                if (!x.Mappers.HasMappingFor<Chemical>(casNumber))
                {
                    Logger.Error($"Toxdata has invalid chemical. CAS NUMBER: {casNumber}", row.ID.ToString());
                    return null;
                }

                bool hasKnownInvalidStatistic = row.StatsID != null && !x.Mappers.HasMappingFor<Statistic>(row.StatsID.ToString());
                bool hasKnownInvalidAge = DapperExtensions.GetValue<string>(row, "Abr Age") == "317";

                var migration = new EntityMigration<ToxicityValueAED>(row)
                    .Map(dest => dest.Identifier, "IDENTIFYER")
                    .MapReference(dest => dest.Chemical, "CAS NUMBER", x.Mappers)
                    .MapReference(dest => dest.ChemicalGrade, "Chemical GradeID", x.Mappers)
                    .Map(dest => dest.ChemicalGradeValue, "Chemical Grade Value")
                    .MapReference(dest => dest.ChemicalForm, "Chemical Form ID", x.Mappers)
                    .Map(dest => dest.IsSolventUsed, "Is Solvent used")
                    .Map(dest => dest.SolventName, "Solvent Name")
                    .Map(dest => dest.SolventConcentration, "Conc of Solvent")
                    .MapReference(dest => dest.SolventUnit, "Solvent UnitID", x.Mappers)
                    .Map(dest => dest.SolventControl, "Solvent Control")
                    .MapReference(dest => dest.Species, "Species No", x.Mappers)
                    .Map(dest => dest.SpeciesStrain, "SpStrain")
                    .Map(dest => dest.SpeciesAgeOrStage, "Age/Stage")
                    .Map(dest => dest.Feeding, "Feeding")
                    .MapReference(dest => dest.Media, "MediaID", x.Mappers)
                    .MapReference(dest => dest.TestForm, "Test ID", x.Mappers)
                    .MapReference(dest => dest.Duration, "DurationID", x.Mappers)
                    .MapReference(dest => dest.DurationUnit, "DurationUnitID", x.Mappers)
                    .MapReference(dest => dest.DurationHour, "DurHourID", x.Mappers)
                    .MapReference(dest => dest.Effect, "EffectID", x.Mappers)
                    .MapReference(dest => dest.Endpoint, "EndpointID", x.Mappers)
                    .MapReference(dest => dest.ExposureType, "MethodID", x.Mappers)
                    .Map(dest => dest.Concentration, "Concentration")
                    .Map(dest => dest.LowerConfidenceLimit95, "95%LCL")
                    .Map(dest => dest.UpperConfidenceLimit95, "95%UCL")
                    .MapReference(dest => dest.ErrorMeasure, "ErrorID", x.Mappers)
                    // Appears as "Measure of Error" in Access?
                    .Map(dest => dest.ErrorValue, "Error Value")
                    .Map(dest => dest.DoseResponse, "Dose responseID")
                    .Map(dest => dest.SignificanceLevel, "Significance Level")
                    .Map(dest => dest.Power, "Power")
                    .MapReference(dest => dest.Replication, "ReplicationID", x.Mappers)
                    .MapReference(dest => dest.TemporalReplication, "TemporalID", x.Mappers)
                    .Map(dest => dest.HasRefTox, "ReftoxID")
                    .Map(dest => dest.RefToxName, "Reftox Name")
                    .Map(dest => dest.HasControlEffect, "Control%effectsID")
                    .Map(dest => dest.ControlEffect, "Control%effect")
                    .Map(dest => dest.InvalidationCriteria, "Invalidation Criteria")
                    .Map(dest => dest.LossOfChemical, "% Loss  of Chemical")
                    .Map(dest => dest.ChemicalFormUsed, "Chemformused")
                    .MapReference(dest => dest.ConcentrationType, "ConcID", x.Mappers)
                    .MapReference(dest => dest.ConcentrationUnit, "UnitID", x.Mappers)
                    .Map(dest => dest.IsPhysicochemMeasured, "Is Physicochem measured")
                    .Map(dest => dest.Temperature, "Temperature(oC)")
                    .Map(dest => dest.PH, "pH")
                    .Map(dest => dest.Conductivity, "Conductivity(uS/Cm)")
                    .Map(dest => dest.Hardness, "Hardness")
                    .Map(dest => dest.Salinity, "Salinity(‰)")
                    .Map(dest => dest.Alkalinity, "Alkalinity")
                    .Map(dest => dest.OrganicCarbon, "Organic Carbon(mg/L)")
                    .Map(dest => dest.LightRegime, "Light Regime")
                    .Map(dest => dest.OtherParameters, "Other Parameters")
                    .MapReference(dest => dest.Reference, "ReferenceNo", x.Mappers)
                    .Map(dest => dest.MethodReference, "method reference")
                    .MapReference(dest => dest.Country, r => CountryRemapper.Remap(r.country), x.Mappers)
                    .Map(dest => dest.SiteLocation, "Site Location")
                    .Map(dest => dest.HardCopy, "Hard Copy")
                    .MapReference(dest => dest.PublicationLanguage, "LanguageID", x.Mappers)
                    .Map(dest => dest.Comments, "Other comments")
                    .Map(dest => dest.QualityScore, "Quality score");

                // Warn about toxicity values with known invalid statistic ids but still import them
                if (!hasKnownInvalidStatistic)
                {
                    migration.MapReference(dest => dest.Statistic, "StatsID", x.Mappers);
                }
                else
                {
                    Logger.Warning($"Toxdata has invalid statistic. Old Id: {row.ID}, Stats Id: {row.StatsID}, Ignoring statistic for this record.");
                }

                // Warn about invalid ages
                if (!hasKnownInvalidAge)
                {
                    migration.MapReference(dest => dest.Age, "Abr Age", x.Mappers);
                }
                else
                {
                    Logger.Warning($"Toxdata has invalid age. Old Id: {row.ID}. Ignoring age for this record.");
                }

                // There's a single record with a missing identifier. We'll just set it as "UNKNOWN"
                if (migration.Result.Identifier == null)
                {
                    migration.MapValue(dest => dest.Identifier, "UNKNOWN");
                    Logger.Warning($"Toxdata has no 'identifier' ('identifyer'). Old Id: {row.ID}. Importing with identifier set to 'UNKNOWN'");
                }

                var newEntity = migration.Result;
                return newEntity;
            },
                (newEntity, entity) => entity.Identifier == newEntity.Identifier);
        }
    }
}
