﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.ToxicologyDatabase
{
    public static class MigrateAnimalType
    {
        public static void Migrate(ToxicologyDatabaseMigrator x)
        {
            x.Migrate<AnimalType>("Animaltype", row =>
            {
                var newEntity = new EntityMigration<AnimalType>(row)
                    .Map(dest => dest.Name, "Animal Type")
                    .Result;

                // Edge case for unnamed "Fish" animal type
                // in toxicology database
                if (newEntity.Name == null)
                {
                    newEntity.Name = "Fish";
                }

                return newEntity;
            },
            (newEntity, entity) => entity.Name == newEntity.Name);
        }
    }
}
