﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Persistence;

namespace TAG.Migrator.Migrators.Common
{
    class PostPopulateTask
    {
        /// <summary>
        /// Populates some fields that are commonly blank with
        /// data from related fields (for example, might map
        /// "common name" to "name" when the latter is blank).
        /// </summary>
        public static void Populate(IRepository repository)
        {
            // Map Chemical.Name to Chemical.CommonName and vice-versa when one is blank
            foreach (var chemical in repository.Query<Chemical>())
            {
                chemical.Name = chemical.Name ?? chemical.CommonName;
                chemical.CommonName = chemical.CommonName ?? chemical.Name;
            }

            // Map Endpoint.Abbreviation to Effect.Name when name is blank
            foreach (var endpoint in repository.Query<Endpoint>())
            {
                endpoint.Name = endpoint.Name ?? endpoint.Abbreviation;
            }
        }
    }
}
