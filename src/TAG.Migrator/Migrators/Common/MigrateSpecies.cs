﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Extensions;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.Common
{
    public static class MigrateSpecies
    {
        public static void Migrate(DatabaseMigrator x)
        {
            x.Migrate<Species>("Speciesdata", row => new EntityMigration<Species>(row)
               .Map(dest => dest.ScientificName, "Latin Name")
               .Map(dest => dest.CommonName, "Common Namesp")
               .Map(dest => dest.Status, "Status")
               .Map(dest => dest.AnimalType, r =>
               {
                   int? animalTypeId = DapperExtensions.GetValue<int?>(r, "Animal Type ID");
                   if (animalTypeId.HasValue && animalTypeId > 4)
                   {
                       Logger.Warning(
                           "Species has invalid Animal Type ID. Species No: "
                           + DapperExtensions.GetValue<string>(r, "Species No")
                           + ", Animal Type ID: " + animalTypeId);

                       animalTypeId = 4; // Remap missing animal types to "Other"
                   }
                   if (animalTypeId.HasValue)
                   {
                       return x.Mappers.Get<AnimalType>(animalTypeId.Value.ToString());
                   }
                   return null;
               })
               .Map(dest => dest.MajorGroup, "Major Group")
               .Map(dest => dest.MinorGroup, "Minor Group")
               .MapReference(dest => dest.AnimalCategory, "WQcategoryID", x.Mappers)
               .Result,
               (newEntity, entity) =>
                   entity.ScientificName == newEntity.ScientificName
                   && entity.CommonName == newEntity.CommonName);
        }
    }
}
