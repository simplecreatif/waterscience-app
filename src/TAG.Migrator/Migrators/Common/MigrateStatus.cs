﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.Common
{
    public static class MigrateStatus
    {
        private static Dictionary<string, string> NameMappings = new Dictionary<string, string>
        {
            {"m", "A"},
            {"i", "A"},
            {"c", "H"}
        };
        /// <summary>
        /// In the old systems the following status codes were available:
        /// M (Moderate)
        /// C (Complete)
        /// I (Incomplete)
        /// 
        /// In the new system the following status codes are available:
        /// A (Acceptable quality)
        /// H (High quality)
        /// U (Unacceptable quality)
        /// 
        /// A remapping needs to be performed:
        /// "M" and "I" records become "A" records
        /// "C" records become "H" records
        /// </summary>
        /// <param name="x"></param>
        public static void Migrate(DatabaseMigrator x)
        {
            x.Migrate<Status>("Status", row =>
            {
                var name = row.ReviewStatus.ToString().Trim().ToLower();

                if (name == "nr")
                {
                    return null;
                }

                var mappedName = NameMappings[name];

                var newEntity = new Status { Name = mappedName };
                return newEntity;
            },
                (newEntity, entity) => entity.Name == newEntity.Name);

            // insert new "U" (unacceptable) status
            if (x.Repository.Query<Status>().SingleOrDefault(y => y.Name == "U") == null)
            {
                x.Repository.Add(new Status
                {
                    Name = "U"
                });
            }
        }
    }
}
