﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.Common
{
    public static class MigrateReference
    {
        public static void Migrate(DatabaseMigrator x, bool forToxicology = true)
        {
            x.Migrate<Reference>("Refdata", row =>
                {
                    var map = new EntityMigration<Reference>(row)
                        .Map(dest => dest.Authors, "Authors")
                        .Map(dest => dest.Title, "Title")
                        .Map(dest => dest.Journal, "Journal")
                        .Map(dest => dest.Year, "Year")
                        .Map(dest => dest.Volume, "Vol")
                        .Map(dest => dest.IssueNumber, "IssueNo")
                        .Map(dest => dest.FirstPage, "FirstPage")
                        .Map(dest => dest.LastPage, "LastPage");

                    if (forToxicology)
                    {
                        map
                            .Map(dest => dest.OrgRefNumber, "OrgRefNo")
                            .Map(dest => dest.AuthorsAbbreviated, "AbbAuthor");
                    }
                    var newEntity = map.Result;

                    return !String.IsNullOrWhiteSpace(newEntity.Authors) ? newEntity : null;
                },
                (newEntity, entity) =>
                    entity.Authors == newEntity.Authors
                    && entity.Title == newEntity.Title
                    && entity.Journal == newEntity.Journal
                    && entity.Year == newEntity.Year
                    && entity.IssueNumber == newEntity.IssueNumber
                    && entity.FirstPage == newEntity.FirstPage);
        }
    }
}
