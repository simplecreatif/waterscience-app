﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Migrator.Extensions;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.Common
{
    public static class MigrateChemical
    {
        public static void Migrate(DatabaseMigrator x)
        {
            x.Migrate<Chemical>("Chemdata", row =>
            {
                var entity = new EntityMigration<Chemical>(row)
                    .Map(dest => dest.CASNumber, "CAS NUMBER")
                    .MapReference(dest => dest.Toxicant, "Toxicant No", x.Mappers)
                    //.Map(dest => dest.Name, "Chemical Name")
                    //.Map(dest => dest.CommonName, "Common Name")
                    .MapReference(dest => dest.ChemicalType, "ChemID", x.Mappers)
                    .Map(dest => dest.SpecificGravity, "Specific Gr")
                    .Map(dest => dest.Solubility, "Solubility (H2O)(mg/L)")
                    .Map(dest => dest.BoilingPoint, "Boiling Pt(oC)")
                    .Map(dest => dest.MeltingPoint, "Melting Pt(oC)")
                    .Map(dest => dest.FlashPoint, "Flash Pt(oC)")
                    .Map(dest => dest.VapourPressure, "Vp (mm Hg)")
                    .Map(dest => dest.LogKoc, "Log Koc")
                    .Map(dest => dest.LogKow, "Log Kow")
                    .Map(dest => dest.MolecularWeight, src => src.MolWeight)
                    //.Map(dest => dest.Formulae, src => src.Formulae)
                    .Map(dest => dest.LogBCF, "Log BCF")
                    .Map(dest => dest.HalfLifeWater, "Half-Life(Water)")
                    .Map(dest => dest.HalfLifeSediment, "Half-Life(Sediment)")
                    .Result;

                // Don't capitalise columns known to contain chemical formulae
                entity.Name = StringRemapper.Remap(DapperExtensions.GetValue<string>(row, "Chemical Name"), capitalise: false);
                entity.CommonName = StringRemapper.Remap(DapperExtensions.GetValue<string>(row, "Common Name"), capitalise: false);
                entity.Formulae = StringRemapper.Remap(DapperExtensions.GetValue<string>(row, "Formulae"), capitalise: false);

                // These can only be migrated once guidelines' Chemref table is loaded
                // todo: Confirm the table from guidelines is correct to map against this. Some ids are missing.
                //entity.ReferenceSolubility = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref solubility"));
                //entity.ReferenceBoilingPoint = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref BPt"));
                //entity.ReferenceMeltingPoint = guidelinesMappings.Map<ChemicalReference>(row.RefMPt);
                //entity.ReferenceVapourPressure = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref VP"));
                //entity.ReferenceKoc = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref KoC"));
                //entity.ReferenceKow = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref KoW"));
                //entity.ReferenceMolecularWeight = guidelinesMappings.Map<ChemicalReference>(row.RefMwt);
                //entity.ReferenceFormulae = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref Formulae"));
                //entity.ReferenceBCF = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref BCF"));
                //entity.ReferenceHalfLifeWater = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref T1/2water"));
                //entity.ReferenceHalfLifeSediment = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "RefT1/2 sediment"));
                //entity.ReferenceSpecificGravity = guidelinesMappings.Map<ChemicalReference>(DapperExtensions.GetValue(row, "Ref SG"));

                return entity;
            },
           (newEntity, entity) => entity.CASNumber == newEntity.CASNumber);

            // These chemicals are referenced in the toxicity values but have no corresponding
            // chemical records, so we'll make them here
            var additionalCASNumbers = new[] { 7782492, 10101981, 7782492 };

            foreach (var casNumber in additionalCASNumbers)
            {
                var exists = x.Repository.Query<Chemical>().SingleOrDefault(y => y.CASNumber == casNumber) != null;

                if (!exists)
                {
                    var chemical = new Chemical { CASNumber = casNumber };
                    x.Repository.Add(chemical);
                    x.Mappers.AddMapping<Chemical>(casNumber.ToString(), chemical);
                    Logger.LogMigratingEntity(casNumber.ToString(), chemical);
                }
            }
        }
    }
}
