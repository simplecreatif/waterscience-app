﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.Common
{
    public static class MigrateChemicalType
    {
        public static void Migrate(DatabaseMigrator x)
        {
            x.Migrate<ChemicalType>("Chemtype", row =>
            {
                var newEntity = new EntityMigration<ChemicalType>(row)
                    .Map(dest => dest.Name, "Chemtype")
                    .Map(dest => dest.Class, "Chemical Class")
                    .Result;

                if (newEntity.Name == null)
                {
                    return null;
                }

                return newEntity;
            },
                (newEntity, entity) => entity.Name == newEntity.Name);
        }
    }
}
