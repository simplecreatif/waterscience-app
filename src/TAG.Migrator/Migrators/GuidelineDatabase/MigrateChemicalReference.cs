﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.GuidelineDatabase
{
    public static class MigrateChemicalReference
    {
        public static void Migrate(GuidelineDatabaseMigrator x)
        {
            x.Migrate<ChemicalReference>("Chemref", row =>
                {
                    var newEntity = new EntityMigration<ChemicalReference>(row)
                        .Map(dest => dest.Authors, "Authors")
                        .Map(dest => dest.Title, "Title")
                        .Map(dest => dest.Journal, "Journal")
                        .Map(dest => dest.Year, "Year")
                        .Map(dest => dest.Publisher, "Publisher")
                        .Map(dest => dest.Volume, "Vol")
                        .Map(dest => dest.IssueNumber, "IssueNo")
                        .Result;

                    string pages = row.Pages;
                    if (!string.IsNullOrWhiteSpace(pages))
                    {
                        var items = pages.Split('-').Select(p => int.Parse(p.Trim())).ToList();
                        newEntity.FirstPage = items[0];
                        newEntity.LastPage = items[1];
                    }

                    return newEntity;
                },
                (newEntity, entity) =>
                    entity.Authors == newEntity.Authors
                    && entity.Title == newEntity.Title
                    && entity.Journal == newEntity.Journal
                    && entity.Year == newEntity.Year
                    && entity.Publisher == newEntity.Publisher
                    && entity.Volume == newEntity.Volume
                    && entity.IssueNumber == newEntity.IssueNumber);
        }
    }
}
