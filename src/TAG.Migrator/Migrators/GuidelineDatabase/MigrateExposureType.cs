﻿using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.GuidelineDatabase
{
    public static class MigrateExposureType
    {
        public static void Migrate(GuidelineDatabaseMigrator x)
        {
            x.Migrate<ExposureType>("Method", row =>
            {
                var newEntity = new EntityMigration<ExposureType>(row)
                    .Map(dest => dest.Name, "Method")
                    .Result;

                if (newEntity.Name == null)
                {
                    return null;
                }

                return newEntity;
            },
            (newEntity, entity) => entity.Name == newEntity.Name);
        }
    }
}
