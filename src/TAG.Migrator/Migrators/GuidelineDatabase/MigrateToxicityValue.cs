﻿using System.Linq;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Extensions;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.GuidelineDatabase
{
    public static class MigrateToxicityValue
    {
        public static void Migrate(GuidelineDatabaseMigrator x)
        {
            var hourUnit = x.Repository.Query<DurationUnit>().Single(y => y.Name == "Hours");

            // todo: find a natural key for toxdata in guidelines db
            x.Migrate<ToxicityValue2000>("Toxdata", row =>
            {
                if (!x.Mappers.HasMappingFor<Chemical>(DapperExtensions.GetValue<string>(row, "CAS NUMBER")))
                {
                    Logger.Error("Toxdata has invalid chemical.", row.ID.ToString());
                    return null;
                }

                var newEntity = new EntityMigration<ToxicityValue2000>(row)
                    .MapReference(dest => dest.Chemical, "CAS NUMBER", x.Mappers)
                    .MapReference(dest => dest.Species, "Species No", x.Mappers)
                    .MapReference(dest => dest.MediaType, "TestMediaID", x.Mappers)
                    .MapReference(dest => dest.TestType, "TestTypeID", x.Mappers)
                    .MapReference(dest => dest.Duration, "DurationID", x.Mappers)
                    .MapReference(dest => dest.DurationHour, "DurationID", x.Mappers)
                    // Duration assumed to be in hours in guidelines db
                    .MapValue(dest => dest.DurationUnit, row.DurationID != null ? hourUnit : null)
                    .MapReference(dest => dest.Effect, "EndpointID", x.Mappers)
                    // Endpoint in guidelines corresponds to Effect in toxicology
                    .MapReference(dest => dest.EffectUsed, "EndpointID(U)", x.Mappers)
                    .MapReference(dest => dest.Endpoint, "EffectID", x.Mappers)
                    .MapReference(dest => dest.ExposureType, "MethodID", x.Mappers)
                    .Map(dest => dest.Concentration, "ConcentratioReported")
                    .Map(dest => dest.ConcentrationUsed, "ConcentrationUsed")
                    .MapReference(dest => dest.ConcentrationCode, "ConcCodeID", x.Mappers)
                    .Map(dest => dest.ConvNOEC, "ConvNOEC")
                    .Map(dest => dest.HardnessCorrectedConc, "HardnesscorrectedConc")
                    .MapReference(dest => dest.ConcentrationType, "ConcID", x.Mappers)
                    .MapReference(dest => dest.ConcentrationUnit, "UnitID", x.Mappers)
                    .Map(dest => dest.Temperature, "Temperature(oC)")
                    .Map(dest => dest.PH, "pH")
                    .Map(dest => dest.Hardness, "Hardness")
                    .Map(dest => dest.Salinity, "Salinity")
                    .MapReference(dest => dest.Reference, "ReferenceNo", x.Mappers)
                    .MapReference(dest => dest.Status, "StatusID", x.Mappers)
                    .Map(dest => dest.TotalAmmoniaMg, "Total ammonia as mg N/L")
                    .Map(dest => dest.TotalAmmoniaPh8, "Total ammonia pH8 (mg N/L)")
                    .Result;
                return newEntity;
            });
        }
    }
}
