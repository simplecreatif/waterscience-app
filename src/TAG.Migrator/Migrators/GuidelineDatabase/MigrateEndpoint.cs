﻿using TAG.Domain.Models;
using TAG.Migrator.Infrastructure;

namespace TAG.Migrator.Migrators.GuidelineDatabase
{
    public static class MigrateEndpoint
    {
        public static void Migrate(GuidelineDatabaseMigrator x)
        {
            x.Migrate<Endpoint>("Effect", row =>
            {
                var newEntity = new Endpoint
                {
                    Abbreviation = StringRemapper.Remap(row.Effect)
                };

                if(newEntity.Abbreviation == null)
                {
                    return null;
                }

                return newEntity;
            },
             (newEntity, entity) => entity.Abbreviation == newEntity.Abbreviation);
        }
    }
}
