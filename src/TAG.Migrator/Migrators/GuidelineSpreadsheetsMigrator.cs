﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Extensions;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators.GuidelineSpreadsheets;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Services;
using TAG.Persistence;

namespace TAG.Migrator.Migrators
{
    /// <summary>
    /// Migrates guidelines spreadsheets to TAG
    /// </summary>
    public class GuidelineSpreadsheetsMigrator : SpreadsheetMigrator
    {
        public GuidelineSpreadsheetsMigrator(IRepository repository) : base(repository)
        {
        }

        public void Migrate()
        {
            var fileNames = Directory.GetFiles("Resources", "*.xlsx");

            foreach (var fileName in fileNames)
            {
                using (var s = new Spreadsheet(fileName))
                {
                    Logger.LogHeading("Migrating Guideline Spreadsheet " + Path.GetFileName(fileName) + " " + DateTime.Now);

                    var reader = new GuidelineSpreadsheetReader(s);
                    foreach (var group in reader.GetToxicityValueGroups())
                    {
                        if (!group.Any())
                        {
                            continue;
                        }

                        Migrate(group);

                        Logger.Log(String.Format("Group with {0} toxicity values, guideline: {1}", group.Count, group.GuidelineValue));
                    }
                }
            }
        }

        private void Migrate(ToxicityValueGroup group)
        {
            MigrateLookup<MediaType>(group.Select(x => x.MediaType));
            MigrateLookup<Age>(group.Select(x => x.LifeStage));
            MigrateLookup<SpeciesPhylum>(group.Select(x => x.Phylum));
            MigrateLookup<AnimalCategory>(group.Select(x => x.OrganismType));
            MigrateLookup<Effect>(group.Select(x => x.Effect));
            MigrateLookup<Duration>(group.Select(x => x.Duration));
            MigrateLookup<DurationUnit>(group.Select(x => x.DurationUnits));
            MigrateLookup<ConcentrationUnit>(group.Select(x => x.ConcentrationUnit), capitalise: false);
            MigrateToxicant.Migrate(this, group);
            MigrateChemical.Migrate(this, group);
            MigrateSpeciesClass.Migrate(this, group);
            MigrateSpecies.Migrate(this, group);
            MigrateEndpoint.Migrate(this, group);
            MigrateToxicityValue.Migrate(this, group);
        }
    }
}
