﻿using System.Data;
using System.Data.OleDb;
using Autofac;
using TAG.Migrator.Infrastructure;
using TAG.Migrator.Migrators;
using TAG.Migrator.Migrators.Common;
using TAG.Migrator.Migrators.GuidelineSpreadsheets;
using TAG.Persistence;

namespace TAG.Migrator
{
    public class Program
    {
        static void Main(string[] args)
        {
            Logger.Level = LogLevel.Warnings;

            // Truncate database (delete every table)
            using (var container = BuildContainer())
            {
                var connection = container.Resolve<IDbConnection>();
                PostgreSQLDatabaseTruncator.Truncate(connection, "tag");
            }

            using (var container = BuildContainer())
            {
                var repository = container.Resolve<IRepository>();

                // Toxicology db migrator
                var toxicologyDbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Resources\\Web Version AED.mdb");
                var toxicologyDbMigrator = new ToxicologyDatabaseMigrator(repository, toxicologyDbConn);

                // Guideline db migrator
                var guidelineDbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Resources\\WQG2000.mdb");
                var guidelineDbMigrator = new GuidelineDatabaseMigrator(repository, guidelineDbConn);

                // Guideline spreadsheets migrator
                var guidelineSpreadsheetsMigrator = new GuidelineSpreadsheetsMigrator(repository);
                toxicologyDbMigrator.Migrate();
                guidelineDbMigrator.Migrate();
                guidelineSpreadsheetsMigrator.Migrate();

                PostPopulateTask.Populate(repository);
            }
        }

        static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new PersistenceModule("Server=127.0.0.1;Port=5432;Database=tag;User Id=tag;Password=tag;", "TAG.Domain"));
            return builder.Build();
        }
    }
}
