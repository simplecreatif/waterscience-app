﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Dapper;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Extensions;
using TAG.Persistence;

namespace TAG.Migrator.Infrastructure
{
    /// <summary>
    /// Fetches data from a table in a source database and converts
    /// the data to entities. Uses pre-populated IdMappers to resolve
    /// properties which correspond to other entities. Uses a natural
    /// key function to prevent creation of new entities if they
    /// already exist in the target database. This also de-duplicates
    /// entities from all sources and allows the migration to be run
    /// multiple times without needing to clear the target database.
    /// </summary>
    public abstract class DatabaseMigrator
    {
        public IRepository Repository { get; }
        public IdMappers Mappers { get; }
        protected readonly IDbConnection conn;

        protected DatabaseMigrator(IRepository repository, IDbConnection conn)
        {
            this.Repository = repository;
            this.conn = conn;
            this.Mappers = new IdMappers();
        }

        /// <summary>
        /// Migrate all records from the specified table to lookup entities.
        /// Uses the source column to retrieve the name for each lookup value.
        /// If no column is specified the second column in the table will be
        /// taken as the lookup name.
        /// </summary>
        /// <typeparam name="TDest">The type of the lookup entity</typeparam>
        /// <param name="sourceTable">The table from which to fetch new lookup data</param>
        /// <param name="sourceColumn">The column to read when generating the lookup's name</param>
        /// <param name="capitalise">Whether to capitalise the first character of the lookup's name</param>
        public void MigrateLookup<TDest>(string sourceTable, string sourceColumn = null, bool capitalise = true) where TDest : Lookup, new()
        {
            Migrate<TDest>(sourceTable, row =>
            {
                // Assume the 2nd column is the lookup code if column not specified
                string name = sourceColumn != null ? row[sourceColumn] : DapperExtensions.GetValue<string>(row, 1);
                name = StringRemapper.Remap(name, capitalise);

                // If the source entity has no name, we'll assume it's a "Not Recorded"
                // value and not migrate it (any references to this lookup should be
                // converted to null)
                if (String.IsNullOrWhiteSpace(name))
                {
                    return null;
                }

                return new TDest { Name = name };
            },
            (newEntity, entity) => entity.Name == newEntity.Name);
        }

        /// <summary>
        /// Migrate data retrieved from the source database with a query. Entities
        /// are created using the map function and deduplication is done by looking
        /// for existing entities in the target database with the comparator function.
        /// </summary>
        /// <typeparam name="TDest">The type of entity to be created</typeparam>
        /// <param name="query">The query used to retrieve data from the source database</param>
        /// <param name="sourceTable">A name or description of the query used for logging purposes</param>
        /// <param name="map">A method which accepts a row from the source database and returns a new entity</param>
        /// <param name="comparator">A method which should return true if an entity is structurally equal to the new entity. This prevents insertion of duplicate entities.</param>
        public void MigrateQuery<TDest>(string query, string sourceTable, Func<dynamic, TDest> map, Expression<Func<TDest, TDest, bool>> comparator = null) where TDest : class, new()
        {
            var rows = conn.Query(query).ToList();
            Logger.LogBeginTableMigration(sourceTable, rows.Count());
            Migrate<TDest>(rows, map, comparator);
        }

        /// <summary>
        /// Migrate data retrieved from the source database table. Entities
        /// are created using the map function and deduplication is done by looking
        /// for existing entities in the target database with the comparator function.
        /// </summary>
        /// <typeparam name="TDest">The type of entity to be created</typeparam>
        /// <param name="sourceTable">The name of the table from which to retrieve data</param>
        /// <param name="map">A method which accepts a row from the source database and returns a new entity</param>
        /// <param name="comparator">A method which should return true if an entity is structurally equal to the new entity. This prevents insertion of duplicate entities.</param>
        public void Migrate<TDest>(string sourceTable, Func<dynamic, TDest> map, Expression<Func<TDest, TDest, bool>> comparator = null) where TDest : class, new()
        {
            var rows = conn.Query("select * from [" + sourceTable + "]").ToList();
            Logger.LogBeginTableMigration(sourceTable, rows.Count());
            Migrate<TDest>(rows, map, comparator);
        }

        private void Migrate<TDest>(IEnumerable<dynamic> source, Func<dynamic, TDest> map, Expression<Func<TDest, TDest, bool>> comparator = null) where TDest : class, new()
        {
            foreach (var row in source)
            {
                // Map the database row to a new entity using the map function
                TDest newEntity = map(row);

                // Assume the first column in the row is the old id of the entity.
                // The new entity will be registered with this id so we can retrieve
                // it later when migrating other entities that have relations to
                // this entity.
                string oldId = DapperExtensions.GetValue<string>(row, 0);

                // Rows can be mapped to null to exclude them from migration
                if (newEntity == null)
                {
                    // Add a mapping from the old id to a null entity. This ensures
                    // that other domain classes which refer to this entity will remap
                    // their reference to null. This is useful when a "Not Recorded"
                    // lookup is rejected and we want all other classes to map their
                    // lookup reference to null.
                    if (!Mappers.HasMappingFor<TDest>(oldId))
                    {
                        Mappers.AddMapping<TDest>(oldId, IdMapper.NullEntity);
                    }

                    Logger.LogRejectingNullEntity(oldId, typeof(TDest));
                    continue;
                }

                // If we have a comparator, use it to prevent insertion of duplicate data
                if (comparator != null)
                {
                    // Curry the comparator with the new entity
                    var appliedComparator = comparator.ApplyPartial(newEntity);

                    // Query the database for any structurally identical entities
                    var entity = Repository.Query<TDest>().Where(appliedComparator).SingleOrDefault();

                    // If there is a structurally identical entity register it with our id mappers
                    if (entity != null)
                    {
                        Mappers.AddMapping<TDest>(oldId, entity);
                        Logger.LogEntityAlreadyExists(oldId, entity);
                        continue;
                    }
                }

                // If there is no structurally identical entity in the database then insert
                // the new entity and register it with our id mappers
                Repository.Add(newEntity);
                Mappers.AddMapping<TDest>(oldId, newEntity);
                Logger.LogMigratingEntity(oldId, newEntity);
            }
        }
    }
}
