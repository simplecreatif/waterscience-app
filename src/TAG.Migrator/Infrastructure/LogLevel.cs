﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Migrator.Infrastructure
{
    public enum LogLevel
    {
        /// <summary>
        /// Log everything
        /// </summary>
        Debug,

        /// <summary>
        /// Log only things that may require user's attention
        /// </summary>
        Warnings
    }
}
