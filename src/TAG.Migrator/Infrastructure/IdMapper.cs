﻿using System;
using System.Collections.Generic;

namespace TAG.Migrator.Infrastructure
{
    /// <summary>
    /// Maps string-based identifiers to entities. This can be used
    /// to keep a record of old entity ids and their corresponding entity
    /// in the new system so other entities are able to load them.
    /// </summary>
    public class IdMapper
    {
        /// <summary>
        /// Used when intentially remapping an old entity to a null entity
        /// (such as a "Not Recorded" lookup value). This is used so we can
        /// distinguish between records that are intentionally mapped to null
        /// versus records which are unintentionally null due to some error
        /// in the mapping process.
        /// </summary>
        public static object NullEntity = new object();

        // Maps old ids to new entities.
        // Strings are used for id because some tables (toxicology Speciesdata) use decimals in object ids.
        private readonly Dictionary<string, object> mappings = new Dictionary<string, object>();

        /// <summary>
        /// Register the id as corresponding to the entity. This allows
        /// the entity to later be retrieved by the id.
        /// </summary>
        /// <param name="oldId"></param>
        /// <param name="newEntity"></param>
        public void AddMapping(string oldId, object newEntity)
        {
            mappings.Add(oldId, newEntity);
        }

        /// <summary>
        /// Get an entity which had been previously associated with
        /// this id. Throws an exception if no entity could be found.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldId"></param>
        /// <returns></returns>
        public T Get<T>(string oldId) where T : class
        {
            T entity;
            if (!TryGet<T>(oldId, out entity))
            {
                throw new ArgumentOutOfRangeException("Tried to retrieve a " + typeof(T).Name + " using old id " + oldId + " but no such entity could be found.");
            }

            return entity;
        }

        /// <summary>
        /// Gets an entity which had been previously associated with
        /// this id. Returns false if no entity could be found.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldId"></param>
        /// <returns></returns>
        public bool TryGet<T>(string oldId, out T entity) where T : class
        {
            object maybeEntity;

            // If we don't have a mapping for this id
            if (!mappings.TryGetValue(oldId, out maybeEntity))
            {
                entity = null;
                return false;
            }

            // If we do have a mapping for this id but it's
            // intentionally null
            if (maybeEntity == IdMapper.NullEntity)
            {
                entity = null;
                return true;
            }

            // We do have a mapping for this id
            entity = (T) maybeEntity;
            return true;
        }
    }
}
