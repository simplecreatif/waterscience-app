﻿using System;
using System.Collections.Generic;
using TAG.Persistence;

namespace TAG.Migrator.Infrastructure
{
    /// <summary>
    /// Wrapper for IdMapper which allows mappings for multiple object types to
    /// be added without causing clashes between ids of different object types.
    /// </summary>
    public class IdMappers
    {
        private readonly Dictionary<Type, IdMapper> mappings = new Dictionary<Type, IdMapper>();

        /// <summary>
        /// Register the id as corresponding to the entity. This allows
        /// the entity to later be retrieved by the id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldId"></param>
        /// <param name="newEntity"></param>
        public void AddMapping<T>(string oldId, object newEntity)
        {
            IdMapper mapper;
            if (!mappings.TryGetValue(typeof (T), out mapper))
            {
                mapper = new IdMapper();
                mappings.Add(typeof(T), mapper);
            }

            mapper.AddMapping(oldId, newEntity);
        }

        /// <summary>
        /// Get an entity which had been previously associated with
        /// this id. Throws an exception if no entity could be found.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldId"></param>
        /// <returns></returns>
        public T Get<T>(string oldId) where T : class
        {
            if (oldId == "0")
            {
                return null;
            }

            T entity;
            if (!TryGet<T>(oldId, out entity))
            {
                throw new ArgumentOutOfRangeException("Tried to retrieve a " + typeof(T).Name + " using old id " + oldId + " but no such entity could be found.");
            }

            return entity;
        }

        /// <summary>
        /// Gets an entity which had been previously associated with
        /// this id. Returns false if no entity could be found.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldId"></param>
        /// <returns></returns>
        public bool TryGet<T>(string oldId, out T entity) where T : class
        {
            var type = typeof (T);

            // If we don't even have a mapper for this entity type
            if (!mappings.ContainsKey(type))
            {
                entity = null;
                return false;
            }

            var mapper = mappings[type];
            T maybeEntity;

            // If we don't have a mapping for this id
            if (!mapper.TryGet(oldId, out maybeEntity))
            {
                entity = null;
                return false;
            }

            // We have a mapping for this id
            entity = maybeEntity;
            return true;
        }

        /// <summary>
        /// Determine whether we have a corresponding entity for the
        /// specified entity type and old id. This may include null
        /// entities where we are intentionally remapping something
        /// to null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldId"></param>
        /// <returns></returns>
        public bool HasMappingFor<T>(string oldId) where T : class
        {
            T _;
            return TryGet<T>(oldId, out _);
        }
    }
}
