﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace TAG.Migrator.Infrastructure
{
    /// <summary>
    /// Drops all tables in a PostgreSQL database
    /// </summary>
    public static class PostgreSQLDatabaseTruncator
    {
        public static void Truncate(IDbConnection connection, string owner)
        {
            var sql =
                "DO $$ DECLARE " +
                "r RECORD; " +
                "BEGIN " +
                    "FOR r IN(SELECT tablename FROM pg_tables WHERE schemaname = current_schema() and tableowner = '" + owner + "') LOOP " +
                "EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE'; " +
                "END LOOP; " +
                "END $$; ";

            connection.Execute(sql);
        }
    }
}
