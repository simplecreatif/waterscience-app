﻿using System;
using System.Linq.Expressions;
using TAG.Migrator.Extensions;

namespace TAG.Migrator.Infrastructure
{
    /// <summary>
    /// Provides a fluent API for mapping a dynamic object to an entity.
    /// Provides methods to load related entities from pre-populated
    /// id mappers.
    /// </summary>
    /// <typeparam name="TEntity">The entity whose properties will be populated during the migration</typeparam>
    public class EntityMigration<TEntity> where TEntity : new()
    {
        public TEntity Result { get; }
        private readonly dynamic row;

        public EntityMigration(dynamic row)
        {
            Result = new TEntity();
            this.row = row;
        }

        /// <summary>
        /// Map the value from the specified function to the destination entity.
        /// </summary>
        /// <param name="dest">The destination property</param>
        /// <param name="src">The source function</param>
        /// <returns></returns>
        public EntityMigration<TEntity> Map(Expression<Func<TEntity, object>> dest, Func<dynamic, object> src)
        {
            SetPropertyValue(Result, dest, src.Invoke(row));
            return this;
        }

        /// <summary>
        /// Map the value from the specified column name to the destination entity.
        /// </summary>
        /// <typeparam name="TProp"></typeparam>
        /// <param name="dest">The destination property</param>
        /// <param name="src">The source column</param>
        /// <returns></returns>
        public EntityMigration<TEntity> Map<TProp>(Expression<Func<TEntity, TProp>> dest, string src)
        {
            SetPropertyValue(Result, dest, DapperExtensions.GetValue<TProp>(row, src));
            return this;
        }

        /// <summary>
        /// Map the specified value to the destination entity.
        /// </summary>
        /// <param name="dest">The destination property</param>
        /// <param name="value">The source value</param>
        /// <returns></returns>
        public EntityMigration<TEntity> MapValue<TProp>(Expression<Func<TEntity, object>> dest, TProp value)
        {
            SetPropertyValue(Result, dest, value);
            return this;
        }

        /// <summary>
        /// Locate an entity from the specified id mappers using the value of the
        /// specified source column, then map that entity to the destination entity.
        /// </summary>
        /// <typeparam name="TProp"></typeparam>
        /// <param name="dest">The destination property</param>
        /// <param name="src">The source column from which a reference identifier will be retrieved</param>
        /// <param name="mappers">The mappers used to map a reference identifier to an entity</param>
        /// <returns></returns>
        public EntityMigration<TEntity> MapReference<TProp>(Expression<Func<TEntity, TProp>> dest, string src, IdMappers mappers) where TProp : class
        {
            string referenceId = DapperExtensions.GetValue<string>(row, src);

            // Don't try to map references when the id is blank or zero, such
            // reference ids are equivalent to a null reference.
            if (String.IsNullOrWhiteSpace(referenceId) || referenceId == "0")
            {
                return this;
            }
            
            var referenceEntity = mappers.Get<TProp>(referenceId);

            SetPropertyValue(Result, dest, referenceEntity);
            return this;
        }

        /// <summary>
        /// Locate an entity from the specified id mappers using the value of the
        /// specified function, then map that entity to the destination entity.
        /// </summary>
        /// <typeparam name="TProp"></typeparam>
        /// <param name="dest">The destination property</param>
        /// <param name="src">The source function from which a reference identifier will be retrieved</param>
        /// <param name="mappers">The mappers used to map a reference identifier to an entity</param>
        /// <returns></returns>
        public EntityMigration<TEntity> MapReference<TProp>(Expression<Func<TEntity, TProp>> dest, Func<dynamic, object> src, IdMappers mappers) where TProp : class
        {
            string referenceId = src.Invoke(row);

            // Don't try to map references when the id is blank or zero, such
            // reference ids are equivalent to a null reference.
            if (String.IsNullOrWhiteSpace(referenceId) || referenceId == "0")
            {
                return this;
            }

            var referenceEntity = mappers.Get<TProp>(referenceId);

            SetPropertyValue(Result, dest, referenceEntity);
            return this;
        }

        private void SetPropertyValue<T, V>(T target, Expression<Func<T, V>> memberExpression, V value)
        {
            var valueAsString = value as string;

            if (valueAsString != null)
            {
                // Remap "Not Recorded" strings to null
                ReflectionExtensions.SetPropertyValue(target, memberExpression, StringRemapper.Remap(valueAsString));
            }
            else
            {
                ReflectionExtensions.SetPropertyValue(target, memberExpression, value);
            }
        }
    }
}
