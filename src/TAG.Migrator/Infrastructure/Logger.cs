﻿using System;
using TAG.Migrator.Extensions;

namespace TAG.Migrator.Infrastructure
{
    public class Logger
    {
        public static LogLevel Level = LogLevel.Debug;

        public static void Error(string message)
        {
            Console.WriteLine("Error: " + message);
        }

        public static void Error(string message, string oldId)
        {
            Console.WriteLine(String.Format("Error: {0} Old Id: {1}", message, oldId));
        }

        public static void Warning(string message)
        {
            if (Level == LogLevel.Debug)
            {
                Console.WriteLine("Warning: " + message);
            }
        }

        public static void Log(string message)
        {
            Console.WriteLine(message);
        }

        public static void LogHeading(string heading)
        {
            Console.WriteLine("");
            Console.WriteLine(heading);
            Console.WriteLine(new String('=', heading.Length));
        }

        public static void LogBeginTableMigration(string tableName, int count)
        {
            LogHeading(String.Format("Migrating table '{0}' ({1} rows) {2}", tableName, count, DateTime.Now));
        }

        public static void LogEntityAlreadyExists(string oldId, object entity)
        {
            if (Level == LogLevel.Debug)
            {
                Console.WriteLine("Entity has already been migrated. Type: {0}, Old Id: {1}, New Id: {2}", entity.GetType().Name, oldId, GetEntityId(entity));
            }
        }

        public static void LogMigratingEntity(string oldId, object entity)
        {
            if (Level == LogLevel.Debug)
            {
                Console.WriteLine("Migrating entity. Type: {0}, Old Id: {1}, New Id: {2}", entity.GetType().Name, oldId, GetEntityId(entity));
            }
        }

        public static void LogRejectingNullEntity(string oldId, Type t)
        {
            if (Level == LogLevel.Debug)
            {
                Console.WriteLine("Rejecting entity because it is equivalent to null (e.g. 'Not Recorded' entities). Type: {0}, Old Id: {1}", t.Name, oldId);
            }
        }

        private static int GetEntityId(object entity)
        {
            return ReflectionExtensions.GetPropertyValue<int>(entity, "Id");
        }
    }
}
