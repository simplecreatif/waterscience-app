﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TAG.Domain.Models;
using TAG.Domain.Models.Lookups;
using TAG.Migrator.Extensions;
using TAG.Migrator.Migrators.GuidelineSpreadsheets.Models;
using TAG.Persistence;

namespace TAG.Migrator.Infrastructure
{
    public abstract class SpreadsheetMigrator
    {
        public IRepository Repository { get; }
        public IdMappers Mappers { get; }

        protected SpreadsheetMigrator(IRepository repository)
        {
            this.Repository = repository;
            this.Mappers = new IdMappers();
        }

        public void MigrateLookup<TDest>(IEnumerable<string> names, bool capitalise = true) where TDest : Lookup, new()
        {
            var preparedNames = names
                .Where(x => x != null)
                .Select(x => StringRemapper.Remap(x, capitalise))
                .Distinct();

            foreach (var name in preparedNames)
            {
                if (Mappers.HasMappingFor<TDest>(name))
                {
                    // Don't try to migrate something migrated in an earlier toxicology group
                    continue;
                }

                // Try to fetch this entity from db first, if no such entity exists create one.
                var entity = Repository.Query<TDest>().SingleOrDefault(x => x.Name == name);
                if (entity == null)
                {
                    entity = new TDest { Name = name };
                    Repository.Add(entity);
                    Logger.LogMigratingEntity(name, entity);
                }
                else
                {
                    Logger.LogEntityAlreadyExists(name, entity);
                }

                Mappers.AddMapping<TDest>(name, entity);
            }
        }

        /// <summary>
        /// This migrates a type of entity within a toxicity value group.
        /// Firstly it deduplicates the toxicity values based on the specified
        /// identifier. This is so we don't try to migrate the same value twice 
        /// (e.g. when the same species is listed one after the other in the
        /// toxicity records).
        /// 
        /// Then we generate the new entity using the specified map function.
        /// 
        /// Then we check to see if this record has already been migrated by
        /// checking against the database for records that match the specified
        /// comparator function. If no such value is found, the new entity is added.
        /// 
        /// Finally an id mapping is generated for this entity using the specified
        /// identifier function. This allows us to map (e.g. species 'X') to the
        /// corresponding entity when migrating data that have associations
        /// with this data.
        /// </summary>
        /// <typeparam name="TDest"></typeparam>
        /// <param name="group"></param>
        /// <param name="map"></param>
        /// <param name="identifier"></param>
        /// <param name="comparator"></param>
        public void Migrate<TDest>(ToxicityValueGroup group, Func<ToxicityValueModel, TDest> map, Func<ToxicityValueModel, string> identifier, Expression<Func<TDest, TDest, bool>> comparator = null) where TDest : class, new()
        {
            foreach (var model in group.DistinctBy(identifier))
            {
                TDest newEntity = map(model);

                if (newEntity == null)
                {
                    continue;
                }

                var oldId = identifier(model);

                if (Mappers.HasMappingFor<TDest>(oldId))
                {
                    // Don't try to migrate something migrated in an earlier toxicology group
                    continue;
                }


                if (comparator != null)
                {
                    var appliedComparator = comparator.ApplyPartial(newEntity);
                    var entity = Repository.Query<TDest>().Where(appliedComparator).SingleOrDefault();

                    // If there is already an entity with the same natural key, register it in our id mappings
                    if (entity != null)
                    {
                        Mappers.AddMapping<TDest>(oldId, entity);
                        Logger.LogEntityAlreadyExists(oldId, entity);
                        continue;
                    }
                }

                // If there is no entity already in the db with the same natural key,
                // insert the new entity
                Repository.Add(newEntity);
                Mappers.AddMapping<TDest>(oldId, newEntity);
                Logger.LogMigratingEntity(oldId, newEntity);
            }
        }
    }
}
