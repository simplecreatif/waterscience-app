﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Migrator.Extensions;

namespace TAG.Migrator.Infrastructure
{
    public static class StringRemapper
    {
        private static readonly string[] rejectedString = { "nr", "none", "not recorded" };

        private static Dictionary<string, string> remappings = new Dictionary<string, string>()
            .AddStringRemapping("Amphibians", "amphibian")
            .AddStringRemapping("Macrophytes", "macrophyte")
            .AddStringRemapping("Microinvertebrate", "microinverterbrate")

            .AddStringRemapping("Days", "day")
            .AddStringRemapping("Hours", "hour")
            .AddStringRemapping("Minutes", "minutes")
            .AddStringRemapping("Months", "months")
            .AddStringRemapping("Weeks", "weeks")

            .AddStringRemapping("% active ingredient", "%active ingred")
            .AddStringRemapping("Organic/Aliphatic", "organic/aliphetic")

            .AddStringRemapping("Standard Error (SE)", "se")
            .AddStringRemapping("Standard Deviation (SD)", "sd")

            .AddStringRemapping("µg CN/L", "µgcn/l")
            .AddStringRemapping("% effluent", "%effluent")
            .AddStringRemapping("% filtrate", "%filtrate")
            .AddStringRemapping("mg NH\x2083/L", "mgnh3/l")
            .AddStringRemapping("mg Cd/L", "mg/l cd")
            .AddStringRemapping("mol/L", "moles/l")
            .AddStringRemapping("% EP1 water", "%ep1 water")
            .AddStringRemapping("mg total NH\x2083/L", "mg total nh3/l")
            .AddStringRemapping("ITU/mg BTI", "itu/mgbti")
            .AddStringRemapping("µg Cr\x2076\x207a/L", "µg/l cr6+")
            .AddStringRemapping("% saturation", "%saturation")

            .AddStringRemapping("Glyphosate", "Glyphosate FRESH")
            .AddStringRemapping("Metolachlor", "Metolachlor FW")
            .AddStringRemapping("Metribuzin", "Metribuzin FW & MW")
            .AddStringRemapping("Simazine", "Simazine MARINE")
            .AddStringRemapping("Imazapic", "Imazapic FW & SW")
            .AddStringRemapping("Isoxaflutole", "Isoxaflutole Marine")
            .AddStringRemapping("Simazine", "SIMAZINE FW");


        /// <summary>
        /// Maps values like "NR", "None" or "Not Recorded" to null.
        /// Maps whitespace to null.
        /// Returns all other values as-is.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="capitalise"></param>
        /// <returns></returns>
        public static string Remap(string value, bool capitalise = true)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            var trimmed = value.Trim();
            var lower = trimmed.ToLower();

            if (rejectedString.Contains(lower))
            {
                return null;
            }

            string maybeRemappedValue;
            if (remappings.TryGetValue(lower, out maybeRemappedValue))
            {
                return maybeRemappedValue;
            }

            return capitalise ? trimmed.Capitalise() : trimmed;
        }
    }
}
