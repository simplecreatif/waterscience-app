﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;

namespace TAG.Persistence
{
    public class NHibernateRepository : IRepository
    {
        private readonly ISession session;

        public NHibernateRepository(ISession session)
        {
            this.session = session;
        }

        public T Get<T>(object id) where T : class
        {
            return session.Get<T>(id);
        }

        public T Get<T>(int? id) where T : class
        {
            if (!id.HasValue)
            {
                return null;
            }

            return session.Get<T>(id.Value);
        }

        public T Get<T>(object id, Type t) where T : class
        {
            return (T)session.Get(t, id);
        }

        public IEnumerable<T> List<T>() where T : class
        {
            return session.Query<T>().AsEnumerable();
        }

        public IEnumerable<T> List<T>(Type t) where T : class
        {
            return session.CreateCriteria(t)
                .List<T>();
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return session.Query<T>();
        }

        /// <summary>
        /// Query the specified runtime type. Can be used when
        /// querying via a base class. Specify the base class
        /// as the generic type then the concrete type as the first
        /// parameter.
        /// This is done through reflection and may not be performant.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public IQueryable<T> Query<T>(Type t) where T : class
        {
            var closedMethod = typeof(NHibernateRepository)
                .GetMethods()
                .Single(x => x.Name == "Query" && !x.GetParameters().Any())
                .MakeGenericMethod(t);

            return (IQueryable<T>)closedMethod.Invoke(this, new object[0]);
        }

        public void Add<T>(T entity) where T : class
        {
            session.Save(entity);
        }

        public void Remove<T>(T entity) where T : class
        {
            session.Delete(entity);
        }
    }
}
