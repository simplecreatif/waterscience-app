﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using TAG.Persistence.Conventions;

namespace TAG.Persistence
{
    internal class PostgreSQLSessionFactoryBuilder
    {
        public static ISessionFactory Build(string connectionString, string mappingAssembly)
        {
            return Fluently.Configure()
                .Database(PostgreSQLConfiguration.PostgreSQL82.ConnectionString(connectionString))
                .ExposeConfiguration(Configure)
                .Mappings(x =>
                {
                    // Load mappings which define how domain classes are mapped to the database
                    x.FluentMappings.AddFromAssembly(Assembly.Load(mappingAssembly));

                    // Customise nHibernate's conventions for constructing a new schema
                    x.FluentMappings.Conventions.Add<PersistStringsAsTextConvention>();
                })
                .BuildSessionFactory();
        }

        private static void Configure(Configuration configuration)
        {
            // Update database schema
            new SchemaUpdate(configuration).Execute(false, true);
        }
    }
}
