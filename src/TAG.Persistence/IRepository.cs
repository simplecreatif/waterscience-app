﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Persistence
{
    public interface IRepository
    {
        T Get<T>(object id) where T : class;
        T Get<T>(int? id) where T : class;
        T Get<T>(object id, Type t) where T : class;
        IEnumerable<T> List<T>() where T : class;
        IEnumerable<T> List<T>(Type t) where T : class;
        IQueryable<T> Query<T>() where T : class;
        IQueryable<T> Query<T>(Type t) where T : class;
        void Add<T>(T entity) where T : class;
        void Remove<T>(T entity) where T : class;
    }
}
