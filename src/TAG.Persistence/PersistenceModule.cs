﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using NHibernate;

namespace TAG.Persistence
{
    public class PersistenceModule : Module
    {
        private readonly string connectionString;
        private readonly string mappingAssembly;

        public PersistenceModule(string connectionString, string mappingAssembly)
        {
            this.connectionString = connectionString;
            this.mappingAssembly = mappingAssembly;
        }

        protected override void Load(ContainerBuilder builder)
        {
            // Session factory builder
            builder.Register(x => PostgreSQLSessionFactoryBuilder.Build(connectionString, mappingAssembly))
                .As<ISessionFactory>()
                .SingleInstance();

            // ISession
            builder.Register(x =>
                {
                    var session = x.Resolve<ISessionFactory>().OpenSession();
                    session.FlushMode = FlushMode.Commit;
                    return session;
                })
                .As<ISession>()
                .InstancePerLifetimeScope()
                .ExternallyOwned()
                .OnActivated(BeginSession)
                .OnRelease(EndSession);

            // IDbConnection (exposed because there are cases where we need to use Dapper)
            builder.Register(x => x.Resolve<ISession>().Connection)
                .As<IDbConnection>()
                .InstancePerLifetimeScope()
                .ExternallyOwned();

            // IRepository
            builder.RegisterType<NHibernateRepository>()
                .As<IRepository>()
                .InstancePerLifetimeScope();
        }

        private void BeginSession(IActivatedEventArgs<ISession> args)
        {
            var session = args.Instance;
            session.BeginTransaction();
        }

        private void EndSession(ISession session)
        {
            try
            {
                session.Transaction.Commit();
            }
            finally
            {
                session.Close();
                session.Dispose();
            }
        }
    }
}
