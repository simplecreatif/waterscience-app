import * as actionTypes from "../constants/actionTypes";
import initialState from "./initialState";
import _ from "lodash";

export default function entities(state = initialState.guidelines, action = null) {
  switch (action.type) {
    case actionTypes.SAVE_GUIDELINE_RESET:
      return _.omit(state, action.guideline.id);

    case actionTypes.SAVE_GUIDELINE_BEGIN:
      return update(state, action.guideline, { status: "saving"});

    case actionTypes.SAVE_GUIDELINE_FAILED:
      return update(state, action.guideline, { status: "saveFailed", saveError: action.saveError });

    case actionTypes.SAVE_GUIDELINE_COMPLETE:
      // Note this action is also handled in entitiesReducer,
      // where the saved guideline's value is applied to all
      // toxicity value 2016.
      return update(state, action.guideline, { status: "saved" });

    default:
      return state;
  }
}

function update(state, guideline, properties) {
  return {
    ... _.omit(state, guideline.id)
    , [guideline.id]: {
      guideline: guideline
      , ...properties
    }
  };
}
