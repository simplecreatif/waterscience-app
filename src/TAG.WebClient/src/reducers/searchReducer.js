import * as actionTypes from "../constants/actionTypes";
import initialState from "./initialState";

export default function search(state = initialState.search, action = null) {
  switch (action.type) {
    case actionTypes.SEARCH_RESET:
      return {
        ...state
        , isSearching: false
        , searchError: null
        , results: null
      };

    case actionTypes.SEARCH_BEGIN:
      return {
        ...state
        , isSearching: true
        , searchError: null
      };

    case actionTypes.SEARCH_FAILED:
      return {
        ...state
        , isSearching: false
        , searchError: "The search could not be performed."
      };

    case actionTypes.SEARCH_COMPLETE:
      return {
        ...state
        , isSearching: false
        , searchError: null
        , results: action.results
      };

    default:
      return state;
  }
}
