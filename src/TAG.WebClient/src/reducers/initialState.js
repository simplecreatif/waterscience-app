export default {
  entities: { }
  , search: {
    isSearching: false
    , searchError: null
    , results: null
  }
  , environment: {
    isInitialised: false
    , initialiseError: null

    // scaffoldConfigurations should be of the form:
    // entityType: {
    //   selectorProperty: "(property name to show when generating a drop-down box with data from this type of entity)"
    //   , displayName: "(text string to use when displaying the name of this type of entity, such as in headings.)"
    //   , styles: {
    //       propertyName: "desiredCSSClass"
    //       , anotherPropertyName: "someOtherCSSClass
    //     }
    //   , listLinks: [ { text: "(text for a hyperlink to show on the entity list page)", href: "/example" } ]
    //   , hiddenProperties: [ "aProperty" ] // a list of properties to hide on the create and update pages
    //   , isScaffolded: false // determine whether to generate navigation and CRUD pages for this entity.
    //                         // can be useful when non-changing entities are scaffolded for lookup purposes (e.g. roles)
    // }
    , scaffoldConfigurations: {
      species: {
        selectorProperty: "scientificName"
        , styles: {
          scientificName: "scientific-name"
        }
      }
      , speciesclass: {
        displayName: "Species’ Class"
      }
      , speciesphylum: {
        displayName: "Species’ Phylum"
      }
      , endpoint: {
        selectorProperty: "endpoint"
      }
      , reference: {
        selectorProperty: "title"
      }
      , guidelinegroup: {
        selectorProperty: x => "Guideline group #" + x.id + ", guideline: " + x.guidelineValue + ", toxicant:" + x.toxicant
      }
      , toxicityvalue2016: {
        listLinks: [
          { text: "Manage Toxicity Value 2016", href: "/toxicityvalue2016/manage" }
        ]
        , hiddenProperties: [ "guidelineGroupId" ]
      }
      , role: {
        isScaffolded: false
      }
    }
  }
  , auth: {
    isLoggedIn: false
    , isLoggingIn: false
    , loginFailed: false
    , currentUser: null
    , isCheckingUser: true
  }
  // Stores guidelines that are in the process
  // of being saved (and some state to indicate whether
  // the save is in progress, completed, or saved.
  // This is keyed by the guideline id
  , guidelines: { }

  // Stores information about a toxicity value that
  // the user wishes to move from one group to another.
  , moveToxicityValue: {
    toxicityValue: null
    , status: null
  }
};
