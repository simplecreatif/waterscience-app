import * as actionTypes from "../constants/actionTypes";
import initialState from "./initialState";
import _ from "lodash";

export default function environment(state = initialState.environment, action = null) {
  switch (action.type) {
    case actionTypes.INITIALISE_ENVIRONMENT_COMPLETE:
      // We're using lodash merge here because we need to do a deep
      // immutable merge of scaffoldConfigurations with initialState.
      // this is because scaffoldConfigurations is a deeply hierarchical
      // object whose properties can be set by default and we don't want
      // to override those defaults by doing a more typical spread operator.
      // Also, the initial state in scaffoldConfigurations takes precedence
      // over what's provided to this method. This will cause issues if this
      // action is called multiple times, however it is expected that this will
      // only be called once.
      return {
        ...state
        , isInitialised: true
        // Randomly generated client id. This is used to drop any
        // realtime notifications which originated from actions performed
        // by this client.
        , clientId: action.clientId
        , initialiseError: action.initialiseError
        , scaffoldConfigurations: _.merge({}, action.scaffoldConfigurations, state.scaffoldConfigurations)
      };

    default:
      return state;
  }
}
