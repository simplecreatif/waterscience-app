import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import entities from "./entitiesReducer";
import guidelines from "./guidelinesReducer";
import moveToxicityValue from "./moveToxicityValueReducer";
import search from "./searchReducer";
import environment from "./environmentReducer";
import auth from "./authReducer";

const rootReducer = combineReducers({
  entities
  , guidelines
  , moveToxicityValue
  , search
  , environment
  , auth
  , routing: routerReducer
});

export default rootReducer;
