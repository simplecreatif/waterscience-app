import * as actionTypes from "../constants/actionTypes";
import initialState from "./initialState";

export default function auth(state = initialState.auth, action = null) {
  switch(action.type) {
    case actionTypes.LOGIN_BEGIN:
      return {
        ...state
        , isLoggingIn: true
        , loginFailed: false
      };

    case actionTypes.LOGIN_COMPLETE:
      return {
        ...state
        , isLoggingIn: false
        , loginFailed: false
        , isLoggedIn: true
        , currentUser: action.user
      };

    case actionTypes.LOGIN_FAILED:
      return {
        ...state
        , isLoggingIn: false
        , loginFailed: true
      };

    case actionTypes.LOGOUT_COMPLETE:
      return {
        ...state
        , isLoggedIn: false
        , currentUser: null
      };

    case actionTypes.CHECK_USER_BEGIN:
      return {
          ...state
        , isCheckingUser: true
      };

    case actionTypes.CHECK_USER_COMPLETE:
      return {
          ...state
        , isLoggedIn: action.isLoggedIn
        , currentUser: action.user
        , isCheckingUser: false
      };

    default:
      return state;
  }
}
