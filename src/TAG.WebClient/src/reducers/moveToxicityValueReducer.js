import * as actionTypes from "../constants/actionTypes";
import initialState from "./initialState";

export default function entities(state = initialState.moveToxicityValue, action = null) {
  switch (action.type) {
    case actionTypes.MOVE_TOXICITY_VALUE_MODE:
      return {
        ...state
        , toxicityValue: action.toxicityValue
      };
    case actionTypes.MOVE_TOXICITY_VALUE_RESET:
      return {
        ...state
        , toxicityValue: null
        , status: null
        , saveError: null
      };
    case actionTypes.MOVE_TOXICITY_VALUE_BEGIN:
      return {
        ...state
        , status: "saving"
      };
    case actionTypes.MOVE_TOXICITY_VALUE_FAILED:
      return {
        ...state
        , status: "saveFailed"
        , saveError: action.saveError
      };
    case actionTypes.MOVE_TOXICITY_VALUE_COMPLETE:
      // Just reset back to original state to exit
      // move mode.
      return {
        ...state
        , toxicityValue: null
        , status: null
        , saveError: null
      };
    default:
      return state;
  }
}
