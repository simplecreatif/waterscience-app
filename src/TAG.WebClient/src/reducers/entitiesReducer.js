import * as actionTypes from "../constants/actionTypes";
import initialState from "./initialState";
import _ from "lodash";

export default function entities(state = initialState.entities, action = null) {
  switch (action.type) {
    case actionTypes.LIST_ENTITIES_BEGIN:
      return update(state, action.entityType, {
        isListing: true
        , listError: null
        , entities: null
      });

    case actionTypes.LIST_ENTITIES_FAILED:
      return update(state, action.entityType, {
        isListing: false
        , listError: action.listError
        , entities: null
      });

    case actionTypes.LIST_ENTITIES_COMPLETE:
      return update(state, action.entityType, {
        isListing: false
        , listError: null
        , entities: action.entities
      });

    case actionTypes.GET_ENTITY_RESET:
      return update(state, action.entityType, {
        isGetting: null
        , getError: null
        , entity: null
      });

    case actionTypes.GET_ENTITY_BEGIN:
      return update(state, action.entityType, {
        isGetting: true
        , getError: null
        , entity: null
      });

    case actionTypes.GET_ENTITY_FAILED:
      return update(state, action.entityType, {
        isGetting: false
        , getError: action.getError
        , entity: null
      });

    case actionTypes.GET_ENTITY_COMPLETE:
      return update(state, action.entityType, {
        isGetting: false
        , getError: null
        , entity: action.entity
      });

    case actionTypes.SAVE_ENTITY_RESET:
      return update(state, action.entityType, {
        isSaving: null
        , saveError: null
        , savedEntity: null
      });

    case actionTypes.SAVE_ENTITY_BEGIN:
      return update(state, action.entityType, {
        isSaving: true
      });

    case actionTypes.SAVE_ENTITY_FAILED:
      return update(state, action.entityType, {
        isSaving: false
        , saveError: action.saveError
      });

    case actionTypes.SAVE_ENTITY_COMPLETE:
      return update(state, action.entityType, {
        ...merge(state, action.entityType, action.listEntity, action.getEntity)
        , isSaving: false
        , saveError: null
        , savedEntity: action.getEntity
      });

    case actionTypes.DELETE_ENTITY_RESET:
      return update(state, action.entityType, {
        isDeleting: null
        , deleteError: null
        , deletedEntityId: null
      });

    case actionTypes.DELETE_ENTITY_BEGIN:
    return update(state, action.entityType, {
      isDeleting: true
      , deleteError: null
    });

    case actionTypes.DELETE_ENTITY_FAILED:
      return update(state, action.entityType, {
        isDeleting: false
        , deleteError: action.deleteError
      });

    case actionTypes.DELETE_ENTITY_COMPLETE:
      return update(state, action.entityType, {
        ...mergeDelete(state, action.entityType, action.id)
        , isDeleting: false
        , deleteError: null
        , deletedEntityId: action.id
      });

    case actionTypes.MERGE_ENTITY:
      return update(state, action.entityType, merge(state, action.entityType, action.listEntity, action.getEntity));

    case actionTypes.MERGE_DELETE_ENTITY:
      return update(state, action.entityType, mergeDelete(state, action.entityType, action.id));

    case actionTypes.SAVE_GUIDELINE_COMPLETE:
      // Apply new guideline value to all toxicity values which
      // belong to the saved guideline group.
      return update(state, "toxicityvalue2016", {
        entities: state.toxicityvalue2016.entities
          .map(x => x.guidelineGroupId == action.guideline.id ? { ...x, guidelineGroupValue: action.guideline.guidelineValue } : x )
      });
    case actionTypes.MOVE_TOXICITY_VALUE_COMPLETE:
      // A toxicity value has moved to a new guideline.
      // we need to update the toxicity value with the new guideline id
      return update(state, "toxicityvalue2016", {
        entities: state.toxicityvalue2016.entities
          .map(x => x.id == action.toxicityValue.id ? {
            ...x
            , guidelineGroupId: action.guideline.id
            , guidelineGroupValue: action.guideline.guidelineValue
          } : x)
      });
    default:
      return state;
  }
}

// Merges a entity which was updated by another client
function merge(state, entityType, listEntity, getEntity) {
  // We've received an updated entity from another client.
  // Merge both the list entity and the get entity into
  // our state if our state has been loaded (otherwise don't
  // bother because the user isn't or hasn't viewed this type
  // of entity yet.
  let entityState = state[entityType];
  if(entityState == null) {
    return {};
  }
  let newState = {};
  if(!entityState.isListing && entityState.entities != null) {
    // Merge into list of entities
    // Immutably replace the old entity with the new one in the entities list
    // (look up the entity by id)
    let i = _.findIndex(entityState.entities, x => x.id == listEntity.id);
    if(i > -1) {
      // We received an edited entity and need to replace our stale copy
      newState.entities = [
        ...entityState.entities.slice(0, i)
        , listEntity
        , ...entityState.entities.slice(i + 1)
      ];
    }
    else {
      // We received a new entity and need to add it to our collection
      newState.entities = [
        listEntity
        , ...entityState.entities
      ];
    }
  }

  if(!entityState.isGetting && entityState.entity != null && entityState.entity.id == getEntity.id){
    // Replace shown entity
    newState.entity = getEntity;
  }

  return newState;
}

// Removes an entity which was deleted by another client
function mergeDelete(state, entityType, id) {
  let entityState = state[entityType];
  if(entityState == null) {
    return {};
  }

  let newState = {};
  if(!entityState.isListing && entityState.entities != null) {
    let i = _.findIndex(entityState.entities, x => x.id == id);
    if(i > -1) {
      newState.entities = [
        ...entityState.entities.slice(0, i)
        , ...entityState.entities.slice(i + 1)
      ];
    }

    if(!entityState.isGetting && entityState.entity != null && entityState.entity.id == id){
      newState.entity = null;
      newState.getError = "The entity was deleted by another user.";
    }
  }

  return newState;
}

// Creates a copy of the state with updates to the
// state of the specified entity type. This is done
// immutably.
function update(state, entityType, newState) {
  if(!entityType) {
    throw "Tried to update entities without an entityType.";
  }
  let oldEntityState = state[entityType];

  if(oldEntityState == null){
    // Set initial state for this entity
    oldEntityState = {
      isListing: null
      , listError: null
      , entities: null

      , isGetting: null
      , getError: null
      , entity: null

      , isSaving: null
      , saveError: null
      , savedEntity: null

      , isDeleting: null
      , deleteError: null
    };
  }
  return {
    ...state
    , [entityType]: {
      ...oldEntityState
      , ...newState
    }
  };
}
