/* eslint-disable import/default */

import React from "react";
import {render} from "react-dom";
import { Provider } from "react-redux";
import { Router, browserHistory } from "react-router";
import "core-js"; // ES6 shim
import "./extensions/stringExtensions";
import routes from "./routes";
import configureStore from "./store/configureStore";
require("./favicon.ico"); // Tell webpack to load favicon.ico
import "./styles/styles.scss"; // Yep, that's right. You can import SASS/CSS files too! Webpack will run the associated loader and plug this into the page.

// Imports for the performant searchable select list (react-virtualized-select)
import "react-virtualized/styles.css";
import "react-select/dist/react-select.css";
import "react-virtualized-select/styles.css";

import "react-bootstrap-table/dist/react-bootstrap-table.min.css";

import { syncHistoryWithStore } from "react-router-redux";
import AxiosConfigurationService from "./services/AxiosConfigurationService";

const store = configureStore(browserHistory);

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);

AxiosConfigurationService.configure(store);

render(
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>, document.getElementById("app")
);
