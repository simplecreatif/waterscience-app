import React, { PropTypes } from "react";
import { Link } from "react-router";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import GuidelineEditor from "../components/GuidelineEditor";

export default class ToxicityValueGroup extends React.Component {
  static propTypes = {
    guideline: PropTypes.object.isRequired
    , toxicityValues: PropTypes.array.isRequired
    , guidelineSaveState: PropTypes.object
    , actions: PropTypes.object.isRequired

    // Callback which is called whenever a user specifies that they want
    // to move a toxicity value from this group to another group. The toxicity
    // value is provided to the handler.
    , onBeginMove: PropTypes.func.isRequired

    // Callback which is called whenever a user has elected to move a toxicity
    // value to this guideline. The toxicity value and guideline entities
    // are provided to the handler.
    , onMove: PropTypes.func.isRequired

    , isMoveMode: PropTypes.bool.isRequired
    , canReceiveToxicityValue: PropTypes.bool.isRequired
    , movingToxicityValue: PropTypes.object
  };

  constructor() {
    super();

    this.renderLinks = this.renderLinks.bind(this);
    this.handleBeginMove = this.handleBeginMove.bind(this);
    this.handleClickMoveTarget = this.handleClickMoveTarget.bind(this);
    this.handleStyleRow = this.handleStyleRow.bind(this);
  }

  handleBeginMove(toxicityValue, e) {
    e.preventDefault();
    this.props.onBeginMove(toxicityValue);
  }

  renderLinks(id, toxicityValue) {
    return (<span>
      <Link to={"/toxicityvalue2016/show/" + id}>Show</Link> &middot;
      <Link to={"/toxicityvalue2016/update/" + id}>Update</Link> &middot;
      <a href="#" title="Move to a different guideline group" onClick={this.handleBeginMove.bind(this, toxicityValue)}>Move</a> &middot;
      <Link to={"/toxicityvalue2016/delete/" + id}>Delete</Link>
    </span>);
  }

  // This handles when a user has clicked this table during move mode.
  // This indicates that the user wishes to move a toxicity value to
  // the guideline group associated with this table.
  handleClickMoveTarget() {
    this.props.onMove(this.props.guideline);
  }

  maybeRenderMoveOverlay() {
    if(!this.props.isMoveMode){
      return <div />;
    }

    let classNames = "table-overlay";
    if(this.props.canReceiveToxicityValue) {
      classNames += " move-target";
    }
    else {
      classNames += " not-target";
    }

    return <div className={classNames} onClick={this.handleClickMoveTarget}></div>;
  }

  handleStyleRow(row) {
    // If we are in move mode and this row corresponds
    // to the toxicity value that we're moving, style
    // the row differently to make it clearer to the user
    // what's being moved.
    const { movingToxicityValue } = this.props;

    if(movingToxicityValue != null && movingToxicityValue.id == row.id) {
      return "moving-toxicity-value-row";
    }

    return null;
  }

  render() {
    let commonProps = {
      dataAlign: "right"
      , dataSort: true
    };

    return (<div className="guideline-group-table">
      <GuidelineEditor guideline={this.props.guideline} guidelineSaveState={this.props.guidelineSaveState} actions={this.props.actions} />
      <Link to={"/toxicityvalue2016/create?guidelineGroupId=" + this.props.guideline.id}>Create New Toxicity Value 2016 in this group</Link>
      <BootstrapTable
        data={this.props.toxicityValues}
        trClassName={this.handleStyleRow}
        striped
        hover>
        <TableHeaderColumn {...commonProps} isKey={true} hidden={true} dataField="id">Id</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="chemicalName">Chemical Name</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="speciesScientificName">Scientific Name</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="duration">Duration</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="durationUnit">Duration Unit</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="effect">Effect</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="endpoint">Endpoint</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="concentration">Concentration</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="concentrationUnit">Concentration Unit</TableHeaderColumn>
        <TableHeaderColumn key="_links" hidden={this.props.isMoveMode} dataAlign="right" dataField="id" dataFormat={this.renderLinks} width="210" />
      </BootstrapTable>
      {this.maybeRenderMoveOverlay()}
    </div>);
  }
}
