import React, { PropTypes } from "react";
import Editor from "../common/Editor";

export default class SearchPropertyField extends React.Component {
  static propTypes = {
    searchProperties: PropTypes.object.isRequired
    , propertyName: PropTypes.string.isRequired
    , displayName: PropTypes.string.isRequired
    , onChange: PropTypes.func
  };

  constructor() {
    super();

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(checked) {
    this.props.searchProperties[this.props.propertyName] = checked;
    if(this.props.onChange) {
      this.props.onChange();
    }
    this.forceUpdate();
  }

  render() {
    const { searchProperties, propertyName, displayName } = this.props;

    let isChecked = searchProperties[propertyName] == true;

    return <Editor label={displayName} value={isChecked} onChange={this.handleChange} type="checkbox" orientation="horizontal" />;
  }
}
