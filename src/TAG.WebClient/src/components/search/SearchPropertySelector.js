import React, { PropTypes } from "react";
import SearchPropertyField from "./SearchPropertyField";
import ToxicityValueProperties from "../../constants/toxicityValueProperties";
import PureRenderMixin from "react-addons-pure-render-mixin";

export default class SearchPropertySelector extends React.Component {
  static propTypes = {
    searchProperties: PropTypes.object.isRequired
    , onChange: PropTypes.func
  };

  constructor(){
    super();

    // Note this only does a shallow compare so if searchProperties contents
    // are changed externally, this won't re-render. In that case a custom
    // shouldComponentUpdate should be developed.
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }

  render() {
    const { searchProperties } = this.props;

    let options = ToxicityValueProperties;

    return (<div style={{maxHeight: "230px", overflowY: "scroll", border: "1px solid #ccc", padding:"10px"}}>
      {Object.keys(options).map(k => <SearchPropertyField searchProperties={searchProperties} propertyName={k} displayName={options[k]} key={k} /> )}
    </div>);
  }
}
