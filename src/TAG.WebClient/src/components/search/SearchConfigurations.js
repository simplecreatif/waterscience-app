import React, { PropTypes } from "react";
// import Editor from "../common/Editor";
// import LabelWithValue from "../common/LabelWithValue";
import ReactSelect from "react-select";
import { Button } from "react-bootstrap";
import ErrorList from "../common/ErrorList";

export default class SearchConfigurations extends React.Component {
  static propTypes = {
    searchConfiguration: PropTypes.object.isRequired
    , searchConfigurationId: PropTypes.number
    , onSelectSearchConfiguration: PropTypes.func.isRequired
    , onSaveExistingSearchConfiguration: PropTypes.func.isRequired
    , onDeleteSearchConfiguration: PropTypes.func.isRequired
    , onSaveNewSearchConfiguration: PropTypes.func.isRequired
    , resetSaveAndDeleteStatus: PropTypes.func.isRequired
  };

  constructor() {
    super();

    this.handleSearchConfigurationSelect = this.handleSearchConfigurationSelect.bind(this);
    this.handleSaveAs = this.handleSaveAs.bind(this);
    this.handleSaveExisting = this.handleSaveExisting.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleNewNameChange = this.handleNewNameChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSaveNew = this.handleSaveNew.bind(this);

    this.state = {
      isSavingAs: false
      , newSearchConfigurationName: ""
    };
  }

  componentWillUpdate(nextProps) {
    // if the selected searchconfiguration change, show dropdown (occurs after successful save as)
    if (this.props.searchConfigurationId !== nextProps.searchConfigurationId) {
      this.setState({
        ...this.state
        , isSavingAs: false
      });
    }
  }

  handleSearchConfigurationSelect(event) {
    const id = event != null ? event.value : null;
    this.props.onSelectSearchConfiguration(id);
  }

  handleSaveAs() {
    this.setState({
      ...this.state
      , isSavingAs: true
      , newSearchConfigurationName: ""
    });

    this.props.resetSaveAndDeleteStatus();
  }

  handleSaveExisting() {
    this.props.onSaveExistingSearchConfiguration();
  }

  handleDelete() {
    this.props.onDeleteSearchConfiguration();
  }

  handleNewNameChange(event) {
    this.setState({
      ...this.state
      , newSearchConfigurationName: event.target.value
    });
  }

  handleCancel() {
    this.setState({
      ...this.state
      , isSavingAs: false
      , newSearchConfigurationName: ""
    });

    this.props.resetSaveAndDeleteStatus();
  }

  handleSaveNew() {
    this.props.onSaveNewSearchConfiguration(this.state.newSearchConfigurationName);
  }

  render() {
    let searchConfigurationOptions = this.props.searchConfiguration.entities
      .map(x => ({ label: x.name, value: x.id }));
    const label = "Saved search name";
    const isProcessing = this.props.searchConfiguration.isSaving || this.props.searchConfiguration.isDeleting;
    const canSaveOrDeleteExisting = this.props.searchConfigurationId && !isProcessing;
    const canSaveNew = this.state.newSearchConfigurationName && !isProcessing;

    return (<div>
      <h4>Saved searches</h4>
      {
        this.state.isSavingAs
        ? <div>
            <label className="sr-only">{label}</label>
            <input type="text" value={this.state.newSearchConfigurationName} onChange={this.handleNewNameChange} placeholder="Enter a name" className="form-control search-configuration-input" />
            <Button type="button" onClick={this.handleCancel} disabled={isProcessing}>Cancel</Button>
            <Button bsStyle="success" onClick={this.handleSaveNew} disabled={!canSaveNew}>Save</Button>
          </div>
        : <div>
            <label className="sr-only">{label}</label>
            <ReactSelect options={searchConfigurationOptions} value={this.props.searchConfigurationId} onChange={this.handleSearchConfigurationSelect} className="search-configuration-input"/>
            <Button type="button" onClick={this.handleSaveAs}>Save As</Button>
            <Button bsStyle="success" onClick={this.handleSaveExisting} disabled={!canSaveOrDeleteExisting}>Save</Button>
            <Button bsStyle="danger" onClick={this.handleDelete} disabled={!canSaveOrDeleteExisting}>Delete</Button>
          </div>
      }
      <ErrorList errors={this.props.searchConfiguration.saveError} />
      <ErrorList errors={this.props.searchConfiguration.deleteError} />
    </div>);
  }
}
