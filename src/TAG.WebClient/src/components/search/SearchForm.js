import React, { PropTypes } from "react";
import Editor from "../common/Editor";
import SearchPropertySelector from "./SearchPropertySelector";
import { Button } from "react-bootstrap";
import _ from "lodash";
import SpeciesSelector from "../common/SpeciesSelector";

export default class SearchForm extends React.Component {
  static propTypes = {
    chemicals: PropTypes.array.isRequired
    , toxicants: PropTypes.array.isRequired
    , species: PropTypes.array.isRequired
    , effects: PropTypes.array.isRequired
    , endpoints: PropTypes.array.isRequired
    , onSearch: PropTypes.func.isRequired
    , onDownloadGuidelineDerivations: PropTypes.func.isRequired
    , formInputs: PropTypes.object.isRequired
  };

  constructor() {
    super();

    this.handleSearch = this.handleSearch.bind(this);
    this.handleCASNumberChanged = this.handleCASNumberChanged.bind(this);
    this.handleChemicalChanged = this.handleChemicalChanged.bind(this);
    this.handleDownloadGuidelineDerivations = this.handleDownloadGuidelineDerivations.bind(this);
  }

  componentWillMount() {
    this.mapChemicalsByCASNumber(this.props.chemicals);
  }

  componentWillReceiveProps(nextProps) {
    this.mapChemicalsByCASNumber(nextProps.chemicals);
  }

  mapChemicalsByCASNumber(chemicals) {
    this.setState({
      chemicalsByCASNumber: _.keyBy(chemicals || {}, x => x.casNumber)
    });
  }

  handleCASNumberChanged(casNumber) {
    // If there's a chemical with a matching CAS Number,
    // propagate the selection to the chemical selector.
    // Strip and trim the casNumber first
    let preppedCASNumber = this.prepareCASNumber(casNumber);
    let chemical = this.props.formInputs.chemicalsByCASNumber[preppedCASNumber];

    this.setState({
      searchOptions: {
        ...this.props.formInputs.searchOptions
        , chemicalId: chemical ? chemical.id : null
      }
    });
  }

  handleChemicalChanged(chemicalId, option) {
    // Propagate chemical selections back to the CAS Number box
    this.setState({
      searchOptions: {
        ...this.props.formInputs.searchOptions
        , casNumber: this.displayCASNumber(_.get(option, "chemical.casNumber"))
      }
    });
  }

  // Strips hyphens from a cas number
  prepareCASNumber(casNumber) {
    if(casNumber == null) {
      return null;
    }

    return casNumber.toString().replace(/-/g, "").trim();
  }

  // Adds hyphens to a cas number
  displayCASNumber(casNumber) {
    if(casNumber == null){
      return null;
    }

    let prepped = this.prepareCASNumber(casNumber);

    if(prepped.length > 3) {
      return prepped.replace(/^(\d+)(\d{2})(\d)$/, "$1-$2-$3");
    }

    if(prepped.length > 1) {
      return prepped.replace(/^(\d+)(\d)$/, "$1-$2");
    }

    return prepped;
  }

  getSearchModel() {
    const { searchOptions } = this.props.formInputs;
    // map searchProperties to an array of strings because this is what the api expects
    return {
      ...searchOptions
      , casNumber: this.prepareCASNumber(searchOptions.casNumber)
      // These options come from multi-selectors. The multi-selectors
      // write directly to state and store options objects. We grab the
      // "value" from these options objects because they correspond to
      // the entity's id.
      , databases: (this.props.formInputs.databases || []).map(x => x.value)
      , speciesIds: (this.props.formInputs.species || []).map(x => x.value)
      , effectIds: (this.props.formInputs.effects || []).map(x => x.value)
      , endpointIds: (this.props.formInputs.endpoints || []).map(x => x.value)
      , searchProperties: Object.keys(searchOptions.searchProperties)
        .filter(x => searchOptions.searchProperties[x] == true)
    };
  }

  handleSearch() {
    let model = this.getSearchModel();
    this.props.onSearch(model);
  }

  handleDownloadGuidelineDerivations() {
    let model = this.getSearchModel();
    this.props.onDownloadGuidelineDerivations(model);
  }

  render() {
    let chemicalOptions = this.props.chemicals.map(x => ({ label: x.name, value: x.id, chemical: x }));
    let toxicantOptions = this.props.toxicants.map(x => ({ label: x.name, value: x.id }));
    let effectOptions = this.props.effects.map(x => ({ label: x.name, value: x.id }));
    let endpointOptions = this.props.endpoints.map(x => ({ label: x.abbreviation, value: x.id }));
    const formInputs = this.props.formInputs;
    const { searchOptions } = formInputs;

    return (<div>
      <h4>Search by...</h4>
      <Editor label="Database" context={formInputs} property="databases" options={formInputs.databaseOptions} type="react-virtualized-select" multi/>
      <Editor label="CAS Number" context={searchOptions} property="casNumber" onChange={this.handleCASNumberChanged}/>
      <Editor label="Chemical" context={searchOptions} property="chemicalId" options={chemicalOptions} type="react-virtualized-select" onChange={this.handleChemicalChanged}/>
      <Editor label="Toxicant" context={searchOptions} property="toxicantId" options={toxicantOptions} type="react-virtualized-select" />
      <SpeciesSelector context={formInputs} property="species" species={this.props.species} placeholder={<span className="selector-placeholder">(All species)</span>} />
      <Editor label="Effect" context={formInputs} property="effects" options={effectOptions} type="react-virtualized-select" placeholder="(All effects)" multi />
      <Editor label="Endpoint" context={formInputs} property="endpoints" options={endpointOptions} type="react-virtualized-select" placeholder="(All endpoints)" multi />

      <div className="search-property-selector-container">
        <h4>Show data for...</h4>
        <SearchPropertySelector searchProperties={searchOptions.searchProperties} />
      </div>

      <Button type="submit" onClick={this.handleSearch}>Search</Button>
      <Button type="submit" onClick={this.handleDownloadGuidelineDerivations}>Download Guideline Derivations</Button>
    </div>);
  }
}
