import React, { PropTypes } from "react";
import { Alert, Panel, ProgressBar } from "react-bootstrap";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import ToxicityValueProperties from "../../constants/toxicityValueProperties";
import CSVFormatterService from "../../services/CSVFormatterService";

export default class SearchResults extends React.Component {
  static propTypes = {
    results: PropTypes.object
    , isSearching: PropTypes.bool
    , error: PropTypes.string
  };

  renderToxicityValuesTable(toxicityValues) {
    let propertyNames = [];
    if(toxicityValues.length > 0){
      propertyNames = Object.keys(toxicityValues[0]);
    }

    return (<BootstrapTable
      data={toxicityValues}
      striped
      hover
      search
      exportCSV
      csvFileName="toxicityvalues.csv"
      pagination>
        {propertyNames.map(k => <TableHeaderColumn
          key={k}
          dataAlign="right"
          dataSort={true}
          isKey={k == "tv_id"}
          hidden={k == "tv_id"}
          dataField={k}
          columnClassName={k == "tv_species_scientificname" ? "scientific-name" : null}
          csvFormat={CSVFormatterService.format}
          csvHeader={ToxicityValueProperties[k]}>{ToxicityValueProperties[k]}</TableHeaderColumn> )}
      </BootstrapTable>);
  }

  renderGuidelinesTable(guidelines) {
    let commonProps = {
      dataAlign: "right"
      , dataSort: true
      , csvFormat: CSVFormatterService.format
    };

    return (<BootstrapTable
      data={guidelines.map((x, i) => ({...x, id:i}))} // Inject fake id because BootstrapTable requires it
      striped
      hover
      search
      exportCSV
      csvFileName="guidelines.csv"
      pagination>
        <TableHeaderColumn {...commonProps} isKey={true} hidden={true} dataField="id">Id</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="toxicant" csvHeader="Toxicant">Toxicant</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="mediaType" csvHeader="Media Type">Media Type</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="animalCategory" csvHeader="Animal Category">Animal Category</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="testType" csvHeader="Test Type">Test Type</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="speciesScientificName" csvHeader="Scientific Name" columnClassName="scientific-name">Scientific Name</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="speciesCommonName" csvHeader="Common Name">Common Name</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="endpoint" csvHeader="Endpoint">Endpoint</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="effect" csvHeader="Effect">Effect</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="guidelineValue" csvHeader="Guideline">Toxic Concentration</TableHeaderColumn>
        <TableHeaderColumn {...commonProps} dataField="concentrationUnit" csvHeader="Unit">Unit</TableHeaderColumn>
      </BootstrapTable>);
  }

  render() {
    if(this.props.isSearching) {
      return (<Panel>
        Searching...
        <ProgressBar active now={100} />
      </Panel>);
    }

    if(this.props.error) {
      return <Alert bsStyle="danger">{this.props.error}</Alert>;
    }

    if(this.props.results != null) {
      const { toxicityValues, guidelines2000, guidelines2016 } = this.props.results;

      if(toxicityValues.length == 0 && guidelines2000.length == 0 && guidelines2016.length == 0){
        return <Alert>No results found.</Alert>;
      }

      return (<div>
        {toxicityValues.length > 0 && <h3>Australiasian Ecotoxicity Data</h3>}
        {toxicityValues.length > 0 && this.renderToxicityValuesTable(toxicityValues)}

        {guidelines2000.length > 0 && <h3>Guidelines WQG2000</h3>}
        {guidelines2000.length > 0 && this.renderGuidelinesTable(guidelines2000)}

        {guidelines2016.length > 0 && <h3>Guidelines WQG2016</h3>}
        {guidelines2016.length > 0 && this.renderGuidelinesTable(guidelines2016)}
      </div>);
    }

    return <div />;
  }
}
