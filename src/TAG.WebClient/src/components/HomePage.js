import React from "react";
import { Button, Glyphicon } from "react-bootstrap";
import { browserHistory } from "react-router";

const HomePage = () => {
  return (
    <div className="intro-container">
      <h2>Welcome to the SETAC Australasia Ecotoxicity Database</h2>

      <div className="intro-text">
        <p>The SETAC Australasia Ecotoxicity Database serves two purposes.
          First, it is a compendium of ecotoxicity data relevant to Australasia
          - specifically data generated for Australasian species or non-native
          species known to be present in Australasia and tested in Australasian
          waters. This data was first presented as a series of four papers
          published between 1998 and 2009 in the <i>Australasian Journal of
          Ecotoxicology</i>. Those original publications are still available on the
          SETAC AU website at <a href="http://www.ecotox.org.au/aje/">http://www.ecotox.org.au/aje/</a>.
          Second, it is a repository of the data used to derive Trigger Values
          (TVs) published in the Australian and New Zealand Guidelines for Fresh
          and Marine Water Quantity (ANZECC and ARMCANZ 2000) and used to derive
          the Default Guideline Values (DGV) generated in subsequent revisions
          of those guidelines.</p>

        <p>This current version of the database is a beta version. Some
          ecotoxicity data may still be missing from the database and we welcome
          feedback from users to inform us of some or any gaps.</p>

          <Button bsStyle="primary" onClick={() => browserHistory.push("/search")}>Search the SETAC Australasia Ecotoxicity Database &nbsp;&nbsp;<Glyphicon glyph="search" /></Button>
      </div>
    </div>
  );
};

export default HomePage;
