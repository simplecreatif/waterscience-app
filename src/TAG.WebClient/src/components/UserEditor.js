import React, { PropTypes } from "react";
import ScaffoldEditor from "./common/ScaffoldEditor";

export default class UserEditor extends ScaffoldEditor {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , scaffoldConfigurations: PropTypes.object.isRequired
    , entity: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
    , showRole: PropTypes.bool.isRequired
    , isSelf: PropTypes.bool.isRequired
  };

  render() {
    const { entity } = this.props;
    const whose = this.props.isSelf ? "your" : "the user's";

    return (<div>
      <div>
        {this.renderEditor("email", null, entity)}
        {
          this.props.showRole
            ? this.renderEditor("roleId", "role", entity)
            : null
        }
      </div>
      <hr/>
      <div>
        <h3>Change password</h3>
        To change {whose} password, enter and confirm a new password. Otherwise, leave these fields blank.
        {this.renderEditor("password", "password", entity)}
        {this.renderEditor("passwordConfirmation", "password", entity)}
      </div>
      <hr/>
    </div>);
  }
}
