import React, { PropTypes } from "react";
import ErrorList from "./common/ErrorList";

export default class GuidelineEditor extends React.Component {
  static propTypes = {
    // The guideline to be shown or edited
    guideline: PropTypes.object.isRequired

    // State about this guideline's save progress
    // (if it is in the process of being saved)
    , guidelineSaveState: PropTypes.object

    // Actions for saving a guideline
    , actions: PropTypes.object.isRequired
  };

  constructor() {
    super();

    this.state = {
      isEditing: false
    };

    this.handleBeginEdit = this.handleBeginEdit.bind(this);
    this.handleCancelEdit = this.handleCancelEdit.bind(this);
    this.handleFinishEdit = this.handleFinishEdit.bind(this);
    this.handleChangeValue = this.handleChangeValue.bind(this);
  }

  handleBeginEdit(e) {
    e.preventDefault();
    this.props.actions.saveGuidelineReset(this.props.guideline);

    this.setState({
      isEditing: true
      , editValue: this.props.guideline.guidelineValue
    });
  }

  handleCancelEdit(e) {
    e.preventDefault();
    this.props.actions.saveGuidelineReset(this.props.guideline);

    this.setState({
      isEditing: false
    });
  }

  handleFinishEdit(e) {
    e.preventDefault();

    this.props.actions.saveGuideline({
      id: this.props.guideline.id
      , guidelineValue: this.state.editValue
    });
  }

  handleChangeValue(e) {
    this.setState({
      editValue: e.target.value
    });
  }

  renderEditor() {
    let guidelineSaveState = this.props.guidelineSaveState || {};
    let isSaving = guidelineSaveState.status == "saving";
    let isSaved = guidelineSaveState.status == "saved";

    return (<div>
      <div className="guideline-editor">
        <h4>Guideline Value:</h4> <input type="number" value={this.state.editValue} onChange={this.handleChangeValue} disabled={isSaving} />
        <a href="#" onClick={this.handleCancelEdit} disabled={isSaving}>Cancel</a> &middot;
        <a href="#" onClick={this.handleFinishEdit} disabled={isSaving}>Save</a>
        {isSaved && <span> &middot; (saved)</span>}
      </div>
      <ErrorList errors={guidelineSaveState.saveError} />
    </div>);
  }

  renderShow() {
    return (<div className="guideline-display">
      <h4>Guideline Value: {this.props.guideline.guidelineValue || "(none)"}</h4>
      <a href="#" onClick={this.handleBeginEdit}>Edit</a>
    </div>);
  }

  render() {
    return this.state.isEditing ? this.renderEditor() : this.renderShow();
  }
}
