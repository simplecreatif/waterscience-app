import React, { PropTypes } from "react";
import Editor from "./common/Editor";
import { Alert, Button } from "react-bootstrap";

export default class LoginForm extends React.Component {
  static propTypes = {
    onLogin: PropTypes.func.isRequired
    , loginFailed: PropTypes.bool.isRequired
  };

  constructor() {
    super();

    this.handleLogin = this.handleLogin.bind(this);

    this.state = {
      email: ''
      , password: ''
    };
  }

  handleLogin() {
    this.props.onLogin(this.state.email, this.state.password);
  }

  render() {
    return (<div>
      <h2>Login</h2>
      <Editor label="Email" context={this.state} property="email" />
      <Editor label="Password" context={this.state} property="password" type="password" />
      <Button type="submit" onClick={this.handleLogin}>Login</Button>
      {this.props.loginFailed
        ? <Alert bsStyle="danger">Invalid credentials.</Alert>
        : null
      }
    </div>);
  }
}
