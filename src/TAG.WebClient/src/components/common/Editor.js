import React, { PropTypes } from "react";
import LabelWithValue from "./LabelWithValue";
import ReactSelect from "react-select";
import ReactVirtualizedSelect from "react-virtualized-select";
import _ from "lodash";

// Creates a label and input with a two way binding between the input
// and the source. The source is specified through the context and
// property attributes (e.g. context={this.state} property="name").
//
// Alternatively an Editor can be used like a normal React input by
// specifying the value and onChange props. This allows it to work
// with other libraries like redux-form if needed.
//
// The Editor is designed to handle simple input scenarios like text,
// numeric and select lists. The type attribute determines what kind
// of input the Editor will render.
//
// A ref property should be specified on each Editor so that validation
// can be passed to the Editor via the ref.
//
// Supported properties: context, property, label, type, showLabel, visible
// orientation (null or horizontal)
// For entityMultiSelector: columns, listAction, store.
export default class Editor extends React.Component {
  static propTypes = {
    type: PropTypes.string
    , context: PropTypes.object
    , property: PropTypes.string
    , onChange: PropTypes.func
    , value: PropTypes.oneOfType([
      PropTypes.number
      , PropTypes.string
      , PropTypes.bool
    ])
    , itemValueProperty: PropTypes.string
    , itemLabelProperty: PropTypes.string
    , options: PropTypes.array
    , visible: PropTypes.bool
    , disabled: PropTypes.bool
    , showLabel: PropTypes.bool
    , label: PropTypes.string
    , orientation: PropTypes.string
    , placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    , multi: PropTypes.bool
    , className: PropTypes.string
    , filterOptions: PropTypes.func
  };

  static defaultProps = {
    visible: true
  };

  constructor() {
    super();
    this.state = {};
    this.userChangedValue = this.userChangedValue.bind(this);
  }

  // Editors can receive error messages. This works in conjunction with
  // App.EntityMixin. If a component uses EntityMixin and renders an
  // Editor with a ref attribute that corresponds to a property, any
  // errors for that property will be displayed on this editor.
  setError(errorMessage) {
    this.setState({errorMessage: errorMessage});
  }

  // When a user enters a value into the Editor (e.g. a textbox)
  // map that value back to the context. Context would typically be
  // the state object of some other component that may contain
  // a form consisting of several Editors.
  userChangedValue(event) {
    let newValue = null;
    switch (this.props.type) {
      case "checkbox":
        newValue = event.target.checked;
        break;
      case "react-virtualized-select":
      case "react-select":
        if(this.props.multi) {
          // Multi selectors return a collection of selected
          // objects directly (not via a value property)
          newValue = event;
        }
        else {
          newValue = event != null ? event.value : null;
        }
        break;
      default:
        newValue = event.target.value;
        break;
    }

    // if the value is a string, convert to null if it's an empty string
    if (typeof newValue == "string" && newValue == "") {
      newValue = null;
    }

    // set value on context
    if(this.props.context) {
      this.props.context[this.props.property] = newValue;
    }

    if (this.props.onChange) {
      switch(this.props.type) {
        // If we are a select box, include the entire event in the
        // onChange call because this allows the user to retrieve
        // associated data (such as an entity) in addition to the
        // entity id if they provided it.
        case "react-virtualized-select":
        case "react-select":
          this.props.onChange(newValue, event);
          break;
        default:
          this.props.onChange(newValue);
      }
    }

    this.forceUpdate();
  }

  getValue() {
    // when getting a value, return an empty string if the value is null. this
    // is to prevent the editor becoming uncontrolled and showing the last user-entered
    // value. See:
    // * https://github.com/facebook/react/issues/2533
    // * http://facebook.github.io/react/tips/controlled-input-null-value.html
    let value = null;
    if(this.props.context) {
      value = this.props.context[this.props.property];
      if(value == null){
        value = "";
      }
    } else{
      value = this.props.value;
    }

    // for select lists with itemValueProperty
    if(this.props.itemValueProperty){
      value = value[this.props.itemValueProperty];
    }

    return value;
  }

  getEditor() {
    switch (this.props.type) {
      case "password":
        return this.getTextEditor("password");
      case "number":
        return this.getTextEditor("number");
      case "select":
        return this.getSelectEditor();
      case "react-select":
        return this.getReactSelectEditor();
      case "react-virtualized-select":
        return this.getReactVirtualizedSelectEditor();
      case "checkbox":
        return this.getCheckboxEditor();
      case "textblock":
        return this.getTextBlockEditor();
      case undefined:
        return this.getTextEditor("text");
      default:
        throw("Unknown editor type " + this.props.type);
    }
  }

  getTextEditor(type) {
    return <input type={type} value={this.getValue()} className="form-control" onChange={this.userChangedValue} placeholder={this.props.placeholder} />;
  }

  getTextBlockEditor() {
    return <textarea className="form-control" rows="7" value={this.getValue()} onChange={this.userChangedValue}></textarea>;
  }

  getArrayEditor(){
    let value = this.getValue();
    if(value && Array.isArray(value)){
      value = value.join(", ");
    }

    return <textarea className="form-control" rows="7" value={value} onChange={this.userChangedValue}></textarea>;
  }

  getSelectEditor() {
    let options = this.props.options.map(function (option) {
      let itemLabel = this.props.itemLabelProperty ? option[this.props.itemLabelProperty] : option;
      let itemValue = this.props.itemValueProperty ? option[this.props.itemValueProperty] : option;

      return <option key={itemValue} value={itemValue}>{itemLabel}</option>;
    }.bind(this));

    return (<select value={this.getValue()} className="form-control" onChange={this.userChangedValue}>
      {options}
    </select>);
  }

  getReactSelectEditor() {
    return <ReactSelect options={this.props.options} value={this.getValue()} onChange={this.userChangedValue} />;
  }

  getReactVirtualizedSelectEditor() {
    return (<ReactVirtualizedSelect
      options={this.props.options}
      value={this.getValue()}
      onChange={this.userChangedValue}
      matchProp="label"
      ignoreAccents={false} // Don't strip diacritics when searching for an item because it's extremely slow
      filterOptions={this.props.filterOptions || this.filterOptions}
      placeholder={this.props.placeholder}
      multi={this.props.multi} />);
  }

  // Generic override for the react-select component's filtering
  // method. This could be refactored out. It behaves similarly
  // to the default filtering method but sorts the outcome by
  // length to ensure the closest matching items appear first
  // when a user types in a selector.
  // Assumes options look like { label: "Some text", value: 123 }
  filterOptions(options, filterValue, excludeOptions) {
    let filterValueLower = filterValue.toLowerCase();

    let excludeOptionsArray = (excludeOptions || []).map(x => x.value);
    return _
      .chain(options)
      .filter(option => {
        // Don't show items that have already been selected
        if(excludeOptionsArray.length && excludeOptionsArray.indexOf(option.value) >= 0) {
          return false;
        }
        // Do a case insensitive substring filter
        let optionLower = String(option.label || "").toLowerCase();
        return optionLower.indexOf(filterValueLower) >= 0;
      })
      .sortBy(x => filterValueLower == "" ? true : (x.label || "").length)
      .value();
  }

  getCheckboxEditor() {
    return <input type="checkbox" value={this.getValue()} checked={this.getValue()} onChange={this.userChangedValue} disabled={this.props.disabled}/>;
  }

  getClassNames() {
    return "form-group" + (this.state.errorMessage ? " has-error" : "");
  }

  getLabel() {
    return this.props.label || this.props.property.toPhrase();
  }

  render() {
    if (!this.props.visible) {
      return <div />;
    }

    let editor = this.getEditor();
    let label = this.getLabel();

    return (<LabelWithValue label={label} errorMessage={this.state.errorMessage} showLabel={this.props.showLabel} orientation={this.props.orientation}>
      <div className={this.props.className}>
        {editor}
        </div>
    </LabelWithValue>);
  }
}
