import React, { PropTypes } from "react";

// Creates a label and a read-only field which shows the
// value of the specified property. Several types of display
// are supported (e.g. text display, checkbox display).
//
// This can be used on show pages to display the current
// value of a property.
//
// This supports specifying values either as a value prop or
// as a combination of a context and property prop.
//
// Supported properties: context, property, value, label, type, showLabel, visible
export default class Display extends React.Component {
  static propTypes = {
    context: PropTypes.object
    , property: PropTypes.string
    , value: PropTypes.object
    , label: PropTypes.string
    , showLabel: PropTypes.bool
    , visible: PropTypes.bool
    , type: PropTypes.string
    , href: PropTypes.string
    , className: PropTypes.string
  };

  static defaultProps = {
    showLabel: true
    , visible: true
  };

  constructor() {
    super();
    this.state = {};
  }

  getLabel() {
    return this.props.label || this.props.property.toPhrase();
  }

  getValue() {
    if(this.props.context) {
      return this.props.context[this.props.property];
    }
    else if(this.props.value) {
      return this.props.value;
    }
    else {
      return null;
    }
  }

  getDisplay() {
    switch (this.props.type) {
      case "checkbox":
        return this.getCheckboxDisplay();
      case "hyperlink":
        return this.getHyperlinkDisplay();
      case "textblock":
        return this.getTextBlockDisplay();
      case "text":
      case undefined:
        return this.getTextDisplay();
      default:
        throw("Unknown display type " + this.props.type);
    }
  }

  getTextDisplay() {
    return <p className="form-control-static">{this.getValue()}</p>;
  }

  getTextBlockDisplay() {
    return <textarea className="form-control" rows="3" value={this.getValue()} disabled></textarea>;
  }

  getHyperlinkDisplay() {
    return <p className="form-control-static"><a href={this.props.href}>{this.getValue()}</a></p>;
  }

  getCheckboxDisplay() {
    return <input type="checkbox" className="form-control-static" checked={this.getValue()} disabled/>;
  }

  render() {
    if(!this.props.visible) {
      return <div />;
    }

    let display = this.getDisplay();
    let label = this.getLabel();

    if(!this.props.showLabel) {
      return display;
    }

    return (<div className="form-group">
      <label className="col-md-2 control-label">{label}</label>
      <div className={"col-md-10 " + this.props.className}>
        {display}
      </div>
    </div>);
  }
}
