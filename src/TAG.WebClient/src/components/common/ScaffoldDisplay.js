import React, { PropTypes } from "react";
import Display from "./Display";
import _ from "lodash";

// Accepts an entity and a scaffold configuration for that entity.
// Produces a list of labels and values which show the properties
// of the entity in a way that is meaninful to a user. Conventions
// are used to hide certain properties (particularly "Id" properties)
export default class ScaffoldDisplay extends React.Component {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , entity: PropTypes.object.isRequired
  };

  isShownProperty(propertyName) {
    return !(
      propertyName.endsWith("Id")
      || propertyName == "id"
    );
  }

  getShownProperties() {
    return Object.keys(this.props.entity).filter(this.isShownProperty);
  }

  render() {
    const { entity, scaffoldConfiguration } = this.props;
    const shownProperties = this.getShownProperties();

    return (<div>
      {shownProperties.map(x => <Display
        key={x}
        context={entity}
        property={x}
        type={(typeof entity[x] == "boolean") ? "checkbox" : "text"}
        className={_.get(scaffoldConfiguration, ["styles", x])} />)
      }
    </div>);
  }
}
