import React, { PropTypes } from "react";

// Shows a label with any value (can be text or another component,
// specified as child elements).
//
// Supported properties: errorMessage, label, showLabel, orientation (null or horizontal)
export default class LabelWithValue extends React.Component {
  static propTypes = {
    label: PropTypes.string.isRequired
    , showLabel: PropTypes.bool
    , orientation: PropTypes.string
    , errorMessage: PropTypes.string
    , children: PropTypes.element.isRequired
  };

  static defaultProps = {
    showLabel: true
  };
  
  getClassNames() {
    return "form-group" + (this.props.errorMessage ? " has-error" : "");
  }

  render() {
    let classNames = this.getClassNames();

    if (this.props.showLabel) {
      if(this.props.orientation == "horizontal") {
        return (<div className={classNames}>
          <label className="col-sm-7">{this.props.label}</label>

          <div className="col-sm-5">
            {this.props.errorMessage ? <div className="input-validation-error">{this.props.errorMessage}</div> : null}
            {this.props.children}
          </div>
        </div>);
      } else {
        return (<div className={classNames}>
          <label>{this.props.label}</label>

          <div>
            {this.props.errorMessage ? <div className="input-validation-error">{this.props.errorMessage}</div> : null}
            {this.props.children}
          </div>
        </div>);
      }
    } else {
      return this.props.children;
    }
  }
}
