import React, { PropTypes } from "react";
import Editor from "./Editor";
import _ from "lodash";

// Accepts an entity and a scaffold configuration for that entity.
// Produces a list of inputs which allow a user to edit the entity.
export default class ScaffoldEditor extends React.Component {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , scaffoldConfigurations: PropTypes.object.isRequired
    , entity: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
  };

  isShownProperty(propertyName) {
    return !(
      propertyName.endsWith("Id")
      || propertyName == "id"
    );
  }

  renderEditor(propertyName, propertyType, entity) {
    // If this property has been explicitly hidden through scaffold configuration,
    // we'll just render it as a hidden input.
    if((this.props.scaffoldConfiguration.hiddenProperties || []).indexOf(propertyName) > -1){
      return <Editor key={propertyName} context={entity} property={propertyName} visible={false} type="hidden" />;
    }

    let className = _.get(this.props.scaffoldConfiguration, ["styles", propertyName]);

    if(propertyType == "numeric") {
      return <Editor key={propertyName} context={entity} property={propertyName} visible={this.isShownProperty(propertyName)} type="number" className={className} />;
    }

    if(propertyType == "bool") {
      return <Editor key={propertyName} context={entity} property={propertyName} visible={this.isShownProperty(propertyName)} type="checkbox" className={className} />;
    }

    if(propertyType == "password") {
      return <Editor key={propertyName} context={entity} property={propertyName} visible={this.isShownProperty(propertyName)} type="password" className={className} />;
    }

    if(propertyType != null) {
      // It's a selector and propertyType is the entityType of the reference type
      let selectorProperty = _.get(this.props.scaffoldConfigurations, [propertyType.toLowerCase(), "selectorProperty"]) || "name";
      let entities = this.props.entities[propertyType].entities;
      if(entities == null) {
        throw "Tried to create a selector for an entityType which has no data available. entityType: " + propertyType;
      }
      let options = entities.map(x => ({
        label: (typeof selectorProperty == "function") ? selectorProperty(x) : x[selectorProperty]
        , value: x.id
      }));

      // Remove "Id" from end of propertyName to create label
      let label = propertyName.substring(0, propertyName.length - 2).toPhrase();
      return <Editor key={propertyName} label={label} context={entity} property={propertyName} options={options} type="react-virtualized-select" className={className} />;
    }

    return <Editor key={propertyName} context={entity} property={propertyName} visible={this.isShownProperty(propertyName)} className={className} />;
  }

  render() {
    const { entity, scaffoldConfiguration } = this.props;
    const properties = Object.keys(scaffoldConfiguration.template);

    return (<div>
      {properties.map(x => this.renderEditor(x, scaffoldConfiguration.template[x], entity))}
    </div>);
  }
}
