import React, { PropTypes } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Link } from "react-router";
import _ from "lodash";
import CSVFormatterService from "../../services/CSVFormatterService";
import AuthenticationService from "../../services/AuthenticationService";

export default class ScaffoldList extends React.Component {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , entities: PropTypes.array.isRequired
    , auth: PropTypes.object.isRequired
  };

  constructor(){
    super();

    this.renderLinks = this.renderLinks.bind(this);
  }

  isShownProperty(propertyName) {
    return !(
      propertyName.endsWith("Id")
      || propertyName == "id"
    );
  }

  getShownProperties() {
    if(this.props.entities.length == 0) {
      return [];
    }
    return Object.keys(this.props.entities[0]).filter(this.isShownProperty);
  }

  renderLinks(id){
    let entityType = this.props.scaffoldConfiguration.entityType;
    return (<span>
      <Link to={"/" + entityType + "/show/" + id}>Show</Link>
      {AuthenticationService.isContributor(this.props.auth)
        ? (<span> &middot;
            <Link to={"/" + entityType + "/update/" + id}>Update</Link> &middot;
            <Link to={"/" + entityType + "/delete/" + id}>Delete</Link>
          </span>)
        : null
      }
    </span>);
  }

  render() {
    const { entities, scaffoldConfiguration } = this.props;
    const shownProperties = this.getShownProperties();
    const linksColumnWidth = AuthenticationService.isContributor(this.props.auth) ? "160" : "60";

    return (<BootstrapTable
      data={entities}
      options={{sizePerPage:20}}
      striped
      hover
      search
      exportCSV
      csvFileName={scaffoldConfiguration.entityType + ".csv"}
      pagination>
      {
        _.flatten([
          <TableHeaderColumn key="_id" dataAlign="right" dataSort={true} isKey={true} hidden={true} dataField="id">Id</TableHeaderColumn>
          , shownProperties.map(x => <TableHeaderColumn
              key={x}
              dataAlign="right"
              dataSort
              dataField={x}
              columnClassName={_.get(scaffoldConfiguration, ["styles", x])}
              csvFormat={CSVFormatterService.format}
              csvHeader={x.toPhrase()}>{x.toPhrase()}</TableHeaderColumn>)
          , <TableHeaderColumn key="_links" dataAlign="right" dataField="id" dataFormat={this.renderLinks} width={linksColumnWidth} />
        ])
      }
    </BootstrapTable>);
  }
}
