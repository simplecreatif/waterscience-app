import React, { PropTypes } from "react";
import { Alert } from "react-bootstrap";

export default class ErrorList extends React.Component {
  static propTypes = {
    errors: PropTypes.object
  };

  render(){
    if(!this.props.errors || this.props.errors.isValid){
      return <div />;
    }

    // Assumes this.props.errors is a .NET FluentValidation ValidationResult
    let errors = this.props.errors.errors;
    return (<Alert bsStyle="warning">
      <ul>
        {errors.map((x, i) => <li key={i}>{x.errorMessage}</li>)}
      </ul>
    </Alert>);
  }
}
