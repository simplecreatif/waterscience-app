import React, { PropTypes } from "react";
import Editor from "./Editor";
import _ from "lodash";

export default class SpeciesSelector extends React.Component {
  static propTypes = {
    // An array of all species entities. These will be used
    // to populate the select lists.
    species: PropTypes.array.isRequired

    // Context and property are used together to retrieve
    // and set the current value as such: value = context.[property]
    , context: PropTypes.object.isRequired
    , property: PropTypes.string.isRequired

    , placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    , onChange: PropTypes.func
  };

  constructor() {
    super();

    this.handleSpeciesChanged = this.handleSpeciesChanged.bind(this);
    this.updateSpeciesState = this.updateSpeciesState.bind(this);
  }

  componentWillUpdate(nextProps) {
    // if context (selected species) has changed, update the species inputs
    const currentSpecies = (this.props.context.species || []).map(x => x.value).sort();
    const nextSpecies = (nextProps.context.species || []).map(x => x.value).sort();
    if (!_.isEqual(currentSpecies, nextSpecies)) {
      this.updateSpeciesState(nextProps.context.species);
    }
  }

  getValue() {
    return this.props.context[this.props.property];
  }

  // Handles species being changed via user input within this control
  handleSpeciesChanged(selected) {
    selected = selected || [];

    this.updateSpeciesState(selected);

    this.props.context[this.props.property] = selected;
    if(this.props.onChange) {
      this.props.onChange(selected);
    }
  }

  updateSpeciesState(selected) {
    // Synchronise the two species boxes (scientific and common name)
    // Pull out the original species entity from the selected options
    // and re-map them to options types with the correct name (scientific
    // or common) for each selector property.
    let species = selected.map(x => x.species);
    this.setState({
      speciesScientific: species.map(x => ({ label: x.scientificName, value: x.id, species: x}))
      , speciesCommon: species.map(x => ({ label: x.commonName, value: x.id, species: x}))
      , species: selected
    });
  }

  render() {
    let speciesOptionsScientific = this.props.species.map(x => ({ label: x.scientificName, value: x.id, species: x }));
    let speciesOptionsCommon = this.props.species.map(x => ({ label: x.commonName, value: x.id, species: x }));

    return (<div>
      <Editor
        label="Species Scientific Name"
        placeholder={this.props.placeholder}
        context={this.state}
        property="speciesScientific"
        options={speciesOptionsScientific}
        type="react-virtualized-select"
        className="scientific-name"
        onChange={this.handleSpeciesChanged}
        multi/>
      <Editor
        label="Species Common Name"
        placeholder={this.props.placeholder}
        context={this.state}
        property="speciesCommon"
        options={speciesOptionsCommon}
        type="react-virtualized-select"
        onChange={this.handleSpeciesChanged}
        multi/>
    </div>);
  }
}
