import React from "react";
import { Alert } from "react-bootstrap";

export default class GettingReady extends React.Component{
  render() {
    return <Alert bsStyle="success"><span className="glyphicon glyphicon-refresh spinning"></span> Getting Ready...</Alert>;
  }
}
