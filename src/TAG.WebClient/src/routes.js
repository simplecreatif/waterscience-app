import React from "react";
import { Route, IndexRoute } from "react-router";
import App from "./containers/App";
import HomePage from "./components/HomePage";
import ScaffoldPage from "./containers/ScaffoldPage.js";
import SearchPage from "./containers/SearchPage";
import LoginPage from "./containers/LoginPage";
import UserUpdatePage from "./containers/UserUpdatePage";
import { AllAuthenticated } from "./authWrappers/Authenticated";
import { Unauthenticated } from "./authWrappers/Unauthenticated";
import { AllContributor } from "./authWrappers/Contributor";
import { AllAdministrator } from "./authWrappers/Administrator";
import ManageToxicityValues2016Page from "./containers/ManageToxicityValues2016Page";

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="login" component={Unauthenticated(LoginPage)}/>
    <Route component={AllAuthenticated}>
      <Route path="search" component={SearchPage}/>
      <Route path="account" component={UserUpdatePage}/>
      <Route component={AllAdministrator}>
        <Route path="user/update/:id" component={UserUpdatePage}/>
        <Route path="user" component={ScaffoldPage}/>
      </Route>
      <Route component={AllContributor}>
        <Route path="*/create" component={ScaffoldPage}/>
        <Route path="*/update/*" component={ScaffoldPage}/>
        <Route path="*/delete/*" component={ScaffoldPage}/>
        <Route path="toxicityvalue2016/manage" component={ManageToxicityValues2016Page}/>
      </Route>
      <Route path="*" component={ScaffoldPage}/>
    </Route>
  </Route>
);
