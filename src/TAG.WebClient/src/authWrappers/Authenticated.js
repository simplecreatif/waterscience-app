import { UserAuthWrapper } from "redux-auth-wrapper";
import { routerActions } from "react-router-redux";
import AuthenticationService from "../services/AuthenticationService";

function isAuthenticated(auth) {
  // If we're checking who the user is, return true so that the browser doesn't redirect
  // immediately (when the user types a URL for example).
  if (auth.isCheckingUser) {
    return true;
  }
  return AuthenticationService.isAuthenticated(auth);
}

// Use this to wrap a single component
export const Authenticated = UserAuthWrapper({
  authSelector: state => state.auth
  , redirectAction: routerActions.replace
  , wrapperDisplayName: "Authenticated"
  , failureRedirectPath: "/login"
  , predicate: isAuthenticated
});

// Use this on a route which doesn't render a component to wrap sub routes
export const AllAuthenticated = Authenticated(props => props.children);
