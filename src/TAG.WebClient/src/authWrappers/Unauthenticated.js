import { UserAuthWrapper } from "redux-auth-wrapper";
import { routerActions } from "react-router-redux";
import AuthenticationService from "../services/AuthenticationService";

function isUnauthenticated(auth) {
  // If we're checking who the user is, return true so that the browser doesn't redirect
  // immediately (when the user types a URL for example).
  if (auth.isCheckingUser) {
    return true;
  }
  return AuthenticationService.isUnauthenticated(auth);
}

// Use this to wrap a single component
export const Unauthenticated = UserAuthWrapper({
  authSelector: state => state.auth
  , redirectAction: routerActions.replace
  , wrapperDisplayName: "Unauthenticated"
  , failureRedirectPath: "/"
  , allowRedirectBack: false
  , predicate: isUnauthenticated
});

// Use this on a route which doesn't render a component to wrap sub routes
export const AllUnauthenticated = Unauthenticated(props => props.children);
