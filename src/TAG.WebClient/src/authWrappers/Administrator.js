import { UserAuthWrapper } from "redux-auth-wrapper";
import { routerActions } from "react-router-redux";
import AuthenticationService from "../services/AuthenticationService";

function isAdministrator(auth) {
  // If we're checking who the user is, return true so that the browser doesn't redirect
  // immediately (when the user types a URL for example).
  if (auth.isCheckingUser) {
    return true;
  }
  return AuthenticationService.isAdministrator(auth);
}

// Use this to wrap a single component
export const Administrator = UserAuthWrapper({
  authSelector: state => state.auth
  , redirectAction: routerActions.replace
  , wrapperDisplayName: "Authenticated"
  , failureRedirectPath: "/"
  , allowRedirectBack: false
  , predicate: isAdministrator
});

// Use this on a route which doesn't render a component to wrap sub routes
export const AllAdministrator = Administrator(props => props.children);
