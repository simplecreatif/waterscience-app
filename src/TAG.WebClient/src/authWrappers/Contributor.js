import { UserAuthWrapper } from "redux-auth-wrapper";
import { routerActions } from "react-router-redux";
import AuthenticationService from "../services/AuthenticationService";

function isContributor(auth) {
  // If we're checking who the user is, return true so that the browser doesn't redirect
  // immediately (when the user types a URL for example).
  if (auth.isCheckingUser) {
    return true;
  }
  return AuthenticationService.isContributor(auth);
}

// Use this to wrap a single component
export const Contributor = UserAuthWrapper({
    authSelector: state => state.auth
    , redirectAction: routerActions.replace
    , wrapperDisplayName: "Authenticated"
    , failureRedirectPath: "/"
    , allowRedirectBack: false
    , predicate: isContributor
});

// Use this on a route which doesn't render a component to wrap sub routes
export const AllContributor = Contributor(props => props.children);
