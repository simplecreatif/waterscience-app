import config from "../config";

export const WEBAPI_URL = config.webApiUrl;
export const REALTIME_URL = config.realTimeUrl;
