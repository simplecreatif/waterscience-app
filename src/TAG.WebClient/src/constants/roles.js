// These should match the `role` table in the database (including the id).
export const STANDARD_USER = 1;
export const CONTRIBUTOR = 2;
export const ADMINISTRATOR = 3;
