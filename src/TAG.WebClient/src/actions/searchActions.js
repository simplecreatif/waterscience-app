import * as actionTypes from "../constants/actionTypes";
import * as config from "../constants/config";
import axios from "axios";
import { saveAs } from "file-saver";

export function search(options) {
  return dispatch => {
    // Notify that a search is beginning
    dispatch(searchBegin(options));

    let handleResponse = (response) => {
      if(response.status == 200){
        // Notify that the search has completed
        dispatch(searchComplete({
          results: response.data
        }));
        return;
      }
      // Notify that the search has failed
      dispatch(searchFailed(options));
      return;
    };

    // Begin the request and return a promise.
    let url = config.WEBAPI_URL + "search";

    return axios({
      method: "post"
      , url: url
      , data: options
      , withCredentials: true
    })
    .then(handleResponse)
    .catch(handleResponse);
  };
}

function searchReset() {
  return {
    type: actionTypes.SEARCH_RESET
  };
}

function searchBegin() {
  return {
    type: actionTypes.SEARCH_BEGIN
  };
}

function searchComplete(options) {
  return {
    type: actionTypes.SEARCH_COMPLETE
    , results: options.results
  };
}

function searchFailed() {
  return {
    type: actionTypes.SEARCH_FAILED
  };
}

export function downloadGuidelineDerivations(options) {
  return dispatch => {
    // Notify that a search is beginning
    dispatch(searchBegin(options));

    let handleResponse = (response) => {
      if(response.status == 200){
        let file = new File([response.data], "guideline-derivation.csv", {type: "text/csv;charset=utf-8"});
        saveAs(file);
        // Reset search to initial state as the download has been done.
        dispatch(searchReset());
        return;
      }
      // Notify that the search has failed
      dispatch(searchFailed(options));
      return;
    };

    // Begin the request and return a promise.
    let url = config.WEBAPI_URL + "search/downloadGuidelines";

    return axios({
      method: "post"
      , url: url
      , data: options
      , withCredentials: true
    })
      .then(handleResponse)
      .catch(handleResponse);
  };
}
