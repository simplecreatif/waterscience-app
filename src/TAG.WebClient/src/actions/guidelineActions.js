import * as actionTypes from "../constants/actionTypes";
import * as config from "../constants/config";
import FluentValidationService from "../services/FluentValidationService";
import axios from "axios";
import _ from "lodash";

// Guideline parameter should be of the form:
// {
//   id: 42
//   guidelineValue: 1234.56
// }
export function saveGuideline(guideline){
  return dispatch => {
    dispatch(saveGuidelineBegin(guideline));

    let handleSuccess = () => {
      dispatch(saveGuidelineComplete(guideline));
    };

    let handleFailure = (error) => {
      let saveError = null;
      let status = _.get(error, ["response", "status"]);
      if(status == 400) {
        // Validation error
        saveError = error.response.data;
      }
      else if(status == 404) {
        saveError = FluentValidationService.validationFailure("The entity no longer exists or could not be found.");
      }
      saveError = saveError || FluentValidationService.validationFailure("A server error occurred.");

      dispatch(saveGuidelineFailed({
        guideline: guideline
        , saveError: saveError
      }));
    };

    return axios({
      method: "PUT"
      , url: config.WEBAPI_URL + "guidelinegroup/" + guideline.id
      , data: guideline
      , withCredentials: true
    })
    .then(handleSuccess)
    .catch(handleFailure);
  };
}

export function saveGuidelineReset(guideline) {
  return {
    type: actionTypes.SAVE_GUIDELINE_RESET
    , guideline: guideline
  };
}

function saveGuidelineBegin(guideline) {
  return {
    type: actionTypes.SAVE_GUIDELINE_BEGIN
    , guideline: guideline
  };
}

function saveGuidelineComplete(guideline) {
  return {
    type: actionTypes.SAVE_GUIDELINE_COMPLETE
    , guideline: guideline
  };
}

function saveGuidelineFailed(result) {
  return {
    type: actionTypes.SAVE_GUIDELINE_FAILED
    , guideline: result.guideline
    , saveError: result.saveError
  };
}
