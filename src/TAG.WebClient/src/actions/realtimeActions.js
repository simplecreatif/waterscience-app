import * as config from "../constants/config";
import * as entityActions from "./entityActions";
import io from "socket.io-client";

let client = null;

export function connect() {
  return (dispatch, getState) => {
    client = io(config.REALTIME_URL);
    // eslint-disable-next-line no-console
    client.on("connect", () => console.log("Connected to realtime endpoint at " + config.REALTIME_URL));

    // Maps socket.io emissions to redux actions. Socket.io
    // emissions have, by convention, three parameters:
    // action: (a constant "action" string)
    // actionType: "edit" or "delete"
    // entityType
    // entityData: { listEntity, getEntity }
    client.on("action", (actionType, entityType, entityData) => {
      // Drop any realtime events which originated from actions
      // performed by this client
      let clientId = getState().environment.clientId;
      if(entityData.clientId == clientId) {
        return;
      }
      switch(actionType) {
        case "edit":
          dispatch(entityActions.mergeEntity({
            entityType: entityType
            , ...entityData
          }));
          break;
        case "delete":
          dispatch(entityActions.mergeDeleteEntity({
            entityType: entityType
            , ...entityData
          }));
          break;
      }
    });
  };
}
