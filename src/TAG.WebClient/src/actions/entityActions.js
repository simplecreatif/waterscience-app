import * as actionTypes from "../constants/actionTypes";
import * as config from "../constants/config";
import FluentValidationService from "../services/FluentValidationService";
import axios from "axios";
import _ from "lodash";

function entityUrl(entityType, id){
  let url = config.WEBAPI_URL + entityType;

  if(id != null) {
    url = url + "/" + id;
  }

  return url;
}

export function listEntities(options) {
  return dispatch => {
    // Notify that entities are beginning to be listed
    dispatch(listEntitiesBegin(options));

    let handleSuccess = (response) => {
      // Notify that the entities have been listed
      dispatch(listEntitiesComplete({
        entityType: options.entityType
        , entities: response.data
      }));
    };

    let handleFailure = (error) => {
      // Notify that the entities have not been listed
      dispatch(listEntitiesFailed({
        entityType: options.entityType
        , listError: _.get(error, ["response", "status"]) == 404 ? "The entities could not be found." : "A server error occurred."
      }));
    };

    // Begin the request and return a promise.
    let url = entityUrl(options.entityType);
    return axios.get(url, { withCredentials: true })
      .then(handleSuccess)
      .catch(handleFailure);
  };
}

export function listEntitiesBatch(options) {
  return dispatch => {
    // Notify that entities are beginning to be listed
    options.entityTypes.forEach(x => dispatch(listEntitiesBegin({ entityType: x })));

    let handleSuccess = (response) => {
      options.entityTypes.forEach(x => {
        let data = response.data[x];

        if(data != null){
          // Notify that the entities have been listed
          dispatch(listEntitiesComplete({
            entityType: x
            , entities: data
          }));
        }
        else
        {
          dispatch(listEntitiesFailed({
            entityType: x
            , listError: "The entities could not be found."
          }));
        }
      });
    };

    let handleFailure = (error) => {
      // Notify that the entities have not been listed
      dispatch(listEntitiesFailed({
        entityType: options.entityType
        , listError: _.get(error, ["response", "status"]) == 404 ? "The entities could not be found." : "A server error occurred."
      }));
    };

    // Begin the request and return a promise.
    let url = config.WEBAPI_URL + "batch/" + options.entityTypes.join(",");
    return axios.get(url, { withCredentials: true })
      .then(handleSuccess)
      .catch(handleFailure);
  };
}

export function getEntityReset(options) {
  return {
    type: actionTypes.GET_ENTITY_RESET
    , entityType: options.entityType
  };
}

export function getEntity(options) {
  return dispatch => {
    // Notify that an enty is being retrieved
    dispatch(getEntityBegin(options));

    let handleSuccess = (response) => {
      // Notify that the entity has been retrieved
      dispatch(getEntityComplete({
        entityType: options.entityType
        , entity: response.data
      }));
    };

    let handleFailure = (error) => {
      // Notify that the entity has not been retrieved
      dispatch(getEntityFailed({
        entityType: options.entityType
        , getError: _.get(error, ["response", "status"]) == 404 ? "The entity could not be found." : "A server error occurred."
      }));
    };

    // Begin the request and return a promise.
    let url = entityUrl(options.entityType, options.id);
    return axios.get(url, { withCredentials: true })
      .then(handleSuccess)
      .catch(handleFailure);
  };
}

export function saveEntityReset(options) {
  return {
    type: actionTypes.SAVE_ENTITY_RESET
    , entityType: options.entityType
  };
}

export function saveEntity(options) {
  return dispatch => {
    dispatch(saveEntityBegin(options));

    let requestType = null;
    let url = null;

    if (options.entity.id) {
      requestType = "PUT";
      url = entityUrl(options.entityType, options.entity.id);
    }
    else {
      requestType = "POST";
      url = entityUrl(options.entityType);
    }

    let handleSuccess = (response) => {
      dispatch(saveEntityComplete({
        entityType: options.entityType
        , listEntity: response.data.listEntity
        , getEntity: response.data.getEntity
      }));
    };

    let handleFailure = (error) => {
      let saveError = null;
      let status = _.get(error, ["response", "status"]);
      if(status == 400) {
        // Validation error
        saveError = error.response.data;
      }
      else if(status == 404) {
        saveError = FluentValidationService.validationFailure("The entity no longer exists or could not be found.");
      }
      saveError = saveError || FluentValidationService.validationFailure("A server error occurred.");

      dispatch(saveEntityFailed({
        entityType: options.entityType
        , saveError: saveError
      }));
    };

    return axios({
      method: requestType
      , url: url
      , data: options.entity
      , withCredentials: true
    })
    .then(handleSuccess)
    .catch(handleFailure);
  };
}

export function deleteEntityReset(options) {
  return {
    type: actionTypes.DELETE_ENTITY_RESET
    , entityType: options.entityType
  };
}

export function deleteEntity(options) {
  return dispatch => {
    // Notify that a deletion is beginning
    dispatch(deleteEntityBegin(options));

    let handleSuccess = (response) => {
      // Notify that the deletion has completed
      dispatch(deleteEntityComplete({
        entityType: options.entityType
        , id: response.data.id
      }));
    };

    let handleFailure = (error) => {
      let deleteError = null;
      let status = _.get(error, ["response", "status"]);
      if(status == 400) {
        // Validation error
        deleteError = error.response.data;
      }
      else if(status == 404) {
        deleteError = FluentValidationService.validationFailure("The entity no longer exists or could not be found.");
      }
      deleteError = deleteError || FluentValidationService.validationFailure("A server error occurred");

      // Notify that the deletion has failed
      dispatch(deleteEntityFailed({
        entityType: options.entityType
        , deleteError: deleteError
      }));
    };

    // Begin the request and return a promise
    let url = entityUrl(options.entityType, options.id);

    return axios.delete(url, { withCredentials: true })
    .then(handleSuccess)
    .catch(handleFailure);
  };
}

// If another client edits or creates an entity, this action
// will be called on all other clients. Other clients can then
// receive the updated entity and merge it into their state.
export function mergeEntity(options) {
  return {
    type: actionTypes.MERGE_ENTITY
    , entityType: options.entityType
    , listEntity: options.listEntity
    , getEntity: options.getEntity
  };
}

// If another client deletes an entity, this action
// will be called on all other clients. Other clients
// can then remove the entity from their state
export function mergeDeleteEntity(options) {
  return {
    type: actionTypes.MERGE_DELETE_ENTITY
    , entityType: options.entityType
    , id: options.id
  };
}

function saveEntityBegin(options) {
  return {
    type: actionTypes.SAVE_ENTITY_BEGIN
    , entityType: options.entityType
  };
}

function saveEntityComplete(result) {
  return {
    type: actionTypes.SAVE_ENTITY_COMPLETE
    , entityType: result.entityType
    , listEntity: result.listEntity
    , getEntity: result.getEntity
  };
}

function saveEntityFailed(result) {
  return {
    type: actionTypes.SAVE_ENTITY_FAILED
    , entityType: result.entityType
    , saveError: result.saveError
  };
}

function listEntitiesBegin(options) {
  return {
    type: actionTypes.LIST_ENTITIES_BEGIN
    , entityType: options.entityType
  };
}

function listEntitiesComplete(result) {
  return {
    type: actionTypes.LIST_ENTITIES_COMPLETE
    , entityType: result.entityType
    , entities: result.entities
  };
}

function listEntitiesFailed(result) {
  return {
    type: actionTypes.LIST_ENTITIES_FAILED
    , entityType: result.entityType
    , listError: result.listError
  };
}

function getEntityBegin(options) {
  return {
    type: actionTypes.GET_ENTITY_BEGIN
    , entityType: options.entityType
  };
}

function getEntityComplete(result) {
  return {
    type: actionTypes.GET_ENTITY_COMPLETE
    , entityType: result.entityType
    , entity: result.entity
  };
}

function getEntityFailed(result) {
  return {
    type: actionTypes.GET_ENTITY_FAILED
    , entityType: result.entityType
    , getError: result.getError
  };
}

function deleteEntityBegin(options) {
  return {
    type: actionTypes.DELETE_ENTITY_BEGIN
    , entityType: options.entityType
    , entity: options.entity
  };
}

function deleteEntityComplete(result) {
  return {
    type: actionTypes.DELETE_ENTITY_COMPLETE
    , entityType: result.entityType
    , id: result.id
  };
}

function deleteEntityFailed(result) {
  return {
    type: actionTypes.DELETE_ENTITY_FAILED
    , entityType: result.entityType
    , entity: result.entity
    , deleteError: result.deleteError
  };
}
