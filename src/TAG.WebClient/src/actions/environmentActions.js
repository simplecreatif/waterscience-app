import * as actionTypes from "../constants/actionTypes";
import * as config from "../constants/config";
import axios from "axios";
import Guid from "guid";
import AxiosConfigurationService from "../services/AxiosConfigurationService";
import Cookies from "js-cookie";

// Checks that the backend is available and fetch scaffolding
export function initialise() {
  return (dispatch) => {
    configureCsrf();

    let handleResponse = (response) => {
      if(response.status == 200){
        let clientId = Guid.raw();
        AxiosConfigurationService.setClientId(clientId);
        dispatch(initialiseComplete({
          scaffoldConfigurations: response.data
          , clientId: clientId
          , initialiseError: false
        }));
        return;
      }
      dispatch(initialiseComplete({
        scaffoldConfigurations: null
        , clientId: null
        , initialiseError: true
      }));
      return;
    };

    let url = config.WEBAPI_URL + "scaffoldConfiguration";
    return axios.get(url, { withCredentials: true })
      .then(handleResponse)
      .catch(handleResponse);
  };
}

function configureCsrf() {
  let token = Cookies.get("Csrf-Token") || Guid.raw();
  Cookies.set("Csrf-Token", token);
  AxiosConfigurationService.setCsrfToken(token);
}

export function initialiseComplete(options) {
  return {
    type: actionTypes.INITIALISE_ENVIRONMENT_COMPLETE
    , scaffoldConfigurations: options.scaffoldConfigurations
    , clientId: options.clientId
    , initialiseError: options.initialiseError
  };
}
