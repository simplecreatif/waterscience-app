import * as actionTypes from "../constants/actionTypes";
import * as config from "../constants/config";
import axios from "axios";
import { routerActions } from "react-router-redux";

export function login(email, password, redirect = "/") {
  return dispatch => {
    // Notify that login request has been sent
    dispatch(loginBegin());

    let handleResponse = response => {
      if (response.status === 200) {
        // Notify that login has completed
        dispatch(loginComplete({
          user: response.data
        }));

        // Redirect to previous page or home
        dispatch(routerActions.push(redirect));

        return;
      }

      // Notify that login has failed
      dispatch(loginFailed());
      return;
    };

    // Begin the request and return a promise
    let url = config.WEBAPI_URL + "auth/signin";

    return axios({
      method: "post"
      , url: url
      , data: { email, password }
      , withCredentials: true
    })
    .then(handleResponse)
    .catch(handleResponse);
  };
}

export function logout() {
  return dispatch => {
    // Notify that the logout request has been sent
    dispatch(logoutBegin());

    let handleResponse = response => {
      if (response.status === 200) {
        // Notify that logout has completed
        dispatch(logoutComplete());

        // Redirect home
        dispatch(routerActions.push("/"));

        return;
      }

      // should never fail to logout
      // eslint-disable-next-line no-console
      console.error("LOGOUT FAILED.", response);
      // throw Error("Logout failed");
    };

    // Begin the request and return a promise
    let url = config.WEBAPI_URL + "auth/signout";

    return axios({
      method: "post"
      , url: url
      , withCredentials: true
    })
    .then(handleResponse)
    .catch(handleResponse);
  };
}

export function checkUser() {
  return dispatch => {
    // Notify that the request has been sent
    dispatch(checkUserBegin());

    let handleResponse = response => {
      if (response.status === 200) {
        // Notify that user has been determined
        dispatch(checkUserComplete({
          isLoggedIn: true
          , user: response.data
        }));

        return;
      }

      // Notify that user is not authenticated
      dispatch(checkUserComplete({
        isLoggedIn: false
        , user: null
      }));
    };

    // Begin the request and return a promise
    let url = config.WEBAPI_URL + "auth/currentuser";

    return axios({
      method: "get"
      , url: url
      , withCredentials: true
    })
    .then(handleResponse)
    .catch(handleResponse);
  };
}

function loginBegin() {
  return {
    type: actionTypes.LOGIN_BEGIN
  };
}

function loginComplete(options) {
  return {
    type: actionTypes.LOGIN_COMPLETE
    , user: options.user
  };
}

function loginFailed() {
  return {
    type: actionTypes.LOGIN_FAILED
  };
}

function logoutBegin() {
  return {
    type: actionTypes.LOGOUT_BEGIN
  };
}

function logoutComplete() {
  return {
    type: actionTypes.LOGOUT_COMPLETE
  };
}

function checkUserBegin() {
  return {
    type: actionTypes.CHECK_USER_BEGIN
  };
}

function checkUserComplete(options) {
  return {
    type: actionTypes.CHECK_USER_COMPLETE
    , isLoggedIn: options.isLoggedIn
    , user: options.user
  };
}
