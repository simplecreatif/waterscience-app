import * as actionTypes from "../constants/actionTypes";
import * as config from "../constants/config";
import FluentValidationService from "../services/FluentValidationService";
import axios from "axios";
import _ from "lodash";

// Indicates that a user wishes to move a toxicity
// value to a different group (the desired group
// has not been selected yet)
export function moveToxicityValueMode(toxicityValue) {
  return {
    type: actionTypes.MOVE_TOXICITY_VALUE_MODE
    , toxicityValue: toxicityValue
  };
}

export function moveToxicityValue(toxicityValue, guideline){
  return dispatch => {
    // If we're trying to move the toxicity value into the group
    // it's already in just cancel move mode because there is
    // nothing to do.
    if(guideline.id == toxicityValue.guidelineGroupId){
      dispatch(moveToxicityValueReset(toxicityValue));
      return;
    }

    dispatch(moveToxicityValueBegin(toxicityValue, guideline));

    let handleSuccess = () => {
      dispatch(moveToxicityValueComplete(toxicityValue, guideline));
    };

    let handleFailure = (error) => {
      let saveError = null;
      let status = _.get(error, ["response", "status"]);
      if(status == 400) {
        // Validation error
        saveError = error.response.data;
      }
      else if(status == 404) {
        saveError = FluentValidationService.validationFailure("The entity no longer exists or could not be found.");
      }
      saveError = saveError || FluentValidationService.validationFailure("A server error occurred.");

      dispatch(moveToxicityValueFailed(toxicityValue, guideline, saveError));
    };

    return axios({
      method: "PUT"
      , url: config.WEBAPI_URL + "guidelinegroup/move"
      , data: {
        toxicityValueId: toxicityValue.id
        , guidelineGroupId: guideline.id
      }
      , withCredentials: true
    })
    .then(handleSuccess)
    .catch(handleFailure);
  };
}

export function moveToxicityValueReset() {
  return {
    type: actionTypes.MOVE_TOXICITY_VALUE_RESET
  };
}

function moveToxicityValueBegin(toxicityValue, guideline) {
  return {
    type: actionTypes.MOVE_TOXICITY_VALUE_BEGIN
    , toxicityValue: toxicityValue
    , guideline: guideline
  };
}

function moveToxicityValueComplete(toxicityValue, guideline) {
  return {
    type: actionTypes.MOVE_TOXICITY_VALUE_COMPLETE
    , toxicityValue: toxicityValue
    , guideline: guideline
  };
}

function moveToxicityValueFailed(toxicityValue, guideline, saveError) {
  return {
    type: actionTypes.MOVE_TOXICITY_VALUE_FAILED
    , toxicityValue: toxicityValue
    , guideline: guideline
    , saveError: saveError
  };
}
