class FluentValidationService {
  // Converts a simple string error message to a format that is compatible
  // with consumers of FluentValidation results.
  validationFailure(message) {
    return { errors: [{errorMessage: message}] };
  }
}

export default new FluentValidationService();
