
// maps entityTypes to a navigation subsection name
let sections = {
  species: "Species"
  , animaltype: "Species"
  , animalcategory: "Species"
  , speciesclass: "Species"
  , speciesphylum: "Species"

  , chemical: "Chemical"
  , toxicant: "Chemical"
  , chemicaltype: "Chemical"
  , chemicalreference: "Chemical"
  , chemicalform: "Chemical"
  , chemicalgrade: "Chemical"

  , media: "Media"
  , mediatype: "Media"

  , toxicityvalueaed: "Toxicity Value"
  , toxicityvalue2000: "Toxicity Value"
  , toxicityvalue2016: "Toxicity Value"
};

let defaultSection = "Configuration";


class NavigationGeneratorService {
  // Generates a hierarchy of navigation based on a set of
  // scaffold configuration.
  // result will look like:
  //  [
  //    "SectionName":
  //    [
  //      {
  //        entityType: "entityType",
  //        displayName: "Display Name"
  //      }
  //    ]
  //  ]
  generate(scaffoldConfigurations) {
    let entityTypes = Object.keys(scaffoldConfigurations)
      .sort()
      .filter(x =>
        scaffoldConfigurations[x].entityType != null &&
        (scaffoldConfigurations[x].isScaffolded == null || scaffoldConfigurations[x] == true));

    let result = {};

    entityTypes.forEach(x => {
      // Don't show User in scaffolded menu
      if (x === "user") {
        return;
      }

      let section = sections[x] || defaultSection;

      if(result[section] == null){
        result[section] = [];
      }

      result[section].push(scaffoldConfigurations[x]);
    });

    return result;
  }
}

export default new NavigationGeneratorService();
