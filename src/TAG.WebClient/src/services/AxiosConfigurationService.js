import axios from "axios";
import _ from "lodash";
import { routerActions } from "react-router-redux";

class AxiosConfigurationService {
  configure({ dispatch }) {
    // When a general api error occurs log some information to the console.
    // An error is considered general when there is no response or the response
    // code is not 400 (400 is known to be a validation error), 401 or 403.
    // These errors can be an indication that there is a server error.
    let handleSuccess = (response) => response;
    let handleFailure = (error) => {
      let status = _.get(error, ["response", "status"]);
      let isValidationError = status == 400;
      let isUnauthorisedError = status == 401;
      let isForbiddenError = status == 403;

      if (isForbiddenError) {
        // Redirect to login
        dispatch(routerActions.push("/login"));
      }

      if(!isValidationError && !isForbiddenError && !isUnauthorisedError) {
        // eslint-disable-next-line no-console
        console.log(
          "A general axios error occurred. This may be due to a server error. " +
          "If you are in development mode, try navigating to the URL directly to " +
          "see if there is a developer exception message: " + error.config.url);
      }

      return Promise.reject(error);
    };

    axios.interceptors.response.use(handleSuccess, handleFailure);
  }

  setClientId(clientId) {
    axios.defaults.headers.common["X-Client-Id"] = clientId;
  }

  setCsrfToken(token) {
    axios.defaults.headers.common["X-Csrf-Token"] = token;
  }
}

export default new AxiosConfigurationService();
