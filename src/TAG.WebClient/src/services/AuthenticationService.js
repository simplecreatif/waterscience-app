import * as roles from "../constants/roles";

// Provides a way to check if the current user is a particular role
class AuthenticationService {
  isAuthenticated(auth) {
    return auth.isLoggedIn;
  }

  isUnauthenticated(auth) {
    return !auth.isLoggedIn;
  }

  isContributor(auth) {
    return auth.isLoggedIn
      && auth.currentUser
      && auth.currentUser.roleId >= roles.CONTRIBUTOR;
  }

  isAdministrator(auth) {
    return auth.isLoggedIn
      && auth.currentUser
      && auth.currentUser.roleId === roles.ADMINISTRATOR;
  }
}

export default new AuthenticationService();
