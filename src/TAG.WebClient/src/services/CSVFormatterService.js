// Custom formatting for the react-bootstrap-table csv export feature.
// replaces the string "null" with an empty string.
class CSVFormatterService {
  format(cell) {
    return (cell === null) ? "" : cell;
  }
}

export default new CSVFormatterService();
