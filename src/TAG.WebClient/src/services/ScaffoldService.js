import _ from "lodash";

class ScaffoldService {
  valueTypes = ["numeric", "bool", "password"];

  // Given a template (which describes the properties of an entity)
  // find all properties which are references to other entities, then
  // fire off requests to list all these entities. The purpose of this
  // is to pre-fetch all required drop-down box data when editing an
  // entity that has relationships to other entities.
  fetchEntitiesFromTemplate(entities, scaffoldConfiguration, entityActions) {
    let template = scaffoldConfiguration.template;

    // Find all the property values which are non-null
    // and which have not yet been loaded (are not present
    // in entities redux state)
    let entityTypes = [];
    for (let propertyName in template) {
      let type = template[propertyName];
      let isEntityType = type != null && this.valueTypes.indexOf(type) == -1;
      let isAlreadyLoaded = type != null && _.get(entities, [type, "entities"]) != null;
      let isHiddenProperty = (scaffoldConfiguration.hiddenProperties || []).indexOf(propertyName) > -1;

      if(isEntityType && !isAlreadyLoaded && !isHiddenProperty) {
        entityTypes.push(type);
      }
    }

    // When a scaffolded entity refers to the same type of entity twice,
    // only load the related entities once.
    entityTypes = _.uniq(entityTypes);

    // Fire off requests to list all the entity types that haven't
    // yet been loaded
    let listEntities = entityTypes.map((entityType) =>
      entityActions.listEntities({
        entityType: entityType
      })
    );
    return Promise.all(listEntities);
  }
}

export default new ScaffoldService();
