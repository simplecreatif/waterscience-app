// First letter becomes uppercase
String.prototype.pascalCase = function() {
  return (this[0].toUpperCase() + this.slice(1)).valueOf();
};

// First letter becomes lowercase
String.prototype.camelCase = function() {
  return (this[0].toLowerCase() + this.slice(1)).valueOf();
};

// First letter becomes uppercase, spaces added before any other capitals
String.prototype.toPhrase = function() {
  if(!this.length){
    return "";
  }

  let phrase = [];
  phrase.push(this[0].toUpperCase());
  // hello -> hello
  // helloWorld -> Hello World
  // casNumber -> Cas Number
  // WQG2000 -> WQG 2000
  for(let i = 1; i < this.length; i++){
    let char = this[i];
    let nextChar = (i < this.length - 1) ? this[i+1] : null;
    let isUpper = char.isUpper() || char.isNumber();
    let isNextUpper = nextChar != null && (nextChar.isUpper() || nextChar.isNumber());
    phrase.push(char);
    if(!isUpper && isNextUpper) {
      phrase.push(" ");
    }
  }
  return phrase.join("");
};

// Determines whether a character is uppercase
// Only accepts strings of length 1.
String.prototype.isUpper = function() {
  if(this.length > 1) {
    throw "Expected a single character, received multiple characters.";
  }
  return this[0] >= "A" && this[0] <= "Z";
};

// Determines whether a character is an integer.
// Only accepts strings of length 1.
String.prototype.isNumber = function() {
  if(this.length > 1) {
    throw "Expected a single character, received multiple characters.";
  }
  return this[0] >= "0" && this[0] <= "0";
};
