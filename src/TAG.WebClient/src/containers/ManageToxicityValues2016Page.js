import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Editor from "../components/common/Editor";
import * as entityActions from "../actions/entityActions";
import * as guidelineActions from "../actions/guidelineActions";
import * as moveToxicityValueActions from "../actions/moveToxicityValueActions";
import { Link } from "react-router";
import _ from "lodash";
import SpeciesSelector from "../components/common/SpeciesSelector";
import ToxicityValueGroup from "../components/ToxicityValueGroup";
import GettingReady from "../components/common/GettingReady";

// This page is used to find all toxicity values (2016) for a
// chemical. The toxicity values are presented in their guideline
// groups and some controls are provided to move them between groups,
// and to create, edit, or delete toxicity values (2016). This is needed
// because toxicity value 2016 has a "guideline group" concept which
// arbitrarily groups toxicity values together.
//
// A user is expected to initially choose one or more chemicals. The page
// will then show all toxicity values (organised into guideline groups)
// for those chemicals. Typically a user would only choose one chemical
// unless they accidentally configured a toxicity value for the wrong
// chemical and need to move it elsewhere.
class ManageToxicityValues2016Page extends React.Component {
  static propTypes = {
    entityActions: PropTypes.object.isRequired
    , guidelineActions: PropTypes.object.isRequired
    , moveToxicityValueActions: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
    , guidelines: PropTypes.object.isRequired
    , moveToxicityValue: PropTypes.object.isRequired
  };

  constructor() {
    super();

    this.handleSpeciesOrChemicalChanged = this.handleSpeciesOrChemicalChanged.bind(this);
    this.handleMoveMode = this.handleMoveMode.bind(this);
    this.handleMove = this.handleMove.bind(this);

    this.state = {
      chemicals: []
      , species: []
      , isReady: false
      , toxicityValueGroups: null
    };
  }

  componentWillMount() {
    // Load chemicals and toxicity values 2016
    this.props.entityActions.listEntitiesBatch({
      entityTypes: ["chemical", "species", "toxicityvalue2016"]
    })
    .then(() => this.setState({ isReady: true }));
  }

  componentWillReceiveProps(nextProps) {
    if(this.state.isReady && _.get(nextProps, ["entities", "toxicityvalue2016", "entities"]) != null) {
      this.rebuildToxicityValueGroups(this.state.chemicals, this.state.species, nextProps.entities.toxicityvalue2016);
    }
  }

  // Handles when a user notifies that they want to move
  // a toxicity value from its current group to a different
  // group.
  handleMoveMode(toxicityValue) {
    this.props.moveToxicityValueActions.moveToxicityValueMode(toxicityValue);
  }

  // Handles when a user has requested the toxicity value
  // be moved to the guideline
  handleMove(guideline) {
    this.props.moveToxicityValueActions.moveToxicityValue(this.props.moveToxicityValue.toxicityValue, guideline);
  }

  handleSpeciesOrChemicalChanged() {
    this.rebuildToxicityValueGroups(this.state.chemicals, this.state.species, this.props.entities.toxicityvalue2016);
  }

  // Rebuilds the tables of tox values. Should be called whenever
  // the selected chemical is changed or whenever underlying data changes
  rebuildToxicityValueGroups(chemicals, species, toxicityvalue2016) {
    let chemicalIds = (chemicals && chemicals.length > 0) ? _.keyBy(chemicals, x => x.chemical.id) : null;
    let speciesIds = (species && species.length > 0) ? _.keyBy(species, x => x.value) : null;

    let toxicityValueGroups = null;
    if(chemicalIds != null && speciesIds != null) {
      // First find all guideline groups which have at least one toxicity value
      // with a matching chemical or species
      let toxicityValues = toxicityvalue2016.entities;
      let guidelineGroupIds = toxicityValues.filter(x =>
        (chemicalIds == null || chemicalIds[x.chemicalId] != null) &&
        (speciesIds == null || speciesIds[x.speciesId] != null))
        .map(x => x.guidelineGroupId);
      let guidelineGroupIdsMap = _.keyBy(guidelineGroupIds, x => x);

      // Then group all toxicity data by their guideline group and return
      // only those groups found in the previous step
      toxicityValueGroups = _.chain(toxicityValues)
        .groupBy(x => x.guidelineGroupId)
        .filter((v, k) => guidelineGroupIdsMap[k] != null)
        .value();
    }

    this.setState({
      toxicityValueGroups: toxicityValueGroups
    });
  }

  renderToxicityValueGroups(toxicityValueGroups) {
    // Whether the user has indicated that they want to move a toxicity value
    // to a different group. In this mode, the toxicity value groups change
    // their behaviour to support recieving the toxicity value.
    let isMoveMode = this.props.moveToxicityValue.toxicityValue != null;

    return toxicityValueGroups.map(toxicityValues => {
      let guideline = {
        id: toxicityValues[0].guidelineGroupId
        , guidelineValue: toxicityValues[0].guidelineGroupValue
      };

      // If we are in move move, any group can receive the toxicity value except
      // the group which the toxicity value currently belongs to.
      let canReceiveToxicityValue = isMoveMode && guideline.id != this.props.moveToxicityValue.toxicityValue.guidelineGroupId;

      return (<ToxicityValueGroup
        key={guideline.id}
        guideline={guideline}
        toxicityValues={toxicityValues}
        guidelineSaveState={this.props.guidelines[guideline.id]}
        actions={this.props.guidelineActions}
        onBeginMove={this.handleMoveMode}
        onMove={this.handleMove}
        isMoveMode={isMoveMode}
        canReceiveToxicityValue={canReceiveToxicityValue}
        movingToxicityValue={this.props.moveToxicityValue.toxicityValue} />);
    });
  }

  render() {
    if(!this.state.isReady) {
      return <GettingReady />;
    }

    let chemical = this.props.entities.chemical;
    let chemicalOptions = chemical.entities.map(x => ({ label: x.name, value: x.id, chemical: x }));

    return (<div>
      <p>Choose a chemical and species to continue.</p>
      <Editor label="Chemical" context={this.state} property="chemicals" options={chemicalOptions} type="react-virtualized-select" multi onChange={this.handleSpeciesOrChemicalChanged} />
      <SpeciesSelector context={this.state} property="species" species={this.props.entities.species.entities} onChange={this.handleSpeciesOrChemicalChanged} />
      <Link to={"/toxicityvalue2016/create"}>Create New Toxicity Value 2016 in new group</Link>
      {this.state.toxicityValueGroups != null ? this.renderToxicityValueGroups(this.state.toxicityValueGroups) : null}
    </div>);
  }
}

function mapStateToProps(state) {
  return {
    entities: state.entities
    , guidelines: state.guidelines

    // This state contains information about the
    // toxicity value being moved (if any)
    , moveToxicityValue: state.moveToxicityValue
  };
}

function mapDispatchToProps(dispatch) {
  return {
    entityActions: bindActionCreators(entityActions, dispatch)
    , guidelineActions: bindActionCreators(guidelineActions, dispatch)
    , moveToxicityValueActions: bindActionCreators(moveToxicityValueActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageToxicityValues2016Page);
