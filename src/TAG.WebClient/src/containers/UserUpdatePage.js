import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as entityActions from "../actions/entityActions";
import { Alert, Button } from "react-bootstrap";
import UserEditor from "../components/UserEditor";
import { browserHistory } from "react-router";
import ScaffoldService from "../services/ScaffoldService";
import ErrorList from "../components/common/ErrorList";
import AuthenticationService from "../services/AuthenticationService";

// Although user is a scaffolded entity we need custom presentation logic
// for editing users. This route for this page is explicitly configured.
class UserUpdatePage extends React.Component {
  static propTypes = {
    // scaffoldConfiguration: PropTypes.object.isRequired
    params: PropTypes.object.isRequired
    , scaffoldConfigurations: PropTypes.object.isRequired
    , entityActions: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
    , auth: PropTypes.object.isRequired
    , pathname: PropTypes.string.isRequired
  };

  constructor() {
    super();

    this.handleSave = this.handleSave.bind(this);

    this.state = {
      editEntity: {}
      , isReady: false
    };
  }

  componentWillMount() {
    this.setState({ isReady: false });

    // If we're in account settings, user id is current user id,
    // otherwise get the id from the route
    let id = this.isAccountSettings()
      ? this.props.auth.currentUser.id
      : this.props.params.id;

    // Load the entity
    this.props.entityActions.getEntityReset({ entityType: "user" });
    this.props.entityActions.saveEntityReset({ entityType: "user" });
    let getPromise = this.props.entityActions.getEntity({
      entityType: "user"
      , id: id
    });

    // Load the template for the entity. This tells us
    // which properties are references to other objects
    // so we can load data and populate drop-downs for them.
    let relatedEntitiesPromise = ScaffoldService.fetchEntitiesFromTemplate(this.props.entities, this.getScaffoldConfiguration(), this.props.entityActions);

    Promise.all([getPromise, relatedEntitiesPromise])
      .then(() => this.setState({ isReady: true }));
  }

  componentWillUpdate(nextProps) {
    let entityContext = this.getEntityContext();
    let nextEntityContext = this.getEntityContext(nextProps);

    if(entityContext == null) {
      return;
    }

    if(entityContext.entity != nextEntityContext.entity) {
      // Make a copy of the entity being edited so
      // we can make changes to it outside of the
      // Redux state. This saves having to fire off
      // actions for every single keystroke a user
      // enters during the edit process.
      this.setState({
        editEntity: {
          ...nextEntityContext.entity
        }
      });
    }
    else {
      // If the user was saved successfully; if editing from account settings,
      // redirect home, otherwise redirect to the show user page.
      if (nextEntityContext.saveError == null
          && nextEntityContext.savedEntity != null
          && this.state.editEntity.id == nextEntityContext.savedEntity.id) {
        if (this.isAccountSettings()) {
          browserHistory.push("/");
        } else {
          browserHistory.push("/user/show/" + nextEntityContext.savedEntity.id);
        }
      }
    }
  }

  getEntityContext(props) {
    props = props || this.props;
    return props.entities["user"];
  }

  handleSave(){
    this.props.entityActions.saveEntity({
      entityType: "user"
      , entity: this.state.editEntity
    });
  }

  getScaffoldConfiguration() {
    return this.props.scaffoldConfigurations["user"];
  }

  isAccountSettings() {
    return this.props.pathname === "/account";
  }

  render() {
    // Get the redux state for the entity type so we can either show
    // the user a loading message or the actual entity once it's loaded
    const { scaffoldConfigurations } = this.props;
    const scaffoldConfiguration = this.getScaffoldConfiguration();
    const entityContext = this.getEntityContext();

    const isAdmin = AuthenticationService.isAdministrator(this.props.auth);
    const isAccountSettings = this.isAccountSettings();
    const pageName = isAccountSettings ? "Account Settings" : "Update User";

    if(!this.state.isReady) {
      return <Alert bsStyle="success">Getting Ready...</Alert>;
    }

    if(entityContext.getError) {
      return <Alert bsStyle="danger">{entityContext.getError}</Alert>;
    }

    return (<div>
      <h2>{pageName}</h2>
      <ErrorList errors={entityContext.saveError} />
      <UserEditor scaffoldConfiguration={scaffoldConfiguration} scaffoldConfigurations={scaffoldConfigurations} entity={this.state.editEntity} entities={this.props.entities} isSelf={isAccountSettings} showRole={isAdmin}/>
      <Button type="submit" onClick={this.handleSave} disabled={entityContext.isSaving}>Save</Button>
      <ErrorList errors={entityContext.saveError} />
    </div>);
  }
}

function mapStateToProps(state, ownProps) {
  return {
    entities: state.entities
    , scaffoldConfigurations: state.environment.scaffoldConfigurations
    , auth: state.auth
    , pathname: ownProps.location.pathname
  };
}

function mapDispatchToProps(dispatch) {
  return {
    entityActions: bindActionCreators(entityActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserUpdatePage);
