import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as entityActions from "../actions/entityActions";
import { Alert } from "react-bootstrap";
import { Link } from "react-router";
import ScaffoldDisplay from "../components/common/ScaffoldDisplay";
import AuthenticationService from "../services/AuthenticationService";
import GettingReady from "../components/common/GettingReady";

// Shows information about an entity based on the entity's scaffold configuration
class ScaffoldShowPage extends React.Component {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , entityId: PropTypes.number.isRequired
    , entityActions: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
    , auth: PropTypes.object.isRequired
  };

  componentWillMount() {
    // Fetch the entity
    let entityType = this.props.scaffoldConfiguration.entityType;
    this.props.entityActions.getEntityReset({ entityType: entityType });
    this.props.entityActions.getEntity({
      entityType: entityType
      , id: this.props.entityId
    });
  }

  render() {
    // Get the redux state for the entity type so we can either show
    // the user a loading message or the actual entity once it's loaded
    const { scaffoldConfiguration } = this.props;
    const entityContext = this.props.entities[scaffoldConfiguration.entityType];

    if(entityContext && entityContext.getError) {
      return <Alert bsStyle="danger">{entityContext.getError}</Alert>;
    }

    if(entityContext == null || entityContext.entity == null) {
      return <GettingReady />;
    }

    return (<div>
      <h2>Show {scaffoldConfiguration.displayName}</h2>
      <ScaffoldDisplay scaffoldConfiguration={scaffoldConfiguration} entity={entityContext.entity} />
      <Link to={"/" + scaffoldConfiguration.entityType}>List</Link>
      {AuthenticationService.isContributor(this.props.auth)
        ? (<span> &middot;
            <Link to={"/" + scaffoldConfiguration.entityType + "/update/" + entityContext.entity.id}>Update</Link> &middot;
            <Link to={"/" + scaffoldConfiguration.entityType + "/delete/" + entityContext.entity.id}>Delete</Link>
          </span>)
          : null
      }
    </div>);
  }
}

function mapStateToProps(state) {
  return {
    entities: state.entities
    , auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    entityActions: bindActionCreators(entityActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScaffoldShowPage);
