import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as entityActions from "../actions/entityActions";
import { Alert, Button } from "react-bootstrap";
import ScaffoldDisplay from "../components/common/ScaffoldDisplay";
import { browserHistory } from "react-router";
import ErrorList from "../components/common/ErrorList";
import GettingReady from "../components/common/GettingReady";

// Shows an entity and allows a user to delete it
class ScaffoldDeletePage extends React.Component {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , entityId: PropTypes.number.isRequired
    , entityActions: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
  };

  constructor(){
    super();

    this.handleDelete = this.handleDelete.bind(this);
  }

  componentWillMount() {
    // Fetch the entity
    let entityType = this.props.scaffoldConfiguration.entityType;
    this.props.entityActions.getEntityReset({ entityType: entityType });
    this.props.entityActions.deleteEntityReset({ entityType: entityType });
    this.props.entityActions.getEntity({
      entityType: entityType
      , id: this.props.entityId
    });
  }

  componentWillUpdate(nextProps) {
    let entityContext = this.getEntityContext();
    let nextEntityContext = this.getEntityContext(nextProps);

    if(entityContext == null) {
      return;
    }
    // If we have successfully deleted the entity
    // redirect to the show entity page
    if (nextEntityContext.deleteError == null
        && nextEntityContext.deletedEntityId != null) {
      browserHistory.push("/" + this.props.scaffoldConfiguration.entityType);
    }
  }

  getEntityContext(props) {
    props = props || this.props;
    return props.entities[props.scaffoldConfiguration.entityType];
  }

  handleDelete() {
    let entityType = this.props.scaffoldConfiguration.entityType;
    this.props.entityActions.deleteEntity({
      entityType: entityType
      , id: this.props.entityId
    });
  }

  render() {
    // Get the redux state for the entity type so we can either show
    // the user a loading message or the actual entity once it's loaded
    const { scaffoldConfiguration } = this.props;
    const entityContext = this.props.entities[scaffoldConfiguration.entityType];

    if(entityContext && entityContext.getError) {
      return <Alert bsStyle="danger">{entityContext.getError}</Alert>;
    }

    if(entityContext == null || entityContext.entity == null) {
      return <GettingReady />;
    }

    return (<div>
      <h2>Are you sure you want to delete this {scaffoldConfiguration.displayName}?</h2>
      <ErrorList errors={entityContext.deleteError} />
      <ScaffoldDisplay scaffoldConfiguration={scaffoldConfiguration} entity={entityContext.entity} />
      <Button type="submit" bsStyle="danger" onClick={this.handleDelete} disabled={entityContext.isDeleting}>Delete</Button>
      <ErrorList errors={entityContext.deleteError} />

    </div>);
  }
}

function mapStateToProps(state) {
  return {
    entities: state.entities
  };
}

function mapDispatchToProps(dispatch) {
  return {
    entityActions: bindActionCreators(entityActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScaffoldDeletePage);
