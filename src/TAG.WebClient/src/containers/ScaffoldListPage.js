import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router";
import * as entityActions from "../actions/entityActions";
import { Alert } from "react-bootstrap";
import ScaffoldList from "../components/common/ScaffoldList";
import AuthenticationService from "../services/AuthenticationService";
import GettingReady from "../components/common/GettingReady";

class ScaffoldListPage extends React.Component {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , entityActions: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
    , auth: PropTypes.object.isRequired
  };

  componentWillMount() {
    this.loadEntities();
  }

  componentDidUpdate() {
    this.loadEntities();
  }

  getEntityContext() {
    return this.props.entities[this.props.scaffoldConfiguration.entityType];
  }

  loadEntities() {
    const entityContext = this.getEntityContext();
    // If entities are loading or have been loaded, don't load them again.
    if(entityContext != null && (entityContext.isListing || entityContext.entities != null || entityContext.listError != null)){
      return;
    }

    let entityType = this.props.scaffoldConfiguration.entityType;
    this.props.entityActions.listEntities({
      entityType: entityType
    });
  }

  renderLinks() {
    const { scaffoldConfiguration } = this.props;

    if (!AuthenticationService.isContributor(this.props.auth)) {
      return null;
    }

    if(scaffoldConfiguration.listLinks == null){
      return <Link to={"/" + scaffoldConfiguration.entityType + "/create"}>Create New {scaffoldConfiguration.displayName}</Link>;
    }

    return (<div>
      {scaffoldConfiguration.listLinks.map(x => <Link key={x.href} to={x.href}>{x.text}</Link>)}
    </div>);
  }

  render() {
    // Get the redux state for the entity type so we can either show
    // the user a loading message or the actual entity list once it's loaded
    const { scaffoldConfiguration } = this.props;
    const entityContext = this.getEntityContext();

    if(entityContext && entityContext.listError) {
      return <Alert bsStyle="danger">{entityContext.listError}</Alert>;
    }

    if(!entityContext || entityContext.entities == null) {
      return <GettingReady />;
    }

    return (<div>
      <h2>List {scaffoldConfiguration.displayName}</h2>
      {this.renderLinks()}
      <ScaffoldList scaffoldConfiguration={scaffoldConfiguration}
                    entities={entityContext.entities}
                    auth={this.props.auth} />
    </div>);
  }
}

function mapStateToProps(state) {
  return {
    entities: state.entities
    , auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    entityActions: bindActionCreators(entityActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScaffoldListPage);
