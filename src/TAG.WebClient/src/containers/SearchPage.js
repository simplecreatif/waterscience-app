import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as searchActions from "../actions/searchActions";
import * as entityActions from "../actions/entityActions";
import SearchForm from "../components/search/SearchForm";
import SearchResults from "../components/search/SearchResults";
import SearchConfigurations from "../components/search/SearchConfigurations";
import _ from "lodash";
import toxicityValueProperties from "../constants/toxicityValueProperties";
import GettingReady from "../components/common/GettingReady";

class SearchPage extends React.Component {
  static propTypes = {
    searchActions: PropTypes.object.isRequired
    , entityActions: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
    , search: PropTypes.object.isRequired
    , auth: PropTypes.object.isRequired
  };

  constructor() {
    super();

    let databaseOptions = ["AED", "WQG2000", "WQG2016"].map(x => ({ label: x, value: x}));
    this.state = {
      isReady: false
      , searchConfigurationId: null
      , searchFormParameters: {
        searchOptions: {
          casNumber: null
          , chemicalId: null
          , toxicantId: null
          , searchProperties: {
            tv_chemical_name: true
            , tv_species_scientificname: true
            , tv_species_commonname: true
            , tv_duration_name: true
            , tv_durationunit_name: true
            , tv_effect_name: true
            , tv_endpoint_name: true
            , tv_concentration: true
            , tv_concentrationunit_name: true
          }
        }
        , chemicalsByCASNumber: {}
        , databaseOptions: databaseOptions
        , databases: databaseOptions // Initially select all databases
        , species: []
        , effects: []
        , endpoints: []
      }
    };
  }

  componentWillMount() {
    this.setState({ isReady: false });

    this.onSelectSearchConfiguration = this.onSelectSearchConfiguration.bind(this);
    this.onSaveExistingSearchConfiguration = this.onSaveExistingSearchConfiguration.bind(this);
    this.onDeleteSearchConfiguration = this.onDeleteSearchConfiguration.bind(this);
    this.onSaveNewSearchConfiguration = this.onSaveNewSearchConfiguration.bind(this);
    this.resetSaveAndDeleteStatus = this.resetSaveAndDeleteStatus.bind(this);

    // Pull down entities which haven't been loaded. These are used
    // to populate the drop-down boxes on the search form
    this.props.entityActions.listEntitiesBatch({
      entityTypes: ["chemical", "toxicant", "species", "effect", "endpoint"
        , "searchconfiguration", "searchproperty", "database"]
    })
      .then(() => this.setState({ isReady: true }));
  }

  componentWillUpdate(nextProps) {
    let entityContext = this.getSearchConfigurationContext();
    let nextEntityContext = this.getSearchConfigurationContext(nextProps);

    if(entityContext == null) {
      return;
    }

    // If we have successfully saved the search configuration, select it
    if (nextEntityContext.saveError == null
        && nextEntityContext.savedEntity != null) {
      if (entityContext.savedEntity
          && nextEntityContext.savedEntity.id == entityContext.savedEntity.id) {
        return;
      }

      this.setState({
        ...this.state
        , searchConfigurationId: nextEntityContext.savedEntity.id
      });
    }

    // If we have successfully deleted the search configuration, deselect it
    if (nextEntityContext.deleteError == null
        && nextEntityContext.deletedEntityId != null
        && nextEntityContext.deletedEntityId != entityContext.deletedEntityId) {
      this.setState({
        ...this.state
        , searchConfigurationId: null
      });
    }
  }

  getSearchConfigurationContext(props) {
    props = props || this.props;
    return props.entities["searchconfiguration"];
  }

  onSelectSearchConfiguration(searchConfigurationId) {
    if (searchConfigurationId !== this.state.searchConfigurationId) {
      this.resetSaveAndDeleteStatus();
    }

    if (!searchConfigurationId) {
      this.setState({
        ...this.state
        , searchConfigurationId
      });
      return;
    }

    // update the form inputs
    const selectedSearchConfiguration = this.getSearchConfiguration(searchConfigurationId);

    const databases = selectedSearchConfiguration.databaseIds
      .map(x => {
        const name = this.getEntity("database", x).name;
        return { label: name, value: name };
      });

    const searchProperties = {};
    selectedSearchConfiguration.searchPropertyIds.forEach(x => {
        const name = this.getEntity("searchproperty", x).name;
        const tv = _.findKey(toxicityValueProperties, x => x === name);
        searchProperties[tv] = true;
      });

    const species = selectedSearchConfiguration.speciesIds
      .map(constructSelectedSpecies.bind(this));
    const effects = selectedSearchConfiguration.effectIds
      .map(x => ({ label: this.getEntity("effect", x).name, value: x }));
    const endpoints = selectedSearchConfiguration.endpointIds
      .map(x => ({ label: this.getEntity("endpoint", x).abbreviation, value: x }));

    this.setState({
      ...this.state
      , searchConfigurationId
      , searchFormParameters: {
        ...this.state.searchFormParameters
        , searchOptions: {
          casNumber: selectedSearchConfiguration.casNumber
          , chemicalId: selectedSearchConfiguration.chemicalId
          , toxicantId: selectedSearchConfiguration.toxicantId
          , searchProperties
        }
        , databases
        , species
        , effects
        , endpoints
      }
    });

    function constructSelectedSpecies(speciesId) {
      const speciesEntity = this.getEntity("species", speciesId);
      return {
        label: speciesEntity.scientificName
        , value: speciesId
        , species: {
          id: speciesId
          , commonName: speciesEntity.commonName
          , scientificName: speciesEntity.scientificName
        }
      };
    }
  }

  onSaveExistingSearchConfiguration() {
    const currentName = this.getSearchConfiguration(this.state.searchConfigurationId).name;
    const searchConfiguration = this.getCurrentSearchConfiguration(currentName);
    searchConfiguration.id = this.state.searchConfigurationId;
    this.saveSearchConfiguration(searchConfiguration);
  }

  onDeleteSearchConfiguration() {
    this.resetSaveAndDeleteStatus();

    this.props.entityActions.deleteEntity({
      entityType: "searchconfiguration"
      , id: this.state.searchConfigurationId
    });
  }

  onSaveNewSearchConfiguration(name) {
    const searchConfiguration = this.getCurrentSearchConfiguration(name);
    this.saveSearchConfiguration(searchConfiguration);
  }

  saveSearchConfiguration(searchConfiguration) {
    this.resetSaveAndDeleteStatus();

    this.props.entityActions.saveEntity({
      entityType: "searchconfiguration"
      , entity: searchConfiguration
    });
  }

  resetSaveAndDeleteStatus() {
    this.props.entityActions.saveEntityReset({
      entityType: "searchconfiguration"
    });

    this.props.entityActions.deleteEntityReset({
      entityType: "searchconfiguration"
    });
  }

  getSearchConfiguration(id) {
    return this.getEntity("searchconfiguration", id);
  }

  getEntity(entityType, id) {
    return _.find(this.props.entities[entityType].entities,
      x => x.id === id);
  }

  getCurrentSearchConfiguration(name) {
    // Construct a search configuration entity from the current state of the form inputs
    const searchParameters = this.state.searchFormParameters;
    const searchOptions = searchParameters.searchOptions;

    const databaseIds = searchParameters.databases
      .map(x => _.find(this.props.entities.database.entities, y => y.name === x.value).id);

    const searchPropertyIds = Object.keys(searchOptions.searchProperties)
      .filter(x => searchOptions.searchProperties[x])
      .map(x => _.find(this.props.entities.searchproperty.entities, y => y.name === toxicityValueProperties[x]).id);

    return {
      name
      , userId: this.props.auth.currentUser.id
      , casNumber: searchOptions.casNumber
      , chemicalId: searchOptions.chemicalId
      , toxicantId: searchOptions.toxicantId
      , speciesIds: searchParameters.species.map(x => x.species.id)
      , effectIds: searchParameters.effects.map(x => x.value)
      , endpointIds: searchParameters.endpoints.map(x => x.value)
      , databaseIds: databaseIds
      , searchPropertyIds: searchPropertyIds
    };
  }

  render() {
    if(!this.state.isReady) {
      return <GettingReady />;
    }

    const { entities } = this.props;

    return (<div>
      <SearchConfigurations
        searchConfiguration={entities.searchconfiguration}
        searchConfigurationId={this.state.searchConfigurationId}
        onSelectSearchConfiguration={this.onSelectSearchConfiguration}
        onSaveExistingSearchConfiguration={this.onSaveExistingSearchConfiguration}
        onDeleteSearchConfiguration={this.onDeleteSearchConfiguration}
        onSaveNewSearchConfiguration={this.onSaveNewSearchConfiguration}
        resetSaveAndDeleteStatus={this.resetSaveAndDeleteStatus} />
      <SearchForm
        formInputs={this.state.searchFormParameters}
        chemicals={entities.chemical.entities}
        toxicants={entities.toxicant.entities}
        species={entities.species.entities}
        effects={entities.effect.entities}
        endpoints={entities.endpoint.entities}
        onSearch={this.props.searchActions.search}
        onDownloadGuidelineDerivations={this.props.searchActions.downloadGuidelineDerivations} />
      <SearchResults results={this.props.search.results} isSearching={this.props.search.isSearching} error={this.props.search.searchError} />
    </div>);
  }
}

// Maps parts of the root redux state object
// to props of the object being connected (which
// in this case is SearchPage.
function mapStateToProps(state) {
  return {
    entities: state.entities
    , search: state.search
    , auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    searchActions: bindActionCreators(searchActions, dispatch)
    , entityActions: bindActionCreators(entityActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPage);
