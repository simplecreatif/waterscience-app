import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LoginForm from "../components/LoginForm";
import * as authActions from "../actions/authActions";

class LoginPage extends React.Component {
  static propTypes = {
    authActions: PropTypes.object.isRequired
    , redirect: PropTypes.string
    , auth: PropTypes.object.isRequired
  };

  constructor() {
    super();

    this.onLogin = this.onLogin.bind(this);
  }

  onLogin(email, password) {
    this.props.authActions.login(email, password, this.props.redirect);
  }

  render() {
    return (<div>
      <LoginForm onLogin={this.onLogin} loginFailed={this.props.auth.loginFailed} />
    </div>);
  }
}

function mapStateToProps(state, ownProps) {
  return {
    redirect: ownProps.location.query.redirect
    , auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  };
}

export default connect(
  mapStateToProps
  , mapDispatchToProps
)(LoginPage);
