import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import ScaffoldCreatePage from "./ScaffoldCreatePage";
import ScaffoldListPage from "./ScaffoldListPage";
import ScaffoldShowPage from "./ScaffoldShowPage";
import ScaffoldUpdatePage from "./ScaffoldUpdatePage";
import ScaffoldDeletePage from "./ScaffoldDeletePage";

// If a URL does not match any existing page it is handled by
// this component. There are two broad cases that this component
// handles: Scaffolded pages and 404 pages. If the URL corresponds
// to an entity and action that is registered in scaffoldConfigurations
// an appropriately scaffolded page is generated. If the URL does
// not correspond to a scaffolded entity then a generic 404 page
// is shown.
class ScaffoldPage extends React.Component {
  static propTypes = {
    routing: PropTypes.object.isRequired
    , environment: PropTypes.object.isRequired
  };

  renderNotFound() {
    return (
      <div>
        <h4>
          404 Page Not Found
        </h4>
        <Link to="/"> Go back to homepage </Link>
      </div>
    );
  }

  // returns { controller, action, id } based on current url
  getIntent() {
    let parts = this.props.routing.locationBeforeTransitions.pathname.split("/");
    parts.splice(0, 1);
    return {
      controller: parts.length > 0 ? parts[0].toLowerCase() : null
      , action: (parts.length > 1 ? parts[1].toLowerCase() : null) || "list"
      , id: parts.length > 2 && !isNaN(parts[2]) ? parseInt(parts[2]) : null
    };
  }

  render() {
    let intent = this.getIntent();
    let config = this.props.environment.scaffoldConfigurations[intent.controller];

    if(config == null || config.isScaffolded == false) {
      return this.renderNotFound();
    }
    else if(intent.action == "create") {
      return <ScaffoldCreatePage scaffoldConfiguration={config} />;
    }
    else if(intent.action == "list") {
      return <ScaffoldListPage scaffoldConfiguration={config} />;
    }
    else if(intent.action == "show" && intent.id > 0) {
      return <ScaffoldShowPage scaffoldConfiguration={config} entityId={intent.id} />;
    }
    else if(intent.action == "update" && intent.id > 0) {
      return <ScaffoldUpdatePage scaffoldConfiguration={config} entityId={intent.id}/>;
    }
    else if(intent.action == "delete" && intent.id > 0) {
      return <ScaffoldDeletePage scaffoldConfiguration={config} entityId={intent.id}/>;
    }
    else {
      return this.renderNotFound();
    }
  }
}


function mapStateToProps(state) {
  return {
    routing: state.routing
    , environment: state.environment
  };
}

function mapDispatchToProps() {
  return { };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScaffoldPage);
