import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as entityActions from "../actions/entityActions";
import { Button } from "react-bootstrap";
import ScaffoldEditor from "../components/common/ScaffoldEditor";
import { browserHistory } from "react-router";
import ScaffoldService from "../services/ScaffoldService";
import ErrorList from "../components/common/ErrorList";
import GettingReady from "../components/common/GettingReady";

// Allows a user to create a new entity
class ScaffoldCreatePage extends React.Component {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , scaffoldConfigurations: PropTypes.object.isRequired
    , entityActions: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
    , urlProperties: PropTypes.object.isRequired
  };

  constructor() {
    super();

    this.handleCreate = this.handleCreate.bind(this);

    this.state = {
      createEntity: {}
      , isReady: false
    };
  }

  componentWillMount() {
    this.setState({ isReady: false });
    this.mergeUrlProperties();
    let entityType = this.props.scaffoldConfiguration.entityType;
    this.props.entityActions.saveEntityReset({ entityType: entityType });

    // Load the template for the entity. This tells us
    // which properties are references to other objects
    // so we can load data and populate drop-downs for them.
    ScaffoldService.fetchEntitiesFromTemplate(this.props.entities, this.props.scaffoldConfiguration, this.props.entityActions)
      .then(() => this.setState({ isReady: true }));
  }

  componentWillUpdate(nextProps) {
    let entityContext = this.getEntityContext();
    let nextEntityContext = this.getEntityContext(nextProps);

    if(entityContext == null) {
      return;
    }

    // If we have successfully created the entity
    // redirect to the show entity page
    if (nextEntityContext.saveError == null
      && nextEntityContext.savedEntity != null) {
      browserHistory.push("/" + this.props.scaffoldConfiguration.entityType + "/show/" + nextEntityContext.savedEntity.id);
    }
  }

  mergeUrlProperties() {
    // Loads any properties specified as a URL parameter into the createEntity
    this.setState({
      createEntity: {
        ...this.state.createEntity,
        ...this.props.urlProperties
      }
    });
  }

  getEntityContext(props) {
    props = props || this.props;
    return props.entities[props.scaffoldConfiguration.entityType];
  }

  handleCreate(){
    this.props.entityActions.saveEntity({
      entityType: this.props.scaffoldConfiguration.entityType
      , entity: this.state.createEntity
    });
  }

  render() {
    // Get the redux state for the entity type so we can either show
    // the user a loading message or the actual entity once it's loaded
    const { scaffoldConfiguration, scaffoldConfigurations } = this.props;
    const entityContext = this.getEntityContext();

    if(!this.state.isReady) {
      return <GettingReady />;
    }

    return (<div>
      <h2>Create {scaffoldConfiguration.displayName}</h2>
      <ErrorList errors={entityContext.saveError} />
      <ScaffoldEditor scaffoldConfiguration={scaffoldConfiguration} scaffoldConfigurations={scaffoldConfigurations} entity={this.state.createEntity} entities={this.props.entities} />
      <Button type="submit" onClick={this.handleCreate} disabled={entityContext.isSaving}>Create</Button>
      <ErrorList errors={entityContext.saveError} />
    </div>);
  }
}

function mapStateToProps(state) {
  return {
    entities: state.entities
    , scaffoldConfigurations: state.environment.scaffoldConfigurations
    , urlProperties: state.routing.locationBeforeTransitions.query
  };
}

function mapDispatchToProps(dispatch) {
  return {
    entityActions: bindActionCreators(entityActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScaffoldCreatePage);
