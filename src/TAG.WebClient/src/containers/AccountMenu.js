import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavItem, NavDropdown, MenuItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import * as authActions from "../actions/authActions";

class AccountMenu extends React.Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
    , authActions: PropTypes.object.isRequired
  };

  constructor() {
    super();
  }

  render() {
    if (!this.props.auth.isLoggedIn) {
      return (
        <LinkContainer to="/login">
          <NavItem>Login</NavItem>
        </LinkContainer>
      );
    }

    return (
      <NavDropdown title="Account" id="nav-dropdown-account">
        <LinkContainer to="/account">
          <MenuItem>Settings</MenuItem>
        </LinkContainer>
        <MenuItem onClick={this.props.authActions.logout}>Logout</MenuItem>
      </NavDropdown>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  };
}

export default connect(
  mapStateToProps
  , mapDispatchToProps
)(AccountMenu);
