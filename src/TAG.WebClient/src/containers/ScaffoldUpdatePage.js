import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as entityActions from "../actions/entityActions";
import { Alert, Button } from "react-bootstrap";
import ScaffoldEditor from "../components/common/ScaffoldEditor";
import { browserHistory } from "react-router";
import ScaffoldService from "../services/ScaffoldService";
import ErrorList from "../components/common/ErrorList";
import GettingReady from "../components/common/GettingReady";

// Shows information about an entity based on the entity's scaffold configuration
// Allows the user to edit and submit updated information
class ScaffoldUpdatePage extends React.Component {
  static propTypes = {
    scaffoldConfiguration: PropTypes.object.isRequired
    , scaffoldConfigurations: PropTypes.object.isRequired
    , entityId: PropTypes.number.isRequired
    , entityActions: PropTypes.object.isRequired
    , entities: PropTypes.object.isRequired
  };

  constructor() {
    super();

    this.handleSave = this.handleSave.bind(this);

    this.state = {
      editEntity: {}
      , isReady: false
    };
  }

  componentWillMount() {
    this.setState({ isReady: false });

    // Load the entity
    let entityType = this.props.scaffoldConfiguration.entityType;
    this.props.entityActions.getEntityReset({ entityType: entityType });
    this.props.entityActions.saveEntityReset({ entityType: entityType });
    let getPromise = this.props.entityActions.getEntity({
      entityType: entityType
      , id: this.props.entityId
    });

    // Load the template for the entity. This tells us
    // which properties are references to other objects
    // so we can load data and populate drop-downs for them.
    let relatedEntitiesPromise = ScaffoldService.fetchEntitiesFromTemplate(this.props.entities, this.props.scaffoldConfiguration, this.props.entityActions);

    Promise.all([getPromise, relatedEntitiesPromise])
      .then(() => this.setState({ isReady: true }));
  }

  componentWillUpdate(nextProps) {
    let entityContext = this.getEntityContext();
    let nextEntityContext = this.getEntityContext(nextProps);

    if(entityContext == null) {
      return;
    }

    if(entityContext.entity != nextEntityContext.entity) {
      // Make a copy of the entity being edited so
      // we can make changes to it outside of the
      // Redux state. This saves having to fire off
      // actions for every single keystroke a user
      // enters during the edit process.
      this.setState({
        editEntity: {
          ...nextEntityContext.entity
        }
      });
    }
    else {
      // If we have successfully saved the entity
      // redirect to the show entity page
      if (nextEntityContext.saveError == null
          && nextEntityContext.savedEntity != null
          && this.state.editEntity.id == nextEntityContext.savedEntity.id) {
        browserHistory.push("/" + this.props.scaffoldConfiguration.entityType + "/show/" + nextEntityContext.savedEntity.id);
      }
    }
  }

  getEntityContext(props) {
    props = props || this.props;
    return props.entities[props.scaffoldConfiguration.entityType];
  }

  handleSave(){
    this.props.entityActions.saveEntity({
      entityType: this.props.scaffoldConfiguration.entityType
      , entity: this.state.editEntity
    });
  }

  render() {
    // Get the redux state for the entity type so we can either show
    // the user a loading message or the actual entity once it's loaded
    const { scaffoldConfiguration, scaffoldConfigurations } = this.props;
    const entityContext = this.getEntityContext();

    if(!this.state.isReady) {
      return <GettingReady />;
    }

    if(entityContext.getError) {
      return <Alert bsStyle="danger">{entityContext.getError}</Alert>;
    }

    return (<div>
      <h2>Update {scaffoldConfiguration.displayName}</h2>
      <ErrorList errors={entityContext.saveError} />
      <ScaffoldEditor scaffoldConfiguration={scaffoldConfiguration} scaffoldConfigurations={scaffoldConfigurations} entity={this.state.editEntity} entities={this.props.entities} />
      <Button type="submit" onClick={this.handleSave} disabled={entityContext.isSaving}>Save</Button>
      <ErrorList errors={entityContext.saveError} />
    </div>);
  }
}

function mapStateToProps(state) {
  return {
    entities: state.entities
    , scaffoldConfigurations: state.environment.scaffoldConfigurations
  };
}

function mapDispatchToProps(dispatch) {
  return {
    entityActions: bindActionCreators(entityActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScaffoldUpdatePage);
