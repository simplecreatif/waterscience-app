import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { IndexLink } from "react-router";
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Alert } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import * as environmentActions from "../actions/environmentActions";
import * as realtimeActions from "../actions/realtimeActions";
import * as authActions from "../actions/authActions";
import NavigationGeneratorService from "../services/NavigationGeneratorService";
import AuthenticationService from "../services/AuthenticationService";
import AccountMenu from "./AccountMenu";
import GettingReady from "../components/common/GettingReady";

class App extends React.Component {
  static propTypes = {
    children: PropTypes.element
    , environment: PropTypes.object.isRequired
    , auth: PropTypes.object.isRequired
    , environmentActions: PropTypes.object.isRequired
    , realtimeActions: PropTypes.object.isRequired
    , authActions: PropTypes.object.isRequired
  };

  componentWillMount() {
    this.props.environmentActions.initialise();
    this.props.realtimeActions.connect();
    this.props.authActions.checkUser();
  }

  render() {
    const { environment } = this.props;

    if(!environment.isInitialised) {
      return <GettingReady />;
    }

    if(environment.initialiseError) {
      return <Alert bsStyle="danger">There was an error communicating with the server. The server may be offline.</Alert>;
    }

    let nav = NavigationGeneratorService.generate(environment.scaffoldConfigurations);
    let isAdmin = AuthenticationService.isAdministrator(this.props.auth);
    return (
      <div className="container">
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <IndexLink to="/">SETAC Australasia Ecotoxicity Database</IndexLink>
            </Navbar.Brand>
          </Navbar.Header>
          <Nav>
            <LinkContainer to="/search">
              <NavItem eventKey={1}>Search</NavItem>
            </LinkContainer>
            {
              Object.keys(nav).map(x =>
                <NavDropdown title={x} key={x} id={"nav-dropdown-" + x}>
                  {
                    nav[x].map(y =>
                      <LinkContainer key={y.entityType} to={"/" + y.entityType}>
                        <MenuItem>{y.displayName}</MenuItem>
                      </LinkContainer>
                    )
                  }
                </NavDropdown>
              )
            }
            {
              isAdmin
                ? <LinkContainer to="/user">
                <NavItem eventKey={2}>User Accounts</NavItem>
              </LinkContainer>
                : null
            }
            <AccountMenu/>
          </Nav>
        </Navbar>
        {this.props.children}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    environment: state.environment
    , auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    environmentActions: bindActionCreators(environmentActions, dispatch)
    , realtimeActions: bindActionCreators(realtimeActions, dispatch)
    , authActions: bindActionCreators(authActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
