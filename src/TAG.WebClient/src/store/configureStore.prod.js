import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers";
import thunk from "redux-thunk";
import { routerMiddleware } from "react-router-redux";

export default function configureStore(history, initialState) {
  const middleware = [
    thunk,
    routerMiddleware(history)
  ];

  // See http://rackt.org/redux/docs/api/applyMiddleware.html
  // and http://rackt.org/redux/docs/advanced/AsyncActions.html
  let createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);

  return createStoreWithMiddleware(rootReducer, initialState);
}
